#version 440 core
  
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 color;
layout (location = 3) in vec2 uv;
layout (location = 4) in vec3 tangent;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

out vec3 Norm;
out vec3 FragPos;
out vec2 UV;
out mat3 TBN;

void main()
{
    gl_Position = P * V * M * vec4(position.x, position.y, position.z, 1.0);
    FragPos = (M * vec4(position.x, position.y, position.z, 1.0)).xyz;
    Norm = normalize((transpose(inverse(M)) * vec4(normal, 0.0)).xyz);
    UV = uv;
    vec3 B = cross(tangent, normal);
    TBN = mat3(tangent, B, normal);
}