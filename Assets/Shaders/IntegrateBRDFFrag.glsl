#version 440 core

#include Hammersley.glsli
#include BRDF.glsli

in vec2 UV;

out vec4 Color;

vec2 IntegrateBRDF(float ndv, float roughness)
{
  vec3 V;
  V.x = sqrt(1.0 - ndv*ndv);
  V.y = 0.0;
  V.z = ndv;

  float A = 0.0;
  float B = 0.0;

  vec3 N = vec3(0.0, 0.0, 1.0);

  const uint sampleCount = 1024u;
  for(uint i = 0u; i < sampleCount; ++i)
  {
    vec2 Xi = Hammersley(i, sampleCount);
    vec3 H = ImportanceSampleGGX(Xi, N, roughness);
    vec3 L = normalize(2.0 * dot(V, H) * H - V);

    float ndl = max(L.z, 0.0);
    float ndh = max(H.z, 0.0);
    float vdh = max(dot(V, H), 0.0);

    if(ndl > 0.0)
    {
      float G = IBLSmithG(N, V, L, roughness);
      float G_Vis = (G * vdh) / (ndh * ndv);
      float Fc = pow(1.0 - vdh, 5.0);

      A += (1.0 - Fc) * G_Vis;
      B += Fc * G_Vis;
    }
  }
  A /= float(sampleCount);
  B /= float(sampleCount);
  return vec2(A, B);
}

void main()
{
  vec2 vec = IntegrateBRDF(UV.x, UV.y);
  Color = vec4(vec.x, vec.y, 0.0f, 1.0f);
}