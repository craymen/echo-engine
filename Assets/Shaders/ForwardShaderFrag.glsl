#version 440 core

#include Material.glsli
#include BRDF.glsli
#include Camera.glsli

out vec4 color;

in vec3 Norm;
in vec3 FragPos;
in vec2 UV;

void main()
{
    color = vec4(0.0, 0.0, 0.0, 0.0);
    for(int i = 0; i < PointLightCount; ++i)
    {
        color += GetBRDFLightColor(FragPos, Norm, normalize(Cam.Eye - FragPos), 
            PointLights[i].Position, PointLights[i].Color, GetAlbedoColor(UV), 
            Mat.Roughness, Mat.Metallic);
    }
} 