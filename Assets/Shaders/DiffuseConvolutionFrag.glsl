#version 440 core

#include GammaCorrection.glsli

uniform samplerCube EnvironmentMap;

in vec3 FragPos;

out vec4 Color;

const float Pi = 3.14159f;

void main()
{
  vec3 normal = normalize(FragPos);
  vec3 irradiance = vec3(0.0);

  vec3 up = vec3(0, 1, 0);
  vec3 right = cross(up, normal);
  up = cross(normal, right);

  const float sampleDelta = 0.025f;
  float numSamples = 0.0f;

  Color = vec4(0.0, 0.0, 0.0, 1.0);
  
  for(float phi = 0.0; phi < 2.0 * Pi; phi += sampleDelta)
  {
    for(float theta = 0.0; theta < 0.5 * Pi; theta += sampleDelta)
    {
      vec3 tanSample = vec3(sin(theta) * cos(phi), 
        sin(theta) * sin(phi), cos(theta));

      vec3 sampleVec = tanSample.x * right + tanSample.y * up + 
        tanSample.z * normal;

      vec3 sampled = texture(EnvironmentMap, sampleVec).rgb;

      if(!isnan(sampled.x) && !isnan(sampled.y) && !isnan(sampled.z))
        irradiance += screenToLinear(sampled) * cos(theta) * sin(theta);

      numSamples += 1.0f;
    }
  }
  irradiance = Pi * irradiance / numSamples;

  Color = vec4(linearToScreen(irradiance), 1.0);
}