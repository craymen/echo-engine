#version 440 core

#include Deferred.glsli
#include Material.glsli
#include Camera.glsli
#include BRDF.glsli

// DefaultShaderFrag.glsl

in vec3 Norm;
in vec3 FragPos;
in vec2 UV;
in mat3 TBN;

uniform samplerCube SpecularConvolution;
uniform samplerCube DiffuseConvolution;
uniform sampler2D BRDFIntegration;

void main()
{
    Position.rgb = FragPos;
    Position.a = 1.0f;
    Normal.rgb = TBN * GetNormal(UV);
    Normal.a = 1.0f;
    Diffuse = GetAlbedoColor(UV);
    Specular.rgb = vec3(1.0);
    Specular.a = Mat.Roughness;
    Specular.r = Mat.Metallic;

    vec3 V = normalize(Cam.Eye - FragPos);
    vec3 R = reflect(-V, Norm);
    vec3 F0 = mix(vec3(0.04), Diffuse.rgb, Specular.a);
    vec3 kS = fresnelSchlickRoughness(max(dot(Normal.rgb, V), 0.0), F0, Specular.a);
    vec3 kD = 1.0 - kS;
    kD *= 1.0 - Specular.r;

    vec3 diffuse = kD * texture(DiffuseConvolution, Normal.rgb).rgb
        * Diffuse.rgb;

    const float maxReflectLOD = 4.0;
    vec3 prefilteredColor = textureLod(SpecularConvolution, R, 
        Specular.a * maxReflectLOD).rgb;
    vec2 envBRDF = 
        texture(BRDFIntegration, vec2(max(dot(Normal.rgb, V), 0.0), Specular.a)).rg;
    vec3 spec = prefilteredColor * (kS * envBRDF.x + envBRDF.y);

    DiffIBL = vec4(diffuse, 1.0);
    SpecIBL = vec4(spec, 1.0);
}  