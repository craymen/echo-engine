#version 440 core

#include Material.glsli
#include PointLight.glsli
#include Camera.glsli

out vec4 color;

in vec3 Norm;
in vec3 FragPos;
in vec2 UV;

void main()
{
    color = GetAlbedoColor(UV);
} 