#version 440 core

#include GammaCorrection.glsli

out vec4 Color;

in vec3 TexCoords;

uniform samplerCube Skybox;

void main()
{    
    Color = texture(Skybox, TexCoords);
    Color.rgb = screenToLinear(Color.rgb);
}