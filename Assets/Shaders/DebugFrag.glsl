#version 440 core

#include Material.glsli
#include PointLight.glsli
#include Camera.glsli

// DebugFrag.glsl

out vec4 color;

in vec4 VertColor;

void main()
{
   color = VertColor;
} 