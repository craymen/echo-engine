#version 440 core

#include Hammersley.glsli
#include GammaCorrection.glsli

uniform samplerCube EnvironmentMap;
uniform float Roughness;

in vec3 FragPos;

out vec4 Color;

const float Pi = 3.14159f;

void main()
{
  vec3 N = normalize(FragPos);
  vec3 R = N;
  vec3 V = R;

  const uint sampleCount = 1024u;
  float totalWeight = 0.0;
  vec3 prefilteredColor = vec3(0.0);
  for(uint i = 0u; i < sampleCount; ++i)
  {
    vec2 Xi = Hammersley(i, sampleCount);
    vec3 H = ImportanceSampleGGX(Xi, N, Roughness);
    vec3 L = normalize(2.0 * dot(V, H) * H - V);

    float ndl = max(dot(N, L), 0.0);
    if(ndl > 0.0)
    {
      vec3 sampled = texture(EnvironmentMap, L).rgb;
      if(!isnan(sampled.x) && !isnan(sampled.y) && !isnan(sampled.z))
        prefilteredColor += sampled * ndl;
      totalWeight += ndl;
    }

    Color = vec4(prefilteredColor.r / totalWeight, prefilteredColor.g / totalWeight, prefilteredColor.b / totalWeight, 1.0);
    Color.rgb = linearToScreen(Color.rgb);
    Color = min(max(Color, 0.0), 1.0);
  }
}