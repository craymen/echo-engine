#version 440 core

layout (location = 0) in vec3 Position;

uniform mat4 P;
uniform mat4 V;

out vec3 FragPos;

void main()
{
  FragPos = Position;
  vec4 pos = P * V * vec4(Position, 1.0);
  gl_Position = pos.xyww;
}
