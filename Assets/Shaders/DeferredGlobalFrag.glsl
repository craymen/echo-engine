#version 440 core

#include Camera.glsli
#include GammaCorrection.glsli

in vec2 UV;

out vec4 Color;

uniform sampler2D DiffIBL;
uniform sampler2D SpecIBL;
uniform sampler2D Normal;

void main()
{
  if(length(texture(Normal, UV).xyz) < 0.01f)
    discard;

  vec3 d = texture(DiffIBL, UV).rgb;
  vec3 s = texture(SpecIBL, UV).rgb;

  Color = vec4(screenToLinear(d) + screenToLinear(s), 1.0f);
}