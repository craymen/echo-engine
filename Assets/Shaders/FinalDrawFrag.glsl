#version 440 core

#include GammaCorrection.glsli

in vec2 UV;

out vec4 Color;

uniform sampler2D ScreenTex;

void main()
{
  vec4 sampled = texture(ScreenTex, UV);
  Color = vec4(linearToScreen(sampled.rgb), sampled.a);
}