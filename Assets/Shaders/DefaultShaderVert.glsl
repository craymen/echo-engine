#version 440 core

// DefaultShaderVert.glsl
  
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 color;
layout (location = 3) in vec2 uv;
layout (location = 4) in vec3 tangent;
layout (location = 5) in mat4 instance_transform;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
uniform bool Instanced;

out vec3 Norm;
out vec3 FragPos;
out vec2 UV;
out mat3 TBN;

void main()
{
    mat4 m = Instanced ? instance_transform : M;
    gl_Position = P * V * m * vec4(position.x, position.y, position.z, 1.0);
    FragPos = (m * vec4(position.x, position.y, position.z, 1.0)).xyz;
    mat3 invTranspose = mat3(transpose(inverse(m)));
    Norm = normalize(invTranspose * normal);
    UV = uv;
    vec3 T = normalize(invTranspose * tangent);
    vec3 B = normalize(cross(T, Norm));
    TBN = mat3(T, B, Norm);
}