#version 440 core
  
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 color;
layout (location = 3) in vec2 uv;

out vec2 UV;

uniform float ScreenDepth;

void main()
{
  gl_Position = vec4(position * 2.0f, 1.0f);
  gl_Position.z = ScreenDepth;
  UV = uv;
}