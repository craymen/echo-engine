// DebugVert.glsl

#version 440 core
  
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 color;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

out vec3 Norm;
out vec4 VertColor;

void main()
{
    gl_Position = P * V * vec4(position.x, position.y, position.z, 1.0);
    VertColor = color;
}