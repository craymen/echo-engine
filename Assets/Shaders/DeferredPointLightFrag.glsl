#version 440 core

#include BRDF.glsli
#include Material.glsli
#include Camera.glsli

out vec4 Color;

uniform sampler2D Position;
uniform sampler2D Normal;
uniform sampler2D Diffuse;
uniform sampler2D Specular;

uniform vec3 LightPosition;
uniform vec3 LightColor;
uniform vec2 ScreenResolution;

layout(origin_upper_left) in vec4 gl_FragCoord;

void main()
{
  vec2 uv = gl_FragCoord.xy / ScreenResolution;
  uv.y = -uv.y;
  vec3 pos = texture(Position, uv).xyz;
  vec3 norm = texture(Normal, uv).xyz;
  vec3 diff = texture(Diffuse, uv).xyz;
  vec4 spec = texture(Specular, uv);

  if(length(norm.xyz) < 0.01f)
    discard;

  norm = normalize(norm);

  Color = GetBRDFLightColor(pos, norm, normalize(Cam.Eye - pos), 
    LightPosition, LightColor, vec4(diff, 1.0), spec.a, spec.r);
}