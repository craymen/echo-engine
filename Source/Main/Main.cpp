/*!
  \file Main/Main.cpp
  \author Chase A Rayment
  \date 9 May 2017
  \brief Contains the entry point definition of the Main app.
*/

#include <iostream>
#include "Log.hpp"
#include "Game.hpp"
#include "Space.hpp"
#include "ResourceManager.hpp"
#include "Cubemap.hpp"
using namespace Echo;

/*!
  \brief The entry point for the Main app.
*/
int main(int argc, char** argv)
{
  Core::Game::Initialize();
  if (argc > 1)
  {
    Core::Spaces[Core::SPACE_GAMEPLAY].LoadLevel(argv[1]);
    Core::Spaces[Core::SPACE_GAMEPLAY].Initialize();
  }
  //Core::Spaces[Core::SPACE_HUD].LoadLevel("hud.json");
  Core::Spaces[Core::SPACE_HUD].Initialize();
  Core::Game::Run();
  Core::Game::Terminate();
  return 0;
}