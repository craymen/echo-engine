/*!
  \file Commando.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the Commando component.
*/

#include "Commando.hpp"
#include "Unit.hpp"
#include "TestAbility.hpp"
#include "FireAbility.hpp"

namespace Echo
{
  namespace Gameplay
  {
    Commando::Commando()
    {
      
    }
     
    void Commando::Initialize()
    {
      GetOwner()->GetComponent<Unit>()->AddAbility(new FireAbility);
    }
     
    void Commando::Terminate()
    {
      Component::Terminate();
      Core::Handle<Commando>::InvalidateHandles(this);
    }
  }
}