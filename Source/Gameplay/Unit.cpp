/*!
  \file Unit.cpp
  \author Chase A Rayment
  \date 7 June 2017
  \brief Contains the implementation of the Unit component.
*/

#include "Unit.hpp"
#include "Board.hpp"
#include "PlayerCommander.hpp"
#include "HUDManager.hpp"
#include "CameraObject.hpp"
#include "Window.hpp"
#include "EnemyCommander.hpp"
#include "Tile.hpp"
#include "StatusDisplay.hpp"

namespace Echo
{
  namespace Gameplay
  {
    Unit::Unit() : IsPlayerUnit(false), MaxHealth(0), hovered(false), 
      abilities(), curr_health(0), status_display(nullptr), CanUseAbility(true),
      CanMove(true), Speed(5)
    {

    }

    void Unit::Initialize()
    {
      Core::ConnectEvent((void*)GetOwner(), "MouseHoverStart", this, 
        &Unit::OnHover);
      Core::ConnectEvent((void*)GetOwner(), "MouseHoverEnd", this,
        &Unit::OnEndHover);
      Core::ConnectEvent((void*)GetSpace(), "Update", this, &Unit::Update);
      Core::ConnectEvent(nullptr, "StartPlayerTurn", this, 
        &Unit::OnPlayerStartTurn);

      if(IsPlayerUnit)
        GetSpace()->FindObjectByName("PlayerCommander")->
          GetComponent<PlayerCommander>()->AddUnit(GetOwner());
      else
        GetSpace()->FindObjectByName("EnemyCommander")->
          GetComponent<EnemyCommander>()->AddUnit(GetOwner());

      curr_health = MaxHealth;

      status_display = Core::Spaces[Core::SPACE_HUD]
        .CreateAtPosition("StatusDisplay", glm::vec3(0.0f, 0.0f, 0.0f));
      status_display->SetName("StatusDisplay");
      status_display->GetComponent<StatusDisplay>()->SetUnit(GetOwner());

      GetSpace()->FindObjectByName("Board")->GetComponent<Board>()
        ->GetTile(GetCoords())->GetComponent<Tile>()->Occupant = GetOwner();
    }

    void Unit::Terminate()
    {
      status_display->Terminate();

      for(auto a : abilities)
        delete a;

      if(!IsPlayerUnit)
      {
        Core::Handle<Core::Object> ec = 
          GetSpace()->FindObjectByName("EnemyCommander");
        if(ec != nullptr)
          ec->GetComponent<EnemyCommander>()->RemoveUnit(GetOwner());
      }
      else
      {
        Core::Handle<Core::Object> pc = 
          GetSpace()->FindObjectByName("PlayerCommander");
        if(pc != nullptr)
          pc->GetComponent<PlayerCommander>()->RemoveUnit(GetOwner());
      }

      Component::Terminate();
      Core::Handle<Unit>::InvalidateHandles(this);
    }

    void Unit::MoveTo(const glm::ivec2& coord)
    {
      // Disable caching on the owner transform
      GetOwner()->Transform.DisableCaching();

      // Get the board object
      Core::Handle<Core::Object> board_obj = 
		  GetSpace()->FindObjectByName("Board");
      if(board_obj == nullptr)
      {
        Core::Log << Core::Priority::Warning << "Tried to move a Unit without "
          << "a Board object!" << std::endl;
        return;
      }

      // Get the board component
      Core::Handle<Board> board = board_obj->GetComponent<Board>();
      if(board == nullptr)
      {
        Core::Log << Core::Priority::Warning << "Tried to move a Unit but the "
          << "Board object does not have a Board component!" << std::endl;
        return;
      }

      // Unset the tile's occupant
      board->GetTile(GetCoords())->GetComponent<Tile>()->Occupant = nullptr;

      // Get the path
      std::vector<Core::Handle<Core::Object> > path = board->GetPath(
        GetCoords(), coord);
      
      // Build the action sequence
      int c = 0;
      Core::Handle<Core::Object> last = nullptr;
      for(auto obj : path)
      {
        GetOwner()->Actions->AddAction(
          new Core::Actions::ActionProperty<glm::vec3>(
          &GetOwner()->Transform.GetPosition(), obj->Transform.GetPosition() 
          + glm::vec3(0.0f, 0.5f, 0.0f),
          0.25f, Core::Actions::EaseLinear));
        last = obj;
        if(++c >= Speed + 1)
          break;
      }
      GetOwner()->Actions->AddAction(
        new Core::Actions::ActionCall(&Unit::handle_path_end, this));

      // Set the end tile's occupant
      last->GetComponent<Tile>()->Occupant = GetOwner();
    }

    glm::ivec2 Unit::GetCoords()
    {
      return glm::ivec2((int)round(GetOwner()->Transform.GetPosition().x), 
        (int)round(GetOwner()->Transform.GetPosition().z));
    }

    void Unit::OnHover(Physics::MouseHoverEvent&)
    {
      GetSpace()->FindObjectByName("PlayerCommander")->
          GetComponent<PlayerCommander>()->HoverUnit(GetOwner());
      hovered = true;
    }

    void Unit::OnEndHover(Physics::MouseHoverEvent&)
    {
      GetSpace()->FindObjectByName("PlayerCommander")->
        GetComponent<PlayerCommander>()->UnhoverUnit(GetOwner());
      hovered = false;
    }

    void Unit::Update(Core::UpdateEvent&)
    {
      if(hovered && Input::Mouse::MouseButtonIsPressed(SDL_BUTTON_LEFT))
      {
        GetSpace()->FindObjectByName("PlayerCommander")->
          GetComponent<PlayerCommander>()->SelectUnit(GetOwner());
      }
    }

    void Unit::AddAbility(Ability* a)
    {
      a->SetSource(GetOwner());
      abilities.push_back(a);
    }

    const std::vector<Ability*>& Unit::GetAbilities() const
    {
      return abilities;
    }

    void Unit::DealDamage(unsigned damage)
    {
      curr_health -= damage;

      status_display->GetComponent<StatusDisplay>()->DealDamage(damage);

      if(curr_health <= 0)
        GetOwner()->Terminate();
    }

    void Unit::OnPlayerStartTurn(Core::Event&)
    {
      if(IsPlayerUnit)
      {
        CanMove = true;
        CanUseAbility = true;
      }
    }

    void Unit::handle_path_end(void* obj)
    {
      Unit* comp = reinterpret_cast<Unit*>(obj);
      Core::Event ev;
      Core::DispatchEvent((void*)comp->GetOwner(), "UnitMoveEnd", ev);
      comp->CanMove = false;
    }

    int Unit::GetHealth() const
    {
      return curr_health;
    }

    std::vector<Core::Handle<Core::Object> > Unit::GetAllMovableTiles()
    {
      // Get the board
      Core::Handle<Core::Object> board = GetSpace()->FindObjectByName("Board");

      return board->GetComponent<Board>()->
        GetTilesWithinDistance(GetCoords(), Speed);
    }
  }
}