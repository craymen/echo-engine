/*!
  \file TestAbility.hpp
  \author Chase A Rayment
  \date 4 July 2017
  \brief Contains the definition of the TestAbility class.
*/

#pragma once
#include "Ability.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class TestAbility : public Ability
    {
    public:
      std::string GetName();
      Core::Handle<Graphics::Material> GetIcon();
      void Use(Core::Handle<Core::Object> target);
      Ability::TargetingMode GetTargetingMode();

    private:
      
    };
  }
}