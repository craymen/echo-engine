/*!
  \file Commando.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the Commando component.
*/
#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class Commando : public Core::Component
    {
    public:
      Commando();
      void Initialize();
      void Terminate();
    
    private:
    
    };
  }
}