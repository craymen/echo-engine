/*!
  \file HUDManager.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the HUDManager component.
*/

#include "HUDManager.hpp"
#include "Ability.hpp"
#include "Unit.hpp"
#include "AbilityButton.hpp"

namespace Echo
{
  namespace Gameplay
  {
    HUDManager::HUDManager() : ability_buttons(), targeting_ability(false)
    {
      
    }
     
    void HUDManager::Initialize()
    {
      
    }
     
    void HUDManager::Terminate()
    {
      Component::Terminate();
      Core::Handle<HUDManager>::InvalidateHandles(this);
    }

    void HUDManager::SelectUnit(Core::Handle<Core::Object> unit)
    {
      GetOwner()->Transform.FindChildByName("UnitName")->
        GetComponent<Graphics::SpriteText>()->Text = unit->GetName();

      for(auto button : ability_buttons)
        button->Terminate();

      ability_buttons.clear();

      const std::vector<Ability*>& abilities = 
        unit->GetComponent<Unit>()->GetAbilities();

      if(abilities.size() == 0)
        return;

      const float spacing = 1.0f;
      float total_space = spacing * (abilities.size() - 1);
      for(unsigned i = 0; i < abilities.size(); ++i)
      {
        ability_buttons.push_back(GetSpace()->CreateAtPosition("AbilityButton", 
          glm::vec3(i * spacing - total_space / 2.0f, -2.0f, 0.0f)));
        ability_buttons.back()->Transform.SetScale(glm::vec3(0.5f, 0.5f, 0.5f));
        ability_buttons.back()->GetComponent<Graphics::Model>()
          ->CurrentMaterial = abilities[i]->GetIcon();
        ability_buttons.back()->Transform.FindChildByName("AbilityName")
          ->GetComponent<Graphics::SpriteText>()->Text 
          = abilities[i]->GetName();
        ability_buttons.back()->GetComponent<AbilityButton>()->
          SetAbility(abilities[i]);
      }
    }

    void HUDManager::SelectAbility(Ability* a)
    {
      if(!a->GetSource()->GetComponent<Unit>()->CanUseAbility
        && !a->IsFreeAction())
        return;
      targeting_ability = a;
    }

    Ability* HUDManager::GetSelectedAbility()
    { 
      return targeting_ability;
    }

    void HUDManager::UseAbility(Ability* a, Core::Handle<Core::Object> target)
    {
      a->Use(target);
      if(!a->IsFreeAction())
        a->GetSource()->GetComponent<Unit>()->CanUseAbility = false;
      targeting_ability = nullptr;
    }
  }
}