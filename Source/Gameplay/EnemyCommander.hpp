/*!
  \file EnemyCommander.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the EnemyCommander component.
*/
#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class EnemyCommander : public Core::Component
    {
    public:
      EnemyCommander();
      void Initialize();
      void Terminate();
      void StartTurn();
      void EndTurn();
      void AddUnit(Core::Handle<Core::Object> unit);
      void RemoveUnit(Core::Handle<Core::Object> unit);
      void OnEnemyTurnTextEnd(Core::Event&);
      void NextUnit();
    
    private:
      bool active;
      std::vector<Core::Handle<Core::Object> > units;
      int curr_unit;
    };
  }
}