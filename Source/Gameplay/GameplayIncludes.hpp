/*!
  \file GameplayIncludes.hpp
  \author Chase A Rayment
  \date 5 June 2017
  \brief Various useful engine/STL includes for gameplay objects.
*/

#include <vector>
#include <string>
#include <unordered_map>
#include <iostream>
#include <queue>

#include <GLM/glm.hpp>
#include "GLM/gtc/matrix_transform.hpp"
#include "GLM/gtx/string_cast.hpp"

#include "Component.hpp"
#include "Handle.hpp"
#include "ResourceManager.hpp"
#include "Archetype.hpp"
#include "Space.hpp"
#include "Object.hpp"
#include "Log.hpp"
#include "Messaging.hpp"
#include "MouseHoverEvent.hpp"
#include "DebugDrawer.hpp"
#include "Scene.hpp"
#include "Actions.hpp"
#include "Input.hpp"
#include "SoundEmitter.hpp"
#include "SpriteText.hpp"
#include "Model.hpp"
#include "Material.hpp"