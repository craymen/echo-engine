/*!
  \file EnemyTurnText.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the EnemyTurnText component.
*/
#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class EnemyTurnText : public Core::Component
    {
    public:
      EnemyTurnText();
      void Initialize();
      void Terminate();
    
    private:
      static void end_text(void* obj);
    };
  }
}