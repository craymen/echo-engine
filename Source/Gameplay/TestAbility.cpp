/*!
  \file TestAbility.cpp
  \author Chase A Rayment
  \brief Contains the implementation of the TestAbility class.
*/

#include "TestAbility.hpp"
#include "Unit.hpp"
#include "Tile.hpp"

namespace Echo
{
  namespace Gameplay
  {
    std::string TestAbility::GetName()
    {
      return "Test Ability";
    }

    Core::Handle<Graphics::Material> TestAbility::GetIcon()
    {
      return Core::ResourceManager<Graphics::Material>::Find("Test");
    }

    Ability::TargetingMode TestAbility::GetTargetingMode()
    {
      return Ability::TargetingMode::SingleTile;
    }

    void TestAbility::Use(Core::Handle<Core::Object> target)
    {
      source->GetComponent<Unit>()->MoveTo(target->GetComponent<Tile>()
        ->GetCoords());
    }
  }
}
