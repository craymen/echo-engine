/*!
  \file Board.cpp
  \author Chase A Rayment
  \date 5 June 2017
  \brief Contains the implementation of the Board component.
*/

#include "Board.hpp"
#include "Tile.hpp"
#include <functional>

namespace Echo
{
  namespace Gameplay
  {
    Board::Board() : tiles()
    {

    }

    void Board::Initialize()
    {

    }

    void Board::Terminate()
    {
      Component::Terminate();
      Core::Handle<Board>::InvalidateHandles(this);
    }

    void Board::AddTile(Core::Handle<Core::Object> tile)
    {
      // Get tile coordinates
      int x = (int)round(tile->Transform.GetPosition().x);
      int y = (int)round(tile->Transform.GetPosition().z);

      // We only want to deal with positive board coordinates
      ASSERT(x >= 0 && y >= 0);

      // Resize the board if needed
      if((int)tiles.size() <= x)
        tiles.resize(x + 1);
      if((int)tiles[x].size() <= y)
        tiles[x].resize(y + 1);
      
      // Assign the tile
      tiles[x][y] = tile;
    }

    Core::Handle<Core::Object> Board::GetTile(const glm::ivec2& coord)
    {
      if(coord.x < tiles.size() && coord.y < tiles[coord.x].size())
        return tiles[coord.x][coord.y];
      return Core::Handle<Core::Object>();
    }

    std::vector<Core::Handle<Core::Object> > Board::GetPath(
      const glm::ivec2& start, const glm::ivec2& end)
    {
      // Get the start and end tiles
      Core::Handle<Core::Object> start_tile = GetTile(start);
      Core::Handle<Core::Object> end_tile = GetTile(end);

      // If either of the tiles aren't in the board, return an empty path
      if(start_tile == nullptr || end_tile == nullptr)
        return std::vector<Core::Handle<Core::Object> >();

      // Reset all tiles
      for(auto i : tiles)
        for(auto j : i)
          if(j != nullptr)
            j->GetComponent<Tile>()->reset();

      // Create the open list
      std::vector<Core::Handle<Core::Object> > open_list;
    

      // Push the start tile onto the open list
      open_list.push_back(start_tile);

      start_tile->GetComponent<Tile>()->status = Tile::list_status::open;
      start_tile->GetComponent<Tile>()->cost = 0;
      start_tile->GetComponent<Tile>()->hcost = 
        calculate_heuristic_cost(start_tile, end_tile);

      // Process the list while it's not empty
      while(open_list.size() > 0)
      {
        // Pop the cheapest off the list
        std::sort(open_list.begin(), open_list.end(), [](
          Core::Handle<Core::Object> a, Core::Handle<Core::Object> b) {
          return a->GetComponent<Tile>()->cost + a->GetComponent<Tile>()->hcost >
            b->GetComponent<Tile>()->cost + b->GetComponent<Tile>()->hcost; });
        Core::Handle<Core::Object> cheapest = open_list.back();
        open_list.pop_back();

        // If this is the goal, end the search
        if (cheapest->GetComponent<Tile>()->GetCoords() == end)
        {
          // Traverse the list of tiles and build the path
          std::vector<Core::Handle<Core::Object> > result;
          Core::Handle<Core::Object> temp = cheapest;
          while (temp != nullptr)
          {
            result.push_back(temp);
            temp = temp->GetComponent<Tile>()->parent;
          }
          std::reverse(result.begin(), result.end());
          return result;
        }

        // Get all the neighbors of this tile
        std::vector<Core::Handle<Core::Object> > neighbors = 
          GetTraversableNeighbors(cheapest->GetComponent<Tile>()->GetCoords());

        // Process all neighbors
        for(Core::Handle<Core::Object> neighbor : neighbors)
        {
          // Calculate the cost at the tile
          Core::Handle<Tile> tile = neighbor->GetComponent<Tile>();
          float old_cost = tile->cost + tile->hcost;
          float dist = glm::length(neighbor->Transform.GetPosition() - 
            cheapest->Transform.GetPosition());
          tile->cost = cheapest->GetComponent<Tile>()->cost + dist;
          tile->hcost = calculate_heuristic_cost(neighbor, end_tile);

          // If the tile is on neither list or the new cost is cheaper,
          // put it on the open list
          if(tile->status == Tile::list_status::neither || 
            tile->cost + tile->hcost < old_cost)
          {
            open_list.push_back(neighbor);
            tile->status = Tile::list_status::open;
            tile->parent = cheapest;
          }
        }

        // Put the cheapest node on the closed list
        cheapest->GetComponent<Tile>()->status = Tile::list_status::closed;
      }

      // We reached the end so we must not have found a path
      return std::vector<Core::Handle<Core::Object> >();
    }

    static bool is_walkable(Core::Handle<Core::Object> t, 
      bool exclude_occupied)
    {
      return t != nullptr && !(t->GetComponent<Tile>()->Occupant != nullptr 
        && exclude_occupied);
    }

    std::vector<Core::Handle<Core::Object> > Board::GetTraversableNeighbors(
      const glm::ivec2& coords, bool exclude_occupied)
    {
      std::vector<Core::Handle<Core::Object> > result;

      glm::ivec2 offsets[4] = {
        glm::ivec2(1, 0),
        glm::ivec2(0, 1),
        glm::ivec2(-1, 0),
        glm::ivec2(0, -1),
      };

      for(unsigned i = 0; i < 4; ++i)
      {
        Core::Handle<Core::Object> t = GetTile(coords + offsets[i]);
        if(is_walkable(t, exclude_occupied))
          result.push_back(t);
      }

      glm::ivec2 diag_offsets[4] = {
        glm::ivec2(1, 1),
        glm::ivec2(-1, 1),
        glm::ivec2(-1, -1),
        glm::ivec2(1, -1)
      };

      for(unsigned i = 0; i < 4; ++i)
      {
        Core::Handle<Core::Object> t1 = GetTile(coords + diag_offsets[i]);
        Core::Handle<Core::Object> t2 = GetTile(coords + glm::ivec2(diag_offsets[i].x, 0));
        Core::Handle<Core::Object> t3 = GetTile(coords + glm::ivec2(0, diag_offsets[i].y));
        if(is_walkable(t1, exclude_occupied) && is_walkable(t2, exclude_occupied) 
          && is_walkable(t3, exclude_occupied))
          result.push_back(t1);
      }

      return result;
    }

    float Board::calculate_heuristic_cost(Core::Handle<Core::Object> start,
      Core::Handle<Core::Object> end)
    {
      return glm::length(end->Transform.GetPosition() - 
        start->Transform.GetPosition());
    }

    std::vector<Core::Handle<Core::Object> > Board::GetAllTiles() const
    {
      std::vector<Core::Handle<Core::Object> > result;
      
      for(auto t1 : tiles)
        for(auto t2 : t1)
          if(t2 != nullptr)
            result.push_back(t2);

      return result;
    }

    std::vector<Core::Handle<Core::Object> > Board::GetTilesWithinDistance(
      const glm::ivec2& start, int dist)
    {
      std::vector<Core::Handle<Core::Object> > result;

      // Get the start tile
      Core::Handle<Core::Object> start_tile = GetTile(start);

      // If the start tile isn't in the board, return an empty list
      if(start_tile == nullptr)
        return std::vector<Core::Handle<Core::Object> >();

      // Reset all tiles
      for(auto i : tiles)
        for(auto j : i)
          if(j != nullptr)
            j->GetComponent<Tile>()->reset();

      // Create the open list
      std::vector<Core::Handle<Core::Object> > open_list;

      // Push the start tile onto the open list
      open_list.push_back(start_tile);

      start_tile->GetComponent<Tile>()->status = Tile::list_status::open;
      start_tile->GetComponent<Tile>()->cost = 0;

      // Process the list while it's not empty
      while(open_list.size() > 0)
      {
        // Pop the next off the list
        Core::Handle<Core::Object> next = open_list.back();
        open_list.pop_back();

        // Get all the neighbors of this tile
        std::vector<Core::Handle<Core::Object> > neighbors = 
          GetTraversableNeighbors(next->GetComponent<Tile>()->GetCoords());

        // Process all neighbors
        for(Core::Handle<Core::Object> neighbor : neighbors)
        {
          glm::vec3 pos = neighbor->Transform.GetPosition();
          
          // Calculate the cost at the tile
          Core::Handle<Tile> tile = neighbor->GetComponent<Tile>();
          
          // If the tile is within distance, put it on the open list
          if(next->GetComponent<Tile>()->cost + 1 <= dist
            && tile->cost > next->GetComponent<Tile>()->cost + 1)
          {
            tile->cost = next->GetComponent<Tile>()->cost + 1;
            open_list.push_back(neighbor);
            if(tile->status != Tile::list_status::open)
              result.push_back(neighbor);
            tile->status = Tile::list_status::open;
          }
        }
      }

      return result;
    }
  }
}