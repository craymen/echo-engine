/*!
  \file Board.hpp
  \author Chase A Rayment
  \date 5 June 2017
  \brief Contains the definition of the Board component.
*/

#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    /*!
      \brief Holds data about a game board.
    */
    class Board : public Core::Component
    {
    public:
      /*!
        \brief Constructs a new Board.
      */
      Board();

      /*!
        \brief Initializes a Board.
      */
      void Initialize();

      /*!
        \brief Terminates a Board.
      */
      void Terminate();

      /*!
        \brief Adds a tile.
        \param tile The tile to add.
      */
      void AddTile(Core::Handle<Core::Object> tile);

      /*!
        \brief Gets a tile at a specific coordinate.
        \param coord The coordinate to get a tile at.
        \returns The tile at the given coordinate.
      */
      Core::Handle<Core::Object> GetTile(const glm::ivec2& coord);

      /*!
        \brief Gets all tiles in this board.
      */
      std::vector<Core::Handle<Core::Object> > GetAllTiles() const;

      /*!
        \brief Gets all tiles reachable from a given start point within the
          given distance.
        \param start The start point.
        \param dist The maximum distance.
      */
      std::vector<Core::Handle<Core::Object> > GetTilesWithinDistance(
        const glm::ivec2& start, int dist);

      /*!
        \brief Gets a path between two coordinates.
        \param start The start coordinate.
        \param end The end coordinate.
        \returns The path, represented as a vector of tiles.
      */
      std::vector<Core::Handle<Core::Object> > GetPath(const glm::ivec2& start,
        const glm::ivec2& end);

      /*!
        \brief Gets all tiles that can be traversed to from a given coordinate.
        \param coords The start coordinate.
        \param exclude_occupied Whether or not to ignore occupied tiles.
        \returns The vector of tiles.
      */
      std::vector<Core::Handle<Core::Object> > GetTraversableNeighbors(
        const glm::ivec2& coords, bool exclude_occupied = true);

    private:
        // All tiles in this Board.
      std::vector<std::vector<Core::Handle<Core::Object> > > tiles;

        // Calculates the heuristic cost between two tiles.
      float calculate_heuristic_cost(Core::Handle<Core::Object> start,
        Core::Handle<Core::Object> end);
    };
  }
}