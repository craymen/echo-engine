/*!
  \file EndTurnButton.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the EndTurnButton component.
*/

#include "EndTurnButton.hpp"
#include "PlayerCommander.hpp"

namespace Echo
{
  namespace Gameplay
  {
    EndTurnButton::EndTurnButton()
    {
      
    }
     
    void EndTurnButton::Initialize()
    {
      Core::ConnectEvent((void*)GetSpace(), "Update", this, 
        &EndTurnButton::Update);
    }
     
    void EndTurnButton::Terminate()
    {
      Component::Terminate();
      Core::Handle<EndTurnButton>::InvalidateHandles(this);
    }

    void EndTurnButton::Update(Core::UpdateEvent&)
    {
      if(GetSpace()->GetHoveredObject() == GetOwner() 
        && Input::Mouse::MouseButtonIsPressed(SDL_BUTTON_LEFT))
      {
        Core::Spaces[Core::SPACE_GAMEPLAY].FindObjectByName("PlayerCommander")
          ->GetComponent<PlayerCommander>()->EndTurn();
      }
    }
  }
}