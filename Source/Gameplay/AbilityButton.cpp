/*!
  \file AbilityButton.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the AbilityButton component.
*/

#include "AbilityButton.hpp"
#include "HUDManager.hpp"

namespace Echo
{
  namespace Gameplay
  {
    AbilityButton::AbilityButton() : hovered(false), ability(nullptr)
    {
      
    }
     
    void AbilityButton::Initialize()
    {
      Core::ConnectEvent((void*)GetOwner(), "MouseHoverStart", this, 
        &AbilityButton::OnHover);
      Core::ConnectEvent((void*)GetOwner(), "MouseHoverEnd", this, 
        &AbilityButton::OnUnHover);
      Core::ConnectEvent((void*)GetSpace(), "Update", this, 
        &AbilityButton::Update);
    }
     
    void AbilityButton::Terminate()
    {
      Component::Terminate();
      Core::Handle<AbilityButton>::InvalidateHandles(this);
    }

    void AbilityButton::OnHover(Physics::MouseHoverEvent&)
    {
      hovered = true;
    }

    void AbilityButton::OnUnHover(Physics::MouseHoverEvent&)
    {
      hovered = false;
    }

    void AbilityButton::Update(Core::UpdateEvent&)
    {
      if(hovered && Input::Mouse::MouseButtonIsPressed(SDL_BUTTON_LEFT))
      {
        GetSpace()->FindObjectByName("HUDManager")->GetComponent<HUDManager>()
          ->SelectAbility(ability);
      }
    }

    void AbilityButton::SetAbility(Ability* a)
    {
      ability = a;
    }
  }
}