/*!
  \file EndTurnButton.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the EndTurnButton component.
*/
#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class EndTurnButton : public Core::Component
    {
    public:
      EndTurnButton();
      void Initialize();
      void Terminate();
      void Update(Core::UpdateEvent&);
    
    private:
    
    };
  }
}