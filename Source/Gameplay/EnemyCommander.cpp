/*!
  \file EnemyCommander.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the EnemyCommander component.
*/

#include "EnemyCommander.hpp"
#include "PlayerCommander.hpp"

namespace Echo
{
  namespace Gameplay
  {
    EnemyCommander::EnemyCommander() : active(false), units(), curr_unit(0)
    {
      
    }
     
    void EnemyCommander::Initialize()
    {
      Core::ConnectEvent(nullptr, "EnemyTurnTextEnded", this, 
        &EnemyCommander::OnEnemyTurnTextEnd);
    }
     
    void EnemyCommander::Terminate()
    {
      Component::Terminate();
      Core::Handle<EnemyCommander>::InvalidateHandles(this);
    }

    void EnemyCommander::StartTurn()
    {
      Core::Spaces[Core::SPACE_HUD].CreateAtPosition("EnemyTurnText", 
        glm::vec3(0, 0, 0));
    }

    void EnemyCommander::OnEnemyTurnTextEnd(Core::Event&)
    {
      active = true;
      curr_unit = 0;
      NextUnit();
    }

    void EnemyCommander::AddUnit(Core::Handle<Core::Object> unit)
    {
      units.push_back(unit);
    }

    void EnemyCommander::RemoveUnit(Core::Handle<Core::Object> unit)
    {
      units.erase(std::remove(units.begin(), units.end(), unit), units.end());
      if(units.size() == 0)
      {
        Core::Log << "You win!" << std::endl;
      }
    }

    void EnemyCommander::EndTurn()
    {
      active = false;
      GetSpace()->FindObjectByName("PlayerCommander")->
        GetComponent<PlayerCommander>()->StartTurn();
    }

    void EnemyCommander::NextUnit()
    {
      if(curr_unit < units.size())
      {
        Core::Log << "dispatched" << std::endl;
        Core::Event ev;
        Core::DispatchEvent((void*)units[curr_unit++], "AITakeTurn", ev);
      }
      else
      {
        EndTurn();
      }
    }
  }
}