/*!
  \file Ability.hpp
  \author Chase A Rayment
  \date 4 July 2017
  \brief Contains the definition of the Ability interface.
*/
#pragma once

#include "Material.hpp"
#include "ResourceManager.hpp"
#include "Handle.hpp"
#include <string>
#include "Object.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class Ability
    {
    protected:
      Core::Handle<Core::Object> source;

    public:
      enum class TargetingMode { SingleTile, AOETile, Ally, Enemy };
      virtual std::string GetName() = 0;
      virtual Core::Handle<Graphics::Material> GetIcon() = 0;
      virtual TargetingMode GetTargetingMode() = 0;
      virtual void Use(Core::Handle<Core::Object> target) = 0;
      virtual bool IsFreeAction() const { return false; }
      virtual ~Ability() { }
      void SetSource(Core::Handle<Core::Object> s) { source = s; }
      Core::Handle<Core::Object> GetSource() const { return source; }
    };
  }
}