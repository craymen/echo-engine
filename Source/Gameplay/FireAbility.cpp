/*!
  \file Fire Ability.cpp
  \author Chase A Rayment
  \date 11 July 2017
  \brief Contains the implementation of the Fire ability.
*/

#include "FireAbility.hpp"
#include "Object.hpp"
#include "ResourceManager.hpp"
#include "Unit.hpp"
#include "Log.hpp"

namespace Echo
{
  namespace Gameplay
  {
    std::string FireAbility::GetName()
    {
      return "Fire";
    }

    Core::Handle<Graphics::Material> FireAbility::GetIcon()
    {
      return Core::ResourceManager<Graphics::Material>::Find("FireIcon");
    }

    Ability::TargetingMode FireAbility::GetTargetingMode()
    {
      return Ability::TargetingMode::Enemy;
    }

    void FireAbility::Use(Core::Handle<Core::Object> target)
    {
      // Get the Unit component
      Core::Handle<Unit> u = target->GetComponent<Unit>();
      ASSERT(u != nullptr);

      u->DealDamage(1);
    }
  }
}