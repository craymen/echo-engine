/*!
  \file PlayerCommander.hpp
  \author Chase A Rayment
  \date 7 June 2017
  \brief Contains the definition of the PlayerCommander component.
*/
#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    /*!
      \brief Controls the player command actions.
    */
    class PlayerCommander : public Core::Component
    {
    public:
      /*!
        \brief Creates a new PlayerCommander.
      */
      PlayerCommander();

      /*!
        \brief Initializes a PlayerCommander.
      */
      void Initialize();

      /*!
        \brief Terminates a PlayerCommander.
      */
      void Terminate();

      /*!
        \brief Updates a PlayerCommander.
      */
      void Update(Core::UpdateEvent&);

      /*!
        \brief Sets a tile as the currently hovered tile.
        \param coord The coordinate of the tile.
      */
      void SelectTile(const glm::ivec2& coord);

      /*!
        \brief Unsets a tile as the currently hovered tile.
        \param coord The coordinate of the tile.
      */
      void DeselectTile(const glm::ivec2& coord);

      /*!
        \brief Selects a unit.
        \param unit The unit to select.
      */
      void SelectUnit(Core::Handle<Core::Object> unit);

      /*!
        \brief Adds a unit.
        \param unit The unit to add.
      */
      void AddUnit(Core::Handle<Core::Object> unit);

      /*!
        \brief Removes a unit.
        \param unit The unit to remove.
      */
      void RemoveUnit(Core::Handle<Core::Object> unit);

      /*!
        \brief Sets a unit as the currently hovered unit.
        \param unit The unit.
      */
      void HoverUnit(Core::Handle<Core::Object> unit);

      /*!
        \brief Unsets a unit as the currently hovered unit.
        \param unit The unit.
      */
      void UnhoverUnit(Core::Handle<Core::Object> unit);

      /*!
        \brief Starts the player's turn.
      */
      void StartTurn();

      /*!
        \brief Ends the player's turn.
      */
      void EndTurn();

      /*!
        \brief Gets all player units.
      */
      const std::vector<Core::Handle<Core::Object> >& GetUnits() const;
    
    private:
      std::vector<Core::Handle<Core::Object> > curr_path;
      glm::ivec2 curr_tile;
      Core::Handle<Core::Object> curr_unit;
      std::vector<Core::Handle<Core::Object> > units;
      Core::Handle<Core::Object> hovered_unit;
      bool active;
    };
  }
}