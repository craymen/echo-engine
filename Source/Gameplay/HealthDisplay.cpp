/*!
  \file HealthDisplay.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the HealthDisplay component.
*/

#include "HealthDisplay.hpp"
#include "Unit.hpp"
#include "Model.hpp"
#include "Material.hpp"
#include "ResourceManager.hpp"

namespace Echo
{
  namespace Gameplay
  {
    HealthDisplay::HealthDisplay()
    {
      
    }
     
    void HealthDisplay::Initialize()
    {
      
    }
     
    void HealthDisplay::Terminate()
    {
      Component::Terminate();
      Core::Handle<HealthDisplay>::InvalidateHandles(this);
    }

    void HealthDisplay::SetUnit(Core::Handle<Core::Object> u)
    {
      unit = u;
      create_blips();
    }

    void HealthDisplay::DealDamage(unsigned damage)
    {
      for(unsigned i = curr_health - 1; i >= curr_health - damage; --i)
      {
        blips[i]->GetComponent<Graphics::Model>()->CurrentMaterial = 
          Core::ResourceManager<Graphics::Material>::Find("Tile");
      }
    }

    void HealthDisplay::create_blips()
    {
      // Destroy all current blips
      for(auto blip : blips)
        blip->Terminate();
      blips.clear();

      for(int i = 0; i < unit->GetComponent<Unit>()->MaxHealth; ++i)
      {
        glm::vec3 pos = glm::vec3((i % 5) * 1.1f, (i / 5) * 1.1f, 0.0f);
        Core::Handle<Core::Object> blip = GetSpace()->
          CreateAtPosition("HealthBlip", pos);
        blip->Transform.SetParent(&GetOwner()->Transform);
        blips.push_back(blip);
      }

      curr_health = unit->GetComponent<Unit>()->MaxHealth;
    }
  }
}