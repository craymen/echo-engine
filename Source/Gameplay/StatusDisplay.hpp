/*!
  \file StatusDisplay.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the StatusDisplay component.
*/
#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class StatusDisplay : public Core::Component
    {
    public:
      StatusDisplay();
      void Initialize();
      void Terminate();
      void Update(Core::UpdateEvent&);
      void SetUnit(Core::Handle<Core::Object> unit);
      void DealDamage(unsigned damage);
    
    private:
      Core::Handle<Core::Object> unit;
    };
  }
}