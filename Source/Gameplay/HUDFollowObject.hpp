/*!
  \file HUDFollowObject.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the HUDFollowObject component.
*/
#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class HUDFollowObject : public Core::Component
    {
    public:
      HUDFollowObject();
      void Initialize();
      void Terminate();
      void Update(Core::UpdateEvent&);
      void SetObject(Core::Handle<Core::Object> obj);

      glm::vec3 Offset;
    
    private:
      Core::Handle<Core::Object> obj;
    };
  }
}