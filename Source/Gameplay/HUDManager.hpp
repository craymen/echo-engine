/*!
  \file HUDManager.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the HUDManager component.
*/
#pragma once

#include "GameplayIncludes.hpp"
#include "Ability.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class HUDManager : public Core::Component
    {
    public:
      HUDManager();
      void Initialize();
      void Terminate();
      void SelectUnit(Core::Handle<Core::Object> unit);
      void SelectAbility(Ability* a);
      Ability* GetSelectedAbility();
      void UseAbility(Ability* a, Core::Handle<Core::Object> target);
    
    private:
      std::vector<Core::Handle<Core::Object> > ability_buttons;
      Ability* targeting_ability;
    };
  }
}