/*!
  \file PlayerCommander.cpp
  \author Chase A Rayment
  \date 7 June 2017
  \brief Contains the implementation of the PlayerCommander component.
*/

#include "PlayerCommander.hpp"
#include "Board.hpp"
#include "Tile.hpp"
#include "Unit.hpp"
#include "HUDManager.hpp"
#include "Board.hpp"
#include "EnemyCommander.hpp"

namespace Echo
{
  namespace Gameplay
  {
    PlayerCommander::PlayerCommander() : curr_path(), curr_tile(-1, -1),
      curr_unit(nullptr), units(), hovered_unit(nullptr), active(true)
    {
      
    }
     
    void PlayerCommander::Initialize()
    {
      // Connect to the update event
      Core::ConnectEvent((void*)GetSpace(), "Update", this, 
        &PlayerCommander::Update);
    }

    void PlayerCommander::Update(Core::UpdateEvent&)
    {
      if(!active)
        return;

      Core::Handle<HUDManager> hud = Core::Spaces[Core::SPACE_HUD].
        FindObjectByName("HUDManager")->GetComponent<HUDManager>();

      Ability* selability = hud->GetSelectedAbility();
      

      if(selability == nullptr)
      {
        if(curr_path.size() > 1)
        {
          Core::Handle<Core::Object> prev = curr_path[0];
          for(unsigned i = 1; i < curr_path.size() && 
            i <= (unsigned)curr_unit->GetComponent<Unit>()->Speed; ++i)
          {
            GetSpace()->GetGraphicsScene()->GetDebugDrawer().DrawLine(
              prev->Transform.GetPosition() + glm::vec3(0.0f, 0.01f, 0.0f),
              curr_path[i]->Transform.GetPosition() + 
              glm::vec3(0.0f, 0.01f, 0.0f));
            prev = curr_path[i];
          }
        }

        if(curr_unit != nullptr && curr_tile.x >= 0 && 
          Input::Mouse::MouseButtonIsPressed(SDL_BUTTON_RIGHT))
        {
          curr_unit->GetComponent<Unit>()->MoveTo(curr_tile);
        }
      }
      else
      {
        if(curr_tile.x >= 0 && 
          Input::Mouse::MouseButtonIsPressed(SDL_BUTTON_LEFT) &&
          selability->GetTargetingMode() == Ability::TargetingMode::SingleTile)
        {
          hud->UseAbility(selability, GetSpace()->FindObjectByName("Board")->
            GetComponent<Board>()->GetTile(curr_tile));
        }
        else if(hovered_unit != nullptr && 
          Input::Mouse::MouseButtonIsPressed(SDL_BUTTON_LEFT))
        {
          if(selability->GetTargetingMode() == Ability::TargetingMode::Enemy
            && !hovered_unit->GetComponent<Unit>()->IsPlayerUnit)
          {
            hud->UseAbility(selability, hovered_unit);
          }
        }
      }

      // Check to see if any units have moves left
      bool canMove = false;
      for(auto unit : units)
      {
        Core::Handle<Unit> u = unit->GetComponent<Unit>();
        if(u->CanMove || u->CanUseAbility)
          canMove = true;
      }

      if(!canMove)
        EndTurn();
    }
     
    void PlayerCommander::Terminate()
    {
      Component::Terminate();
      Core::Handle<PlayerCommander>::InvalidateHandles(this);
    }

    void PlayerCommander::SelectTile(const glm::ivec2& coord)
    {
      if(curr_unit == nullptr || !curr_unit->GetComponent<Unit>()->IsPlayerUnit
        || !curr_unit->GetComponent<Unit>()->CanMove)
        return;

      Core::Handle<Board> board = GetSpace()->FindObjectByName("Board")
        ->GetComponent<Board>();

      curr_path = board->GetPath(curr_unit->GetComponent<Unit>()->GetCoords(), 
        coord);
      curr_tile = coord;
    }

    void PlayerCommander::DeselectTile(const glm::ivec2& coord)
    {
      if(coord == curr_tile)
      {
        curr_tile = glm::ivec2(-1, -1);
        curr_path.clear();
      }
    }

    void PlayerCommander::SelectUnit(Core::Handle<Core::Object> unit)
    {
      Core::Handle<HUDManager> hud = Core::Spaces[Core::SPACE_HUD].
        FindObjectByName("HUDManager")->GetComponent<HUDManager>();

      Ability* selability = hud->GetSelectedAbility();
      if(!selability)
      {
        curr_unit = unit;
        hud->SelectUnit(unit);
      }

      for(auto tile : GetSpace()->FindObjectByName("Board")->
        GetComponent<Board>()->GetAllTiles())
      {
        tile->GetComponent<Tile>()->SetMovable(false);
      }

      for(auto tile : unit->GetComponent<Unit>()->GetAllMovableTiles())
      {
        tile->GetComponent<Tile>()->SetMovable(true);
      }
    }

    void PlayerCommander::AddUnit(Core::Handle<Core::Object> unit)
    {
      units.push_back(unit);
    }

    void PlayerCommander::HoverUnit(Core::Handle<Core::Object> unit)
    {
      hovered_unit = unit;
    }

    void PlayerCommander::UnhoverUnit(Core::Handle<Core::Object> unit)
    {
      if(unit == hovered_unit)
        hovered_unit = nullptr;
    }

    void PlayerCommander::StartTurn()
    {
      Core::Event ev;
      Core::DispatchEvent((void*)nullptr, "StartPlayerTurn", ev);
      active = true;
    }

    void PlayerCommander::EndTurn()
    {
      active = false;
      GetSpace()->FindObjectByName("EnemyCommander")->
        GetComponent<EnemyCommander>()->StartTurn();
    }

    const std::vector<Core::Handle<Core::Object> >& 
      PlayerCommander::GetUnits() const
    {
      return units;
    }

    void PlayerCommander::RemoveUnit(Core::Handle<Core::Object> unit)
    {
      units.erase(std::remove(units.begin(), units.end(), unit), units.end());
      if(units.size() == 0)
      {
        Core::Log << "You lose!" << std::endl;
      }
    }
  }
}