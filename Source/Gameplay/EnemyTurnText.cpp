/*!
  \file EnemyTurnText.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the EnemyTurnText component.
*/

#include "EnemyTurnText.hpp"

namespace Echo
{
  namespace Gameplay
  {
    EnemyTurnText::EnemyTurnText()
    {
      
    }
     
    void EnemyTurnText::end_text(void* obj)
    {
      EnemyTurnText* comp = reinterpret_cast<EnemyTurnText*>(obj);
      comp->GetOwner()->Terminate();
      Core::Event ev;
      Core::DispatchEvent(nullptr, "EnemyTurnTextEnded", ev);
    }

    void EnemyTurnText::Initialize()
    {
      Core::Actions::ActionList* al = GetOwner()->Actions;
      GetOwner()->Transform.SetScale(glm::vec3(0.0f, 0.0f, 0.0f));
      GetOwner()->Transform.DisableCaching();
      al->AddAction(new Core::Actions::ActionProperty<glm::vec3>(&GetOwner()->
        Transform.GetScale(), glm::vec3(1.0f, 1.0f, 1.0f), 0.25f, 
        Core::Actions::EaseLinear));
      al->AddAction(new Core::Actions::ActionDelay(1.0f));
      al->AddAction(new Core::Actions::ActionProperty<glm::vec3>(&GetOwner()->
        Transform.GetScale(), glm::vec3(0.0f, 0.0f, 0.0f), 0.25f, 
        Core::Actions::EaseLinear));
      al->AddAction(new Core::Actions::ActionCall(&EnemyTurnText::end_text, 
        this));
    }
     
    void EnemyTurnText::Terminate()
    {
      Component::Terminate();
      Core::Handle<EnemyTurnText>::InvalidateHandles(this);
    }
  }
}