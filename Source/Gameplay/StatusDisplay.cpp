/*!
  \file StatusDisplay.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the StatusDisplay component.
*/

#include "StatusDisplay.hpp"
#include "HUDFollowObject.hpp"
#include "Unit.hpp"
#include "HealthDisplay.hpp"

namespace Echo
{
  namespace Gameplay
  {
    StatusDisplay::StatusDisplay() : unit(nullptr)
    {
      
    }
     
    void StatusDisplay::Initialize()
    {
      Core::ConnectEvent((void*)GetSpace(), "Update", this, 
        &StatusDisplay::Update);
    }
     
    void StatusDisplay::Terminate()
    {
      Component::Terminate();
      Core::Handle<StatusDisplay>::InvalidateHandles(this);
    }

    void StatusDisplay::Update(Core::UpdateEvent&)
    {

    }

    void StatusDisplay::SetUnit(Core::Handle<Core::Object> u)
    {
      unit = u;
      GetOwner()->Transform.FindChildByName("HealthDisplay")->
        GetComponent<HealthDisplay>()->SetUnit(u);
      GetOwner()->GetComponent<HUDFollowObject>()->SetObject(u);
    }

    void StatusDisplay::DealDamage(unsigned damage)
    {
      GetOwner()->Transform.FindChildByName("HealthDisplay")->
        GetComponent<HealthDisplay>()->DealDamage(damage);
    }
  }
}