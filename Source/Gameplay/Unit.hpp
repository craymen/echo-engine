/*!
  \file Unit.hpp
  \author Chase A Rayment
  \date 7 June 2017
  \brief Contains the definition of the Unit component.
*/
#pragma once

#include "GameplayIncludes.hpp"
#include "Ability.hpp"

namespace Echo
{
  namespace Gameplay
  {
    /*!
      \brief Represents a unit on the board.
    */
    class Unit : public Core::Component
    {
    public:
      /*!
        \brief Creates a new Unit.
      */
      Unit();

      /*!
        \brief Initializes a Unit.
      */
      void Initialize();

      /*!
        \brief Terminates a Unit.
      */
      void Terminate();

      /*!
        \brief Moves this unit to the given coordinates.
        \param coord The coordinates to move to.
      */
      void MoveTo(const glm::ivec2& coord);

      /*!
        \brief Handles the mouse hover event.
      */
      void OnHover(Physics::MouseHoverEvent&);

      /*!
        \brief Handles the mouse unhover event.
      */
      void OnEndHover(Physics::MouseHoverEvent&);

      /*!
        \brief Updates a Unit.
      */
      void Update(Core::UpdateEvent&);

      /*!
        \brief Gets the coordinates of this Unit.
      */
      glm::ivec2 GetCoords();

      /*!
        \brief Whether or not this Unit is controlled by the player.
      */
      bool IsPlayerUnit;

      /*!
        \brief Adds an ability to this Unit.
        \param a The ability to add.
      */
      void AddAbility(Ability* a);

      /*!
        \brief Gets all this unit's abilities.
      */
      const std::vector<Ability*>& GetAbilities() const;

      /*!
        \brief Deals damage to this Unit.
        \param damage How much damage to deal.
      */
      void DealDamage(unsigned damage);

      /*!
        \brief Handles starting the player turn.
      */
      void OnPlayerStartTurn(Core::Event&);

      /*!
        \brief Gets the player's current health.
      */
      int GetHealth() const;

      /*!
        \brief Gets all tiles this unit can move to.
      */
      std::vector<Core::Handle<Core::Object> > GetAllMovableTiles();

      int MaxHealth;      //!< The maximum health of this unit.
      bool CanUseAbility; //!< Whether or not this unit can use an ability.
      bool CanMove;       //!< Whether or not this unit can move.
      int Speed;          //!< The maximum distance this unit can move.

    private:
      bool hovered;                    // Whether or not this Unit is hovered.
      std::vector<Ability*> abilities; // This unit's abilities.
      int curr_health;                 // This unit's current health.
      Core::Handle<Core::Object> status_display; // The unit's health display.

      static void handle_path_end(void* obj);
    };
  }
}