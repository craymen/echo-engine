/*!
  \file HealthDisplay.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the HealthDisplay component.
*/
#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class HealthDisplay : public Core::Component
    {
    public:
      HealthDisplay();
      void Initialize();
      void Terminate();
      void SetUnit(Core::Handle<Core::Object> unit);
      void DealDamage(unsigned damage);
    
    private:
      Core::Handle<Core::Object> unit;
      std::vector<Core::Handle<Core::Object> > blips;
      int curr_health;
      void create_blips();
    };
  }
}