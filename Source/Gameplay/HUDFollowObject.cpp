/*!
  \file HUDFollowObject.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the HUDFollowObject component.
*/

#include "HUDFollowObject.hpp"
#include "Window.hpp"

namespace Echo
{
  namespace Gameplay
  {
    HUDFollowObject::HUDFollowObject() : Offset()
    {
      
    }
     
    void HUDFollowObject::Initialize()
    {
      Core::ConnectEvent((void*)GetSpace(), "Update", this, 
        &HUDFollowObject::Update);
    }
     
    void HUDFollowObject::Terminate()
    {
      Component::Terminate();
      Core::Handle<HUDFollowObject>::InvalidateHandles(this);
    }

    void HUDFollowObject::Update(Core::UpdateEvent&)
    {
      if(obj == nullptr)
        return;

      glm::mat4 hud_v = GetSpace()->GetGraphicsScene()
        ->GetCurrentCamera()->GetViewMatrix();
      glm::mat4 hud_p = GetSpace()->GetGraphicsScene()
        ->GetCurrentCamera()->GetProjectionMatrix();

      glm::mat4 gam_v = obj->GetSpace()->GetGraphicsScene()
        ->GetCurrentCamera()->GetViewMatrix();
      glm::mat4 gam_p = obj->GetSpace()->GetGraphicsScene()
        ->GetCurrentCamera()->GetProjectionMatrix();

      glm::vec4 transformed_pos = gam_p * gam_v * 
        glm::vec4(obj->Transform.GetPosition() + Offset, 1.0f);

      transformed_pos /= transformed_pos.w;
      transformed_pos.y = -transformed_pos.y;
      transformed_pos = 
        (transformed_pos + glm::vec4(1.0f, 1.0f, 0.0f, 0.0f)) / 2.0f;
      transformed_pos *= glm::vec4(Graphics::Window::GetWidth(), 
        Graphics::Window::GetHeight(), 1.0f, 1.0f);

      glm::ivec2 screenpos = glm::ivec2(
        (int)round(transformed_pos.x), 
        (int)round(transformed_pos.y));
      
      glm::vec3 start, dir;
      Core::Spaces[Core::SPACE_HUD].GetGraphicsScene()->GetCurrentCamera()
        ->ScreenCoordsToWorldRay(screenpos, start, dir);

      GetOwner()->Transform.SetPosition(start + 5.0f * dir);
    }

    void HUDFollowObject::SetObject(Core::Handle<Core::Object> o)
    {
      obj = o;
    }

  }
}