/*!
  \file Tile.cpp
  \author Chase A Rayment
  \date 5 June 2017
  \brief Contains the implementation of the Tile component.
*/

#include "Tile.hpp"
#include "Board.hpp"
#include "PlayerCommander.hpp"
#include <limits>
#undef max

namespace Echo
{
  namespace Gameplay
  {
    Tile::Tile() : Occupant(nullptr), cost(0.0f), hcost(0.0f), 
      status(list_status::neither), parent(nullptr)
    {

    }

    void Tile::Initialize()
    {
      // Get the board object
      Core::Handle<Core::Object> board = GetSpace()->FindObjectByName("Board");

      // Check to make sure the object exists
      if(board == nullptr)
      {
        Core::Log << Core::Priority::Warning << "Tried to initialize a tile "
          "without a board!" << std::endl;
        return;
      }
      
      // Check to make sure the board component exists
      if(board->GetComponent<Board>() == nullptr)
      {
        Core::Log << Core::Priority::Warning << "Tried to initialize a tile "
          " without a Board component attached to the board object!" 
          << std::endl;
        return;
      }

      // Add the tile to the board
      board->GetComponent<Board>()->AddTile(GetOwner());

      // Connect to the hover event
      Core::ConnectEvent((void*)GetOwner(), "MouseHoverStart", this, 
        &Tile::OnHover);
      Core::ConnectEvent((void*)GetOwner(), "MouseHoverEnd", this, 
        &Tile::OnEndHover);
    }

    void Tile::Terminate()
    {
      Component::Terminate();
      Core::Handle<Tile>::InvalidateHandles(this); 
    }

    void Tile::OnHover(Physics::MouseHoverEvent&)
    {
      Core::Handle<Core::Object> pcobj = GetSpace()->
        FindObjectByName("PlayerCommander");
      if(pcobj != nullptr)
      {
        Core::Handle<PlayerCommander> pc = 
          pcobj->GetComponent<PlayerCommander>();
        if(pc != nullptr)
        {
          pc->SelectTile(GetCoords());
        }
      }
    }

    void Tile::OnEndHover(Physics::MouseHoverEvent&)
    {
      Core::Handle<Core::Object> pcobj = GetSpace()->
        FindObjectByName("PlayerCommander");
      if(pcobj != nullptr)
      {
        Core::Handle<PlayerCommander> pc = 
          pcobj->GetComponent<PlayerCommander>();
        if(pc != nullptr)
        {
          pc->DeselectTile(GetCoords());
        }
      }
    }

    glm::ivec2 Tile::GetCoords()
    {
      return glm::ivec2((int)round(GetOwner()->Transform.GetPosition().x),
        (int)round(GetOwner()->Transform.GetPosition().z));
    }

    void Tile::reset()
    {
      cost = 10000.0f;
      hcost = 10000.0f;
      status = list_status::neither;
      parent = nullptr;
    }

    void Tile::SetMovable(bool val)
    {
      glm::vec3 p = GetOwner()->Transform.GetPosition();
      if(val)
        GetOwner()->GetComponent<Graphics::Model>()->CurrentMaterial =
          Core::ResourceManager<Graphics::Material>::Find("MovableTile");
      else
        GetOwner()->GetComponent<Graphics::Model>()->CurrentMaterial =
          Core::ResourceManager<Graphics::Material>::Find("Tile");    
    }
  }
}