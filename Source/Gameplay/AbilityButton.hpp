/*!
  \file AbilityButton.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the AbilityButton component.
*/
#pragma once

#include "GameplayIncludes.hpp"
#include "Ability.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class AbilityButton : public Core::Component
    {
    public:
      AbilityButton();
      void Initialize();
      void Terminate();
      void OnHover(Physics::MouseHoverEvent&);
      void OnUnHover(Physics::MouseHoverEvent&);
      void Update(Core::UpdateEvent&);
      void SetAbility(Ability* a);
    
    private:
      bool hovered;
      Ability* ability;
    };
  }
}