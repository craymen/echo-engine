/*!
  \file CameraController.hpp
  \author Chase A Rayment
  \date 8 June 2017
  \brief Contains the definition of the CameraController component.
*/
#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    /*!
      \brief Controls the main gameplay camera.
    */
    class CameraController : public Core::Component
    {
    public:
      /*!
        \brief Creates a new CameraController.
      */
      CameraController();

      /*!
        \brief Initializes a CameraController.
      */
      void Initialize();

      /*!
        \brief Updates a CameraController.
        \param evt The update event.
      */
      void Update(Core::UpdateEvent& evt);

      /*!
        \brief Terminates a CameraController.
      */
      void Terminate();
    
    private:
      glm::vec3 target_pos; // The target position to look at.
      float move_speed;     // The camera's move speed if WASD are pressed.
    };
  }
}