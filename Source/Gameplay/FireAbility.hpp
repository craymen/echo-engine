/*!
  \file FireAbility.hpp
  \author Chase A Rayment
  \date 11 July 2017
  \brief Contains the definition of the Fire ability.
*/
#pragma once

#include "Ability.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class FireAbility : public Ability
    {
      std::string GetName();
      Core::Handle<Graphics::Material> GetIcon();
      TargetingMode GetTargetingMode();
      void Use(Core::Handle<Core::Object> target);
    };
  }
}