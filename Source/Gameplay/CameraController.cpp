/*!
  \file CameraController.cpp
  \author Chase A Rayment
  \date 8 June 2017
  \brief Contains the implementation of the CameraController component.
*/

#include "CameraController.hpp"

namespace Echo
{
  namespace Gameplay
  {
    CameraController::CameraController() : target_pos(),
      move_speed(5.0f)
    {
      
    }
     
    void CameraController::Initialize()
    {
      Core::ConnectEvent((void*)GetSpace(), "Update", this, 
        &CameraController::Update);
    }

    void CameraController::Update(Core::UpdateEvent& evt)
    {
      glm::vec3 forward = GetOwner()->Transform.GetForward();
      glm::vec3 right = GetOwner()->Transform.GetRight();
      forward.y = 0;
      right.y = 0;

      if(Input::Keyboard::KeyIsDown(SDL_SCANCODE_W))
        target_pos += glm::normalize(forward) * move_speed * evt.Dt;
      if(Input::Keyboard::KeyIsDown(SDL_SCANCODE_S))
        target_pos -= glm::normalize(forward) * move_speed * evt.Dt;
      if(Input::Keyboard::KeyIsDown(SDL_SCANCODE_A))
        target_pos -= glm::normalize(right) * move_speed * evt.Dt;
      if(Input::Keyboard::KeyIsDown(SDL_SCANCODE_D))
        target_pos += glm::normalize(right) * move_speed * evt.Dt;
      if(Input::Keyboard::KeyIsPressed(SDL_SCANCODE_Q))
        GetOwner()->Transform.RotateAround(glm::vec3(0.0f, 1.0f, 0.0f), 
          glm::radians(90.0f));
      if(Input::Keyboard::KeyIsPressed(SDL_SCANCODE_E))
        GetOwner()->Transform.RotateAround(glm::vec3(0.0f, 1.0f, 0.0f), 
          glm::radians(-90.0f));

      glm::vec3 moveto = target_pos + -GetOwner()->Transform.GetForward() * 5.0f;

      GetOwner()->Transform.SetPosition(GetOwner()->Transform.GetPosition() + 
        (moveto - GetOwner()->Transform.GetPosition()) * 0.1f);
    }
     
    void CameraController::Terminate()
    {
      Component::Terminate();
      Core::Handle<CameraController>::InvalidateHandles(this);
    }
  }
}