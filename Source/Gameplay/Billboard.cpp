/*!
  \file Billboard.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the Billboard component.
*/

#include "Billboard.hpp"
#include "CameraObject.hpp"

namespace Echo
{
  namespace Gameplay
  {
    Billboard::Billboard()
    {
      
    }
     
    void Billboard::Initialize()
    {
      Core::ConnectEvent((void*)GetSpace(), "Update", this, &Billboard::Update);
    }
     
    void Billboard::Terminate()
    {
      Component::Terminate();
      Core::Handle<Billboard>::InvalidateHandles(this);
    }

    void Billboard::Update(Core::UpdateEvent&)
    {
      Graphics::CameraObject* cam = 
        GetSpace()->GetGraphicsScene()->GetCurrentCamera();
      glm::mat4 mat = cam->GetViewMatrix();
      glm::vec3 scale = GetOwner()->Transform.GetScale();
      mat[3] = glm::vec4(GetOwner()->Transform.GetPosition(), 1.0f);
      GetOwner()->Transform.SetTransform(mat);
      GetOwner()->Transform.SetScale(scale);
    }
  }
}