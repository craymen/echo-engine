/*!
  \file MeleeDroid.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the MeleeDroid component.
*/

#include "MeleeDroid.hpp"
#include "Unit.hpp"
#include "Tile.hpp"
#include "PlayerCommander.hpp"
#include "Board.hpp"
#include "EnemyCommander.hpp"

namespace Echo
{
  namespace Gameplay
  {
    MeleeDroid::MeleeDroid()
    {
      
    }
     
    void MeleeDroid::Initialize()
    {
      Core::ConnectEvent((void*)GetOwner(), "AITakeTurn", this, 
        &MeleeDroid::AITakeTurn);
      Core::ConnectEvent((void*)GetOwner(), "UnitMoveEnd", this,
        &MeleeDroid::OnUnitMoveEnd);
    }
     
    void MeleeDroid::Terminate()
    {
      Component::Terminate();
      Core::Handle<MeleeDroid>::InvalidateHandles(this);
    }

    void MeleeDroid::AITakeTurn(Core::Event&)
    {
      if(try_attack())
      {
        GetSpace()->FindObjectByName("EnemyCommander")->
          GetComponent<EnemyCommander>()->NextUnit();
        return;
      }

      // If not neighboring a player unit, run to the closest one
      float min_dist = 99999.0f;
      Core::Handle<Core::Object> closest;
      glm::ivec2 my_pos = GetOwner()->GetComponent<Unit>()->GetCoords();
      for(auto unit : GetSpace()->FindObjectByName("PlayerCommander")->
        GetComponent<PlayerCommander>()->GetUnits())
      {
        for(auto tile : GetSpace()->FindObjectByName("Board")->
          GetComponent<Board>()->GetTraversableNeighbors(unit->
          GetComponent<Unit>()->GetCoords()))
        {
          float dist = glm::length(
            glm::vec2(tile->GetComponent<Tile>()->GetCoords()) 
            - glm::vec2(my_pos));
          if(dist < min_dist)
          {
            min_dist = dist;
            closest = tile;
          }
        }
      }

      GetOwner()->GetComponent<Unit>()->MoveTo(
        closest->GetComponent<Tile>()->GetCoords());
    }

    void MeleeDroid::OnUnitMoveEnd(Core::Event&)
    {
      try_attack();
      GetSpace()->FindObjectByName("EnemyCommander")->
        GetComponent<EnemyCommander>()->NextUnit();
    }

    bool MeleeDroid::try_attack()
    {
      // Look for a neighboring player unit and attack it if one exists
      for(auto tile : GetSpace()->FindObjectByName("Board")->
        GetComponent<Board>()->GetTraversableNeighbors(GetOwner()->
        GetComponent<Unit>()->GetCoords(), false))
      {
        Core::Handle<Core::Object> occupant = 
          tile->GetComponent<Tile>()->Occupant;
        if(occupant != nullptr && occupant->GetComponent<Unit>()->IsPlayerUnit)
        {
          occupant->GetComponent<Unit>()->DealDamage(1);
          return true;
        }
      }

      return false;
    }
  }
}