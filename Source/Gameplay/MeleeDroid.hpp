/*!
  \file MeleeDroid.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the MeleeDroid component.
*/
#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class MeleeDroid : public Core::Component
    {
    public:
      MeleeDroid();
      void Initialize();
      void Terminate();
      void AITakeTurn(Core::Event&);
      void OnUnitMoveEnd(Core::Event&);
    
    private:
      bool try_attack();
    };
  }
}