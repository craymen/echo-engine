/*!
  \file Billboard.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the Billboard component.
*/
#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    class Billboard : public Core::Component
    {
    public:
      Billboard();
      void Initialize();
      void Update(Core::UpdateEvent&);
      void Terminate();
    
    private:
    
    };
  }
}