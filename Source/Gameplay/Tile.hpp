/*!
  \file Tile.hpp
  \author Chase A Rayment
  \date 5 June 2017
  \brief Contains the definition of the Tile component.
*/

#pragma once

#include "GameplayIncludes.hpp"

namespace Echo
{
  namespace Gameplay
  {
    /*!
      \brief Represents a tile on the board.
    */
    class Tile : public Core::Component
    {
      friend class Board;
    public:
      /*!
        \brief Creates a new tile.
      */
      Tile();

      /*!
        \brief Initializes a tile.
      */
      void Initialize();

      /*!
        \brief Terminates a tile.
      */
      void Terminate();

      /*!
        \brief Handles the mouse hover event.
      */
      void OnHover(Physics::MouseHoverEvent&);

      /*!
        \brief Handles the mouse unhover event.
      */
      void OnEndHover(Physics::MouseHoverEvent&);

      /*!
        \brief Gets the coordinates of this tile.
      */
      glm::ivec2 GetCoords();

      /*!
        \brief Sets whether or not this tile is possible to move to.
      */
      void SetMovable(bool val);

      Core::Handle<Core::Object> Occupant; //!< The current occupant.

    private:
        // The A* closed/open list status.
      enum class list_status { closed, open, neither };

      float cost, hcost;                 // The A* given/heuristic cost.
      list_status status;                // The list status.
      Core::Handle<Core::Object> parent; // The parent node.

      void reset();                      // Resets this tile for A* pathfinding.
    };
  }
}