/*!
  \file Listener.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the Listener component.
*/

#include "Listener.hpp"
#include "Space.hpp"
#include "Transform.hpp"
#include "AudioScene.hpp"
#include "Messaging.hpp"

namespace Echo
{
  namespace Audio
  {
    Listener::Listener() : Active(true)
    {
      
    }
     
    void Listener::Initialize()
    {
      Core::ConnectEvent(nullptr, "PreAudio", this, &Listener::PreAudio);
    }
     
    void Listener::Terminate()
    {
      Component::Terminate();
      Core::Handle<Listener>::InvalidateHandles(this);
    }

    void Listener::PreAudio(PreAudioEvent&)
    {
      if(Active)
      {
        Graphics::Transform& t = GetOwner()->Transform;
        GetSpace()->GetAudioScene().SetListenerPosition(
          t.GetPosition(), t.GetUp(), t.GetForward());
      }
    }
  }
}