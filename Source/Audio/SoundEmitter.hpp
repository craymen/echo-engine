/*!
  \file SoundEmitter.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the SoundEmitter component.
*/
#pragma once

#include "Component.hpp"
#include "GameObject.hpp"
#include "Handle.hpp"
#include "PreAudioEvent.hpp"

namespace Echo
{
  namespace Audio
  {
    class SoundEmitter : public Core::Component
    {
    public:
      SoundEmitter();
      void Initialize();
      void Terminate();
      void PostEvent(const std::string& name);
      void PreAudio(PreAudioEvent&);

    private:
      Core::Handle<GameObject> obj;   
    };
  }
}