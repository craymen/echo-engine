/*!
  \file AudioUtilities.hpp
  \author Chase A Rayment
  \date 30 June 2017
  \brief Contains functions to convert between GLM and AK types.
*/
#pragma once

#include "AudioPlatform.hpp"

namespace Echo
{
  namespace Audio
  {
    inline AkVector GLMVec3ToAkVector(const glm::vec3& vec)
    {
      AkVector result;
      result.X = vec.x;
      result.Y = vec.y;
      result.Z = -vec.z;
      return result;
    }
  }
}