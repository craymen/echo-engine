/*!
  \file Listener.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the Listener component.
*/
#pragma once

#include "Component.hpp"
#include "Handle.hpp"
#include "PreAudioEvent.hpp"

namespace Echo
{
  namespace Audio
  {
    class Listener : public Core::Component
    {
    public:
      Listener();
      void Initialize();
      void Terminate();
      void PreAudio(PreAudioEvent&);

      bool Active;
    
    private:
    
    };
  }
}