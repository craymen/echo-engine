/*!
  \file GameObject.hpp
  \author Chase A Rayment
  \date 30 June 2017
  \brief Contains the definition of the GameObject class.
*/
#pragma once

#include "GLM/glm.hpp"
#include <string>

namespace Echo
{
  namespace Audio
  {
    class GameObject
    {
    public:

      GameObject(unsigned id, unsigned listener_id);

      ~GameObject();

      void SetPosition(const glm::vec3& position, const glm::vec3& forward, 
        const glm::vec3& up);

      void PostAudioEvent(const std::string& name);

    private:
      unsigned id;
    };
  }
}