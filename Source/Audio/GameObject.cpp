/*!
  \file GameObject.cpp
  \author Chase A Rayment
  \date 30 June 2017
  \brief Contains the implementation of the GameObject class.
*/

#include "GameObject.hpp"
#include "AudioPlatform.hpp"
#include "AudioUtilities.hpp"

namespace Echo
{
  namespace Audio
  {
    GameObject::GameObject(unsigned i, unsigned listener_id) : id(i + 0x3)
    {
      AK::SoundEngine::RegisterGameObj(id);
      AK::SoundEngine::SetActiveListeners(id, (1 << listener_id));
    }

    GameObject::~GameObject()
    {
      AK::SoundEngine::UnregisterGameObj(id);
    }

    void GameObject::SetPosition(const glm::vec3& position, 
      const glm::vec3& forward, const glm::vec3& up)
    {
      AkSoundPosition pos;
      pos.Set(GLMVec3ToAkVector(position), GLMVec3ToAkVector(forward),
        GLMVec3ToAkVector(up));
      AK::SoundEngine::SetPosition(id, pos);
    }

    void GameObject::PostAudioEvent(const std::string& name)
    {
      AK::SoundEngine::PostEvent(name.c_str(), id);
    }
  }
}