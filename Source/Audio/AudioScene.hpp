/*!
  \file AudioScene.hpp
  \author Chase A Rayment
  \date 30 June 2017
  \brief Contains the definition of the AudioScene class.
*/
#pragma once

#include "Handle.hpp"
#include "GameObject.hpp"

namespace Echo
{
  namespace Audio
  {
    class Scene
    {
    public:
      Scene(unsigned id);
      
      void Initialize();

      void Terminate();

      Core::Handle<GameObject> AddGameObject(unsigned id);

      void RemoveGameObject(Core::Handle<GameObject> obj);

      void SetListenerPosition(const glm::vec3& position, const glm::vec3& up, 
        const glm::vec3& forward);

    private:
      unsigned id;
    };
  }
}