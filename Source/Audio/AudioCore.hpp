/*!
  \file AudioCore.hpp
  \author Chase A Rayment
  \date 22 May 2017
  \brief Contains the definition of the Audio system.
*/
#pragma once

#include <string>

namespace Echo
{
  namespace Audio
  {
    /*!
      \brief Initializes the Audio system.
    */
    void Initialize();

    /*!
      \brief Updates the Audio system.
    */
    void Update();

    /*!
      \brief Terminates the Audio system.
    */
    void Terminate();

    /*!
      \brief Posts a global audio event.
      \param name The name of the event.
    */
    void PostAudioEvent(const std::string& name);

    /*!
      \brief Sets a global RTPC.
      \param name The name of the RTPC.
      \param val The value to set the RTPC to.
    */
    void SetRTPC(const std::string& name, float val);
  }
}