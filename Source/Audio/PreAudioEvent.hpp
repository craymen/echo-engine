/*!
  \file PreAudioEvent.hpp
  \author Chase A Rayment
  \date 30 June 2017
  \brief Contains the definition of the PreAudioEvent.
*/
#pragma once

#include "Messaging.hpp"

namespace Echo
{
  namespace Audio
  {
    class PreAudioEvent : public Core::Event
    {
      
    };
  }
}