/*!
  \file AudioCore.cpp
  \author Chase A Rayment
  \date 22 May 2017
  \brief Contains the implementation of the Audio system.
*/

#include "AudioCore.hpp"
#include <AK/SoundEngine/Common/AkMemoryMgr.h>
#include <AK/SoundEngine/Common/AkModule.h>
#include <AK/SoundEngine/Common/IAkStreamMgr.h>
#include <AK/Tools/Common/AkPlatformFuncs.h>
#include <AK/SoundEngine/Common/AkSoundEngine.h>
#ifdef _WIN32
  #include "AK/SoundEngine/Win32/AKFilePackageLowLevelIOBlocking.h"
#endif

#ifdef __linux__
  #include "AK/SoundEngine/POSIX/AkFilePackageLowLevelIOBlocking.h"
#endif

#include "Log.hpp"
#include "PreAudioEvent.hpp"

namespace AK
{
#ifdef WIN32
  void* AllocHook(size_t in_size)
  {
    return malloc(in_size);
  }

  void FreeHook(void * in_ptr)
  {
    free(in_ptr);
  }

  void * VirtualAllocHook(void * in_pMemAddress, size_t in_size,
    DWORD in_dwAllocationType, DWORD in_dwProtect)
  {
    return VirtualAlloc(in_pMemAddress, in_size, in_dwAllocationType,
      in_dwProtect);
  }
  void VirtualFreeHook(void * in_pMemAddress, size_t in_size,
    DWORD in_dwFreeType)
  {
    VirtualFree(in_pMemAddress, in_size, in_dwFreeType);
  }
#endif

#ifdef __linux__
    void * AllocHook( size_t in_size )
    {
        return malloc( in_size );
    }
    void FreeHook( void * in_ptr )
    {
        free( in_ptr );
    }
#endif
}

namespace Echo
{
  namespace Audio
  {
    static CAkFilePackageLowLevelIOBlocking lowLevelIO;

    static const AkGameObjectID gid = 0x2;

    void Initialize()
    {
      // Set up Wwise memory settings
      AkMemSettings memSettings;
      memSettings.uMaxNumPools = 20;

      // Initialize memory manager
      if(AK::MemoryMgr::Init(&memSettings) != AK_Success)
      {
        Core::Log << Core::Priority::Warning << "Could not initialize Wwise"
          " memory manager!" << std::endl;
        return;
      }

      // Set up Wwise stream manager settings
      AkStreamMgrSettings stmSettings;
      AK::StreamMgr::GetDefaultSettings(stmSettings);

      // Initialize stream manager
      if(!AK::StreamMgr::Create(stmSettings))
      {
        Core::Log << Core::Priority::Warning << "Could not initialize Wwise"
          " stream manager!" << std::endl;
        return;
      }

      // Set up streaming device settings
      AkDeviceSettings deviceSettings;
      AK::StreamMgr::GetDefaultDeviceSettings(deviceSettings);

      // Initialize streaming device
      if(lowLevelIO.Init(deviceSettings) != AK_Success)
      {
        Core::Log << Core::Priority::Warning << "Could not initialize Wwise"
          " streaming device!" << std::endl;
        return;
      }

      // Set up Wwise init settings
      AkInitSettings initSettings;
      AkPlatformInitSettings platformInitSettings;
      AK::SoundEngine::GetDefaultInitSettings(initSettings);
      AK::SoundEngine::GetDefaultPlatformInitSettings(platformInitSettings);
      
      // Initialize Wwise
      if(AK::SoundEngine::Init(&initSettings, &platformInitSettings)
        != AK_Success)
      {
        Core::Log << Core::Priority::Warning << "Could not initialize Wwise!" 
          << std::endl;
        return;
      }

      // Load banks
      lowLevelIO.SetBasePath(AKTEXT("../Assets/SoundBanks"));
      AkBankID id;
      AKRESULT eResult = AK::SoundEngine::LoadBank(L"Init.bnk", AK_DEFAULT_POOL_ID, id);
      eResult = AK::SoundEngine::LoadBank(L"Test.bnk", AK_DEFAULT_POOL_ID, id);
      AK::SoundEngine::RegisterGameObj(gid, "Global");
    }

    void Update()
    {
      PreAudioEvent evt;
      Core::DispatchEvent(nullptr, "PreAudio", evt);
      AK::SoundEngine::RenderAudio();
    }

    void Terminate()
    {
      AK::SoundEngine::Term();
      if(AK::IAkStreamMgr::Get())
        AK::IAkStreamMgr::Get()->Destroy();
      AK::MemoryMgr::Term();
    }

    void PostAudioEvent(const std::string& name)
    {
      AK::SoundEngine::PostEvent(name.c_str(), gid);
    }    
    
    void SetRTPC(const std::string& name, float val)
    {
      AK::SoundEngine::SetRTPCValue(name.c_str(), val);
    }
  }
}