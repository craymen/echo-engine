/*!
  \file AudioScene.cpp
  \author Chase A Rayment
  \date 30 June 2017
  \brief Contains the implementation of the AudioScene class.
*/
#pragma once

#include "AudioScene.hpp"
#include "AudioPlatform.hpp"
#include "AudioUtilities.hpp"

namespace Echo
{
  namespace Audio
  {
    Scene::Scene(unsigned i) : id(i)
    {
      
    }
    
    void Scene::Initialize()
    {

    }

    void Scene::Terminate()
    {

    }

    Core::Handle<GameObject> Scene::AddGameObject(unsigned i)
    {
      return new GameObject(i, id);
    }

    void Scene::RemoveGameObject(Core::Handle<GameObject> obj)
    {
      delete (GameObject*)obj;
    }

    void Scene::SetListenerPosition(const glm::vec3& position, 
      const glm::vec3& up, const glm::vec3& forward)
    {
      AkListenerPosition pos;
      pos.Set(GLMVec3ToAkVector(position), GLMVec3ToAkVector(forward),
        GLMVec3ToAkVector(up));
      AK::SoundEngine::SetListenerPosition(pos, id);
    }
  }
}