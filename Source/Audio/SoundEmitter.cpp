/*!
  \file SoundEmitter.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the SoundEmitter component.
*/

#include "SoundEmitter.hpp"
#include "AudioScene.hpp"
#include "Space.hpp"

namespace Echo
{
  namespace Audio
  {
    SoundEmitter::SoundEmitter() : obj(nullptr)
    {
      
    }
     
    void SoundEmitter::Initialize()
    {
      Core::ConnectEvent(nullptr, "PreAudio", this, &SoundEmitter::PreAudio);
      obj = GetSpace()->GetAudioScene().AddGameObject(GetOwner()->GetID() +
        GetSpace()->GetID() * Core::Space::MaxObjects);
    }
     
    void SoundEmitter::Terminate()
    {
      GetSpace()->GetAudioScene().RemoveGameObject(obj);

      Component::Terminate();
      Core::Handle<SoundEmitter>::InvalidateHandles(this);
    }

    void SoundEmitter::PostEvent(const std::string& name)
    {
      obj->PostAudioEvent(name);
    }

    void SoundEmitter::PreAudio(PreAudioEvent&)
    {
      Graphics::Transform& t = GetOwner()->Transform;
      obj->SetPosition(t.GetPosition(), t.GetForward(), t.GetUp());
    }
  }
}