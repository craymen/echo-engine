/*!
  \file RigidBody.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the RigidBody component.
*/

#include "RigidBody.hpp"
#include "BoxCollider.hpp"
#include "Collider.hpp"
#include "Log.hpp"
#include "Messaging.hpp"
#include "PhysicsScene.hpp"

namespace Echo
{
  namespace Physics
  {
    RigidBody::RigidBody() : Mass(0.0f), obj(nullptr)
    {
      
    }
     
    void RigidBody::Initialize()
    {
      btCollisionShape* shape = nullptr;
      Core::Handle<BoxCollider> c = nullptr;
      if((c = GetOwner()->GetComponent<BoxCollider>()) != nullptr)
        shape = c->GetObject()->GetShape();
      if(shape == nullptr)
      {
        Core::Log << Core::Priority::Warning << 
          "Tried to use a rigid body without a valid shape!" << std::endl;
        return;
      }

      // Create the rigid body
      obj = GetSpace()->GetPhysicsScene()->AddRigidBody(shape, Mass);
      Graphics::Transform& t = GetOwner()->Transform;
      obj->SetTransform(Graphics::Transform::ObjectToWorld(t.GetPosition(), 
        t.GetRotationQuaternion(), glm::vec3(1, 1, 1)));
      obj->SetUserPointer((void*)GetOwner());

      Core::ConnectEvent(nullptr, "PrePhysics", this, &RigidBody::PrePhysics);
      Core::ConnectEvent(nullptr, "PostPhysics", this, &RigidBody::PostPhysics);
    }
     
    void RigidBody::Terminate()
    {
      GetSpace()->GetPhysicsScene()->RemoveRigidBody(obj);

      Component::Terminate();
      Core::Handle<RigidBody>::InvalidateHandles(this);
    }

    void RigidBody::PrePhysics(const PrePhysicsEvent&)
    {
      obj->SetMass(Mass);
      if(Mass == 0.0f)
      {
        Graphics::Transform& t = GetOwner()->Transform;
        obj->SetTransform(Graphics::Transform::ObjectToWorld(t.GetPosition(), 
          t.GetRotationQuaternion(), glm::vec3(1, 1, 1)));
      }
    }

    void RigidBody::PostPhysics(const PostPhysicsEvent&)
    {
      if(Mass > 0.0f && !GetSpace()->GetPaused())
        GetOwner()->Transform.SetTransform(obj->GetTransform(), true);
    }
  }
}