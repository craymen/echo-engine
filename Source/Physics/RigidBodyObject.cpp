/*!
  \file RigidBodyObject.cpp
  \author Chase A Rayment
  \date 28 June 2017
  \brief Contains the implementation of the RigidBodyObject class.
*/
#include "RigidBodyObject.hpp"
#include "PhysicsUtilities.hpp"
#include "Log.hpp"

namespace Echo
{
  namespace Physics
  {
    RigidBodyObject::RigidBodyObject(btDiscreteDynamicsWorld* w, 
      btCollisionShape* s, float m) : 
      body(nullptr), motion_state(nullptr), 
      world(w), shape(s), mass(m), transform(), dirty(false)
    {
      // Create the motion state
      motion_state = new btDefaultMotionState(
      btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 50, 0)));
      
      // Calculate inertia
      btVector3 inertia(0, 0, 0);
      shape->calculateLocalInertia(mass, inertia);

      // Create the rigid body
      body = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(
        mass, motion_state, shape, inertia));

      // Add the rigid body to the world
      world->addRigidBody(body);
    }

    RigidBodyObject::~RigidBodyObject()
    {
      if(body)
      {
        world->removeRigidBody(body);        
        delete body;
      }
      if(motion_state)
        delete motion_state;
    }

    void RigidBodyObject::SetCollisionShape(btCollisionShape* s)
    {
      body->setCollisionShape(s);
    }

    btCollisionShape* RigidBodyObject::GetCollisionShape() const
    {
      return shape;
    }

    void RigidBodyObject::SetMass(float m)
    {
      // Calculate inertia
      btVector3 inertia(0, 0, 0);
      shape->calculateLocalInertia(m, inertia);
      body->setMassProps(m, inertia);
    }

    float RigidBodyObject::GetMass() const
    {
      return mass;
    }

    btRigidBody* RigidBodyObject::GetRigidBody()
    {
      return body;
    }

    void RigidBodyObject::SetTransform(const glm::mat4& t)
    {
      body->getWorldTransform() = GLMMat4ToBulletTransform(t);
      body->setLinearVelocity(btVector3(0.0f, 0.0f, 0.0f));
    }

    glm::mat4 RigidBodyObject::GetTransform()
    {
      btTransform t = body->getWorldTransform();
      return BulletTransformToGLMMat4(t);
    }

    void RigidBodyObject::SetUserPointer(void* ptr)
    {
      body->setUserPointer(ptr);
    }
  }
}
