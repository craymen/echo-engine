/*!
  \file PhysicsScene.hpp
  \author Chase A Rayment
  \date 23 May 2017
  \brief Contains the implementation of the Physics::Scene class.
*/

#pragma once

#include "Platform.hpp"
#include "Collider.hpp"
#include "BoxCollider.hpp"
#include "Handle.hpp"
#include <vector>
#include "GLM/glm.hpp"
#include <limits>
#include "PlaneColliderObject.hpp"
#include "RigidBodyObject.hpp"
#include "PhysicsDebugDrawer.hpp"

namespace Echo
{
  namespace Core { class Object; }
  
  namespace Physics
  {
    /*!
      \brief Holds data about a ray cast test.
    */
    struct RayCastResult
    {
      bool Hit;                             //!< If the ray hit an object.
      const btCollisionObject* HitCollider; //!< The hit collider object.
      Core::Handle<Core::Object> HitObject; //!< The hit game object.
      glm::vec3 HitPosition;                //!< The hit position.
    };

    /*!
      \brief Represents a collection of collision objects in 3D space.
    */
    class Scene
    {
    public:
      /*!
        \brief Creates a new Scene.
      */
      Scene();

      /*!
        \brief Adds a new BoxColliderObject.
      */
      Core::Handle<BoxColliderObject> AddBoxCollider();

      /*!
        \brief Removes a BoxColliderObject.
        \param coll The BoxColliderObject to remove.
      */
      void RemoveBoxCollider(Core::Handle<BoxColliderObject> coll);

      /*!
        \brief Adds a PlaneColliderObject.
      */
      Core::Handle<PlaneColliderObject> AddPlaneCollider();

      /*!
        \brief Removes a PlaneColliderObject.
        \param coll The PlaneColliderObject to remove.
      */
      void RemovePlaneCollider(Core::Handle<PlaneColliderObject> coll);

      /*!
        \brief Adds a RigidBody.
      */
      Core::Handle<RigidBodyObject> AddRigidBody(btCollisionShape* shape, 
        float mass);

      /*!
        \brief Removes a RigidBody.
        \param coll The RigidBody to remove.
      */
      void RemoveRigidBody(Core::Handle<RigidBodyObject> body);

      /*!
        \brief Updates the scene.
        \param dt The frame time.
      */
      void Update(float dt);
    
      /*!
        \brief Destroys a Scene.
      */
      ~Scene();

      /*!
        \brief Gets the first ray cast hit.
        \param start The ray start.
        \param direction The ray direction.
        \param maxDistance The maximum distance to cast.
        \returns A ray cast result structure with all information about the hit.
      */
      RayCastResult RayCastFirst(glm::vec3 start, glm::vec3 direction, 
        float maxDistance = -1.0f);

      /*!
        \brief Gets ALL ray cast hits.
        \param start The ray start.
        \param direction The ray direction.
        \param maxDistance The maximum distance to cast.
        \returns A vector of ray cast results.
      */
      std::vector<RayCastResult> RayCastAll(glm::vec3 start, 
        glm::vec3 direction, float maxDistance = -1.0f);

      /*!
        \brief Gets the scene's debug drawer.
      */
      DebugDrawer* GetDebugDrawer();

      /*!
        \brief Sets whether or not this scene is paused.
      */
      void SetPaused(bool p);

    private:
      btBroadphaseInterface* broadphase;
      btDefaultCollisionConfiguration* collision_config;
      btCollisionDispatcher* dispatcher;
      btSequentialImpulseConstraintSolver* solver;
      btDiscreteDynamicsWorld* dynamics_world;
      std::vector<Collider*> colliders;
      DebugDrawer* debug_drawer;
      bool paused;
    };
  }
}