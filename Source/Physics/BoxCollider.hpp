/*!
  \file BoxCollider.hpp
  \author Chase A Rayment
  \date 24 May 2017
  \brief Contains the definition of the BoxCollider component.
*/

#pragma once

#include "Component.hpp"
#include "BoxColliderObject.hpp"
#include "PrePhysicsEvent.hpp"
#include "Handle.hpp"

namespace Echo
{
  namespace Physics
  {
    /*!
      \brief Represents a box collider in a physics scene.
    */
    class BoxCollider : public Core::Component
    {
    public:
      /*!
        \brief Creates a new BoxCollider.
      */
      BoxCollider();
      
      /*!
        \brief Initializes a BoxCollider.
      */
      void Initialize();

      /*!
        \brief Handles the pre-physics update event.
      */
      void PrePhysics(const PrePhysicsEvent&);

      /*!
        \brief Terminates a BoxCollider.
      */
      void Terminate();

      /*!
        \brief Gets this BoxCollider's physics object.
      */
      Core::Handle<BoxColliderObject> GetObject();
      
      glm::vec3 Size; //!< The size of the collider.

    private:
      Core::Handle<BoxColliderObject> coll; // The box collider in the scene.
    };
  }
}