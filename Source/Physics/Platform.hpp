/*!
  \file Physics/Platform.hpp
  \author Chase A Rayment
  \date 29 May 2017
  \brief Contains Bullet includes.
*/

// Disable warnings for Bullet includes
#ifdef _WIN32
  #pragma warning(push, 0)
#endif

#include <Bullet/btBulletDynamicsCommon.h>
#include "Bullet/BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h"

#ifdef _WIN32
  #pragma warning(pop)
#endif