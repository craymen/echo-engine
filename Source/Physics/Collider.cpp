/*!
  \file Collider.cpp
  \author Chase A Rayment
  \date 24 May 2017
  \brief Contains the implementation of the Collider interface.
*/

#include "Collider.hpp"
#include "PhysicsUtilities.hpp"

namespace Echo
{
  namespace Physics
  {
    Collider::Collider() : shape(nullptr), gobj(nullptr)
    {

    }

    btCollisionShape* Collider::GetShape()
    {
      return shape;
    }

    void Collider::SetGameObject(Core::Handle<Core::Object> obj)
    {
      gobj = obj;
    }

    Core::Handle<Core::Object> Collider::GetGameObject()
    {
      return gobj;
    }

    Collider::~Collider()
    {
      delete shape;
    }

    void Collider::SetScale(const glm::vec3& scale)
    {
      shape->setLocalScaling(GLMVec3ToBulletVector3(scale));
    }
  }
}