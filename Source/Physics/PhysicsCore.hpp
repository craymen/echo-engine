/*!
  \file PhysicsCore.hpp
  \author Chase A Rayment
  \date 23 May 2017
  \brief Contains the definition of the Physics system.
*/

#pragma once

#include "PhysicsScene.hpp"
#include "Handle.hpp"

namespace Echo
{
  namespace Physics
  {
    /*!
      \brief Initializes the Physics system.
    */
    void Initialize();

    /*!
      \brief Updates the Physics system.
      \param dt The frame time.
    */
    void Update(float dt);

    /*!
      \brief Terminates the Physics system.
    */
    void Terminate();

    /*!
      \brief Adds a physics scene.
    */
    Core::Handle<Scene> AddScene();

    /*!
      \brief Removes a physics scene.
      \param scene The scene to remove.
    */
    void RemoveScene(Core::Handle<Scene> scene);
  }
}