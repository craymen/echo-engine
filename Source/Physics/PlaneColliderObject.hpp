/*!
  \file PlaneColliderObject.hpp
  \author Chase A Rayment
  \date 3 June 2017
  \brief Contains the definition of the PlaneColliderObject class.
*/
#pragma once

#include "Collider.hpp"

namespace Echo
{
  namespace Physics
  {
    /*!
      \brief Represents a plane collider.
    */
    class PlaneColliderObject : public Collider
    {
    public:
      /*!
        \brief Creates a new PlaneColliderObject.
      */
      PlaneColliderObject();
    };
  }
}