/*!
  \file MouseHoverEvent.hpp
  \author Chase A Rayment
  \date 25 May 2017
  \brief Contains the definition of the MouseHoverEvent event.
*/

#pragma once

#include "Messaging.hpp"

namespace Echo
{
  namespace Physics
  {
    /*!
      \brief An event sent to an object hovered with the mouse.
    */
    struct MouseHoverEvent : public Core::Event
    {

    };
  }
}