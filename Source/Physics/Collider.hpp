/*!
  \file Collider.hpp
  \author Chase A Rayment
  \date 24 May 2017
  \brief Contains the definition of the Collider interface.
*/

#pragma once

#include "Platform.hpp"
#include "Handle.hpp"
#include "Object.hpp"

namespace Echo
{
  namespace Physics
  {
    /*!
      \brief An interface for basic collider operation.
    */
    class Collider
    {
    public:
      /*!
        \brief Creates a new Collider.
      */
      Collider();

      /*!
        \brief Gets the collision shape of a collider.
      */
      btCollisionShape* GetShape();

      /*!
        \brief Sets the associated game object of a collider.
        \param obj The associated game object.
      */
      void SetGameObject(Core::Handle<Core::Object> obj);

      /*!
        \brief Gets the associated agme object of a collider.
      */
      Core::Handle<Core::Object> GetGameObject();

      /*!
        \brief Sets the scale of a collider.
        \param scale The new scale.
      */
      void SetScale(const glm::vec3& scale);

      /*!
        \brief Destroys a collider.
      */
      virtual ~Collider();

    protected:
      btCollisionShape* shape;         //!< The Bullet collision shape.
      Core::Handle<Core::Object> gobj; //!< The associated game object.
    };
  }
}