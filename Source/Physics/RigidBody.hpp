/*!
  \file RigidBody.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the RigidBody component.
*/
#pragma once

#include "RigidBodyObject.hpp"
#include "PrePhysicsEvent.hpp"
#include "Handle.hpp"
#include "Component.hpp"

namespace Echo
{
  namespace Physics
  {
    class RigidBody : public Core::Component
    {
    public:
      RigidBody();
      void Initialize();
      void Terminate();
      void PrePhysics(const PrePhysicsEvent&);
      void PostPhysics(const PostPhysicsEvent&);

      float Mass;
    
    private:
      Core::Handle<RigidBodyObject> obj;
    };
  }
}