/*!
  \file BoxColliderObject.cpp
  \author Chase A Rayment
  \date 24 May 2017
  \brief Contains the implementation of the BoxColliderObject class.
*/

#include "BoxColliderObject.hpp"
#include "Platform.hpp"
#include "PhysicsUtilities.hpp"

namespace Echo
{
  namespace Physics
  {
    BoxColliderObject::BoxColliderObject() : Collider()
    {
      shape = new btBoxShape(btVector3(0.5f, 0.5f, 0.5f));
    }
  }
}