/*!
  \file PhysicsDebugDrawer.cpp
  \author Chase A Rayment
  \date 28 June 2017
  \brief Contains the implementation of the Physics DebugDrawer class.
*/

#include "PhysicsDebugDrawer.hpp"
#include "GLM/glm.hpp"
#include "PhysicsUtilities.hpp"
#include "Scene.hpp"
#include "Font.hpp"
#include "ResourceManager.hpp"

namespace Echo
{
  namespace Physics
  {
    static int last_debugmode = 0;

    DebugDrawer::DebugDrawer() : debugMode(last_debugmode), tos(), 
      space(nullptr)
    {

    }

    void DebugDrawer::drawLine(const btVector3& from, const btVector3& to, 
      const btVector3& color)
    {
      glm::vec3 c = BulletVector3ToGLMVec3(color);
      space->GetGraphicsScene()->GetDebugDrawer().
        DrawLine(BulletVector3ToGLMVec3(from), BulletVector3ToGLMVec3(to))
        ->SetColor(glm::vec4(c.x, c.y, c.z, 1.0f));
    }

    int DebugDrawer::getDebugMode() const
    { 
      return debugMode; 
    }

    void DebugDrawer::setDebugMode(int d) 
    { 
      debugMode = d;
      last_debugmode = d;
    }

    void DebugDrawer::reportErrorWarning(const char *warningString)
    {
      Core::Log << Core::Priority::Warning << warningString << std::endl;
    }

    void DebugDrawer::draw3dText(const btVector3 &location, 
      const char *textString)
    {
      Core::Handle<Graphics::TextObject> obj = 
        space->GetGraphicsScene()->AddText();
      obj->SetTransform(Graphics::Transform::ObjectToWorld(
        BulletVector3ToGLMVec3(location), glm::quat(), glm::vec3(1, 1, 1)));
      obj->SetFont(Core::ResourceManager<Graphics::Font>::Find("DefaultFont"));
      obj->SetMaterial(Core::ResourceManager<Graphics::Material>::
        Find("DefaultMaterial"));
      obj->SetText(textString);
      tos.push_back(obj);
    }

    void DebugDrawer::drawContactPoint(const btVector3 &PointOnB, 
      const btVector3 &normalOnB, btScalar distance, int lifeTime, 
      const btVector3 &color)
    {
      glm::vec3 pob = BulletVector3ToGLMVec3(PointOnB);
      glm::vec3 nob = BulletVector3ToGLMVec3(normalOnB);
      float d = distance;
      glm::vec3 c = BulletVector3ToGLMVec3(color);

      Graphics::DebugDrawer& dd = space->GetGraphicsScene()->GetDebugDrawer();
      dd.DrawLine(pob, pob+nob*d)->SetColor(glm::vec4(c.x, c.y, c.z, 1.0f));
      dd.DrawLine(pob, pob+nob*0.01f)->SetColor(
        glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
    }

    void DebugDrawer::Update()
    {
      for(auto to : tos)
      {
        space->GetGraphicsScene()->
          RemoveText((Graphics::TextObject*)to);
      }
      tos.clear();
    }

    void DebugDrawer::SetSpace(Core::Handle<Core::Space> s)
    {
      space = s;
    }
  }
}