/*!
  \file PrePhysicsEvent.hpp
  \author Chase A Rayment
  \date 24 May 2017
  \brief Contains the definition o fhte PrePhysicsEvent event.
*/

#pragma once

#include "Messaging.hpp"

namespace Echo
{
  namespace Physics
  {
    /*!
      \brief An event sent immediately before a physics update.
    */
    struct PrePhysicsEvent : public Core::Event
    {

    };

    /*!
      \brief An event sent immediately after a physics update.
    */
    struct PostPhysicsEvent : public Core::Event
    {

    };
  }
}