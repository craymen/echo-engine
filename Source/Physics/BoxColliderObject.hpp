/*!
  \file BoxColliderObject.hpp
  \author Chase A Rayment
  \date 24 May 2017
  \brief Contains the definition of the BoxCollider class.
*/

#pragma once

#include "Collider.hpp"
#include "GLM/glm.hpp"

namespace Echo
{
  namespace Physics
  {
    /*!
      \brief Represents a box collider.
    */
    class BoxColliderObject : public Collider
    {
    public:
      /*!
        \brief Creates a new BoxColliderObject.
      */
      BoxColliderObject();
    };
  }
}