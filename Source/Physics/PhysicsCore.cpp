/*!
  \file PhysicsCore.cpp
  \author Chase A Rayment
  \date 23 May 2017
  \brief Contains the implementation of the Physics system.
*/

#include "PhysicsCore.hpp"
#include <vector>
#include <algorithm>
#include "PrePhysicsEvent.hpp"

namespace Echo
{
  namespace Physics
  {
    static std::vector<Scene*> scenes;

    void Initialize()
    {

    }

    void Update(float dt)
    {
      // Dispatch the pre-physics update event
      PrePhysicsEvent ev;
      Core::DispatchEvent(nullptr, "PrePhysics", ev);

      for(auto s : scenes)
      {
        s->Update(dt);
      }

      // Dispatch the post-physics update event
      PostPhysicsEvent ev2;
      Core::DispatchEvent(nullptr, "PostPhysics", ev2);
    }

    void Terminate()
    {

    }

    Core::Handle<Scene> AddScene()
    {
      scenes.push_back(new Scene);
      return scenes.back();
    }

    void RemoveScene(Core::Handle<Scene> scene)
    {
      delete (void*)scene;
      scenes.erase(std::remove(scenes.begin(), scenes.end(), (void*)scene), 
        scenes.end());
    }
  }
}