/*!
  \file PlaneColliderObject.cpp
  \author Chase A Rayment
  \date 3 June 2017
  \brief Contains the implementation of the PlaneColliderObject class.
*/

#include "PlaneColliderObject.hpp"
#include "Platform.hpp"

namespace Echo
{
  namespace Physics
  {
    PlaneColliderObject::PlaneColliderObject() : Collider()
    {
      shape = new btStaticPlaneShape(btVector3(0.0f, 0.0f, 1.0f), 0.0f);
    }
  }
}