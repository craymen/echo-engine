/*!
  \file PhysicsDebugDrawer.hpp
  \author Chase A Rayment
  \date 28 June 2017
  \brief Contains the definition of the Physics DebugDrawer class.
*/
#pragma once

#include "Platform.hpp"
#include "Handle.hpp"
#include "Space.hpp"
#include "TextObject.hpp"
#include <vector>

namespace Echo
{
  namespace Physics
  {
    class DebugDrawer : public btIDebugDraw
    {  
    public:
      DebugDrawer();

      void drawLine(const btVector3& from, const btVector3& to, 
        const btVector3& color);

      int getDebugMode() const;

      void setDebugMode(int d);

      void reportErrorWarning(const char *warningString);

      void draw3dText(const btVector3 &location, const char *textString);

      void drawContactPoint(const btVector3 &PointOnB, 
        const btVector3 &normalOnB, btScalar distance, int lifeTime, 
        const btVector3 &color);

      void Update();

      void SetSpace(Core::Handle<Core::Space> space);

    private:
      int debugMode;
      std::vector<Core::Handle<Graphics::TextObject> > tos;
      Core::Handle<Core::Space> space;
    };
  }
}