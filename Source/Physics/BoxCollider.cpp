/*!
  \file BoxCollider.cpp
  \author Chase A Rayment
  \date 24 May 2017
  \brief Contains the implementation of the BoxCollider component.
*/

#include "BoxCollider.hpp"
#include "Messaging.hpp"
#include "Transform.hpp"
#include "Object.hpp"
#include "Space.hpp"
#include "PhysicsScene.hpp"
#include "Handle.hpp"

namespace Echo
{
  namespace Physics
  {
    BoxCollider::BoxCollider() : coll(nullptr), Size(1.0f, 1.0f, 1.0f)
    {

    }
  
    void BoxCollider::Initialize()
    {
      // Create collider object
      coll = GetSpace()->GetPhysicsScene()->AddBoxCollider();

      // Register the game object
      coll->SetGameObject(GetOwner());

      Core::ConnectEvent(nullptr, "PrePhysics", this, &BoxCollider::PrePhysics);
    }

    void BoxCollider::PrePhysics(const PrePhysicsEvent&)
    {
      coll->SetScale(GetOwner()->Transform.GetScale() * Size);
    }

    void BoxCollider::Terminate()
    {
      // Delete collider object
      GetSpace()->GetPhysicsScene()->RemoveBoxCollider(coll);

      Component::Terminate();
      Core::Handle<BoxCollider>::InvalidateHandles(this);
    }

    Core::Handle<BoxColliderObject> BoxCollider::GetObject()
    {
      return coll;
    }

  }
}