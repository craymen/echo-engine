/*!
  \file PhysicsScene.cpp
  \author Chase A Rayment
  \date 23 May 2017
  \brief Contains the implementation of the Physics::Scene class.
*/

#include "PhysicsScene.hpp"
#include "Platform.hpp"
#include <algorithm>
#include "PhysicsUtilities.hpp"
#include "Log.hpp"
#include "Object.hpp"
#include "Space.hpp"

namespace Echo
{
  namespace Physics
  {
    Scene::Scene() : broadphase(nullptr), collision_config(nullptr),
      dispatcher(nullptr), solver(nullptr), dynamics_world(nullptr), 
      colliders(), paused(false)
    {
      // Create the broadphase
      broadphase = new btDbvtBroadphase();

      // Create the collision configuration
      collision_config = new btDefaultCollisionConfiguration();

      // Create the collision dispatcher
      dispatcher = new btCollisionDispatcher(collision_config);

      // Register the collision dispatcher
      btGImpactCollisionAlgorithm::registerAlgorithm(dispatcher);

      // Create the solver
      solver = new btSequentialImpulseConstraintSolver;

      // Create the world
      dynamics_world = new btDiscreteDynamicsWorld(dispatcher, broadphase, 
        solver, collision_config);

      // Set gravity
      dynamics_world->setGravity(btVector3(0.0f, -9.8f, 0.0f));

      // Set up the debug drawer
      debug_drawer = new DebugDrawer;
      dynamics_world->setDebugDrawer(debug_drawer);
    }

    void Scene::Update(float dt)
    {
      dynamics_world->stepSimulation(btScalar(paused ? 0.0f : dt), 10);
      debug_drawer->Update();
      dynamics_world->debugDrawWorld();
    }

    Core::Handle<BoxColliderObject> Scene::AddBoxCollider()
    {
      colliders.push_back((Collider*)new BoxColliderObject);
      return (BoxColliderObject*)colliders.back();
    }

    void Scene::RemoveBoxCollider(Core::Handle<BoxColliderObject> coll)
    {
      Core::Handle<BoxColliderObject>::InvalidateHandles((BoxColliderObject*)coll);
      delete (BoxColliderObject*)coll;
      colliders.erase(std::remove(colliders.begin(), colliders.end(), 
        (BoxColliderObject*)coll), colliders.end());
    }

    Core::Handle<PlaneColliderObject> Scene::AddPlaneCollider()
    {
      colliders.push_back((Collider*)new PlaneColliderObject);
      return (PlaneColliderObject*)colliders.back();
    }

    void Scene::RemovePlaneCollider(Core::Handle<PlaneColliderObject> coll)
    {
      Core::Handle<PlaneColliderObject>::InvalidateHandles((PlaneColliderObject*)coll);
      delete (PlaneColliderObject*)coll;
      colliders.erase(std::remove(colliders.begin(), colliders.end(), 
        (PlaneColliderObject*)coll), colliders.end());
    }

    RayCastResult Scene::RayCastFirst(glm::vec3 start, 
      glm::vec3 direction, float maxDistance)
    {
      if(maxDistance < 0.0f)
        maxDistance = 100000.0f;

      btVector3 end = GLMVec3ToBulletVector3(start + 
        glm::normalize(direction) * maxDistance);
      btVector3 s = GLMVec3ToBulletVector3(start);
      btCollisionWorld::ClosestRayResultCallback callback(s, end);
      dynamics_world->rayTest(s, end, callback);

      RayCastResult result;
      result.Hit = false;

      if(callback.hasHit())
      {
        result.Hit = true;
        result.HitPosition = BulletVector3ToGLMVec3(callback.m_hitPointWorld);
        result.HitCollider = callback.m_collisionObject;
        result.HitObject = (Core::Object*)result.HitCollider->getUserPointer(); 
      }

      return result;
    }

    std::vector<RayCastResult> Scene::RayCastAll(glm::vec3 start, 
      glm::vec3 direction, float maxDistance)
    {
      if(maxDistance < 0.0f)
        maxDistance = 100000.0f;

      btVector3 end = GLMVec3ToBulletVector3(start + 
        glm::normalize(direction) * maxDistance);
      btVector3 s = GLMVec3ToBulletVector3(start);
      btCollisionWorld::AllHitsRayResultCallback callback(s, end);
      dynamics_world->rayTest(s, end, callback);

      std::vector<RayCastResult> result;

      if(callback.hasHit())
      {
        for(int i = 0; i < callback.m_collisionObjects.size(); ++i)
        {
          RayCastResult rcr;
          rcr.Hit = true;
          rcr.HitPosition = BulletVector3ToGLMVec3(callback.m_hitPointWorld[i]);
          rcr.HitCollider = callback.m_collisionObjects[i];
          rcr.HitObject = (Core::Object*)rcr.HitCollider->getUserPointer(); 
          result.push_back(rcr);
        }
      }

      return result;
    }

    Core::Handle<RigidBodyObject> Scene::AddRigidBody(btCollisionShape* shape,
      float mass)
    {
      return new RigidBodyObject(dynamics_world, shape, mass);
    }

    void Scene::RemoveRigidBody(Core::Handle<RigidBodyObject> body)
    {
      delete (RigidBodyObject*)body;
    }

    Scene::~Scene()
    {
      delete broadphase;
      delete collision_config;
      delete dispatcher;
      delete solver;
      delete dynamics_world;
      delete debug_drawer;
    }

    DebugDrawer* Scene::GetDebugDrawer()
    {
      return debug_drawer;
    }

    void Scene::SetPaused(bool p)
    {
      paused = p;
    }
  }
}