/*!
  \file PhysicsUtilities.hpp
  \author Chase A Rayment
  \date 24 May 2017
  \brief Contains the definition of various physics utility functions.
*/
#pragma once

#include "Platform.hpp"
#include "GLM/glm.hpp"
#include "GLM/gtc/type_ptr.hpp"

namespace Echo
{
  namespace Physics
  {
    /*!
      \brief Converts a glm::mat4 to a btTransform.
      \param transform The GLM matrix to convert.
    */
    inline btTransform GLMMat4ToBulletTransform(const glm::mat4& transform)
    {
      const float* data = glm::value_ptr(transform);
      btTransform bulletTransform;
      bulletTransform.setFromOpenGLMatrix(data);
      return bulletTransform;
    }

    /*!
      \brief Converts a btTransform to a glm::mat4.
      \param transform The btTransform to convert.
    */
    inline glm::mat4 BulletTransformToGLMMat4(const btTransform& t)
    {
      // Credit to drleviathan on the Bullet forums:
      // http://bulletphysics.org/Bullet/phpBB3/viewtopic.php?t=11462
      glm::mat4 m;
      const btMatrix3x3& basis = t.getBasis();
      // rotation
      for (int r = 0; r < 3; r++)
      {
          for (int c = 0; c < 3; c++)
          {
              m[c][r] = basis[r][c];
          }
      }
      // traslation
      btVector3 origin = t.getOrigin();
      m[3][0] = origin.getX();
      m[3][1] = origin.getY();
      m[3][2] = origin.getZ();
      // unit scale
      m[0][3] = 0.0f;
      m[1][3] = 0.0f;
      m[2][3] = 0.0f;
      m[3][3] = 1.0f;
      return m;
    }

    /*!
      \brief Converts a glm::vec3 to a btVector3.
      \param vec The GLM vector to convert.
    */
    inline btVector3 GLMVec3ToBulletVector3(const glm::vec3& vec)
    {
      btVector3 result;
      result.setX(vec.x);
      result.setY(vec.y);
      result.setZ(vec.z);
      return result;
    }

    /*!
      \brief Converts a btVector3 to a glm::vec3.
      \param vec The btVector3 to convert.
    */
    inline glm::vec3 BulletVector3ToGLMVec3(const btVector3& vec)
    {
      glm::vec3 result;
      result.x = vec.getX();
      result.y = vec.getY();
      result.z = vec.getZ();
      return result;
    }
  }
}