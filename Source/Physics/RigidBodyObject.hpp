/*!
  \file RigidBodyObject.hpp
  \author Chase A Rayment
  \date 28 June 2017
  \brief Contains the definition of the RigidBodyObject class.
*/
#pragma once

#include "Platform.hpp"
#include "GLM/glm.hpp"

namespace Echo
{
  namespace Physics
  {
    class RigidBodyObject
    {
    public:
      RigidBodyObject(btDiscreteDynamicsWorld* world, btCollisionShape* shape,
        float mass);
      ~RigidBodyObject();

      void SetCollisionShape(btCollisionShape* shape);
      btCollisionShape* GetCollisionShape() const;

      void SetMass(float mass);
      float GetMass() const;

      btRigidBody* GetRigidBody();

      void SetTransform(const glm::mat4& transform);
      glm::mat4 GetTransform();

      void SetUserPointer(void* ptr);

      void* operator new(size_t i)
      {
        return _mm_malloc(i,16);
      }

      void operator delete(void* p)
      {
        _mm_free(p);
      }

    private:
      btRigidBody* body;
      btDefaultMotionState* motion_state;
      btDiscreteDynamicsWorld* world;
      btCollisionShape* shape;
      float mass;
      btTransform transform;
      bool dirty;
    };
  }
}