/*!
  \file ResourceManager.hpp
  \author Chase A Rayment
  \date 1 June 2017
  \brief Contains the definition of the ResourceManager class.
*/

#pragma once

#include "Handle.hpp"
#include <unordered_map>

namespace Echo
{
  namespace Core
  {
    /*!
      \brief Handles loading and management of resources.
    */
    template <typename T>
    class ResourceManager
    {
    public:
      /*!
        \brief Creates a new resource with the given name.
        \param name The name to give the new resource.
        \returns The newly created resource.
      */
      static Handle<T> Create(const std::string& name);

      /*!
        \brief Loads a new resource.
        \param name The name to give the resource.
        \param filename The filename to load from.
        \returns The newly loaded resource.
      */
      static Handle<T> Load(const std::string& name, 
        const std::string& filename);

      /*!
        \brief Loads all resources in a given folder.
        \param root The root folder.
        \param extension An optional extension to match.
      */
      static void LoadAll(const std::string& root, 
        const std::string& extension = "");

      /*!
        \brief Adds an existing resource to the manager.
        \param name The name to give the resource.
        \param resource The resource to add.
      */
      static void Add(const std::string& name, Handle<T> resource);

      /*!
        \brief Finds a resource by name.
        \param name The resource to find.
        \returns The found resource if it exists, otherwise returns
          an invalid handle.
      */
      static Handle<T> Find(const std::string& name);

      /*!
        \brief Unloads all resources of this type.
      */
      static void UnloadAll();

      /*!
        \brief Gets all resources of this type.
      */
      static const std::unordered_map<std::string, T*>& GetAll();

      /*!
        \brief Checks for any updates to the resources of this type and updates
          them if necessary.
      */
      static void Update();

    private:
        // Adds the given resource with the given name
      static void add(const std::string& name, T* resource);
    
        // All resources of this type
      static std::unordered_map<std::string, T*> resources;

        // All the directories to load from
      static std::vector<std::string> directories;

        // All the associated extensions
      static std::vector<std::string> extensions;
    };

    /*!
      \brief Loads all resources of all types.
    */
    void LoadAllResources();

    /*!
      \brief Updates all resources of all types.
    */
    void UpdateAllResources();

    /*!
      \brief Unloads all resources of all types.
    */
    void UnloadAllResources();
  }
}

#include "ResourceManager.inl"
