/*!
  \file Member.cpp
  \author Chase A Rayment
  \date 5 April 2017
  \brief Contains the implementation of the Member class.
*/

#include "Member.hpp"

namespace Echo
{
  namespace Core
  {
    Member::Member() : type(nullptr), name("") { }

    Member::Member(const TypeData* t, const std::string& n, unsigned o) : 
      type(t), name(n), offset(o)
    {

    }
    
    const TypeData* Member::GetType() const
    {
      return type;
    }

    const std::string& Member::GetName() const
    {
      return name;
    }

    unsigned Member::GetOffset() const
    {
      return offset;
    }
  }
}