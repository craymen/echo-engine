/*!
  \file Log.hpp
  \author Chase A Rayment
  \date 5 April 2017
  \brief Contains the definition of the Logger class.
*/
#pragma once

#include <iostream>
#include <fstream>
#include <string>

#ifdef _WIN32
  #include <windows.h>
#endif

namespace Echo
{
  namespace Core
  {
    /*!
      \brief The priority of a message. Errors are critical issues that cannot
        be recovered from. Warnings are issues that may cause issues later,
        but the game can still keep running. (at least for the short term)
        Infos are for debug text or general status updates.
    */
    enum class Priority { Error, Warning, Info };

    /*!
      \brief Manages writing output to file and to console.
    */
    class Logger
    {
    public:
      /*!
        \brief Constructs a new Logger.
      */
      Logger();
      
      /*!
        \brief Initializes a Logger.
        \param filename The filename to write to.
      */
      void Init(const std::string& filename);

      /*!
        \brief Terminates a Logger.
      */
      void Terminate();

      /*!
        \brief Prints a value to log. Uses the currently set priority.
        \param rhs The value to print.
      */
      template <typename T>
      Logger& operator<<(const T& rhs);

      /*!
        \brief Prints std::endl to log.
      */
      Logger& operator<<(std::ostream& (*pf)(std::ostream&));

      /*!
        \brief Sets the priority for the logger to use. Priority is reset on
          std::endl.
      */
      Logger& operator<<(const Priority& priority);

    private:
      std::ofstream file_log;   // File log handle
      Priority curr_priority;   // The current priority

      void set_console_color(); // Sets the color of the console to match the
                                // current priority

    #ifdef _WIN32               // Windows console stuff (so we can set color)
      HANDLE console_handle;    // Handle to the console

        // Information about the initial state of the console
      CONSOLE_SCREEN_BUFFER_INFO init_console_info;
    #endif
    };

    /*!
      \brief A globally-accessable Logger instance.
    */
    extern Logger Log;
  };
}

#include "Log.inl"
