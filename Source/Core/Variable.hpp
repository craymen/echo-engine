/*!
  \file Variable.hpp
  \author Chase A Rayment
  \date 5 April 2017
  \brief Contains the definition of the Variable class.
*/
#pragma once
#include "json/json.h"

namespace Echo
{
  namespace Core
  {
    class TypeData;

    /*!
      \brief Holds generic data and information about its type.
    */
    class Variable
    {
    public:
      /*!
        \brief Creates a new Variable.
        \param type The type of the Variable.
        \param data The data to fill the Variable with.
      */
      Variable(const TypeData* type, void* data);

      /*!
        \brief Gets the raw data from the Variable.
      */
      void* GetData();
      
      /*!
        \overload
      */
      const void* GetData() const;

      /*!
        \brief Gets the type data from the Variable.
      */
      const TypeData* GetType() const;

      /*!
        \brief Serializes the data within this Variable to a Json::Value.
        \param value The value to store the serialized data into.
      */
      void Serialize(Json::Value& value);

      /*!
        \brief Deserializes the data from a Json::Value.
        \param value The value to read from.
      */
      void Deserialize(const Json::Value& value);

      /*!
        \brief Gets a member of this Variable.
        \param name The name of the member.
      */
      void* GetMember(const std::string& name);

      /*!
        \overload
      */
      const void* GetMember(const std::string& name) const;

    private:
      void* data;           // The data this Variable points to.
      const TypeData* type; // The type of the data.
    };
  }
}