/*!
  \file Log.inl
  \author Chase A Rayment
  \date 5 April 2017
  \brief Contains the template implementations for the Logger class.
*/

#include "DevConsole.hpp"
#include <sstream>

namespace Echo
{
  namespace Core
  {
    template <typename T>
    Logger& Logger::operator<<(const T& rhs)
    {
      // Print the value
      std::cout << rhs;
      if(file_log.is_open())
        file_log << rhs;

      std::stringstream ss;
      ss << rhs;
      DevConsole::PushText(ss.str());

      return *this;
    }
  }
}