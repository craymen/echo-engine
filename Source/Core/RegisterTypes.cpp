/*!
  \file RegisterTypes.cpp
  \author Chase A Rayment
  \date 11 April 2017
  \brief Contains the implementation of the RegisterTypes function.
*/

#include "TypeData.hpp"
#include "Object.hpp"
#include "Model.hpp"
#include "Camera.hpp"
#include "Transform.hpp"
#include "Material.hpp"
#include "PointLight.hpp"
#include "BoxCollider.hpp"
#include "Tile.hpp"
#include "Texture.hpp"
#include "Mesh.hpp"
#include "Archetype.hpp"
#include "Shader.hpp"
#include "Board.hpp"
#include "Unit.hpp"
#include "PlayerCommander.hpp"
#include "CameraController.hpp"
#include "SpriteText.hpp"
#include "RigidBody.hpp"
#include "Listener.hpp"
#include "SoundEmitter.hpp"
#include "Commando.hpp"
#include "HUDManager.hpp"
#include "AbilityButton.hpp"
#include "Billboard.hpp"
#include "EndTurnButton.hpp"
#include "EnemyCommander.hpp"
#include "EnemyTurnText.hpp"
#include "MeleeDroid.hpp"
#include "HUDFollowObject.hpp"
#include "StatusDisplay.hpp"
#include "HealthDisplay.hpp"
#include "Cubemap.hpp"
#include "ReflectionProbe.hpp"
#include "StressTester.hpp"
using namespace Echo::Graphics;
using namespace Echo::Physics;
using namespace Echo::Gameplay;
using namespace Echo::Audio;

// Disable warnings for offsetof on non-POD types. TECHNICALLY not part
// of the standard, however it appears to be working properly under
// MSVC and Clang. (MSVC doesn't even warn for it) If memory corruption
// starts to happen though this might be the root of it.
#ifdef __linux__
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Winvalid-offsetof"
#endif

namespace Echo
{
  namespace Core
  {
    void RegisterTypes()
    {
      REGISTER_TYPE(int);
      REGISTER_TYPE(unsigned);
      REGISTER_TYPE(float);
      REGISTER_TYPE(double);
      REGISTER_TYPE(bool);
      REGISTER_TYPE(std::string);
      REGISTER_TYPE(glm::vec3);
      REGISTER_TYPE(glm::vec4);
      REGISTER_TYPE(Handle<Object>);
      REGISTER_TYPE(Handle<Transform>);
      REGISTER_RESOURCE_HANDLE(Material);
      REGISTER_RESOURCE_HANDLE(Mesh);
      REGISTER_RESOURCE_HANDLE(Texture);

      REGISTER_TYPE(Transform);
      REGISTER_RENAMED_MEMBER(Transform, glm::vec3, position, Position);
      REGISTER_RENAMED_MEMBER(Transform, glm::vec3, angles, Rotation);
      REGISTER_RENAMED_MEMBER(Transform, glm::vec3, scale, Scale);
      REGISTER_RENAMED_MEMBER(Transform, Handle<Transform>, parent, Parent);

      REGISTER_TYPE(Object);
      REGISTER_MEMBER(Object, std::string, ArchetypeName);
      REGISTER_MEMBER(Object, Transform, Transform);

      REGISTER_TYPE(Space);

      Echo::Core::TypeData::AddType<Texture>(Echo::Core::TypeData("Texture", 
        typeid(Texture).hash_code()));

      Echo::Core::TypeData::AddType<Mesh>(Echo::Core::TypeData("Mesh", 
        typeid(Mesh).hash_code()));

      Echo::Core::TypeData::AddType<Archetype>(Echo::Core::TypeData("Archetype", 
        typeid(Archetype).hash_code()));

      Echo::Core::TypeData::AddType<Shader>(Echo::Core::TypeData("Shader", 
        typeid(Shader).hash_code()));

      Echo::Core::TypeData::AddType<Font>(Echo::Core::TypeData("Font", 
        typeid(Font).hash_code()));

      Echo::Core::TypeData::AddType<Cubemap>(Echo::Core::TypeData("Cubemap", 
        typeid(Cubemap).hash_code()));

      REGISTER_TYPE(Material);
      REGISTER_MEMBER(Material, std::string, ShaderName);
      REGISTER_MEMBER(Material, glm::vec4, AlbedoColor);
      REGISTER_MEMBER(Material, float, Roughness);
      REGISTER_MEMBER(Material, float, Metallic);
      REGISTER_MEMBER(Material, Handle<Texture>, AlbedoTexture);
      REGISTER_MEMBER(Material, Handle<Texture>, NormalMap);

      REGISTER_TYPE(ShaderSaveData);
      REGISTER_MEMBER(ShaderSaveData, std::string, VertexShader);
      REGISTER_MEMBER(ShaderSaveData, std::string, FragmentShader);
      REGISTER_MEMBER(ShaderSaveData, std::string, GeometryShader);
      REGISTER_MEMBER(ShaderSaveData, std::string, TessContShader);
      REGISTER_MEMBER(ShaderSaveData, std::string, TessEvalShader);
      REGISTER_MEMBER(ShaderSaveData, std::string, ComputeShader);
      REGISTER_MEMBER(ShaderSaveData, std::string, RenderStage);

      // Manually register Component as it can't be constructed
      Echo::Core::TypeData::AddType<Component>(Echo::Core::TypeData("Component", 
        typeid(Component).hash_code()));

      REGISTER_DERIVED_TYPE(Model, Component);
      REGISTER_MEMBER(Model, std::string, ModelName);
      REGISTER_MEMBER(Model, Handle<Material>, CurrentMaterial);

      REGISTER_DERIVED_TYPE(SpriteText, Component);
      REGISTER_MEMBER(SpriteText, std::string, Text);
      REGISTER_MEMBER(SpriteText, std::string, FontName);
      REGISTER_MEMBER(SpriteText, std::string, MaterialName);
      REGISTER_MEMBER(SpriteText, int, HorizontalCenterMode);
      REGISTER_MEMBER(SpriteText, int, VerticalCenterMode);

      REGISTER_DERIVED_TYPE(Camera, Component);
      REGISTER_MEMBER(Camera, bool, IsOrtho);
      REGISTER_MEMBER(Camera, float, Fov);
      REGISTER_MEMBER(Camera, float, Size);
      REGISTER_MEMBER(Camera, float, NearZ);
      REGISTER_MEMBER(Camera, float, FarZ);

      REGISTER_DERIVED_TYPE(PointLight, Component);
      REGISTER_MEMBER(PointLight, glm::vec3, Color);

      REGISTER_DERIVED_TYPE(BoxCollider, Component);
      REGISTER_MEMBER(BoxCollider, glm::vec3, Size);

      REGISTER_DERIVED_TYPE(RigidBody, Component);
      REGISTER_MEMBER(RigidBody, float, Mass);

      REGISTER_DERIVED_TYPE(Listener, Component);
      REGISTER_MEMBER(Listener, bool, Active);

      REGISTER_DERIVED_TYPE(SoundEmitter, Component);

      REGISTER_DERIVED_TYPE(Tile, Component);
      ADD_COMPONENT_DEPENDENCY(Tile, BoxCollider);

      REGISTER_DERIVED_TYPE(Board, Component);

      REGISTER_DERIVED_TYPE(Unit, Component);
      REGISTER_MEMBER(Unit, bool, IsPlayerUnit);
      REGISTER_MEMBER(Unit, int, MaxHealth);
      REGISTER_MEMBER(Unit, int, Speed);
      ADD_COMPONENT_DEPENDENCY(Unit, BoxCollider);

      REGISTER_DERIVED_TYPE(PlayerCommander, Component);

      REGISTER_DERIVED_TYPE(CameraController, Component); 
      ADD_COMPONENT_DEPENDENCY(CameraController, Camera);

      REGISTER_DERIVED_TYPE(HUDManager, Component);

      REGISTER_DERIVED_TYPE(Commando, Component);

      REGISTER_DERIVED_TYPE(AbilityButton, Component);

      REGISTER_DERIVED_TYPE(Billboard, Component);

      REGISTER_DERIVED_TYPE(EndTurnButton, Component);

      REGISTER_DERIVED_TYPE(EnemyCommander, Component);

      REGISTER_DERIVED_TYPE(EnemyTurnText, Component);

      REGISTER_DERIVED_TYPE(MeleeDroid, Component);

      REGISTER_DERIVED_TYPE(HUDFollowObject, Component);
      REGISTER_MEMBER(HUDFollowObject, glm::vec3, Offset);

      REGISTER_DERIVED_TYPE(StatusDisplay, Component);

      REGISTER_DERIVED_TYPE(HealthDisplay, Component);

      REGISTER_DERIVED_TYPE(ReflectionProbe, Component);

	  REGISTER_DERIVED_TYPE(StressTester, Component);
    }
  }
}

#ifdef __linux__
  #pragma clang diagnostic pop
#endif
