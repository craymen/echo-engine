/*!
  \file Space.hpp
  \author Chase A Rayment
  \date 11 April 2017
  \brief Contains the definition of the Space class.
*/
#pragma once

#include "Object.hpp"
#include <string>
#include "Messaging.hpp"
#include "Scene.hpp"
#include "Handle.hpp"
#include "AudioScene.hpp"

namespace Echo
{
  namespace Physics { class Scene; }
  namespace Core
  {
    namespace Game { void Run(); }
    namespace Game { void Terminate(); }
    
    /*!
      \brief Contains game objects.
    */
    class Space
    {
      friend void Game::Run();
      friend void Game::Terminate();
      friend class Object;
    public:
      /*!
        \brief The maximum number of objects a Space can hold.
      */
      const static unsigned MaxObjects = 500;

      /*!
        \brief Constructs a new Space.
      */
      Space();

      /*!
        \brief Destroys a Space.
      */
      ~Space();

      /*!
        \brief Initializes this Space.
      */
      void Initialize();

      /*!
        \brief Updates this Space.
      */
      void Update(float dt);

      /*!
        \brief Terminates this Space.
      */
      void Terminate();

      /*!
        \brief Gets this Space's name.
      */
      const std::string& GetName() const;

      /*!
        \brief Sets this Space's name.
      */
      void SetName(const std::string& name);

      /*!
        \brief Adds an empty Object to this Space.
      */
      Handle<Object> AddObject();

      /*!
        \brief Finds an Object by name.
      */
      Handle<Object> FindObjectByName(const std::string& name);

      /*!
        \brief Gets all objects in the space.
      */
      std::vector<Handle<Object> > GetAllObjects();

      /*!
        \brief Gets the graphics scene of this space.
      */
      Graphics::Scene* GetGraphicsScene();

      /*!
        \brief Gets the physics scene of this space.
      */
      Core::Handle<Echo::Physics::Scene> GetPhysicsScene();

      /*!
        \brief Loads a level.
      */
      void LoadLevel(const std::string& name);

      /*!
        \brief Consolidates all  objects so they are contiguous in memory.
      */
      void Consolidate();

      /*!
        \brief Sets if this space is paused.
        \param paused Whether or not the space is paused.
      */
      void SetPaused(bool paused);

      /*!
        \brief Gets whether or not this space is paused.
      */
      bool GetPaused() const;

      /*!
        \brief Gets whether or not this space is active.
      */
      bool GetActive() const;

      /*!
        \brief Gets the ID of this space.
      */
      unsigned GetID() const;

      /*!
        \brief Gets the audio scene.
      */
      Audio::Scene& GetAudioScene();

      /*!
        \brief Gets the name of the last loaded level.
      */
      const std::string& GetLastLoadedLevelName() const;

      /*!
        \brief Creates an archetype at the given position.
        \param archetypeName The name of the archetype to spawn.
        \param pos The position of the archetype to spawn.
      */
      Handle<Object> CreateAtPosition(std::string archetypeName, 
        const glm::vec3& pos);

      /*!
        \brief Gets the object currently hovered by the mouse.
      */
      Handle<Object> GetHoveredObject() const;

    private:
      bool active;                   // Whether or not this space is active.
      std::string name;              // The name of the space.
      Object objects[MaxObjects];    // The objects in the space.
      Graphics::Scene* scene;        // The graphics scene for this space.
        // The physics scene for this space.
      Core::Handle<Echo::Physics::Scene> physics_scene; 
        // The currently hovered object.
      Core::Handle<Core::Object> hover_obj;
        // Stores the space currently being loaded.
      static Handle<Space> curr_loading_space;
      bool paused;                   // Whether or not the space is paused.
      Audio::Scene audio_scene;      // The audio scene.
      std::string filename;          // The filename of the last loaded level.

      template <typename T>
      friend void SerializeType(Variable var, Json::Value& val);
      
      template <typename T>
      friend void DeserializeType(const Json::Value& val, Variable& var);
    };

    /*!
      \brief Enumerates the available spaces.
    */
    enum Spaces
    {
      SPACE_GAMEPLAY,
      SPACE_HUD,
      SPACE_DEBUG,
      SPACE_COUNT
    };

    /*!
      \brief All the available spaces.
    */
    extern Space Spaces[SPACE_COUNT];
  }
}