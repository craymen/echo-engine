/*!
  \file ResourceManager.cpp
  \author Chase A Rayment
  \date 1 June 2017
  \brief Contains the implementation of the ResourceManager system.
*/
#include "ResourceManager.hpp"
#include "FileSystem.hpp"
#include "Texture.hpp"
#include "Mesh.hpp"
#include "Archetype.hpp"
#include "Material.hpp"
#include "Shader.hpp"
#include "Font.hpp"
#include "Cubemap.hpp"

namespace Echo
{
  namespace Core
  {
    void LoadAllResources()
    {
      ResourceManager<Graphics::Texture>::LoadAll(
        FileSystem::GetGameDir(FileSystem::Dir::Textures));
      ResourceManager<Graphics::Cubemap>::LoadAll(
        FileSystem::GetGameDir(FileSystem::Dir::Cubemaps), "cmap");
      ResourceManager<Graphics::Mesh>::LoadAll(
        FileSystem::GetGameDir(FileSystem::Dir::Models), "obj");
      ResourceManager<Graphics::Material>::LoadAll(
        FileSystem::GetGameDir(FileSystem::Dir::Materials), "mat");
      ResourceManager<Archetype>::LoadAll(FileSystem::GetGameDir(
          FileSystem::Dir::Archetypes), "json");
      ResourceManager<Graphics::Shader>::LoadAll(FileSystem::GetGameDir(
          FileSystem::Dir::Shaders), "shad");
      ResourceManager<Graphics::Font>::LoadAll(FileSystem::GetGameDir(
          FileSystem::Dir::Fonts), "otf");
      ResourceManager<Graphics::Font>::LoadAll(FileSystem::GetGameDir(
          FileSystem::Dir::Fonts), "ttf");
    }

    void UpdateAllResources()
    {
      ResourceManager<Graphics::Texture>::Update();
      ResourceManager<Graphics::Cubemap>::Update();
      ResourceManager<Graphics::Mesh>::Update();
      ResourceManager<Graphics::Material>::Update();
      ResourceManager<Graphics::Shader>::Update();
      ResourceManager<Graphics::Font>::Update();
      ResourceManager<Archetype>::Update();
    }

    void UnloadAllResources()
    {
      ResourceManager<Graphics::Texture>::UnloadAll();
      ResourceManager<Graphics::Cubemap>::UnloadAll();
      ResourceManager<Graphics::Mesh>::UnloadAll();
      ResourceManager<Graphics::Material>::UnloadAll();
      ResourceManager<Archetype>::UnloadAll();
      ResourceManager<Graphics::Shader>::UnloadAll();
      ResourceManager<Graphics::Font>::UnloadAll();
      Graphics::Font::Shutdown();
    }
  }
}