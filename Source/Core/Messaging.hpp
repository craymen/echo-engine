/*!
  \file Messaging.hpp
  \author Chase A Rayment
  \date 12 April 2017
  \brief Contains the definition of the Messaging system.
*/
#pragma once

#include <string>
#include "Handle.hpp"

#ifdef _WIN32
  #pragma warning(disable:4503) // Kill "decorated name is too long" warnings
  #pragma warning(push)
#endif

namespace Echo
{
  namespace Core
  {
    /*!
      \brief A container for data about a message.
    */
    struct Event
    {
      virtual ~Event() { };
    };

    /*!
      \brief An event called every frame the game runs.
    */
    struct UpdateEvent : public Event
    {
      float Dt; //!< The time passed last frame.

      /*!
        \brief Creates a new UpdateEvent.
        \param d The time passed last frame.
      */
      UpdateEvent(float d) : Dt(d) { }
    };

    /*!
      \brief Connects a listener to an event.
      \param scope The scope of the event to handle. (nullptr = global)
      \param channel The event name to connect to.
      \param listener The listener to connect.
      \param handler The event handler function.
    */
    template <typename HandleType, typename ListenerType, typename EventType>
    void ConnectEvent(Handle<HandleType> scope, const std::string& channel, 
      ListenerType* listener, void (ListenerType::*handler)(EventType&));

    /*! \overload */
    template <typename ListenerType, typename EventType>
    void ConnectEvent(void* scope, const std::string& channel, 
      ListenerType* listener, void (ListenerType::*handler)(EventType&));

    /*!
      \brief Disconnects a listener from an event.
      \param scope The scope of the event to disconnect. (nullptr = global)
      \param channel The event name to disconnect.
      \param listener The listener to disconnect.
    */
    template <typename HandleType>
    void DisconnectEvent(Handle<HandleType> scope, const std::string& channel, 
      void* listener);

    /*! \overload */
    void DisconnectEvent(void* scope, const std::string& channel, 
      void* listener);

    /*!
      \brief Dispatches an event.
      \param scope The scope of the event to dispatch.
      \param channel The channel name of the event to dispatch.
      \param event The event to dispatch.
    */
    template <typename EventType>
    void DispatchEvent(void* scope, const std::string& channel, 
      EventType& event);
  }
}

#include "Messaging.inl"

#ifdef _WIN32
  #pragma warning(pop)
#endif