/*!
  \file Object.hpp
  \author Chase A Rayment
  \date 7 April 2017
  \brief Contains the definition of the Object class.
*/
#pragma once

#include <unordered_map>
#include "Handle.hpp"
#include <string>
#include "Variable.hpp"
#include "json/json.h"
#include "GLM/glm.hpp"
#include "GLM/gtc/quaternion.hpp"
#include "Transform.hpp"
#include "Actions.hpp"

namespace Echo
{
  namespace Core
  {
    class Space;
    class Component;
    class TypeData;

    /*!
      \brief A game object containing Components within a Space.
    */
    class Object
    {
      friend class Space;
    public:
      /*!
        \brief Creates a new Object.
      */
      Object();

      /*!
        \brief Deletes an Object.
      */
      ~Object();
      
      /*!
        \brief Initializes an Object and makes it active.
      */
      void Initialize();

      /*!
        \brief Terminates an Object and makes it inactive.
      */
      void Terminate();

      /*!
        \brief Gets the name of this Object.
      */
      const std::string& GetName() const;

      /*!
        \brief Sets the name of this Object.
      */
      void SetName(const std::string& name);

      /*!
        \brief Adds a component to this Object.
        \param name The TypeData name of the component.
      */
      Handle<Component> AddComponent(const std::string& name);

      /*!
        \brief Gets a component on this Object.
        \param name The TypeData name of the component.
      */
      Handle<Component> GetComponent(const std::string& name);

      /*!
        \brief Removes a component on this Object.
        \param name The TypeData name of the component.
      */
      void RemoveComponent(const std::string& name);
      
      /*!
        \brief Adds a component to this Object.
      */
      template <typename T>
      Handle<T> AddComponent();

      /*!
        \brief Gets a component on this Object.
      */
      template <typename T>
      Handle<T> GetComponent();

      /*!
        \brief Removes a component on this Object.
      */
      template <typename T>
      void RemoveComponent();

      /*!
        \brief Gets the Space this Object belongs to.
      */
      Handle<Space> GetSpace() const;

      /*!
        \brief Gets a hash of all type hashes of components to their respective
          Component pointers.
      */
      const std::unordered_map<size_t, Component*>& GetAllComponents();

      /*!
        \brief Removes all components from this Object.
      */
      void RemoveAllComponents();

      /*!
        \brief Initializes all components but does not initialize the Object.
      */
      void Reinitialize();

      /*!
        \brief Clones this object into another object in the same space.
      */
      Handle<Object> Clone();

      /*!
        \brief Debug draws all components of this object.
      */
      void DebugDraw();

      /*!
        \brief Gets this object's ID.
      */
      unsigned GetID() const;

      /*!
        \brief Prints data about this Object's components.
      */
      void PrintComponents() const;

      Graphics::Transform Transform;          //!< The object's transform.
      Handle<Object> Parent;                  //!< The object's parent.
      Actions::ActionList* Actions;           //!< The object's active actions.
      std::string ArchetypeName;              //!< The object's archetype name.

    private:
      bool active;         // Whether or not this Object is active.
      std::string name;    // The name of this Object.
                           // All the components this Object contains.                     
      std::unordered_map<size_t, Component*> components;
      Handle<Space> space; // The space this Object is in.
                           // Adds a component to this Object
      Component* add_component(const TypeData* td, bool suppress_dupe = false);
                           // Gets a component from this Object
      Component* get_component(const TypeData* td);
                           // Removes a component from this Object
      void remove_component(const TypeData* td);

      template <typename T>
      friend void SerializeType(Variable var, Json::Value& val);

      template <typename T>
      friend void DeserializeType(const Json::Value& val, Variable& var);
    };
  }
}

#include "Object.inl"
