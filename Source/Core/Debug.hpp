/*!
  \file Debug.hpp
  \author Chase A Rayment
  \date 5 April 2017
  \brief Contains useful debugging macros.
*/

#pragma once
#include "Log.hpp"
#include <signal.h>


/*!
  \def DEBUG_BREAK()
  \brief Stops execution of the program and allows a debugger to set a
    breakpoint.
*/
#ifdef DEBUG
  #ifdef __linux__
    #define DEBUG_BREAK() do { raise(SIGTRAP); } while(0)
  #endif
  #ifdef _WIN32
    #define DEBUG_BREAK() do { __debugbreak(); } while(0)
  #endif
#else
  #define DEBUG_BREAK() do {} while(0)
#endif

/*!
  \def ASSERT(condition) 
  \brief Stops execution if a condition is not met.
*/
#ifdef DEBUG
  #define ASSERT(condition)                                                    \
  do                                                                           \
  {                                                                            \
    if(!(condition))                                                           \
    {                                                                          \
      Echo::Core::Log << Echo::Core::Priority::Error << "Debug assertion \""   \
        << #condition << "\" FAILED in " << __func__ << ", line " << __LINE__  \
        << ", file " << __FILE__ << std::endl;                                 \
      DEBUG_BREAK();                                                          \
    }                                                                          \
  } while(0)                                                                   
#else
  #define ASSERT(condition) do{} while(0)
#endif
