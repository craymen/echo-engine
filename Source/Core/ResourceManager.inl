/*!
  \file ResourceManager.inl
  \author Chase A Rayment
  \date 1 June 2017
  \brief Contains the template implementation of the ResourceManager
    class.
*/

#include "Log.hpp"
#include "TypeData.hpp"
#include "FileSystem.hpp"

namespace Echo
{
  namespace Core
  {
    template <typename T>
    std::unordered_map<std::string, T*> ResourceManager<T>::resources;

    template <typename T>
    std::vector<std::string> ResourceManager<T>::directories;

    template <typename T>
    std::vector<std::string> ResourceManager<T>::extensions;

    template <typename T>
    Handle<T> ResourceManager<T>::Create(const std::string& name)
    {
      T* newResource = new T;
      add(name, newResource);
      return Handle<T>(newResource);
    }

    template <typename T>
    Handle<T> ResourceManager<T>::Load(const std::string& name, 
      const std::string& filename)
    {
      Log << "Loading " << name << " from " << filename << std::endl;
      T* newResource = new T(filename);
      if (!newResource)
      {
        Log << Priority::Error << "Could not allocate memory for resource "
          << filename << std::endl;
      }
      add(name, newResource);
      return Handle<T>(newResource);
    }

    template <typename T>
    void ResourceManager<T>::Add(const std::string& name, Handle<T> resource)
    {
      add(name, (T*)resource);
    }

    template <typename T>
    void ResourceManager<T>::LoadAll(const std::string& root, 
      const std::string& extension)
    {
      if(std::find(directories.begin(), directories.end(), root) 
        == directories.end())
        directories.push_back(root);
      if(extension != "" && std::find(extensions.begin(), extensions.end(), extension) 
        == extensions.end())
        extensions.push_back(extension);

      for(auto item : FileSystem::GetAllFiles(root))
      {
        size_t epos = item.find_last_of(".");
        if(extension == "" || item.substr(item.size() - extension.size() - 1, 
          extension.size() + 1) == std::string(".") + extension)
        {
          size_t npos = item.find_last_of("/");
          Load(item.substr(npos + 1, epos - npos - 1), 
            item);
        }
      }
    }

    template <typename T>
    Handle<T> ResourceManager<T>::Find(const std::string& name)
    {
      if(resources.find(name) == resources.end())
      {
        Log << Priority::Warning << "Tried to find a " <<
          TypeData::FindType<T>()->GetName() << " of name " << name <<
          " but it doesn't exist!" << std::endl;
        return T::GetDefault();
      }

      return Handle<T>(resources[name]);
    }

    template <typename T>
    void ResourceManager<T>::add(const std::string& name, T* resource)
    {
      // Ensure that the resource isn't already added
      if(resources.find(name) != resources.end())
      {
        Log << Priority::Warning << "Tried to add a " << 
          TypeData::FindType<T>()->GetName() << " of name " << name <<
          " but it already exists!" << std::endl;
        return;
      }

      // Add the resource to the list
      resources[name] = resource;
      resource->name = name;
    }

    template <typename T>
    void ResourceManager<T>::UnloadAll()
    {
      for(auto r : resources)
        delete r.second;
      resources.clear();
    }

    template <typename T>
    const std::unordered_map<std::string, T*>& ResourceManager<T>::GetAll()
    {
      return resources;
    }

    template <typename T>
    void ResourceManager<T>::Update()
    {
      // Look for all updated files
      for(auto r : resources)
      {
        if(!r.second->reload_on_write)
          continue;
        FILETIME ft = FileSystem::GetLastFileWrite(r.second->filename);
        if(CompareFileTime(&r.second->last_write, &ft) == -1)
        {
          std::string f = r.second->filename;
          delete r.second;
          T* newResource = new T(f);
          Handle<T>::UpdateHandles(r.second, newResource);
          resources[r.first] = newResource;
        }
      }

      // Look for added/removed files
      std::vector<std::string> files;
      for(auto root : directories)
      {
        for(auto file : FileSystem::GetAllFiles(root))
        {
          if(extensions.size() > 0)
          {
            for(auto ex : extensions)
            {
              if(file.substr(file.size() - ex.size() - 1, 
                ex.size() + 1) == std::string(".") + ex)
              {
                files.push_back(file);
              }
            }
          }
          else
            files.push_back(file);
        }
      }

      std::vector<std::string> added_files;
      std::vector<std::string> removed_files;
      for(auto file : files)
      {
        bool found = false;
        for(auto r : resources)
        {
          if(r.second->filename == file)
          {
            found = true;
            break;
          }
        }
        if(!found)
          added_files.push_back(file);
      }

      for(auto r : resources)
      {
        bool found = false;
        for(auto file : files)
        {
          if(r.second->filename == file)
          {
            found = true;
            break;
          }
        }
        if(!found && r.second->reload_on_write)
          removed_files.push_back(r.first);
      }

      for(auto f : added_files)
      {
        size_t epos = f.find_last_of(".");
        size_t npos = f.find_last_of("/");
        Load(f.substr(npos + 1, epos - npos - 1), f);
      }

      for(auto f : removed_files)
      {
        Handle<T>::InvalidateHandles(resources[f]);
        resources.erase(f);
      }
    }
  }
}
