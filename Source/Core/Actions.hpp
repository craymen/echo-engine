/*!
  \file Actions.hpp
  \author Chase A Rayment
  \date 21 May 2017
  \brief Contains the definition of the Actions system.
*/

#pragma once
#include <vector>

namespace Echo
{
  namespace Core
  {
    namespace Actions
    {
      /*!
        \brief An interface for an action in an ActionList.
      */
      class Action
      {
      public:
        /*!
          \brief The different states an Action can be in.
        */
        enum class Status { NotStarted, InProgress, Complete };

        /*!
          \brief Starts an Action.
        */
        virtual void Start() { };

        /*!
          \brief Updates an Action. Call every frame.
        */
        virtual void Update(float) { };

        /*!
          \brief Gets the status of an Action.
        */
        virtual Status GetStatus() = 0;

        /*!
          \brief Destroys an Action.
        */
        virtual ~Action() { };
      };

      /*!
        \brief An Action that calls a function on completion.
      */
      class ActionCall : public Action
      {
      public:
        /*!
          \brief A callback function usable with an ActionCall.
        */
        typedef void (*CallFunc)(void*);

        /*!
          \brief Constructs an ActionCall.
          \param callback The function to call.
          \param data The user data to call the callback with.
        */
        ActionCall(void(*callback)(void*), void* data);

        /*!
          \brief Starts the action and immediately calls its callback function.
        */
        void Start();

        /*!
          \brief Gets the status.
        */
        Status GetStatus();
      
      private:
        CallFunc callback; // The callback function.
        void* user_data;   // The user data.
        float fired;       // Whether or not this event has been fired.
      };

      /*!
        \brief Interpolates a value over time.
      */
      template <typename T>
      class ActionProperty : public Action
      {
      public:
        /*!
          \brief A function to ease between two values.
        */
        typedef T (*EaseFunc)(const T&, const T&, float);
        
        /*!
          \brief Creates a new ActionProperty.
          \param data A pointer to the data to interpolate.
          \param final_val The final value to interpolate to.
          \param time The time to interpolate over.
          \param func The ease function to use.
        */
        ActionProperty(T* data, const T& final_val, float time, EaseFunc func);
        
        /*!
          \brief Starts an ActionProperty.
        */
        void Start();

        /*!
          \brief Updates an ActionProperty.
          \param dt The frame time.
        */
        void Update(float dt);

        /*!
          \brief Gets the status.
        */
        Status GetStatus();

      private:
        T* data;                     // The data to interpolate.
        T initial_val, final_val;    // The initial/final values.
        float total_time, curr_time; // The total/current times.
        EaseFunc ease;               // The ease function to use.
        Status status;               // The current status.
      };

      /*!
        \brief Eases linearly between two values.
        \param start The start value.
        \param end The end value.
        \param t The interpolation fraction. 0 = start, 1 = end, 0.5 = halfway
        \returns The interpolated value.
      */
      template <typename T>
      T EaseLinear(const T& start, const T& end, float t);

      /*!
        \brief Delays an ActionList.
      */
      class ActionDelay : public Action
      {
      public:
        /*!
          \brief Creates a new ActionDelay.
        */
        ActionDelay(float delay);

        /*!
          \brief Starts an ActionDelay.
        */
        void Start();

        /*!
          \brief Updates an ActionDelay.
          \param dt The frame time.
        */
        void Update(float dt);

        /*!
          \brief Gets the current status.
        */
        Status GetStatus();

      private:
        float time_left; // The amount of time left before proceeding.
        bool fired;      // Whether or not this action has fired.
      };

      /*!
        \brief A group of simultaneously executed actions.
      */
      class ActionGroup : public Action
      {
      public:
        /*!
          \brief Creates a new ActionGroup.
        */
        ActionGroup();

        /*!
          \brief Starts an ActionGroup.
        */
        void Start();

        /*!
          \brief Updates an ActionGroup.
          \param dt The frame time.
        */
        void Update(float dt);

        /*!
          \brief Gets the status of an ActionGroup.
        */
        Status GetStatus();

        /*!
          \brief Adds an action to an ActionGroup.
        */
        void AddAction(Action* action);

      private:
        std::vector<Action*> actions; // The list of actions to execute.
        bool fired;                   // Whether or not this group was fired.
      };

      /*!
        \brief A list of Actions to execute sequentially.
      */
      class ActionList
      {
      public:
        /*!
          \brief Creates a new ActionList.
        */
        ActionList();

        /*!
          \brief Updates an ActionList.
          \param dt The frame time.
        */
        void Update(float dt);

        /*!
          \brief Determines whether or not this ActionList is finished.
        */
        bool IsFinished();
        
        /*!
          \brief Adds an action to an ActionList.
          \param action The action to add.
        */
        void AddAction(Action* action);

        /*!
          \brief Destroys an ActionList.
        */
        ~ActionList();

      private:
        std::vector<Action*> actions; // The list of actions.
      };

      /*!
        \brief Updates the Actions system.
        \param dt The frame time.
      */
      void Update(float dt);
    }
  }
}

#include "Actions.inl"
