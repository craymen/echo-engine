/*!
  \file FPSDisplay.cpp
  \author Chase A Rayment
  \date 18 Jan 2017
  \brief Contains the implementation of the FPSDisplay class.
*/

#include "FPSDisplay.hpp"
#include "TextObject.hpp"
#include "Scene.hpp"
#include "Space.hpp"
#include "Game.hpp"
#include "Transform.hpp"
#include "Handle.hpp"
#include "Font.hpp"
#include "ResourceManager.hpp"
#include <cmath>
#include "Material.hpp"
#include "Window.hpp"

namespace Echo
{
  namespace Core
  {
    namespace FPSDisplay
    {
      static Handle<Graphics::TextObject> DisplayText = nullptr;
			static Mode mode;
      Handle<Graphics::Scene> sc = nullptr;

      void Initialize()
      {
        
      }

      void Terminate()
      {

      }

      void Update()
      {
				if (mode != Mode::Disabled)
				{
					ImGui::SetNextWindowPos(ImVec2(0, 0));
					ImGui::Begin("FPS", nullptr, 
						ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | 
						ImGuiWindowFlags_NoMove | ImGuiWindowFlags_AlwaysAutoResize);
					ImGui::Text("Avg FPS: %i", (int)round(Game::GetAverageFramerate()));
					ImGui::Text("Polycount: %i", Core::Spaces[Core::SPACE_GAMEPLAY].GetGraphicsScene()->GetPolycount());
					ImGui::End();
				}
      }

      void SetMode(Mode m)
      {
				mode = m;
      }
    }
  }
}