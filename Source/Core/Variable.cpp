/*!
  \file Variable.cpp
  \author Chase A Rayment
  \date 6 April 2017
  \brief Contains the implementation of the Variable class.
*/
#include "Variable.hpp"
#include "Debug.hpp"
#include "TypeData.hpp"

namespace Echo
{
  namespace Core
  {
    Variable::Variable(const TypeData* t, void* d) : data(d), type(t)
    {

    }

    void* Variable::GetData()
    {
      return data;
    }

    const void* Variable::GetData() const
    {
      return data;
    }

    const TypeData* Variable::GetType() const
    {
      return type;
    }

    void Variable::Serialize(Json::Value& value)
    {
      // Ensure this is a valid Variable
      ASSERT(type && data);

      type->GetSerializeFunc()(*this, value);
    }

    void Variable::Deserialize(const Json::Value& value)
    {
      // Ensure we have a valid type and valid data to point to
      ASSERT(type && data);
      type->GetDeserializeFunc()(value, *this);
    }

    void* Variable::GetMember(const std::string& name)
    {
      return reinterpret_cast<void*>(reinterpret_cast<char*>(data) + 
        type->GetMembers().find(name)->second.GetOffset());
    }

    const void* Variable::GetMember(const std::string& name) const
    {
      return reinterpret_cast<const void*>(reinterpret_cast<char*>(data) + 
        type->GetMembers().find(name)->second.GetOffset());
    }
  }
}