/*!
  \file Object.inl
  \author Chase A Rayment
  \date 11 April 2017
  \brief Contains the template implementations for the Object class.
*/

#include "Log.hpp"
#include "TypeData.hpp"

namespace Echo
{
  namespace Core
  {
    template <typename T>
    Handle<T> Object::AddComponent()
    {
      // Get the type data of the given type
      const TypeData* td = TypeData::FindType<T>();

      // Add and return the component
      return reinterpret_cast<T*>(add_component(td));
    }

    template <typename T>
    Handle<T> Object::GetComponent()
    {
      // Get the type data of the given type
      const TypeData* td = TypeData::FindType<T>();

      // Get and return the component
      return reinterpret_cast<T*>(get_component(td));
    }

    template <typename T>
    void Object::RemoveComponent()
    {
      // Get the type data of the given type
      TypeData* td = TypeData::FindType<T>();

      // Remove the component
      remove_component(td);
    }
  }
}