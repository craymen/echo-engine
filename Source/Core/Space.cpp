/*!
  \file Space.cpp
  \author Chase A Rayment
  \date 11 April 2017
  \brief Contains the implementation of the Space class.
*/

#include "Space.hpp"
#include "GraphicsCore.hpp"
#include "FileSystem.hpp"
#include "PhysicsCore.hpp"
#include "Scene.hpp"
#include "CameraObject.hpp"
#include "Mouse.hpp"
#include "Log.hpp"
#include "PhysicsScene.hpp"
#include "MouseHoverEvent.hpp"
#include "Archetype.hpp"
#include "ResourceManager.hpp"
#include "glm/gtx/string_cast.hpp"
#include "Cubemap.hpp"

namespace Echo
{
  namespace Core
  {
    Space Spaces[SPACE_COUNT];

    Space::Space() : active(false), name(""), objects(), scene(nullptr),
      physics_scene(nullptr), hover_obj(nullptr), paused(false), 
      audio_scene(GetID())
    {
    }

    Space::~Space()
    {
      if(active)
        Terminate();
    }

    void Space::Initialize()
    {
      active = true;

      // Create new graphics scene
      scene = Graphics::AddScene();

	    scene->SetDepth(-0.1f * (this - Spaces));

      if(this - Spaces == 0)
      {
        scene->SetSkybox(ResourceManager<Graphics::Cubemap>::
          Find("DefaultCubemap"));
      }

      // Create new physics scene
      physics_scene = Physics::AddScene();
      physics_scene->GetDebugDrawer()->SetSpace(this);

      // Set every object's space pointer
      for (unsigned i = 0; i < MaxObjects; ++i)
        objects[i].space = this;

      // Initialize all objects
      for (unsigned i = 0; i < MaxObjects; ++i)
        if (objects[i].active && objects[i].Transform.GetParent() == nullptr)
          objects[i].Initialize();
    }

    void Space::Update(float dt)
    {
      if(physics_scene != nullptr)
        physics_scene->SetPaused(paused);

      // Mouse hover update
      if (GetGraphicsScene()->GetCurrentCamera())
      {
        glm::vec3 mStart, mDir;
        GetGraphicsScene()->GetCurrentCamera()->ScreenCoordsToWorldRay(
          Input::Mouse::GetAbsoluteMousePos(), mStart, mDir);

        Physics::RayCastResult rcr = GetPhysicsScene()->RayCastFirst(mStart, 
          mDir);

        if(rcr.Hit && rcr.HitObject != hover_obj)
        {
          Physics::MouseHoverEvent mhe;
          if(hover_obj)
            DispatchEvent((void*)hover_obj, "MouseHoverEnd", mhe);
          hover_obj = rcr.HitObject;
          if(hover_obj)
            DispatchEvent((void*)hover_obj, "MouseHoverStart", mhe);
        }
        else if(!rcr.Hit && hover_obj)
        {
          Physics::MouseHoverEvent mhe;          
          DispatchEvent((void*)hover_obj, "MouseHoverEnd", mhe);
          hover_obj = nullptr;
        }
      }

      if(!paused)
      {
        UpdateEvent ev(dt);
        DispatchEvent(this, "Update", ev);
      }
    }

    void Space::Terminate()
    {
      // Terminate all objects
      for(unsigned i = 0; i < MaxObjects; ++i)
        if(objects[i].active && objects[i].Transform.GetParent() == nullptr)
          objects[i].Terminate();
      
      // Reset values
      active = false;
      name = "";

      // Remove the graphics scene
      Graphics::RemoveScene(scene);

      // Remove the physics scene
      Physics::RemoveScene(physics_scene);
      physics_scene = nullptr;

      hover_obj = nullptr;

      // Invalidate handles to this space
      Handle<Space>::InvalidateHandles(this);
    }

    const std::string& Space::GetName() const
    {
      return name;
    }

    void Space::SetName(const std::string& n)
    {
      name = n;
    }

    Handle<Object> Space::AddObject()
    {
      // Find the first open object
      for(unsigned i = 0; i < MaxObjects; ++i)
      {
        if(!objects[i].active)
        {
          objects[i].space = this;
          objects[i].Initialize();
          return Handle<Object>(objects + i);
        }
      }

      Log << Priority::Warning << "Tried to add an object to space " << name
        << " but the space is full! Increase Space::MaxObjects." << std::endl;
      return Handle<Object>(nullptr);
    }

    Handle<Object> Space::FindObjectByName(const std::string& n)
    {
      // Find an object with the given name
      for(unsigned i = 0; i < MaxObjects; ++i)
      {
        if(objects[i].active && objects[i].name == n)
          return Handle<Object>(objects + i);
      }

      return Handle<Object>(nullptr);
    }

    std::vector<Handle<Object> > Space::GetAllObjects()
    {
      std::vector<Handle<Object> > result;

      // Add all active objects
      for(unsigned i = 0; i < MaxObjects; ++i)
      {
        if(objects[i].active)
          result.push_back(Handle<Object>(objects + i));
      }
      return result;
    }

    Graphics::Scene* Space::GetGraphicsScene()
    {
      return scene;
    }

    Core::Handle<Physics::Scene> Space::GetPhysicsScene()
    {
      return physics_scene;
    }

    void Space::LoadLevel(const std::string& n)
    {
      Core::Log << "Loading level " << n << std::endl;
      Core::Variable v(Core::TypeData::FindType<Core::Space>(), this);
      std::string path = Core::FileSystem::GetGameDir(
        Core::FileSystem::Dir::Levels) + "/" + n;
      std::ifstream in(path);
      Json::Value val;
      if(!in.is_open())
      {
        Core::Log << Core::Priority::Warning << "Could not load from "
          << path << std::endl;
        return;
      }
      else
        in >> val;

      Terminate();
      curr_loading_space = Handle<Space>(this);
      v.Deserialize(val);
      filename = n.substr(0, n.find_last_of('.'));
    }

    void Space::Consolidate()
    {
      Object* last_inactive = objects;
      for(unsigned i = 0; i < MaxObjects; ++i)
      {
        if(!objects[i].active && last_inactive->active)
          last_inactive = objects + i;
        else if(objects[i].active && !last_inactive->active && 
          objects + i > last_inactive)
        {
          *last_inactive = objects[i];
          Core::Handle<Object>::UpdateHandles(objects + i, last_inactive);
          objects[i].active = false;
          while(last_inactive->active)
            last_inactive++;
        }
      }
    }

    void Space::SetPaused(bool p)
    {
      paused = p;
    }

    bool Space::GetPaused() const
    {
      return paused;
    }

    bool Space::GetActive() const
    {
      return active;
    }

    unsigned Space::GetID() const
    {
      return this - Spaces;
    }

    Audio::Scene& Space::GetAudioScene()
    {
      return audio_scene;
    }

    Handle<Object> Space::CreateAtPosition(std::string archetypeName, 
      const glm::vec3& pos)
    {
      Handle<Object> result = AddObject();
      ResourceManager<Archetype>::Find(archetypeName)->ApplyArchetype(result);
      result->Transform.SetPosition(pos);
      result->Initialize();
      return result;
    }

    Handle<Object> Space::GetHoveredObject() const
    {
      return hover_obj;
    }

    const std::string& Space::GetLastLoadedLevelName() const
    {
      return filename;
    }


    Handle<Space> Space::curr_loading_space = nullptr;
  }
}