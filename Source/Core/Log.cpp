/*!
  \file Log.cpp
  \author Chase A Rayment
  \date 5 April 2017
  \brief Contains the implementation of the Logger class.
*/
#include "Log.hpp"
#include "FileSystem.hpp"
#include <sstream>

namespace Echo
{
  namespace Core
  {
    Logger Log;

    Logger::Logger() : file_log(), 
      curr_priority(Priority::Info)
    #ifdef WIN32
      , console_handle(nullptr), init_console_info()
    #endif
    {

    }

    void Logger::Init(const std::string& filename)
    {
      // Open log file
      std::stringstream fss;
      fss << FileSystem::GetGameDir(FileSystem::Dir::Logs) << "/" << filename;
      file_log.open(fss.str());
      if (!file_log.is_open())
        *this << Priority::Warning << "Could not open log file " << fss.str() 
          << "!" << std::endl;

      // Get console handle and initial info so we can set color
    #ifdef _WIN32
      console_handle = GetStdHandle(STD_OUTPUT_HANDLE);
      GetConsoleScreenBufferInfo(console_handle, &init_console_info);
    #endif

      // Print init text
      *this << Priority::Info << "Logger initialized: " << filename << 
        std::endl;
    }

    void Logger::Terminate()
    {
      // Reset console color
    #ifdef _WIN32
      SetConsoleTextAttribute(console_handle, init_console_info.wAttributes);
    #endif

      // Close file
      if(file_log.is_open())
        file_log.close();
    }

    Logger& Logger::operator<<(std::ostream& (*pf)(std::ostream&))
    {
      // Reset the priority
      curr_priority = Priority::Info;

      // Set console color
      set_console_color();

      // Print the value
      pf(std::cout);
      if(file_log.is_open())
        pf(file_log);

      DevConsole::PushNewline();

      return *this;
    }

    Logger& Logger::operator<<(const Priority& priority)
    {
      // Set the priority to the given one
      curr_priority = priority;

      // Print the priority level and change console color
      set_console_color();
      switch(priority)
      {
        case Priority::Warning:
          std::cout << "WARNING: ";
          DevConsole::PushTextColor(1, 1, 0, 1);
          DevConsole::PushText("WARNING: ");
          break;

        case Priority::Error:
          std::cout << "ERROR: ";
          DevConsole::PushTextColor(1, 0, 0, 1);
          DevConsole::PushText("ERROR: ");
          break;

        case Priority::Info:
          break;
      }

      return *this;
    }

    void Logger::set_console_color()
    {
      // TODO: Port to Linux
      switch(curr_priority)
      {
      case Priority::Info:
      #ifdef _WIN32
        SetConsoleTextAttribute(console_handle, init_console_info.wAttributes);
      #endif
        break;

      case Priority::Warning:
      #ifdef _WIN32
        SetConsoleTextAttribute(console_handle, 
          FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
      #endif
        break;

      case Priority::Error:
      #ifdef _WIN32
        SetConsoleTextAttribute(console_handle, 
          BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN | 
          FOREGROUND_INTENSITY | BACKGROUND_INTENSITY);
      #endif
        break;
      }
    }
  }
}