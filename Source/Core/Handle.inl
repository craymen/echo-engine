/*!
  \file Handle.inl
  \author Chase A Rayment
  \date 7 April 2017
  \brief Contains the template implementation of the Handle class.
*/

#include "Debug.hpp"

namespace Echo
{
  namespace Core
  {
    template <typename T>
    Handle<T>::Handle(T* obj) : ptr(obj)
    {
      register_addr();
    }

    template <typename T>
    Handle<T>::Handle(const Handle& rhs) : ptr(rhs.ptr)
    {
      register_addr();
    }

    template <typename T>
    Handle<T>::~Handle()
    {
      deregister_addr();
    }

    template <typename T>
    bool Handle<T>::IsValid() const
    {
      return ptr != nullptr;
    }

    template <typename T>
    T* Handle<T>::operator->()
    {
      ASSERT(IsValid());
      return ptr;
    }

    template <typename T>
    const T* Handle<T>::operator->() const
    {
      ASSERT(IsValid());
      return ptr;
    }

    template <typename T>
    T& Handle<T>::operator*()
    {
      ASSERT(IsValid());
      return *ptr;
    }

    template <typename T>
    const T& Handle<T>::operator*() const
    {
      ASSERT(IsValid());
      return *ptr;
    }

    template <typename T>
    bool Handle<T>::operator==(const Handle& rhs) const
    {
      return ptr == rhs.ptr;
    }

    template <typename T>
    bool Handle<T>::operator==(const T* rhs) const
    {
      return ptr == rhs;
    }

    template <typename T>
    bool Handle<T>::operator!=(const Handle& rhs) const
    {
      return ptr != rhs.ptr;
    }

    template <typename T>
    bool Handle<T>::operator!=(const T* rhs) const
    {
      return ptr != rhs;
    }

    template <typename T>
    Handle<T>& Handle<T>::operator=(const Handle& rhs)
    {
      deregister_addr();
      ptr = rhs.ptr;
      register_addr();
      return *this;
    }

    template <typename T>
    Handle<T>& Handle<T>::operator=(T* rhs)
    {
      deregister_addr();
      ptr = rhs;
      register_addr();
      return *this;
    }

    template <typename T>
    Handle<T>::operator T*()
    {
      return ptr;
    }

    template <typename T>
    Handle<T>::operator const T*() const
    {
      return ptr;
    }

    template <typename T>
    Handle<T>::operator void*()
    {
      return ptr;
    }

    template <typename T>
    Handle<T>::operator const void*() const
    {
      return ptr;
    }
    
    template <typename T>
    Handle<T>::operator bool() const
    {
      return IsValid();
    }

    template <typename T>
    void Handle<T>::UpdateHandles(const T* oldAddr, T* newAddr)
    {
      // If there are no handles pointing to this address, just return.
      // We don't necessarily care about this case. (it's not user error)
      if(handles.find(oldAddr) == handles.end())
        return;

      // For each handle in the list, update each
      auto h = handles[oldAddr];
      for(auto handle : h)
        *handle = newAddr;
    }

    template <typename T>
    void Handle<T>::InvalidateHandles(const T* addr)
    {
      // If there are no handles pointing to this address, just return.
      // We don't necessarily care about this case. (it's not user error)
      if(handles.size() == 0 || handles.find(addr) == handles.end())
        return;

      // For each handle in the list, invalidate each
      for(auto handle : handles[addr])
        handle->ptr = nullptr;
      
      // Remove the list from the registry
      handles.erase(addr);
    }

    template <typename T>
    void Handle<T>::register_addr()
    {
      // If this Handle is invalid, we don't want to register it
      if(!IsValid())
        return;

      // Find the address, if it exists
      auto found = handles.find(ptr);

      // If the address wasn't found, make a new list
      if (found == handles.end())
        handles[ptr] = std::list<Handle*>();

      // Push this into the list
      handles[ptr].push_back(this);
    }

    template <typename T>
    void Handle<T>::deregister_addr()
    {
      // If this Handle is invalid, we don't want to deregister it
      if (!IsValid())
        return;

      // Remove this handle from the list
      handles[ptr].remove(this);

      // If the list is empty now, remove the list
      if (handles[ptr].empty())
        handles.erase(ptr);

      // Set ptr to null (this Handle is invalid now)
      ptr = nullptr;
    }

    template <typename T>
    std::unordered_map<const T*, std::list<Handle<T>*> > Handle<T>::handles;
  }
}

namespace std
{
	template <typename T>
	struct hash<Echo::Core::Handle<T> >
	{
		size_t operator()(const Echo::Core::Handle<T>& h) const
		{
			const void* v = (const void*)h;
			return hash<const void*>()(v);
		}
	};
}