/*!
  \file Component.cpp
  \author Chase A Rayment
  \date 21 June 2017
  \brief Contains the implementation of the Component class.
*/

#include "Component.hpp"
#include <unordered_map>
#include <unordered_set>
#include "Log.hpp"

namespace Echo
{
  namespace Core
  {
    static std::unordered_map<size_t, std::unordered_set<size_t> > 
      dependencies;
    
    void Component::AddDependency(const TypeData* a, const TypeData* b)
    {
      std::unordered_set<size_t>& rev = dependencies[b->GetHash()];
      if(rev.find(a->GetHash()) != rev.end())
      {
        Log << Priority::Warning << "Tried to add a component dependency for " 
          << a->GetName() << " on " << b->GetName() << " but the reverse "
          "dependency already exists! Did not add dependency to avoid circular"
          " dependency.";
        return;
      }
      dependencies[a->GetHash()].insert(b->GetHash());
    }

    bool Component::IsDependentOn(const TypeData* a, const TypeData* b)
    {
      std::unordered_set<size_t>& s = dependencies[a->GetHash()];
      return s.find(b->GetHash()) != s.end();
    }

    std::vector<const TypeData*> Component::GetAllDependencies(
      const TypeData* a)
    {
      std::vector<const TypeData*> result;
      for(auto d : dependencies[a->GetHash()])
        result.push_back(TypeData::FindType(d));
      return result;
    }
  }
}
