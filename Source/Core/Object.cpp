/*!
  \file Object.cpp
  \author Chase A Rayment
  \date 11 April 2017
  \brief Contains the implementation of the Object class.
*/

#include "Object.hpp"
#include "Component.hpp"
#include "TypeData.hpp"
#include "Space.hpp"
#include "Game.hpp"

namespace Echo
{
  namespace Core
  {
    Object::Object() : Transform(), Actions(), Parent(nullptr), 
      ArchetypeName(""), active(false), name(""), components(), space(nullptr)
    {
      Transform.SetObject(Handle<Object>(this));
    }

    Object::~Object()
    {
      if(active)
        Terminate();
    }
    
    void Object::Initialize()
    {
      active = true;

      Actions = new Actions::ActionList;

      // Initialize all components
      Reinitialize();
    }

    void Object::Terminate()
    { 
      // Terminate children
      std::vector<Handle<Graphics::Transform> > children = 
        Transform.GetChildren();
      for(auto child : children)
        if(child->GetObject() != nullptr)
          child->GetObject()->Terminate();

      // Free all components
      for(auto& comp : components)
      {
        if(Game::GetEditorMode())
          comp.second->EditorTerminate();
        else
          comp.second->Terminate();
      }
      for(auto& comp : components)
        TypeData::FindType(comp.first)->GetDeleteFunc()(comp.second);
      components.clear();
      Transform.Terminate();
      
      delete Actions;

      // Invalidate handles to this Object
      Handle<Object>::InvalidateHandles(this);
      Handle<Graphics::Transform>::InvalidateHandles(&Transform);

      // Set to inactive
      active = false;

      Transform.SetObject(Handle<Object>(this));      
    }

    const std::string& Object::GetName() const
    {
      return name;
    }

    void Object::SetName(const std::string& n)
    {
      name = n;
    }

    Handle<Component> Object::AddComponent(const std::string& n)
    {
      return Handle<Component>(add_component(TypeData::FindType(n)));
    }

    Handle<Component> Object::GetComponent(const std::string& n)
    {
      return Handle<Component>(get_component(TypeData::FindType(n)));
    }

    void Object::RemoveComponent(const std::string& n)
    {
      remove_component(TypeData::FindType(n));
    }

    Handle<Space> Object::GetSpace() const
    {
      return space;
    }

    Component* Object::add_component(const TypeData* td, bool suppress_dupe)
    {
      // Ensure the type exists
      ASSERT(td != nullptr);

      // Ensure this object doesn't already have the component
      if(components.find(td->GetHash()) != components.end())
      {
        if(!suppress_dupe)
          Log << Priority::Warning << "Trying to add component " 
            << td->GetName() << " to Object " << GetName() 
            << " but the component already exists!" << std::endl;
        return nullptr;
      }

      // Get and add all dependent components
      for(auto c : Component::GetAllDependencies(td))
        add_component(c, true);
      
      // Create the object
      Component* comp = reinterpret_cast<Component*>(td->GetNewFunc()());

      // Set the component's owner/space
      comp->owner = this;
      comp->space = space;

      // Put it in the map of components
      components[td->GetHash()] = comp;

      return comp;
    }
    
    Component* Object::get_component(const TypeData* td)
    {
      // Ensure the component type exists (did you forget to register it?)
      ASSERT(td);

      // Find the component
      auto found = components.find(td->GetHash());

      // If we didn't find it, return nullptr
      if(found == components.end())
        return nullptr;
      
      // Otherwise return the found component
      return (*found).second;
    }
    
    void Object::remove_component(const TypeData* td)
    {
      // Find the component
      auto found = components.find(td->GetHash());

      // Ensure the component exists
      if(found == components.end())
      {
        Log << Priority::Warning << "Tried to remove component " << td->GetName()
          << " from object " << GetName() << " but the component doesn't exist!"
          << std::endl;
        return;
      }
      
      // Ensure there are no dependencies on the component
      for(auto comp : components)
      {
        if(comp.first == td->GetHash())
          continue;
        const TypeData* td2 = TypeData::FindType(comp.first);
        if(Component::IsDependentOn(td2, td))
        {
          Log << Priority::Warning << "Tried to remove component " <<
            td->GetName() << " from object " << GetName() << " but " << 
            td2->GetName() << " is dependent on it!" << std::endl;
          return;
        }
      }

      // Remove the component
      found->second->Terminate();
      td->GetDeleteFunc()(found->second);
      components.erase(found);
    }

    const std::unordered_map<size_t, Component*>& Object::GetAllComponents()
    {
      return components;
    }

    void Object::RemoveAllComponents()
    {
      std::unordered_map<size_t, Component*> temp = components;
      for(auto component : temp)
      {
        remove_component(TypeData::FindType(component.first));
      }
    }

    void Object::Reinitialize()
    {
      Transform.Initialize();

      for(auto& comp : components)
      {
        comp.second->owner = this;
        comp.second->space = space;
        if(Core::Game::GetEditorMode())
          comp.second->EditorInitialize();
        else
          comp.second->Initialize();
      }

      for(auto child : Transform.GetChildren())
        if(child->GetObject() != nullptr)
          child->GetObject()->Initialize();
    }

    Handle<Object> Object::Clone()
    {
      Handle<Object> result = space->AddObject();
      for(auto comp : components)
      {
        TypeData* td = TypeData::FindType(comp.first);
        Variable va(td, comp.second);
        Variable vb(td, (void*)result->AddComponent(td->GetName()));
        td->GetAssignFunc()(vb, va);
      }
      result->SetName(name);
      result->Transform = Transform;
      result->ArchetypeName = ArchetypeName;
      result->Parent = Parent;
      result->Initialize();
      return result;
    }

    void Object::DebugDraw()
    {
      for(auto comp : components)
        comp.second->DebugDraw();
    }

    unsigned Object::GetID() const
    {
      return this - space->objects;
    }

    void Object::PrintComponents() const
    {
      Core::Log << "Components on " << GetName() << ":" << std::endl;
      for(auto comp : components)
        Core::Log << "  " << TypeData::FindType(comp.first)->GetName() 
          << std::endl;
      Core::Log << std::endl;
    }
  }
}