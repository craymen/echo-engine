/*!
  \file Archetype.hpp
  \author Chase A Rayment
  \date 22 May 2017
  \brief Contains the definition of the Archetype class.
*/
#pragma once

#include <string>
#include "Handle.hpp"
#include "json/json.h"
#include "Object.hpp"
#include "Resource.hpp"

namespace Echo
{
  namespace Core
  {
    /*!
      \brief A serialized object template that can be applied to other objects.
    */
    class Archetype : public Core::Resource
    {
    public:
      /*!
        \brief Creates a new Archetype.
        \param path The filepath to load.
      */
      Archetype(const std::string& path);

      /*!
        \brief Gets the JSON representation of an Archetype.
      */
      const Json::Value& GetJson();

      /*!
        \brief Applies an archetype to an object.
        \param obj The object to apply the archetype to.
      */
      void ApplyArchetype(Handle<Object> obj);

      /*!
        \brief Gets the differences between an object and an Archetype
          in serialized JSON form.
        \param obj The object to compare.
      */
      Json::Value GetDifferences(Handle<Object> obj);

      /*!
        \brief Gets the default Archetype.
      */
      static Handle<Archetype> GetDefault();

    private:
      Json::Value val; // The JSON representation of the archetype.
    };
  }
}