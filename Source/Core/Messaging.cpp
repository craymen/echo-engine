/*!
  \file Messaging.cpp
  \author Chase A Rayment
  \date 12 April 2017
  \brief Contains the implementation of the Messaging system.
*/

#include "Messaging.hpp"

namespace Echo
{
  namespace Core
  {
    Messaging::ConnectionMap Messaging::Connections;

    void DisconnectEvent(void* scope, const std::string& channel, 
      void* listener)
    {
      // Return early if the scope/channel aren't found
      if(Messaging::Connections.find(scope) == Messaging::Connections.end())
        return;
      if(Messaging::Connections[scope].find(channel) == 
        Messaging::Connections[scope].end())
        return;

      Messaging::DelegateList& delList = Messaging::Connections[scope][channel];
      
      // Find the listener
      auto foundDel = delList.end();
      for(auto del = delList.begin(); del != delList.end(); ++del)
      {
        if((*del)->GetListener() == listener)
        {
          foundDel = del;
          break;
        }
      }

      // If the listener isn't found return early
      if(foundDel == delList.end())
        return;

      // Remove from the delegate list
      delList.erase(foundDel);
      delete *foundDel;

      // If the delegate list is empty, remove it
      if(delList.size() == 0)
        Messaging::Connections[scope].erase(channel);

      // If the scope list is empty, remove it
      if(Messaging::Connections[scope].size() == 0)
        Messaging::Connections.erase(scope);
    }
  }
}