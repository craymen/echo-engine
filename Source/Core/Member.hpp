/*!
  \file Member.hpp
  \author Chase A Rayment
  \date 5 April 2017
  \brief Contains the definition of the Member class.
*/

#pragma once
#include <string>

namespace Echo
{
  namespace Core
  {
    class TypeData;

    /*!
      \brief Stores data about a member of a type.
    */
    class Member
    {
    public:
      /*!
        \brief Constructs an empty Member. Use only for error types.
      */
      Member();

      /*!
        \brief Constructs a new Member.
        \param type The type of the member.
        \param name The name of the member.
        \param offset The offset of the member.
      */
      Member(const TypeData* type, const std::string& name, unsigned offset);

      /*!
        \brief Gets the type of this Member.
      */
      const TypeData* GetType() const;

      /*!
        \brief Gets the name of this Member.
      */
      const std::string& GetName() const;

      /*!
        \brief Gets the offset of this Member.
      */
      unsigned GetOffset() const;

    private:
      const TypeData* type; // The type of this member.
      std::string name;     // The name of this member.
      unsigned offset;      // The offset of this member.
    };
  }
}