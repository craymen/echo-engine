/*!
  \file DevConsole.hpp
  \author Chase A Rayment
  \date 20 June 2017
  \brief Contains the implementation of the DevConsole system.
*/

#pragma once

#include <string>

namespace Echo
{
  namespace Core
  {
    namespace DevConsole
    {
      void Initialize();

      void Update();

      void PushText(const std::string& text);

      void PushNewline();

      void PushTextColor(float x, float y, float z, float w);
    }
  }
}