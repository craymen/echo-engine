/*!
  \file Component.hpp
  \author Chase A Rayment
  \date 7 April 2017
  \brief Contains the definition of the Component interface.
*/
#pragma once

#include "Handle.hpp"
#include "TypeData.hpp"
#include <vector>

namespace Echo
{
  namespace Core
  {
    class Object;
    class Space;

    /*!
      \brief An interface for Object components.
    */
    class Component
    {
      friend class Object;
    public:
      /*!
        \brief Initializes a Component.
      */
      virtual void Initialize() { };

      /*!
        \brief Initializes a Component in the editor.
      */
      virtual void EditorInitialize() { };

      /*!
        \brief Terminates a Component.
      */
      virtual void Terminate() 
      { 
        Handle<Component>::InvalidateHandles(this);
      };

      /*!
        \brief Terminates a Component in the editor.
      */
      virtual void EditorTerminate()
      {
        Handle<Component>::InvalidateHandles(this);
      };

      /*!
        \brief Gets a component's owner.
      */
      Handle<Object> GetOwner() { return owner; }

      /*!
        \brief Gets a component's space.
      */
      Handle<Space> GetSpace() { return space; }

      /*!
        \brief Destroys this Component.
      */
      virtual ~Component() { }

      /*!
        \brief Debug draws this Component.
      */
      virtual void DebugDraw() { }

      /*!
        \brief Adds a dependency between two components. A dependent component
          will automatically add all of its dependees to an Object. A dependee
          cannnot be removed with a dependent component present.
        \param a The dependent component's type.
        \param b The dependee component's type.
      */
      static void AddDependency(const TypeData* a, const TypeData* b);

      /*!
        \brief Returns whether or not a dependency exists between the two given
          components.
        \param a The dependent component.
        \param b The dependee component.
      */
      static bool IsDependentOn(const TypeData* a, const TypeData* b);

      /*!
        \brief Gets all dependencies of a given component.
        \param a The component to query.
      */
      static std::vector<const TypeData*> GetAllDependencies(const TypeData* a);

    private:
      Handle<Object> owner; // The owner of this Component.
      Handle<Space> space;  // The space of this Component.
    };

    /*!
      \brief The minimal implementation of a Component Terminate function.
    */
    #define COMPONENT_TERMINATE(comp)                                          \
      Component::Terminate();                                                  \
      Handle<comp>::InvalidateHandles(this);                                   

    #define ADD_COMPONENT_DEPENDENCY(comp1, comp2)                             \
      do{                                                                      \
      Component::AddDependency(TypeData::FindType<comp1>(),                    \
        TypeData::FindType<comp2>());                                          \
      } while(0)                                                               
  }
}