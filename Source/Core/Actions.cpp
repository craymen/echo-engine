/*!
  \file Actions.cpp
  \author Chase A Rayment
  \date 22 May 2017
  \brief Contains the implementation of the Actions class.
*/

#include "Actions.hpp"
#include <algorithm>
#include "Handle.hpp"

namespace Echo
{
  namespace Core
  {
    namespace Actions
    {
      static std::vector<ActionList*> lists;
      
      ActionCall::ActionCall(void(*c)(void*), void* d) : callback(c),
        user_data(d), fired(false)
      {

      }

      void ActionCall::Start()
      {
        callback(user_data);
        fired = true;
      }

      Action::Status ActionCall::GetStatus()
      {
        return fired ? Status::Complete : Status::NotStarted;
      }

      ActionDelay::ActionDelay(float t) : time_left(t), fired(false)
      {

      }

      void ActionDelay::Start()
      {
        fired = true;
      }

      void ActionDelay::Update(float dt)
      {
        time_left -= dt;
      }

      Action::Status ActionDelay::GetStatus()
      {
        if(fired)
          return time_left <= 0.0f ? Status::Complete : Status::InProgress;
        else
          return Status::NotStarted;
      }

      ActionGroup::ActionGroup() : actions(), fired(false)
      {

      }

      void ActionGroup::Start()
      {
        fired = true;
      }

      void ActionGroup::Update(float dt)
      {
        std::vector<Action*> a = actions;
        for(Action* act : a)
        {
          if(act->GetStatus() == Action::Status::NotStarted)
            act->Start();
          if(act->GetStatus() == Action::Status::InProgress)
            act->Update(dt);
          if(act->GetStatus() == Action::Status::Complete)
          {
            delete act;
            actions.erase(std::remove(actions.begin(), actions.end(), act), 
              actions.end());
          }
        }
      }

      Action::Status ActionGroup::GetStatus()
      {
        if(!fired)
          return Status::NotStarted;
        else return actions.size() == 0 ? Status::Complete : Status::InProgress;
      }

      void ActionGroup::AddAction(Action* action)
      {
        actions.push_back(action);
      }

      ActionList::ActionList() : actions()
      {
        lists.push_back(this);
      }

      void ActionList::Update(float dt)
      {
        std::vector<Action*> a = actions;
        for(Action* act : a)
        {
          if (act->GetStatus() == Action::Status::NotStarted)
          {
            act->Start();
            return;
          }
          if(act->GetStatus() == Action::Status::InProgress)
          {
            act->Update(dt);
            break;
          }
          if(act->GetStatus() == Action::Status::Complete)
          {
            delete act;
            actions.erase(std::remove(actions.begin(), actions.end(), act), 
              actions.end());
          }
        }
      }

      bool ActionList::IsFinished()
      {
        return actions.size() == 0;
      }

      void ActionList::AddAction(Action* action)
      {
        actions.push_back(action);
      }

      ActionList::~ActionList()
      {
        auto found = std::find(lists.begin(), lists.end(), this);
        if(found != lists.end())
          lists.erase(found);
      }

      void Update(float dt)
      {
        std::vector<ActionList*> l = lists;
        for(ActionList* al : l)
          al->Update(dt);
      }
    }
  }
}