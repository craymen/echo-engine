/*!
  \file Resource.cpp
  \author Chase A Rayment
  \date 19 June 2017
  \brief Contains the implementation of the Resource class.
*/

#include "Resource.hpp"

namespace Echo
{
  namespace Core
  {
    Resource::Resource(const std::string& f) : reload_on_write(true),
      last_write(FileSystem::GetLastFileWrite(f)), filename(f)
    {

    }

    Resource::Resource() : reload_on_write(false)
    {

    }

    Resource::~Resource()
    {

    }

    const std::string& Resource::GetName() const
    {
      return name;
    }
  }
}