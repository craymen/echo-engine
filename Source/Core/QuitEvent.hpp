/*!
  \file QuitEvent.hpp
  \author Chase A Rayment
  \date 16 June 2017
  \brief Contains the definition of the Quit event.
*/

#include "Messaging.hpp"

namespace Echo
{
  namespace Core
  {
    /*!
      \brief The event sent upon quitting the game.
    */
    struct QuitEvent : public Event
    {

    };
  }
}