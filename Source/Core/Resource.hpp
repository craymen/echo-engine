/*!
  \file Resource.hpp
  \author Chase A Rayment
  \date 19 June 2017
  \brief Contains the definition of the Resource interface.
*/

#pragma once

#include <string>
#include "FileSystem.hpp"

namespace Echo
{
  namespace Core
  {
    /*!
      \brief An interface for resources loaded from file.
    */
    class Resource
    {
      template <typename T>
      friend class ResourceManager;
    public:
      /*!
        \brief Creates a new Resource.
        \param filename The file to load.
      */
      Resource(const std::string& filename);

      /*!
        \brief Creates a new Resource.
      */
      Resource();

      /*!
        \brief Destroys a Resource.
      */
      virtual ~Resource();

      /*!
        \brief Gets the resource name.
      */
      const std::string& GetName() const;

    protected:
      bool reload_on_write; // Whether or not this resource should be reloaded.
      FILETIME last_write;  // The last write time of the resource.
      std::string filename; // The filename of the resource.
      std::string name;     // The name of the resource.
    };
  }
}