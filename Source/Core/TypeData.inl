/*!
  \file TypeData.inl
  \author Chase A Rayment
  \date 6 April 2017
  \brief Contains template definitions for TypeData.
*/

#include "Variable.hpp"
#include <unordered_map>
#include "Debug.hpp"
#include "Space.hpp"
#include "imgui/imgui.h"
#include "GLM/glm.hpp"
#include "Transform.hpp"
#include "ResourceManager.hpp"

namespace Echo
{
  namespace Core
  {
    class Object;
    class Space;

    /*!
      \brief Creates a new generic type.
    */
    template <typename T>
    void* NewType()
    {
      return new T();
    }

    /*!
      \brief Deletes a generic object.
    */  
    template <typename T>
    void DeleteType(void* obj)
    {
      delete reinterpret_cast<T*>(obj);
    }

    /*!
      \brief Serializes a generic type into a Json::Value.
    */
    template <typename T>
    void SerializeType(Variable var, Json::Value& val)
    {
      // Recursively serialize all members
      for(auto& member : var.GetType()->GetMembers())
      {
        // HACK: prevent transforms from serializing their parents
        if(member.second.GetName() == "Parent"
          && var.GetType() == TypeData::FindType("Transform"))
          continue;
        void* memberData = reinterpret_cast<char*>(var.GetData())
          + member.second.GetOffset();
        member.second.GetType()->GetSerializeFunc()(
          Variable(member.second.GetType(), memberData), 
          val[member.second.GetName()]);
      }
    }

    /*! \overload */
    template <>
    void SerializeType<int>(Variable var, Json::Value& val);

    /*! \overload */
    template <>
    void SerializeType<unsigned>(Variable var, Json::Value& val);

    /*! \overload */
    template <>
    void SerializeType<std::string>(Variable var, Json::Value& val);

    /*! \overload */
    template <>
    void SerializeType<float>(Variable var, Json::Value& val);

    /*! \overload */
    template <>
    void SerializeType<bool>(Variable var, Json::Value& val);

    /*! \overload */
    template <>
    void SerializeType<Object>(Variable var, Json::Value& val);

    /*! \overload */
    template <>
    void SerializeType<Space>(Variable var, Json::Value& val);

    /*! \overload */
    template <>
    void SerializeType<glm::vec3>(Variable var, Json::Value& val);

    /*! \overload */
    template <>
    void SerializeType<glm::vec4>(Variable var, Json::Value& val);

    /*! \overload */
    // Assuming the template type is a resource. If you're getting template 
    // errors with GetName then T is not a resource!
    template <typename T>
    void SerializeResource(Variable var, Json::Value& val)
    {
      const Handle<T>* data = reinterpret_cast<const Handle<T>*>(var.GetData());
      if(*data == nullptr)
        val = T::GetDefault()->GetName();
      else
        val = (*data)->GetName();
    }

    /*! \overload */
    template <>
    void SerializeType<Handle<Object> >(Variable var, Json::Value& val);

    /*! \overload */
    template <>
    void SerializeType<Handle<Graphics::Transform> >(Variable var, 
      Json::Value& val);

    /*!
      \brief Deserializes a generic type from a Json::Value.
    */
    template <typename T>
    void DeserializeType(const Json::Value& val, Variable& var)
    {
      std::unordered_map<std::string, Member> members = 
        var.GetType()->GetMembers();
      for(std::string sub : val.getMemberNames())
      {
        Member m = members[sub];
        void* memberData = reinterpret_cast<char*>(var.GetData())
          + m.GetOffset();
        if (m.GetType())
        {
          Variable v(m.GetType(), memberData);
          m.GetType()->GetDeserializeFunc()(val[sub], v);
        }
      }
    }

    /*! \overload */
    template <>
    void DeserializeType<int>(const Json::Value& val, Variable& var);

    /*! \overload */
    template <>
    void DeserializeType<unsigned>(const Json::Value& val, Variable& var);

    /*! \overload */
    template <>
    void DeserializeType<float>(const Json::Value& val, Variable& var);

    /*! \overload */
    template <>
    void DeserializeType<bool>(const Json::Value& val, Variable& var);

    /*! \overload */
    template <>
    void DeserializeType<std::string>(const Json::Value& val, Variable& var);

    /*! \overload */
    template <>
    void DeserializeType<Object>(const Json::Value& val, Variable& var);

    /*! \overload */
    template <>
    void DeserializeType<Space>(const Json::Value& val, Variable& var);

    /*! \overload */
    template <>
    void DeserializeType<glm::vec3>(const Json::Value& val, Variable& var);

    /*! \overload */
    template <>
    void DeserializeType<glm::vec4>(const Json::Value& val, Variable& var);

    /*! \overload */
    template <typename T>
    void DeserializeResource(const Json::Value& val, Variable& var)
    {
      Handle<T>* data = reinterpret_cast<Handle<T>*>(var.GetData());
      if(val.asString() == "")
        *data = nullptr;
      else
        *data = ResourceManager<T>::Find(val.asString());
    }

    /*! \overload */
    template <>
    void DeserializeType<Handle<Object> >(const Json::Value& val, 
      Variable& var);

    /*! \overload */
    template <>
    void DeserializeType<Handle<Graphics::Transform> >(const Json::Value& val, 
      Variable& var);

    /*!
      \brief Implements an editor widget for a given type.
      \param p A pointer to the data.
      \param name The name to give the widget.
    */
    template <typename T>
    void EditType(void* p, const std::string& name)
    {
      char* data = reinterpret_cast<char*>(p);
      if(ImGui::CollapsingHeader(name.c_str()))
        for(auto& member : TypeData::FindType<T>()->GetMembers())
          member.second.GetType()->GetEditorWidgetFunc()(
            data + member.second.GetOffset(), member.first);
    }

    /*! \overload */
    template <> 
    void EditType<int>(void* p, const std::string& name);

    /*! \overload */
    template <> 
    void EditType<float>(void* p, const std::string& name);

    /*! \overload */
    template <> 
    void EditType<double>(void* p, const std::string& name);

    /*! \overload */
    template <> 
    void EditType<bool>(void* p, const std::string& name);

    /*! \overload */
    template <> 
    void EditType<std::string>(void* p, const std::string& name);

    /*! \overload */
    template <> 
    void EditType<glm::vec3>(void* p, const std::string& name);

    /*! \overload */
    template <> 
    void EditType<glm::vec4>(void* p, const std::string& name);

    /*! \overload */
    template <typename T>
    void EditResource(void* p, const std::string& name)
    {
      Handle<T>* data = reinterpret_cast<Handle<T>*>(p);
      const std::unordered_map<std::string, T*>& resources =
        ResourceManager<T>::GetAll();
      std::vector<const char*> strs;
      std::vector<std::string*> strings;
      
      strs.push_back("<none>");

      int idx = 0;
      int ctr = 0;
      for(auto r : resources)
      {
        strings.push_back(new std::string(r.first));
        strs.push_back(strings.back()->c_str());
        if(*data == r.second)
          idx = ctr + 1;
        ++ctr;
      }

      ImGui::Combo(name.c_str(), &idx, &strs[0], strs.size());

      if(idx == 0)
        *data = nullptr;
      else
        *data = resources.find(*strings[idx - 1])->second;

      for(auto s : strings)
        delete s;
    }

    /*! \overload */
    template <> 
    void EditType<Handle<Object> >(void* p, const std::string& name);

    /*! \overload */
    template <> 
    void EditType<Handle<Graphics::Transform> >(void* p, 
      const std::string& name);

    /*! \overload */
    template <>
    void EditType<Graphics::Transform>(void* p,
      const std::string& name);

    /*!
      \brief Assigns two variables of the same type.
      \param a The first variable.
      \param b The second variable.
    */
    template <typename T>
    void AssignType(Variable& a, Variable& b)
    {
      ASSERT(a.GetType() == b.GetType());
      const TypeData* td = a.GetType();
      if(td == nullptr || td->GetMembers().size() > 0)
      {
        for(auto mem : td->GetMembers())
        {
          Variable va(mem.second.GetType(), a.GetMember(mem.first));
          Variable vb(mem.second.GetType(), b.GetMember(mem.first));
          mem.second.GetType()->GetAssignFunc()(va, vb);
        }
      }
      else
      {
        *reinterpret_cast<T*>(a.GetData()) = 
          *reinterpret_cast<const T*>(b.GetData());
      }
    }

    template <typename T>
    void TypeData::AddType(const TypeData& type)
    {
      // Ensure the type hasn't already been added
      ASSERT(FindType(type.GetName()) == nullptr);

      // Add it to the lists
      types[type.GetName()] = type;
      ctTypes[type.hash] = type;
    }

    template <typename T>
    TypeData* TypeData::FindType()
    {
      // Find the type
      auto value = ctTypes.find(typeid(T).hash_code());

      // Ensure the type exists, otherwise return nullptr
      if(value != ctTypes.end())
        return &((*value).second);
      else
        return nullptr;
    }

    /*!
      \def INNER_REGISTER_TYPE(type)
      \brief Only for internal use in TypeData.inl.
    */
    #define INNER_REGISTER_TYPE(type)                                          \
      TypeData td = Echo::Core::TypeData(#type, typeid(type).hash_code());     \
      td.SetNewFunc(Echo::Core::NewType<type>);                                \
      td.SetDeleteFunc(Echo::Core::DeleteType<type>);                          \
      td.SetSerializeFunc(Echo::Core::SerializeType<type>);                    \
      td.SetDeserializeFunc(Echo::Core::DeserializeType<type>);                \
      td.SetEditorWidgetFunc(Echo::Core::EditType<type>);                      \
      td.SetAssignFunc(Echo::Core::AssignType<type>);                      

    /*!
      \def REGISTER_RESOURCE_HANDLE(type)
      \brief Only for internal use in TypeData.inl.
    */
    #define REGISTER_RESOURCE_HANDLE(type)                                     \
      do{                                                                      \
        TypeData td = Echo::Core::TypeData("Handle<"#type">",                  \
          typeid(Handle<type>).hash_code());                                   \
        td.SetNewFunc(Echo::Core::NewType<Handle<type> >);                     \
        td.SetDeleteFunc(Echo::Core::DeleteType<Handle<type> >);               \
        td.SetSerializeFunc(Echo::Core::SerializeResource<type>);              \
        td.SetDeserializeFunc(Echo::Core::DeserializeResource<type>);          \
        td.SetEditorWidgetFunc(Echo::Core::EditResource<type>);                \
        td.SetAssignFunc(Echo::Core::AssignType<Handle<type> >);               \
        Echo::Core::TypeData::AddType<Handle<type> >(td);                      \
                                                                               \
      }while(0)

    /*!
      \def REGISTER_TYPE(type)
      \brief Registers a type into the reflection system.
    */
    #define REGISTER_TYPE(type)                                                \
      do                                                                       \
      {                                                                        \
        INNER_REGISTER_TYPE(type)                                              \
        Echo::Core::TypeData::AddType<type>(td);                               \
      } while(0)
    
    /*!
      \def REGISTER_MEMBER(type, memberType, memberName)
      \brief Registers a member of a registered type.
      \param type The type name to register the member to.
      \param memberType The type of the member.
      \param memberName The name of the member.
    */
    #define REGISTER_MEMBER(type, memberType, memberName)                      \
      do                                                                       \
      {                                                                        \
        Echo::Core::Member m(Echo::Core::TypeData::FindType(#memberType),      \
          #memberName, offsetof(type, memberName));                            \
        Echo::Core::TypeData::FindType(#type)->AddMember(m);                   \
        Echo::Core::TypeData::FindType<type>()->AddMember(m);                  \
      } while(0)

    /*!
      \def REGISTER_RENAMED_MEMBER(type, memberType, memberName, memberRename)
      \brief Registers a member of a registered type with a different internal
        name.
      \param type The type name to register the member to.
      \param memberType The type of the member.
      \param memberName The name of the member.
      \param memberRename The name to use internally.
    */
    #define REGISTER_RENAMED_MEMBER(type, memberType, memberName, memberRename)\
      do                                                                       \
      {                                                                        \
        Echo::Core::Member m(Echo::Core::TypeData::FindType(#memberType),      \
          #memberRename, offsetof(type, memberName));                          \
        Echo::Core::TypeData::FindType(#type)->AddMember(m);                   \
        Echo::Core::TypeData::FindType<type>()->AddMember(m);                  \
      } while(0)

    /*!
      \def REGISTER_DERIVED_TYPE(type, parentType)
      \brief Registers a type derived from another type.
      \param type The type name.
      \param parentType The parent type name.
    */
    #define REGISTER_DERIVED_TYPE(type, parentType)                            \
      do                                                                       \
      {                                                                        \
        INNER_REGISTER_TYPE(type)                                              \
        td.AddParentType(Echo::Core::TypeData::FindType<parentType>());        \
        Echo::Core::TypeData::AddType<type>(td);                               \
      } while(0)
  }
}
