/*!
  \file Messaging.inl
  \author Chase A Rayment
  \date 12 April 2017
  \brief Contains the template definitions of the Messaging system.
*/

#include <unordered_map>
#include <vector>
#include "Handle.hpp"

namespace Echo
{
  namespace Core
  {
    namespace Messaging
    {
      /*!
        \brief A member function that can be called with an event.
      */
      struct Delegate
      {
        /*!
          \brief Calls this Delegate.
          \param evt The event to call the delegate with.
        */
        virtual void Call(Event& evt) = 0;

        /*!
          \brief Gets the listener of the Delegate.
        */
        virtual void* GetListener() = 0;

        /*!
          \brief Destroys this Delegate.
        */
        virtual ~Delegate() { }
      };

      /*!
        \brief A list of Delegates.
      */
      typedef std::vector<Delegate*> DelegateList;

      /*!
        \brief A map of channels to their lists of delegates.
      */
      typedef std::unordered_map<std::string, DelegateList> ChannelList;

      /*!
        \brief A map of scopes to their channels.
      */
      typedef std::unordered_map<void*, ChannelList> ConnectionMap;

      /*!
        \brief A globally-accessable list of events.
      */
      extern ConnectionMap Connections;

      /*!
        \brief A type of Delegate meant for events.
      */
      template <typename ListenerType, typename EventType>
      struct EventDelegate : public Delegate
      {
        /*!
          \brief A function for event handlers.
        */
        typedef void (ListenerType::*HandlerType)(EventType&);
        
        Handle<ListenerType> Listener; //!< The object this delegate corresponds to.
        HandlerType Handler; //!< The function this delegate calls.

        /*!
          \brief Calls the handler.
          \param evt The event to call the handler with.
        */
        void Call(Event& evt)
        {
		      if(Listener)
            (((ListenerType*)Listener)->*Handler)(dynamic_cast<EventType&>(evt));
        }

        /*!
          \brief Gets the listener this delegate corresponds to.
        */
        void* GetListener()
        {
          return (void*)Listener;
        }

        /*!
          \brief Creates a new EventDelegate.
        */
        EventDelegate(ListenerType* l, HandlerType h) 
          : Listener(l), Handler(h) { };  
      };
    }

    template <typename HandleType, typename ListenerType, typename EventType>
    void ConnectEvent(Handle<HandleType> scope, const std::string& channel, 
      ListenerType* listener, void (ListenerType::*handler)(EventType&))
    {
      ConnectEvent((void*)scope, channel, listener, handler);
    }

    template <typename ListenerType, typename EventType>
    void ConnectEvent(void* scope, const std::string& channel, 
      ListenerType* listener, void (ListenerType::*handler)(EventType&))
    {
      // Create the necessary lists if needed
      if(Messaging::Connections.find(scope) == Messaging::Connections.end())
        Messaging::Connections[scope] = Messaging::ChannelList();
      if(Messaging::Connections[scope].find(channel) == 
        Messaging::Connections[scope].end())
        Messaging::Connections[scope][channel] = Messaging::DelegateList();

      // Add the delegate
      Messaging::Delegate* d = new Messaging::EventDelegate
        <ListenerType, EventType>(listener, handler);
      Messaging::Connections[scope][channel].push_back(d);
        
    }

    template <typename HandleType>
    void DisconnectEvent(Handle<HandleType> scope, const std::string& channel, 
      void* listener)
    {
      DisconnectEvent((void*)scope, channel, listener);
    }

    template <typename EventType>
    void DispatchEvent(void* scope, const std::string& channel, 
      EventType& event)
    {
      // Return early if the scope/channel aren't found
      if(Messaging::Connections.find(scope) == Messaging::Connections.end())
        return;
      if(Messaging::Connections[scope].find(channel) == 
        Messaging::Connections[scope].end())
        return;

      Messaging::DelegateList& delList = Messaging::Connections[scope][channel];

      // Send event to all delegates
      for(auto delegate : delList)
        delegate->Call(event);
    }
  }
}