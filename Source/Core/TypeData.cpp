/*!
  \file TypeData.cpp
  \author Chase A Rayment
  \date 5 April 2017
  \brief Contains the implementation of the TypeData class.
*/

#include "TypeData.hpp"
#include "Debug.hpp"
#include "Variable.hpp"
#include "Object.hpp"
#include "Space.hpp"
#include "Archetype.hpp"
#include <sstream>
#include "ResourceManager.hpp"

namespace Echo
{
  namespace Core
  {
    std::unordered_map<std::string, TypeData> TypeData::types;
    std::unordered_map<size_t, TypeData> TypeData::ctTypes;

    TypeData::TypeData() : name(""), members(), newfunc(nullptr), 
      delfunc(nullptr), serialfunc(nullptr), deserialfunc(nullptr),
      editfunc(nullptr), parent_types()
    { 

    }

    TypeData::TypeData(const std::string& n, size_t h) : name(n), hash(h),
      members(), newfunc(nullptr), delfunc(nullptr), serialfunc(nullptr), 
      deserialfunc(nullptr), editfunc(nullptr), parent_types()
    { 

    }

    bool TypeData::operator==(const TypeData& rhs) const
    {
      return hash == rhs.hash;
    }

    bool TypeData::operator!=(const TypeData& rhs) const
    {
      return hash != rhs.hash;
    }

    const std::string& TypeData::GetName() const
    {
      return name;
    }

    size_t TypeData::GetHash() const
    {
      return hash;
    }

    void TypeData::AddMember(const Member& member)
    {
      // Ensure the member hasn't already been added
      ASSERT(members.find(member.GetName()) == members.end());

      members[member.GetName()] = member;
    }

    const std::unordered_map<std::string, Member>& TypeData::GetMembers() const
    {
      return members;
    }

    void TypeData::SetNewFunc(NewFunc func)
    {
      newfunc = func;
    }

    TypeData::NewFunc TypeData::GetNewFunc(void) const
    {
      return newfunc;
    }

    void TypeData::SetDeleteFunc(DeleteFunc func)
    {
      delfunc = func;
    }

    TypeData::DeleteFunc TypeData::GetDeleteFunc(void) const
    {
      return delfunc;
    }

    void TypeData::SetSerializeFunc(SerializeFunc func)
    {
      serialfunc = func;
    }

    TypeData::SerializeFunc TypeData::GetSerializeFunc(void) const
    {
      return serialfunc;
    }

    void TypeData::SetDeserializeFunc(DeserializeFunc func)
    {
      deserialfunc = func;
    }

    TypeData::DeserializeFunc TypeData::GetDeserializeFunc(void) const
    {
      return deserialfunc;
    }

    void TypeData::SetEditorWidgetFunc(TypeData::EditorWidgetFunc func)
    {
      editfunc = func;
    }

    TypeData::EditorWidgetFunc TypeData::GetEditorWidgetFunc(void) const
    {
      return editfunc;
    }

    void TypeData::SetAssignFunc(TypeData::AssignFunc func)
    {
      assignfunc = func;
    }

    TypeData::AssignFunc TypeData::GetAssignFunc(void) const
    {
      return assignfunc;
    }

    TypeData* TypeData::FindType(const std::string& name)
    {
      // Look through all the types and find one with the given name
      for(auto& type : types)
      {
        if(type.second.GetName() == name)
          return &type.second;
      }
      return nullptr;
    }

    TypeData* TypeData::FindType(size_t hash)
    {
      // Find the type
      auto value = ctTypes.find(hash);

      // Ensure the type exists, otherwise return nullptr
      if(value != ctTypes.end())
        return &((*value).second);
      else
        return nullptr;
    }

    template <>
    void SerializeType<int>(Variable var, Json::Value& val)
    {
      val = *reinterpret_cast<const int*>(var.GetData());
    }

    template <>
    void SerializeType<unsigned>(Variable var, Json::Value& val)
    {
      val = *reinterpret_cast<const unsigned*>(var.GetData());
    }

    template <>
    void SerializeType<std::string>(Variable var, Json::Value& val)
    {
      val = *reinterpret_cast<const std::string*>(var.GetData());
    }

    template <>
    void SerializeType<float>(Variable var, Json::Value& val)
    {
      val = *reinterpret_cast<const float*>(var.GetData());
    }

    template <>
    void SerializeType<bool>(Variable var, Json::Value& val)
    {
      val = *reinterpret_cast<const bool*>(var.GetData());
    }

    template <>
    void SerializeType<Object>(Variable var, Json::Value& val)
    {
      Object* o = reinterpret_cast<Object*>(var.GetData());
      val["Name"] = o->GetName();

      // Recursively serialize all registered members
      for(auto& member : var.GetType()->GetMembers())
      {
        void* memberData = reinterpret_cast<char*>(var.GetData())
          + member.second.GetOffset();
        member.second.GetType()->GetSerializeFunc()(
          Variable(member.second.GetType(), memberData), 
          val[member.second.GetName()]);
      }

      std::vector<Handle<Graphics::Transform> > children = 
        o->Transform.GetChildren();

      Handle<Archetype> arch;
      if(o->ArchetypeName != "")
        arch = ResourceManager<Archetype>::Find(o->ArchetypeName);
      
      if(arch == nullptr)
      {
        for(auto comp : o->components)
        {
          const TypeData* td = TypeData::FindType(comp.first);
          Variable v(td, comp.second);
          td->GetSerializeFunc()(v, val["Components"][td->GetName()]);
        }

        for(int i = 0; i < children.size(); ++i)
        {
          Variable v(TypeData::FindType("Object"), 
            (void*)children[i]->GetObject());
          v.Serialize(val["Children"]
            [std::to_string(children[i]->GetObject()->GetID())]);
        }
      }
      else
      {
        val = arch->GetDifferences(Handle<Object>(o));
      }
    }

    template <>
    void SerializeType<Space>(Variable var, Json::Value& val)
    {
      Space* s = reinterpret_cast<Space*>(var.GetData());
      for(unsigned i = 0; i < Space::MaxObjects; ++i)
      {
        if(s->objects[i].active && s->objects[i].name != "EditorCamera"
          && s->objects[i].Transform.GetParent() == nullptr)
        {
          Variable v(TypeData::FindType<Object>(), s->objects + i);
          std::stringstream ss;
          ss << i;
          v.Serialize(val["Objects"][ss.str()]);
        }
      }
    }

    template <>
    void SerializeType<glm::vec3>(Variable var, Json::Value& val)
    {
      const glm::vec3* v = reinterpret_cast<const glm::vec3*>(var.GetData());
      std::stringstream ss;
      ss << "[ " << v->x << " " << v->y << " " << v->z << " ]";
      val = ss.str();
    }

    template <>
    void SerializeType<glm::vec4>(Variable var, Json::Value& val)
    {
      const glm::vec4* v = reinterpret_cast<const glm::vec4*>(var.GetData());
      std::stringstream ss;
      ss << "[ " << v->x << " " << v->y << " " << v->z << " " << v->w << " ]";
      val = ss.str();
    }

    template <>
    void SerializeType<Handle<Object> >(Variable var, Json::Value& val)
    {
      const Handle<Object>* v = 
        reinterpret_cast<const Handle<Object>*>(var.GetData());
      if(*v == nullptr)
        val = -1;
      else
        val = (int)(&(**v) - (*v)->GetSpace()->objects);
    }

    template <>
    void SerializeType<Handle<Graphics::Transform> >
      (Variable var, Json::Value& val)
    {
      const Handle<Graphics::Transform>* t = 
        reinterpret_cast<const Handle<Graphics::Transform>*>(var.GetData());
      if(*t == nullptr)
      {
        val = -1;
        return;
      }
      Handle<Object> o = (*t)->GetObject();
      Variable v(TypeData::FindType("Handle<Object>"), &o);
      v.Serialize(val);
    }


    template <>
    void DeserializeType<int>(const Json::Value& val, Variable& var)
    {
      *reinterpret_cast<int*>(var.GetData()) = val.asInt();
    }

    template <>
    void DeserializeType<unsigned>(const Json::Value& val, Variable& var)
    {
      *reinterpret_cast<unsigned*>(var.GetData()) = val.asUInt();
    }

    template <>
    void DeserializeType<float>(const Json::Value& val, Variable& var)
    {
      *reinterpret_cast<float*>(var.GetData()) = val.asFloat();
    }

    template <>
    void DeserializeType<bool>(const Json::Value& val, Variable& var)
    {
      *reinterpret_cast<bool*>(var.GetData()) = val.asBool();
    }

    template <>
    void DeserializeType<std::string>(const Json::Value& val, Variable& var)
    {
      *reinterpret_cast<std::string*>(var.GetData()) = val.asString();
    }

    template <>
    void DeserializeType<Object>(const Json::Value& val, Variable& var)
    {
      Object* o = reinterpret_cast<Object*>(var.GetData());
      o->SetName(val["Name"].asString());

      // Recursively deserialize registered members
      std::unordered_map<std::string, Member> members = 
        var.GetType()->GetMembers();
      for(std::string sub : val.getMemberNames())
      {
        Member m = members[sub];
        void* memberData = reinterpret_cast<char*>(var.GetData())
          + m.GetOffset();
        Variable v(m.GetType(), memberData);
        if(m.GetType())
          m.GetType()->GetDeserializeFunc()(val[sub], v);
      }

      // Apply archetype
      if(o->ArchetypeName != "")
      {
        Handle<Archetype> arch = 
          ResourceManager<Archetype>::Find(o->ArchetypeName);
          
        if(arch)
          arch->ApplyArchetype(o);
      }

      for(std::string sub : val["Components"].getMemberNames())
      {
        Handle<Component> h = o->AddComponent(sub);
        Variable v(TypeData::FindType(sub), static_cast<Component*>(h));
        v.Deserialize(val["Components"][sub]);
      }

      // Recursively deserialize children
      for(std::string sub : val["Children"].getMemberNames())
      {
        Object* p = Space::curr_loading_space->objects + atoi(sub.c_str());
        p->active = true;
        p->space = Space::curr_loading_space;
        Variable v(TypeData::FindType<Object>(), 
          Space::curr_loading_space->objects + atoi(sub.c_str()));
        v.Deserialize(val["Children"][sub]);
        p->Transform.SetParent(&o->Transform);
      }
    }

    template <>
    void DeserializeType<Space>(const Json::Value& val, Variable& var)
    {
      Space* s = reinterpret_cast<Space*>(var.GetData());
      for(std::string sub : val["Objects"].getMemberNames())
      {
        Object* o = s->objects + atoi(sub.c_str());
        o->active = true;
        o->space = s;
        Variable v(TypeData::FindType<Object>(), 
          s->objects + atoi(sub.c_str()));
        v.Deserialize(val["Objects"][sub]);
      }
    }

    template <>
    void DeserializeType<glm::vec3>(const Json::Value& val, Variable& var)
    {
      glm::vec3* out = reinterpret_cast<glm::vec3*>(var.GetData());
      sscanf(val.asString().c_str(), "[ %f %f %f ]", &out->x, &out->y, &out->z);
    }

    template <>
    void DeserializeType<glm::vec4>(const Json::Value& val, Variable& var)
    {
      glm::vec4* out = reinterpret_cast<glm::vec4*>(var.GetData());
      sscanf(val.asString().c_str(), "[ %f %f %f %f ]", &out->x, &out->y, 
        &out->z, &out->w);
    }

    template <>
    void DeserializeType<Handle<Object> >(const Json::Value& val, Variable& var)
    {
      if(val.asInt() == -1)
        *reinterpret_cast<Handle<Object>*>(var.GetData()) = nullptr;
      else
      {
        *reinterpret_cast<Handle<Object>*>(var.GetData()) = 
          Handle<Object>(Space::curr_loading_space->objects + val.asInt());
      }
    }

    template <>
    void DeserializeType<Handle<Graphics::Transform> >
      (const Json::Value& val, Variable& var)
    {
      Handle<Object> o;
      Variable v(TypeData::FindType("Handle<Object>"), &o);
      v.Deserialize(val);
      if(o == nullptr)
        *reinterpret_cast<Handle<Graphics::Transform>*>
          (var.GetData()) = nullptr;
      else
        *reinterpret_cast<Handle<Graphics::Transform>*>
          (var.GetData()) = &o->Transform;
    }

    template <> 
    void EditType<int>(void* p, const std::string& name)
    {
      int* data = reinterpret_cast<int*>(p);
      ImGui::InputInt(name.c_str(), data);
    }

    template <> 
    void EditType<float>(void* p, const std::string& name)
    {
      float* data = reinterpret_cast<float*>(p);
      ImGui::DragFloat(name.c_str(), data, 0.01f);
    }

    template <> 
    void EditType<double>(void* p, const std::string& name)
    {
      double* data = reinterpret_cast<double*>(p);
      float f = (float)*data;
      ImGui::DragFloat(name.c_str(), &f);
      *data = f;
    }

    template <> 
    void EditType<bool>(void* p, const std::string& name)
    {
      bool* data = reinterpret_cast<bool*>(p);
      ImGui::Checkbox(name.c_str(), data);
    }

    template <> 
    void EditType<std::string>(void* p, const std::string& name)
    {
      std::string* data = reinterpret_cast<std::string*>(p);
      char text[1024];
      std::strcpy(text, data->c_str());
      ImGui::InputText(name.c_str(), text, 1024);
      *data = std::string(text);
    }

    template <> 
    void EditType<glm::vec3>(void* p, const std::string& name)
    {
      glm::vec3* data = reinterpret_cast<glm::vec3*>(p);
      ImGui::DragFloat3(name.c_str(), &data->x);
    }

    template <>
    void EditType<glm::vec4>(void* p, const std::string& name)
    {
      glm::vec4* data = reinterpret_cast<glm::vec4*>(p);
      ImGui::ColorEdit4(name.c_str(), &data->r);
    }

    template <>
    void EditType<Handle<Object> >(void* p, const std::string& name)
    {
      Handle<Object>* data = reinterpret_cast<Handle<Object>*>(p);
      std::vector<Handle<Object> > objs = Spaces[SPACE_GAMEPLAY]
        .GetAllObjects();
      int ind = -1;
      std::vector<const char*> strings;
      std::vector<std::string*> strs;
      std::string names;
      strings.push_back("<none>");
      for(unsigned i = 0; i < objs.size(); ++i)
      {
        strs.push_back(new std::string(std::to_string(i) + std::string(": ")
          + objs[i]->GetName()));
        strings.push_back(strs.back()->c_str());
        if(objs[i] == *data)
          ind = i + 1;
      }
      ImGui::Combo(name.c_str(), &ind, &strings[0], strings.size());
      if(ind == -1 || ind == 0)
        *data = nullptr;
      else
        *data = objs[ind - 1];
      for (auto str : strs)
        delete str;
    }

    template <> 
    void EditType<Handle<Graphics::Transform> >
      (void* p, const std::string& name)
    {
      Handle<Graphics::Transform>* data = 
        reinterpret_cast<Handle<Graphics::Transform>*>(p);
      Handle<Object> o = *data == nullptr ? nullptr : (*data)->GetObject();
      EditType<Handle<Object> >(&o, name);
      if(o == nullptr)
        *data = nullptr;
      else
        *data = &o->Transform;
    }

    template <>
    void EditType<Graphics::Transform>(void* p,
      const std::string&)
    {
      Graphics::Transform* data =
        reinterpret_cast<Graphics::Transform*>(p);
      EditType<glm::vec3>(&data->position, "Position");
      glm::vec3 rot = glm::degrees(data->GetRotationEuler());
      EditType<glm::vec3>(&rot, "Rotation");
      data->SetRotationEuler(glm::radians(rot));
      EditType<glm::vec3>(&data->scale, "Scale");
      data->dirty = true;
      Handle<Graphics::Transform> o = data->parent;
      Handle<Graphics::Transform> orig = o;
      EditType<Handle<Graphics::Transform> >(&o, "Parent");
      if (o != orig)
        data->SetParent(o);
    }

    void TypeData::AddParentType(const TypeData* type)
    {
      parent_types.push_back(type);
    }

    bool TypeData::IsDerivedFrom(const TypeData* type) const
    {
      for(auto t : parent_types)
      {
        if(*t == *type || t->IsDerivedFrom(type))
          return true;
      }
      return false;
    }

    const std::unordered_map<std::string, TypeData>& TypeData::GetAllTypes()
    {
      return types;
    }

    std::unordered_map<std::string, const TypeData*> 
      TypeData::GetAllTypesDerivedFrom(const TypeData* parent)
    {
      std::unordered_map<std::string, const TypeData*> result;
      for(auto& t : types)
      {
        if(t.second.IsDerivedFrom(parent))
          result[t.second.GetName()] = &t.second;
      }
      return result;
    }
  }
}