/*!
  \file DevConsole.cpp
  \author Chase A Rayment
  \date 20 June 2017
  \brief Contains the implementation of the DevConsole system.
*/

#include "DevConsole.hpp"
#include "Input.hpp"
#include "imgui/imgui.h"
#include "Log.hpp"
#include <vector>
#include <unordered_map>
#include <sstream>
#include <vector>
#include <iterator>
#include "Space.hpp"
#include "PhysicsScene.hpp"
#include "AudioCore.hpp"
#include "FPSDisplay.hpp"

namespace Echo
{
  namespace Core
  {
    namespace DevConsole
    {
      static bool active = false;
      static std::vector<std::string> lines;
      static std::string curr_text;
      static bool just_pushed_newline = false;
      static ImVec4 curr_color = ImVec4(1, 1, 1, 1);
      static std::vector<ImVec4> colors;
      typedef bool (*CommandFunc)(const std::vector<std::string>&);
      static std::unordered_map<std::string, CommandFunc> funcs;
      static std::unordered_map<std::string, std::string> helptext;
      static std::unordered_map<std::string, std::string> shortdesc;
      bool just_opened = false;

      template<typename T>
      static void split(const std::string &s, char delim, T result) {
          std::stringstream ss;
          ss.str(s);
          std::string item;
          while (std::getline(ss, item, delim)) {
              *(result++) = item;
          }
      }

      static std::vector<std::string> split(const std::string &s, char delim) {
          std::vector<std::string> elems;
          split(s, delim, std::back_inserter(elems));
          return elems;
      }

      static bool help(const std::vector<std::string>& args)
      {
        if(args.size() == 0)
        {
          for(auto text : shortdesc)
            Log << text.first << ": " << text.second << std::endl;
        }
        else
        {
          if(funcs.find(args[0]) == funcs.end())
            Log << "Command '" << args[0] << "' not found. Type 'help' for a"
              " list of commands." << std::endl;
          else
            Log << helptext[args[0]] << std::endl;
        }
        return true;
      }

      static bool map(const std::vector<std::string>& args)
      {
        if(args.size() == 0)
          return false;
        Spaces[SPACE_GAMEPLAY].LoadLevel(args[0]);
        Spaces[SPACE_GAMEPLAY].Initialize();
        return true;
      }

      static bool phy_debug(const std::vector<std::string>& args)
      {
        if(args.size() == 0)
          return false;
        for(unsigned i = 0; i < SPACE_COUNT; ++i)
        {
          if(Spaces[i].GetActive())
          {
            Spaces[i].GetPhysicsScene()->GetDebugDrawer()->setDebugMode(
              atoi(args[0].c_str()));
          }
        }
        return true;
      }

      static bool aud_postevent(const std::vector<std::string>& args)
      {
        if(args.size() == 0)
          return false;
        Audio::PostAudioEvent(args[0]);
        return true;
      }

      static bool aud_setrtpc(const std::vector<std::string>& args)
      {
        if(args.size() < 2)
          return false;
        float val;
        sscanf(args[1].c_str(), "%f", &val);
        Audio::SetRTPC(args[0], val);
        return true;
      }

      static bool showfps(const std::vector<std::string>& args)
      {
        if(args.size() == 0)
          FPSDisplay::SetMode(FPSDisplay::Mode::Simple);
        else
        {
          unsigned val;
          sscanf(args[0].c_str(), "%u", &val);
          FPSDisplay::SetMode((FPSDisplay::Mode)val);
        }
        return true;
      }

      void Initialize()
      {
        funcs["help"] = help;
        shortdesc["help"] = "Displays help text about a command.";
        helptext["help"] = "Usage: help [commandname]\nDisplays help text about"
          " commandname.";

        funcs["map"] = map;
        shortdesc["map"] = "Loads a given map.";
        helptext["map"] = "Usage: map [mapname]\nLoads mapname into the "
          "gameplay space.";

        funcs["phy_debug"] = phy_debug;
        shortdesc["phy_debug"] = "Sets the physics debug drawing mode.";
        helptext["phy_debug"] = "Usage: phy_debug [mode]\nSets the physics "
          "debug drawing mode to the given number.";

        funcs["aud_postevent"] = aud_postevent;
        shortdesc["aud_postevent"] = "Posts a Wwise audio event.";
        helptext["aud_postevent"] = "Usage: aud_postevent [name]\nPosts the "
          "Wwise event with the given name.";

        funcs["aud_setrtpc"] = aud_setrtpc;
        shortdesc["aud_setrtpc"] = "Sets a Wwise RTPC.";
        helptext["aud_setrtpc"] = "Usage: aud_setrtpc [name] [value]\nSets the"
          " Wwise RTPC with the given name to the given value.";

        funcs["showfps"] = showfps;
        shortdesc["showfps"] = "Shows or hides the FPS counter.";
        helptext["showfps"] = "Usage: showfps [(optional) type]\nShows the"
          " FPS display with the given verbosity. 0 is disabled, 1 is a simple"
          " counter, 2 displays minimum and maximum framerates, and 3 shows "
          " a graph.";
      }

      void Update()
      {
        if(Input::Keyboard::KeyIsPressed(SDL_SCANCODE_GRAVE, true))
        {
          active = !active;
          if(active) just_opened = true;
          Spaces[SPACE_GAMEPLAY].SetPaused(active);
        }
        
        if(active)
        {
          ImGui::Begin("Developer Console");

          char text[1024] = { 0 };

          ImGui::BeginChild("DeveloperConsoleLines", ImVec2(0, -40));

          for(unsigned i = 0; i < lines.size(); ++i)
            ImGui::TextColored(colors[i], lines[i].c_str());
      
          for(int i = 0; i < 20 - (int)lines.size(); ++i)
            ImGui::Text("");
            
          ImGui::Separator();

          if(just_pushed_newline)
          {
            if(ImGui::GetScrollY() == ImGui::GetScrollMaxY())
              ImGui::SetScrollHere(1.0f);
            just_pushed_newline = false;
          }

          if(just_opened)
            ImGui::SetScrollHere(1.0f);

          ImGui::EndChild();

          if(ImGui::InputText("##DevConsoleInput", text, 1024, 
            ImGuiInputTextFlags_EnterReturnsTrue))
          {
            Log << "> " << text << std::endl;
            std::vector<std::string> tokens = split(std::string(text), ' ');
            std::string command = tokens[0];
            if(funcs.find(command) != funcs.end())
            {
              tokens.erase(tokens.begin());
              if(!funcs[command](tokens))
              {
                Log << "Invalid usage." << std::endl;
                Log << helptext[command] << std::endl;
              }
            }
            else
            {
              Log << "Command '" << command << "' not found. Type 'help' for a"
                " list of commands." << std::endl;
            }

            ImGui::SetKeyboardFocusHere();
          }

          if(just_opened)
          {
            ImGui::SetKeyboardFocusHere();
            just_opened = false;
          }

          ImGui::End();
        }
      }

      void PushText(const std::string& text)
      {
        curr_text += text;
      }

      void PushNewline()
      {
        lines.push_back(curr_text);
        colors.push_back(curr_color);
        curr_text = "";
        just_pushed_newline = true;
        PushTextColor(1, 1, 1, 1);
      }

      void PushTextColor(float x, float y, float z, float w)
      {
        curr_color = ImVec4(x, y, z, w);
      }
    }
  }
}