/*!
  \file Game.cpp
  \author Chase A Rayment
  \date 12 April 2017
  \brief Contains the implementation of the Game functions.
*/

#include "Game.hpp"
#include <chrono>
#include <thread>
#include "Space.hpp"
#include "FileSystem.hpp"
#include "GraphicsCore.hpp"
#include "QuitEvent.hpp"
#include "Input.hpp"
#include "Actions.hpp"
#include "AudioCore.hpp"
#include "Archetype.hpp"
#include "PhysicsCore.hpp"
#include "ResourceManager.hpp"
#include "DevConsole.hpp"
#include "FPSDisplay.hpp"

namespace Echo
{
  namespace Core
  {
    namespace Game
    {
      static bool ShouldQuit = false;
      static float FramerateCap = 60.0f;
      static float DeltaTime = 1.0f / FramerateCap;
      static bool EditorMode = false;
      static float Framerate = 60.0f;
      static float AverageFramerate = 60.0f;
      static const unsigned AverageFramerateSamples = 15;

      struct QuitListener
      {
        void Initialize()
        {
          ConnectEvent(nullptr, "Quit", this, &QuitListener::HandleQuit);
        }

        void HandleQuit(QuitEvent&)
        {
          ShouldQuit = true;
        }
      };

      static QuitListener quit;
      
      void Initialize()
      {
        RegisterTypes();
        FileSystem::Initialize();
        Log.Init("Log.txt");
        Graphics::Initialize();
        Input::Initialize();
        Audio::Initialize();
        Physics::Initialize();
        quit.Initialize();
        DevConsole::Initialize();
        LoadAllResources();
        Core::Spaces[Core::SPACE_DEBUG].Initialize();
        FPSDisplay::Initialize();
      }

      void Run()
      {
        // Run the game while quit hasn't been requested
        while(!ShouldQuit)
        {
          // Get the frame start time
          auto frameStart = std::chrono::high_resolution_clock::now();

          // Update all spaces
          for(unsigned i = 0; i < SPACE_COUNT; ++i)
          {
            if(Spaces[i].active)
              Spaces[i].Update(DeltaTime);
          }

          // Update systems
          Input::Update();
          FPSDisplay::Update();
          Graphics::Update();
          Audio::Update();
          Actions::Update(DeltaTime);
          Physics::Update(DeltaTime);
          DevConsole::Update();

          // Wait for the next frame
          auto timeToWait = 
            frameStart + std::chrono::duration<float>(1.0f / FramerateCap);
          std::this_thread::sleep_until(timeToWait);

          // Calculate the delta time
          DeltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - frameStart).count() 
            / 1000.0f;

          // Calculate framerate
          Framerate = 1.0f / DeltaTime;
          AverageFramerate = 
            ((AverageFramerateSamples - 1.0f) / AverageFramerateSamples)
            * AverageFramerate + (1.0f / AverageFramerateSamples) * Framerate;
        }
      }

      void Terminate()
      {
        // Terminate all spaces
        for (unsigned i = 0; i < SPACE_COUNT; ++i)
          if (Spaces[i].active)
            Spaces[i].Terminate();
        UnloadAllResources();
        Physics::Terminate();
        FPSDisplay::Terminate();
        Graphics::Terminate();
        Audio::Terminate();
        Log.Terminate();
      }

      void Quit()
      {
        ShouldQuit = true;
      }

      float GetFramerateCap()
      {
        return FramerateCap;
      }

      void SetFramerateCap(float val)
      {
        FramerateCap = val;
      }

      void SetEditorMode(bool val)
      {
        EditorMode = val;
      }

      bool GetEditorMode()
      {
        return EditorMode;
      }

      float GetAverageFramerate()
      {
        return AverageFramerate;
      }
    }
  }
}