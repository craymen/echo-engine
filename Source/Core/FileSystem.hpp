/*!
  \file FileSystem.hpp
  \author Chase A Rayment
  \date 12 April 2017
  \brief Contains the definition of the FileSystem paths.
*/
#pragma once

#include <string>
#include <vector>
#include <windows.h>

namespace Echo
{
  namespace Core
  {
    namespace FileSystem
    {
      /*!
        \brief Contains all the directories the game accesses.
      */
      enum class Dir { DocsRoot, Saves, Logs, Settings, AssetsRoot, Models,
        Textures, Fonts, SoundBanks, Shaders, Archetypes, Levels, Materials, 
        Cubemaps };

      /*!
        \brief Initializes the Filesystem paths.
      */
      void Initialize();

      /*!
        \brief Gets a game directory.
        \param dir The directory to get.
      */
      const std::string& GetGameDir(Dir dir);

      /*!
        \brief Gets all files in a directory.
        \param root The directory to search in
        \param recursive Whether or not the search recurses in subdirectories
      */
      std::vector<std::string> GetAllFiles(const std::string& root, 
        bool recursive = true);

      /*!
        \brief Gets the last file write time of a given file.
        \param file The file to query.
        \returns A FILETIME struct containing the last write time.
      */
      FILETIME GetLastFileWrite(const std::string& file);
    }
  }
}