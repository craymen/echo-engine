/*!
  \file Actions.inl
  \author Chase A Rayment
  \date 22 May 2017
  \brief Contains the template implementations for the Actions system.
*/

#include "Log.hpp"

namespace Echo
{
  namespace Core
  {
    namespace Actions
    {
      template <typename T>
      ActionProperty<T>::ActionProperty(T* d, const T& fv, float t, 
        typename ActionProperty<T>::EaseFunc e) : data(d), initial_val(*d), final_val(fv),
        total_time(t), curr_time(0.0f), ease(e), status(Status::NotStarted)
      {

      }

      template <typename T>
      void ActionProperty<T>::Start()
      {
        status = Status::InProgress;
        initial_val = *data;
      }

      template <typename T>
      void ActionProperty<T>::Update(float dt)
      {
        curr_time += dt;
        if(curr_time < total_time)
          *data = ease(initial_val, final_val, curr_time / total_time);
        else
        {
          *data = final_val;
          status = Status::Complete;
        }
      }

      template <typename T>
      Action::Status ActionProperty<T>::GetStatus()
      {
        return status;
      }

      template <typename T>
      T EaseLinear(const T& start, const T& end, float t)
      {
        return start + (end - start) * t;
      }
    }
  }
}

