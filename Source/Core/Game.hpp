/*!
  \file Game.hpp
  \author Chase A Rayment
  \date 12 April 2017
  \brief Contains the definition of the Game functions.
*/

namespace Echo
{
  namespace Core
  {
    namespace Game
    {
      /*!
        \brief Initializes the game.
      */
      void Initialize();

      /*!
        \brief Runs the game.
      */
      void Run();

      /*!
        \brief Terminates the game.
      */
      void Terminate();

      /*!
        \brief Quits the game.
      */
      void Quit();

      /*!
        \brief Gets the current framerate cap.
      */
      float GetFramerateCap();

      /*!
        \brief Sets the current framerate cap.
      */
      void SetFramerateCap(float val);

      /*!
        \brief Sets whether or not the editor is active.
        \bool val Whether or not the editor is active.
      */
      void SetEditorMode(bool val);

      /*!
        \brief Gets whether or not the editor is active.
      */
      bool GetEditorMode();

      /*!
        \brief Gets the average framerate.
      */
      float GetAverageFramerate();
    }
  }
}