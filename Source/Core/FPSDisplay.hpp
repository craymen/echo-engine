/*!
  \file FPSDisplay.hpp
  \author Chase A Rayment
  \date 18 January 2017
  \brief Contains the definition of the FPSDisplay class.
*/
#pragma once

namespace Echo
{
  namespace Core
  {
    namespace FPSDisplay
    {
      enum class Mode { Disabled, Simple, Verbose, Graph };
  
      void Initialize();
      void Terminate();
      void Update();
      void SetMode(Mode m);
    }
  }
}