/*!
  \file FileSystem.cpp
  \author Chase A Rayment
  \date 12 April 2017
  \brief Contains the implementation of FileSystem.
*/

#include "FileSystem.hpp"

// TODO: Port to Linux
#ifdef _WIN32
  #include <windows.h>
  #include <shlobj.h>
#endif

#include <sstream>
#include <vector>
#include <locale>
#include <codecvt>
#include <cstdlib>
#include "dirent.h"

namespace Echo
{
  namespace Core
  {
    namespace FileSystem
    {
      static std::vector<std::string> Dirs;

      void Initialize()
      {
        // Get the MyDocuments folder.
      #ifdef _WIN32
        wchar_t* docsStr;
        SHGetKnownFolderPath(FOLDERID_Documents, 0, NULL, &docsStr);
        std::wstringstream wss, wssS, wssL, wssSe;
        wss << docsStr << L"/BlockAndLoad";
        std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
        Dirs.push_back(converter.to_bytes(wss.str()));
        wssS << wss.str() << L"/Saves";
        wssL << wss.str() << L"/Logs";
        wssSe << wss.str() << L"/Settings";
        // Create MyDocs directories if they don't exist
        CreateDirectory(wss.str().c_str(), 0);
        CreateDirectory(wssS.str().c_str(), 0);
        CreateDirectory(wssL.str().c_str(), 0);
        CreateDirectory(wssSe.str().c_str(), 0);
      #endif

      #ifdef __linux__
        Dirs.push_back(getenv("HOME"));
      #endif

        Dirs.push_back(Dirs[static_cast<unsigned>(Dir::DocsRoot)] + "/Saves");
        Dirs.push_back(Dirs[static_cast<unsigned>(Dir::DocsRoot)] + "/Logs");
        Dirs.push_back(Dirs[static_cast<unsigned>(Dir::DocsRoot)] + "/Settings");
        Dirs.push_back("../Assets");
        Dirs.push_back(Dirs[static_cast<unsigned>(Dir::AssetsRoot)] + "/Models");
        Dirs.push_back(Dirs[static_cast<unsigned>(Dir::AssetsRoot)] + "/Textures");
        Dirs.push_back(Dirs[static_cast<unsigned>(Dir::AssetsRoot)] + "/Fonts");
        Dirs.push_back(Dirs[static_cast<unsigned>(Dir::AssetsRoot)] + "/SoundBanks");
        Dirs.push_back(Dirs[static_cast<unsigned>(Dir::AssetsRoot)] + "/Shaders");
        Dirs.push_back(Dirs[static_cast<unsigned>(Dir::AssetsRoot)] + "/Archetypes");
        Dirs.push_back(Dirs[static_cast<unsigned>(Dir::AssetsRoot)] + "/Levels");
        Dirs.push_back(Dirs[static_cast<unsigned>(Dir::AssetsRoot)] + "/Materials");
        Dirs.push_back(Dirs[static_cast<unsigned>(Dir::AssetsRoot)] + "/Cubemaps");
      }

      const std::string& GetGameDir(Dir dir)
      {
        return Dirs[static_cast<unsigned>(dir)];
      }

      std::vector<std::string> GetAllFiles(const std::string& root, 
        bool recursive)
      {
        std::vector<std::string> result;
        DIR* dir;
        dirent* ent;

        // Open directory
        dir = opendir(root.c_str());
        
        if(dir != NULL)
        {
          while((ent = readdir(dir)) != NULL)
          {
            switch(ent->d_type)
            {
              case DT_REG:
                result.push_back(root + "/" + 
                  std::string(ent->d_name));
                break;
              case DT_DIR:
                if(recursive && std::string(ent->d_name) != "." 
                  && std::string(ent->d_name) != "..")
                {
                  // Recurse into the directory
                  for(auto item : GetAllFiles(root + "/" + 
                    std::string(ent->d_name), true))
                    result.push_back(item);
                }
              case DT_LNK:
                // Not concerned about symlinks.
                break;
              
              default:
                break;
            }
          }
        }

        closedir(dir);

        return result;
      }

      FILETIME GetLastFileWrite(const std::string& f)
      {
        // Open the file
        HANDLE file = CreateFileA(f.c_str(), GENERIC_READ, FILE_SHARE_READ,
          NULL, OPEN_EXISTING, 0, NULL);

        if(file == INVALID_HANDLE_VALUE)
          return FILETIME();

        FILETIME create, access, write;
        if(!GetFileTime(file, &create, &access, &write))
          return FILETIME();

        CloseHandle(file);

        return write;
      }
    }
  }
}