/*!
  \file Archetype.cpp
  \author Chase A Rayment
  \date 22 May 2017
  \brief Contains the implementation of the Archetype class.
*/

#include "Archetype.hpp"
#include <unordered_map>
#include <fstream>
#include "FileSystem.hpp"
#include "Log.hpp"
#include "TypeData.hpp"
#include "Transform.hpp"

namespace Echo
{
  namespace Core
  {
    Archetype::Archetype(const std::string& path) : Resource(path), val()
    {
      // Open the file
      std::ifstream file(path);

      // Check for error
      if(!file.is_open())
      {
        Core::Log << Core::Priority::Warning << "Could not open Archetype "
          << path << std::endl;
        return;
      }

      // Load the JSON value
      file >> val;
    }

    const Json::Value& Archetype::GetJson()
    {
      return val;
    }

    static void apply(Handle<Object> obj, Json::Value val)
    {
      if(obj->Transform.GetParent() != nullptr)
      {
        obj->SetName(val["Name"].asString());
        Variable v(TypeData::FindType<Graphics::Transform>(), &obj->Transform);
        v.Deserialize(val["Transform"]);
      }
      for(std::string sub : val["Components"].getMemberNames())
      {
        Handle<Component> h = obj->GetComponent(sub);
        if(h == nullptr)
          h = obj->AddComponent(sub);
        Variable v(TypeData::FindType(sub), static_cast<Component*>(h));
        v.Deserialize(val["Components"][sub]);
      }
      for(std::string sub : val["Children"].getMemberNames())
      {
        Handle<Object> o;
        for(auto t : obj->Transform.GetChildren())
        {
          if(val["Children"][sub]["Name"] == t->GetObject()->GetName())
          {
            o = t->GetObject();
            break;
          }
        }
        if(o == nullptr)
          o = obj->GetSpace()->AddObject();
        o->Transform.SetParent(&obj->Transform);
        apply(o, val["Children"][sub]);
      }
      // HACK: make sure transform object is correct
      obj->Transform.SetObject(obj);
    }

    void Archetype::ApplyArchetype(Handle<Object> obj)
    {
      apply(obj, val);
    }

    static Json::Value diff(Json::Value& v1, Json::Value& v2)
    {
      if(v1 == v2)
        return Json::Value();
      Json::Value result;
      if(v1.isObject())
      {
        for(auto sub : v1.getMemberNames())
        {
          if(sub == "ArchetypeName")
            continue;
          if(!v2.isMember(sub))
            result[sub] = v1[sub];
          else
          {
            result[sub] = diff(v1[sub], v2[sub]);
            if(result[sub].isNull() || 
              (result[sub].isObject() && 
              result[sub].getMemberNames().size() == 0))
              result.removeMember(sub);
          }
        }
      }
      else
        result = v1;
      return result;
    }

    Json::Value Archetype::GetDifferences(Handle<Object> obj)
    {
      // Serialize object into a Json::Value
      Json::Value other_val;
      Variable v(TypeData::FindType<Object>(), (void*)obj);
      std::string orig = obj->ArchetypeName;
      obj->ArchetypeName = "";
      SerializeType<Object>(v, other_val);
      obj->ArchetypeName = orig;
      Json::Value result;

      val.removeMember("Transform");
      result = diff(other_val, val);
      result["ArchetypeName"] = orig;
      return result;
    }

    Handle<Archetype> Archetype::GetDefault()
    {
      return Handle<Archetype>();
    }
  }
}

