/*!
  \file TypeData.hpp
  \author Chase A Rayment
  \date 5 April 2017
  \brief Contains the definition of the TypeData class. Heavily inspired by
    Andrew Dillon's reflection system.
*/
#pragma once

#include <string>
#include <unordered_map>
#include <typeinfo>
#include "Member.hpp"
#include "json/json.h"

namespace Echo
{
  namespace Core
  {
    class Variable;

    /*!
      \brief Contains data about a generic type. Useful for reflection.
    */
    class TypeData
    {
    public:
      /*!
        \brief A function that creates a new type.
      */
      typedef void* (*NewFunc)(void);

      /*!
        \brief A function that frees a given pointer.
      */
      typedef void (*DeleteFunc)(void*);

      /*!
        \brief A function that puts data from a Variable into a Json::Value.
      */
      typedef void (*SerializeFunc)(Variable, Json::Value&);

      /*!
        \brief A function that puts data from a Json::Value into a Variable.
      */
      typedef void (*DeserializeFunc)(const Json::Value&, Variable&);

      /*!
        \brief A function used for editing the data in the editor.
      */
      typedef void (*EditorWidgetFunc)(void*, const std::string&);

      /*!
        \brief A function used for comparing two types.
      */
      typedef void (*CompareFunc)(Variable, Variable);

      /*!
        \brief A function used for comparing two types.
      */
      typedef void (*AssignFunc)(Variable&, Variable&);

      /*!
        \brief Constructs an invalid TypeData (for use for errors)
      */
      TypeData();

      /*!
        \brief Constructs a new TypeData.
        \param name The name to give the type. Should be unique for ALL types.
        \param hash The typeid hash of the type.
      */
      TypeData(const std::string& name, size_t hash);

      /*!
        \brief Gets the name of this type.
      */
      const std::string& GetName() const;

      /*!
        \brief Gets the hash of this type.
      */
      size_t GetHash() const;

      /*!
        \brief Tests if this type is equivalent to another type.
        \param rhs The other type.
      */
      bool operator==(const TypeData& rhs) const;

      /*!
        \brief Tests if this type is NOT equivalent to another type.
        \param rhs The other type.
      */
      bool operator!=(const TypeData& rhs) const;

      /*!
        \brief Adds a member to this TypeData.
        \param member The member to add.
      */
      void AddMember(const Member& member);

      /*!
        \brief Gets all members of this type.
      */
      const std::unordered_map<std::string, Member>& GetMembers() const;

      /*!
        \brief Sets the New function for this type.
        \param func The function to set to.
      */
      void SetNewFunc(NewFunc func);

      /*!
        \brief Gets the New function for this type.
      */
      NewFunc GetNewFunc(void) const;

      /*!
        \brief Sets the Delete function for this type.
        \param func The function to set to.
      */
      void SetDeleteFunc(DeleteFunc func);

      /*!
        \brief Gets the Delete function for this type.
      */
      DeleteFunc GetDeleteFunc(void) const;

      /*!
        \brief Sets the Serialize function for this type.
        \param func The function to set to.
      */
      void SetSerializeFunc(SerializeFunc func);

      /*!
        \brief Gets the Serialize function for this type.
      */
      SerializeFunc GetSerializeFunc(void) const;

      /*!
        \brief Sets the Deserialize function for this type.
        \param func The function to set to.
      */
      void SetDeserializeFunc(DeserializeFunc func);

      /*!
        \brief Gets the Deserialize function for this type.
      */
      DeserializeFunc GetDeserializeFunc(void) const;

      /*!
        \brief Sets the editor widget function for this type.
        \param func The function to set to.
      */
      void SetEditorWidgetFunc(EditorWidgetFunc func);

      /*!
        \brief Gets the editor widget function for this type.
      */
      EditorWidgetFunc GetEditorWidgetFunc(void) const;

      /*!
        \brief Sets the assignment function for this type.
        \param func The function to set to.
      */
      void SetAssignFunc(AssignFunc func);

      /*!
        \brief Gets the assignment function for this type.
      */
      AssignFunc GetAssignFunc(void) const;

      /*!
        \brief Adds a parent type.
      */
      void AddParentType(const TypeData* type);

      /*!
        \brief Determines whether or not this type inherits from another.
      */
      bool IsDerivedFrom(const TypeData* type) const;

      /*!
        \brief Adds a type to the global list.
        \param type The type to add.
      */
      template <typename T>
      static void AddType(const TypeData& type);

      /*!
        \brief Finds a type by name.
        \param name The name to look for.
      */
      static TypeData* FindType(const std::string& name);

      /*!
        \brief Finds a type by hash.
        \param hash The hash to look for.
      */
      static TypeData* FindType(size_t hash);

      /*!
        \brief Finds a type. Template parameter T is the type to look for.
      */
      template <typename T>
      static TypeData* FindType();

      /*!
        \brief Gets all registered types.
      */
      static const std::unordered_map<std::string, TypeData>& GetAllTypes();

      /*!
        \brief Gets all registered types that inherit from a certain type.
        \param parent The parent type.
      */
      static std::unordered_map<std::string, const TypeData*> 
        GetAllTypesDerivedFrom(const TypeData* parent);


    private:
      std::string name;             // The given name of the type
      size_t hash;                  // The typeid hash of the type
                                    // All the members of this type
      std::unordered_map<std::string, Member> members;
      NewFunc newfunc;              // Creates a new instance of this type. 
      DeleteFunc delfunc;           // Deletes a instance of this type.
      SerializeFunc serialfunc;     // Serializes an instance of this type.
      DeserializeFunc deserialfunc; // Deserializes an instance of this type.
      EditorWidgetFunc editfunc;    // Edits an instance of this type.
      AssignFunc assignfunc;        // Assigns a member of this type to another
                                    // member of this type.
                                    // All the parent types of this type.
      std::vector<const TypeData*> parent_types;

        // All the types added to the list.
      static std::unordered_map<std::string, TypeData> types;
        // The same types, but hashed by typeid hash
      static std::unordered_map<size_t, TypeData> ctTypes;
    };

    /*!
      \brief Registers all types into TypeData.
    */
    void RegisterTypes();
  }
}

#include "TypeData.inl"
