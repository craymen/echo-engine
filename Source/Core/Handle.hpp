/*!
  \file Handle.hpp
  \author Chase A Rayment
  \date 7 August 2017
  \brief Contains the definition of the Handle class.
*/
#pragma once

#include <unordered_map>
#include <list>

namespace Echo
{
  namespace Core
  {
    /*!
      \brief A specialized pointer to an object, which can be invalid. All
        Handles to a given address can be invalidated at once.
    */
    template <typename T>
    class Handle
    {
    public:
      /*!
        \brief Creates a new invalid handle.
      */
      Handle() : ptr(nullptr) { };

      /*!
        \brief Gets a new Handle to a given object.
        \param obj The object to get a Handle to.
      */
      Handle(T* obj);

      /*!
        \brief Copies a Handle.
        \param rhs The Handle to copy.
      */
      Handle(const Handle& rhs);

      /*!
        \brief Destroys a Handle.
      */
      ~Handle();

      /*!
        \brief Returns true if this Handle is valid.
      */
      bool IsValid() const;

      /*!
        \brief Dereferences this Handle like a normal arrow operator.
      */
      T* operator->();

      /*! \overload */
      const T* operator->() const;

      /*!
        \brief Dereferences this Handle like a normal dereference operator.
      */
      T& operator*();

      /*! \overload */
      const T& operator*() const;

      /*!
        \brief Returns true if this Handle points to the same address
          as another Handle.
      */
      bool operator==(const Handle& rhs) const;

      /*!
        \brief Returns true if this Handle points to a certain address.
      */
      bool operator==(const T* rhs) const;

      /*!
        \brief Returns true if this Handle does NOT point to the same
          address as another Handle.
      */
      bool operator!=(const Handle& rhs) const;

      /*!
        \brief Returns true if this Handle does NOT point to a certain address.
      */
      bool operator!=(const T* rhs) const;

      /*!
        \brief Assigns this Handle to point to another address.
      */
      Handle& operator=(const Handle& rhs);

      /*!
        \brief Assigns this Handle to point to another address.
      */
      Handle& operator=(T* rhs);

      /*!
        \brief Casts to a pointer to the inner data.
      */
      explicit operator T*();

      /*! \overload */
      explicit operator const T*() const;

      /*! \overload */
      explicit operator void*();

      /*! \overload */
      explicit operator const void*() const;

      /*!
        \brief Casts to a bool, returning true if the handle is valid.
      */
      operator bool() const;

      /*!
        \brief Updates all handles of a given address.
        \param oldAddr The address of the old data.
        \param newAddr The address of the new data.
      */
      static void UpdateHandles(const T* oldAddr, T* newAddr);

      /*!
        \brief Invalidates all handles of a given address.
      */
      static void InvalidateHandles(const T* addr);

    private:
        // All handles of this type.
      static std::unordered_map<const T*, std::list<Handle*> > handles;
      T* ptr;                 // The data this handle points to.

      void register_addr();   // Registers this handle's pointer.
      void deregister_addr(); // Deregisters this handle's pointer.
    };
  }
}

#include "Handle.inl"
