/*!
  \file Mouse.hpp
  \author Chase A Rayment
  \date 12 May 2017
  \brief Contains the definition of the Mouse system.
*/

#pragma once

#include "GLM/glm.hpp"
#include "SDL/SDL.h"

namespace Echo
{
  namespace Input
  {
    namespace Mouse
    {
      /*!
        \brief Initializes the Mouse input.
      */
      void Initialize();

      /*!
        \brief Handles an SDL mouse motion event.
        \param event The SDL event.
      */
      void MouseCallback(SDL_Event& event);

      /*!
        \brief Handles an SDL mouse button event.
        \param event The SDL event.
      */
      void MouseButtonCallback(SDL_Event& event);

      /*!
        \brief Handles an SDL mouse wheel event.
        \param event The SDL event.
      */
      void MousewheelCallback(SDL_Event& event);

      /*!
        \brief Updates the Mouse system. Should be called BEFORE
          Graphics::Update()!
      */
      void Update();

      /*!
        \brief Gets the relative mouse position.
      */
      glm::ivec2 GetRelativeMousePos();

      /*!
        \brief Gets the absolute mouse position.
      */
      glm::ivec2 GetAbsoluteMousePos();

      /*!
        \brief Sets whether or not the mouse is locked to the window.
      */
      void SetLocked(bool val);

      /*!
        \brief Gets whether or not the specified mouse button is down.
      */
      bool MouseButtonIsDown(unsigned button);

      /*!
        \brief Gets whether or not the specified mouse button was pressed this
          frame.
      */
      bool MouseButtonIsPressed(unsigned button);

      /*!
        \brief Gets the delta mouse wheel position this frame.
      */
      int GetMouseWheelDelta();
    }
  }
}