/*!
  \file Mouse.cpp
  \author Chase A Rayment
  \date 12 May 2017
  \brief Contains the implementation of the Mouse system.
*/

#include "Mouse.hpp"
#include <unordered_map>
#include "Keyboard.hpp"

namespace Echo
{
  namespace Input
  {
    namespace Mouse
    {
      static glm::ivec2 relPos;
      static glm::ivec2 absPos;
      static std::unordered_map<unsigned, Keyboard::KeyState> states;
      static int mwheel = 0;

      void Initialize()
      {
        for(unsigned i = 0; i < 5; ++i)
        {
          states[i] = Keyboard::KeyState::Released;
        }
      }

      void MouseCallback(SDL_Event& event)
      {
        relPos = glm::ivec2(event.motion.xrel, event.motion.yrel);
        absPos = glm::ivec2(event.motion.x, event.motion.y);
      }

      void MouseButtonCallback(SDL_Event& event)
      {
        states[event.button.button] = event.button.state == SDL_PRESSED ?
          Keyboard::KeyState::Pressed : Keyboard::KeyState::Released;
      }

      void MousewheelCallback(SDL_Event& event)
      {
        if(event.wheel.direction == SDL_MOUSEWHEEL_NORMAL)
          mwheel += event.wheel.y;
        else
          mwheel -= event.wheel.y;
      }

      void Update()
      {
        relPos = glm::ivec2(0, 0);
        mwheel = 0;
        for (unsigned i = 0; i < 5; ++i)
          if(states[i] == Keyboard::KeyState::Pressed)
            states[i] = Keyboard::KeyState::Down;
      }

      glm::ivec2 GetRelativeMousePos()
      {
        return relPos;
      }

      glm::ivec2 GetAbsoluteMousePos()
      {
        return absPos;
      }

      void SetLocked(bool val)
      {
        SDL_SetRelativeMouseMode(val ? SDL_TRUE : SDL_FALSE);
      }

      bool MouseButtonIsDown(unsigned button)
      {
        return states[button] != Keyboard::KeyState::Released;
      }

      bool MouseButtonIsPressed(unsigned button)
      {
        return states[button] == Keyboard::KeyState::Pressed;
      }

      int GetMouseWheelDelta()
      {
        return mwheel;
      }
    }
  }
}