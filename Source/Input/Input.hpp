/*!
  \file Input.hpp
  \author Chase A Rayment
  \date 11 May 2017
  \brief Contains the definition of the Input system.
*/

#pragma once

#include "Keyboard.hpp"
#include "Mouse.hpp"

namespace Echo
{
  namespace Input
  {
    /*!
      \brief Initializes all Input systems.
    */
    void Initialize();

    /*!
      \brief Updates all Input systems.
    */
    void Update();
  }
}