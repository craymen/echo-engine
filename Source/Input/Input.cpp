/*!
  \file Input.cpp
  \author Chase A Rayment
  \date 11 May 2017
  \brief Contains the implementation of the Input system.
*/

#include "Input.hpp"

namespace Echo
{
  namespace Input
  {
    void Initialize()
    {
      Keyboard::Initialize();
      Mouse::Initialize();
    }

    void Update()
    {
      Keyboard::Update();
      Mouse::Update();
    }
  }
}