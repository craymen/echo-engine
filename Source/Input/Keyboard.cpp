/*!
  \file Input.cpp
  \author Chase A Rayment
  \date 11 May 2017
  \brief Contains the implementation of the Input system.
*/

#include "Input.hpp"
#include <unordered_map>
#include "Log.hpp"
#include "imgui/imgui.h"

namespace Echo
{
  namespace Input
  {
    namespace Keyboard
    {
      static std::unordered_map<unsigned, KeyState> states;

      void Initialize()
      {
        // Reset all states
        for(unsigned i = 0; i < (unsigned)SDL_SCANCODE_APP2; 
          i = (unsigned)i + 1)
          states[i] = KeyState::Released;
      }

      void Update()
      {
        // Set all "pressed" states to "down"
        for(auto& state : states)
        {
          if(state.second == KeyState::Pressed)
            state.second = KeyState::Down;
          if(state.second == KeyState::Down)
          {
            KeyEvent evt;
            evt.State = KeyState::Down;
            evt.Key = (SDL_Scancode)state.first;
            Core::DispatchEvent(nullptr, "KeyHeld", evt);
          }
        }
      }

      void KeyCallback(SDL_Event& event)
      {
        if(!event.key.repeat)
        {
          KeyState state = event.key.type == SDL_KEYDOWN ? 
            KeyState::Pressed : KeyState::Released;
          states[(unsigned)event.key.keysym.scancode] = state;
          KeyEvent evt;
          evt.State = state;
          evt.Key = event.key.keysym.scancode;
          Core::DispatchEvent(nullptr, "KeyEvent", evt);
          if(state == KeyState::Pressed)
            Core::DispatchEvent(nullptr, "KeyPressed", evt);
          else
            Core::DispatchEvent(nullptr, "KeyReleased", evt);
        }
      }

      bool KeyIsPressed(SDL_Scancode key, bool ignoreImgui)
      {
        if(!ignoreImgui && ImGui::IsAnyItemActive())
          return false;
        return states[(unsigned)key] == KeyState::Pressed;
      }

      bool KeyIsDown(SDL_Scancode key, bool ignoreImgui)
      {
        if(!ignoreImgui && ImGui::IsAnyItemActive())
          return false;
        return states[(unsigned)key] != KeyState::Released;
      }

      bool KeyIsReleased(SDL_Scancode key, bool ignoreImgui)
      {
        if(!ignoreImgui && ImGui::IsAnyItemActive())
          return true;
        return states[(unsigned)key] == KeyState::Released;
      }

      KeyState GetKeyState(SDL_Scancode key)
      {
        return states[(unsigned)key];
      }
    }
  }
}