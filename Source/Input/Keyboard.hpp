/*!
  \file Keyboard.hpp
  \author Chase A Rayment
  \date 11 May 2017
  \brief Contains the definition of the Keyboard system.
*/

#pragma once

#include "SDL/SDL.h"
#include "Messaging.hpp"

namespace Echo
{
  namespace Input
  {
    namespace Keyboard
    {
      /*!
        \brief Contains all of the possible states for a key.
      */
      enum class KeyState { Pressed, Down, Released };
      
      /*!
        \brief Initializes the Keyboard system.
      */
      void Initialize();

      /*!
        \brief Updates the Keyboard system. Make sure this is done BEFORE
          Graphics::Update()!
      */  
      void Update();

      /*!
        \brief Handles the SDL key event.
        \param evt The key event.
      */
      void KeyCallback(SDL_Event& evt);

      /*!
        \brief Returns true if the specified key was pressed THIS frame.
        \param key The key to test.
        \param ignoreImgui Whether or not to ignore ImGUI focus.
      */
      bool KeyIsPressed(SDL_Scancode key, bool ignoreImgui = false);

      /*!
        \brief Returns true if the specified key is down.
        \param key The key to test.
        \param ignoreImgui Whether or not to ignore ImGUI focus.
      */
      bool KeyIsDown(SDL_Scancode key, bool ignoreImgui = false);

      /*!
        \brief Returns true if the specified key is up.
        \param key The key to test.
        \param ignoreImgui Whether or not to ignore ImGUI focus.
      */
      bool KeyIsReleased(SDL_Scancode key, bool ignoreImgui = false);

      /*!
        \brief Gets the state of the specified key.
        \param key The key to test.
      */
      KeyState GetKeyState(SDL_Scancode key);
    }
  
    /*!
      \brief Contains information about a pressed key.
    */
    struct KeyEvent : public Core::Event
    {
      Keyboard::KeyState State; //!< The new key state.
      SDL_Scancode Key;         //!< The key updated.
    };
  }
}