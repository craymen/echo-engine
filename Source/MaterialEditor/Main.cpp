/*!
  \file Main.cpp
  \author Chase A Rayment
  \date 13 July 2017
  \brief Contains the main function for the MaterialEditor app.
*/

#include "MaterialEditor.hpp"

int main(void)
{
  Echo::MaterialEditor::Initialize();
  Echo::MaterialEditor::Run();
  Echo::MaterialEditor::Terminate();
  return 0;
}