/*!
  \file MaterialEditor.cpp
  \author Chase A Rayment
  \date 13 July 2017
  \brief Contains the implementation of the Material Editor.
*/

#include "GraphicsCore.hpp"
#include "Input.hpp"
#include "Log.hpp"
#include "Game.hpp"
#include "TypeData.hpp"
#include "ResourceManager.hpp"
#include "ModelObject.hpp"
#include "PointLight.hpp"
#include "Material.hpp"
#include "CameraObject.hpp"
#include "Transform.hpp"
#include "Messaging.hpp"
#include "QuitEvent.hpp"
#include "imgui/imgui.h"
#include <chrono>
#include <thread>
#include "ReflectionProbeObject.hpp"
#include "FileSystem.hpp"

namespace Echo
{
  namespace MaterialEditor
  {
    static bool shouldQuit = false;

    struct QuitListener
    {
      void Initialize()
      {
        Core::ConnectEvent(nullptr, "Quit", this, &QuitListener::HandleQuit);
      }

      void HandleQuit(Core::QuitEvent&)
      {
        shouldQuit = true;
      }
    };

    static QuitListener quit;

    static Graphics::Scene* scene;
    static Graphics::ModelObject* model;
    static Graphics::PointLightObject* light;
    static Graphics::CameraObject* cam;
    static Graphics::Transform cam_transform;
    static Graphics::Transform model_transform;
    static Graphics::Material material;
    static float curr_time;
    static Core::Handle<Graphics::Mesh> mesh;
    static Graphics::ReflectionProbeObject* probe;
    static float zoom = 2.0f;
    static glm::vec3 pan;

    void Initialize()
    {
      Core::FileSystem::Initialize();
      Core::RegisterTypes();
      Core::Game::SetEditorMode(true);
      Core::Log.Init("Log.txt");
      Graphics::Initialize();
      Input::Initialize();
      Core::LoadAllResources();
      quit.Initialize();
      scene = Graphics::AddScene();
      model = scene->AddModel();
      light = scene->AddPointLight();
      cam = scene->AddCamera();
      cam->SetFov(75.0f);
      cam->SetNearZ(0.01f);
      cam->SetFarZ(1000.0f);
      cam->SetIsOrtho(false);
      scene->SetCurrentCamera(cam);
      scene->SetDepth(0.0f);
      cam_transform.SetPosition(glm::vec3(0, 0, 2));
      model_transform.SetPosition(glm::vec3(0, 0, 0));
      model_transform.SetScale(glm::vec3(1, 1, 1));
      light->Position = glm::vec3(5, 5, 5);
      light->Color = glm::vec3(50, 50, 50);
      mesh = Core::ResourceManager<Graphics::Mesh>::Find("Sphere");
      material = 
        *Core::ResourceManager<Graphics::Material>::Find("DefaultMaterial");
      model->SetMaterial(&material);
      scene->SetSkybox(Core::ResourceManager<Graphics::Cubemap>::
        Find("DefaultCubemap"));
      probe = scene->AddReflectionProbe();
      probe->Name = "MaterialEditor";
      probe->Position = glm::vec3(0, 0, 0);
      model->SetMesh(mesh);
      scene->BakeReflections();
    }

    void Run()
    {
      while(!shouldQuit)
      {
        // Get the frame start time
        auto frameStart = std::chrono::high_resolution_clock::now();

        // Edit material
        ImGui::Begin("Material Properties");
        Core::TypeData::FindType<Graphics::Material>()->GetEditorWidgetFunc()(&material, "Material");

        static char savename[256] = { 0 };

        ImGui::InputText("Save Name", savename, 256);
        if(ImGui::Button("Save"))
        {
          Core::Variable v(Core::TypeData::FindType<Graphics::Material>(), 
            &material);
          std::string filename = Core::FileSystem::GetGameDir(
            Core::FileSystem::Dir::Materials) + "/" + std::string(savename)
            + ".mat";
          Json::Value val;
          v.Serialize(val);
          std::ofstream f(filename);
          f << val;
        }

        ImGui::End();

        ImGui::Begin("Scene Properties");
        Core::TypeData::FindType<Core::Handle<Graphics::Mesh> >()->
          GetEditorWidgetFunc()(&mesh, "Mesh");
        model->SetMesh(mesh);
        ImGui::End();

        zoom += -0.25f * Input::Mouse::GetMouseWheelDelta();
        if(zoom < 0.0f)
          zoom = 0.0f;

        if(Input::Mouse::MouseButtonIsDown(SDL_BUTTON_MIDDLE))
        {
          glm::vec2 dpos = Input::Mouse::GetRelativeMousePos();

          if(Input::Keyboard::KeyIsDown(SDL_SCANCODE_LSHIFT))
          {
            pan += 0.01f * (-dpos.x * cam_transform.GetRight() + 
              dpos.y * cam_transform.GetUp());
          }
          else
          {
            cam_transform.RotateAround(glm::vec3(0.0f, 1.0f, 0.0f), 0.03f * -dpos.x);
            cam_transform.RotateAround(cam_transform.GetRight(), 0.03f * -dpos.y);
          }
        }

        cam_transform.SetPosition(pan + -cam_transform.GetForward() * zoom);

        cam->SetEye(cam_transform.GetPosition());
        cam->SetLookDir(cam_transform.GetForward());
        cam->SetUp(cam_transform.GetUp());
        model->SetTransform(model_transform.GetMatrix());

        light->Position = glm::vec3(glm::sin(curr_time) * 5, 0, glm::cos(curr_time) * 5);

        Input::Update();
        Graphics::Update();
        Core::UpdateAllResources();

        // Wait for the next frame
        auto timeToWait = 
          frameStart + std::chrono::duration<float>(1.0f / 60.0f);
        std::this_thread::sleep_until(timeToWait);

        curr_time += std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - frameStart).count() 
            / 1000.0f;
      }
    }

    void Terminate()
    {
      Graphics::Terminate();
      Core::Log.Terminate();
    }
  }
}