/*!
  \file MaterialEditor.hpp
  \author Chase A Rayment
  \date 13 July 2017
  \brief Contains the definition of the Material Editor.
*/

namespace Echo
{
  namespace MaterialEditor
  {
    void Initialize();

    void Run();

    void Terminate();
  }
}