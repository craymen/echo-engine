/*!
  \file Editor.hpp
  \author Chase A Rayment
  \date 17 April 2017
  \brief Contains the definition of the Editor system.
*/

namespace Echo
{
  namespace Editor
  {
    /*!
      \brief Initializes the Editor.
    */
    void Initialize();

    /*!
      \brief Runs the Editor.
    */
    void Run();

    /*!
      \brief Terminates the Editor.
    */
    void Terminate();
  }
}