/*!
  \file ComponentList.hpp
  \author Chase A Rayment
  \date 17 April 2017
  \brief Contains the definition of the ComponentList view.
*/
#pragma once

#include "Handle.hpp"
#include "Object.hpp"

namespace Echo
{
  namespace Editor
  {
    namespace ComponentList
    {
      /*!
        \brief Updates the ComponentList view.
        \param selectedObject The currently selected object.
      */  
      void Update(Core::Handle<Core::Object> selectedObject);
    }
  }
}