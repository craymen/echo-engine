/*!
  \file ObjectList.hpp
  \author Chase A Rayment
  \date 17 April 2017
  \brief Contains the definition of the ObjectList panel.
*/  

#pragma once

#include "Space.hpp"
#include "Object.hpp"
#include "Handle.hpp"

namespace Echo
{
  namespace Editor
  {
    namespace ObjectList
    {
      /*!
        \brief Updates the ObjectList.
        \param space The space to view.
        \param selectedObject A reference to the selected object handle.
      */
      void Update(Core::Handle<Core::Space> space, 
        Core::Handle<Core::Object>& selectedObject);
    }
  }
}