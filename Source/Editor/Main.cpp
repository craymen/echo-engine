/*!
  \file Editor/Main.cpp
  \author Chase A Rayment
  \date 17 April 2017
  \brief Contains the implementation of the Editor app.
*/

#include "GraphicsCore.hpp"
#include "Editor.hpp"

/*!
  \brief The entry point for the Editor app.
*/
int main(void)
{
  Echo::Editor::Initialize();
  Echo::Editor::Run();
  Echo::Editor::Terminate();
  return 0;
}