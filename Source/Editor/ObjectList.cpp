/*!
  \file ObjectList.cpp
  \author Chase A Rayment
  \date 17 April 2017
  \brief Contains the implementation of the ObjectList panel.
*/

#include "ObjectList.hpp"
#include "imgui/imgui.h"
#include "Object.hpp"
#include "Handle.hpp"
#include "Log.hpp"
#include "Window.hpp"

namespace Echo
{
  namespace Editor
  {
    static struct ObjectListItem
    {
      Core::Handle<Core::Object> obj;
      int depth;
    };

    static std::vector<ObjectListItem> items;

    static void add_items(Core::Handle<Core::Object> obj, 
      Core::Handle<Core::Object>& selectedObject)
    {
      std::stringstream name;
      name << "(" << obj->GetID() << ") " << obj->GetName();
      int flags = 0;
      if(obj->Transform.GetChildren().size() == 0)
        flags |= ImGuiTreeNodeFlags_Leaf;
      else
        flags |= ImGuiTreeNodeFlags_OpenOnArrow;
      if(obj == selectedObject)
        flags |= ImGuiTreeNodeFlags_Selected;
      bool opened = ImGui::TreeNodeEx(name.str().c_str(), flags);
      if(ImGui::IsItemClicked())
        selectedObject = obj;
      if(opened)
      {
        for (auto child : obj->Transform.GetChildren())
          if(child->GetObject())
            add_items(child->GetObject(), selectedObject);
        ImGui::TreePop();
      }
    }

    namespace ObjectList
    {
      void Update(Core::Handle<Core::Space> space, 
        Core::Handle<Core::Object>& selectedObject)
      {
        ImGui::SetNextWindowPos(ImVec2(0, 20));
        ImGui::SetNextWindowSize(
          ImVec2(300, Graphics::Window::GetHeight() - 20));
        ImGui::Begin("Object List", NULL, ImGuiWindowFlags_NoResize | 
          ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse);

        // Get objects in the space
        std::vector<Core::Handle<Core::Object> > objects 
          = space->GetAllObjects();

        items.clear();

        // Create the item list
        for(unsigned i = 1; i < objects.size(); ++i)
          if(objects[i]->Transform.GetParent() == nullptr)
            add_items(objects[i], selectedObject);

        ImGui::Separator();

        // Add Object button
        if(ImGui::Button("+##Add Object"))
        {
          selectedObject = space->AddObject();
          selectedObject->SetName("New Object");
        }

        ImGui::SameLine();
        
        // Remove Object button
        if(ImGui::Button("-##Remove Object"))
        {
          selectedObject->Terminate();
          selectedObject = nullptr;
          Core::Spaces[Core::SPACE_GAMEPLAY].Consolidate();
        }
        ImGui::End();
      }
    }
  }
}