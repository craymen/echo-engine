/*!
  \file Menu.cpp
  \author Chase A Rayment
  \date 17 April 2017
  \brief Contains the implementation of the Menu system.
*/

#include "Menu.hpp"
#include "Variable.hpp"
#include <fstream>
#include "json/json.h"
#include "FileSystem.hpp"
#include "EditorCamera.hpp"
#include "Widget.hpp"
#include "Space.hpp"
#include "Scene.hpp"
#include "GraphicsCore.hpp"

namespace Echo
{
  namespace Editor
  {
    namespace Menu
    {
      void Update(Core::Handle<Core::Space> space)
      {
        static bool saveAsDialogOpen = false;
        static bool openDialogOpen = false;
        static bool editorPreferencesOpen = false;

        if (ImGui::BeginMainMenuBar())
        {
          if (ImGui::BeginMenu("File"))
          {
            if (ImGui::MenuItem("New"))
            {
              // TODO: New
            }

            if (ImGui::MenuItem("Open"))
            {
              openDialogOpen = true;
            }

            if (ImGui::MenuItem("Save"))
            {
              saveAsDialogOpen = true;
            }
            ImGui::EndMenu();
          }

          if (ImGui::BeginMenu("Edit"))
          {
            if(ImGui::MenuItem("Editor Preferences..."))
            {
              editorPreferencesOpen = true;
            }

            ImGui::EndMenu();
          }

          if(ImGui::BeginMenu("Scene"))
          {
            if(ImGui::MenuItem("Bake Reflections"))
            {
              Graphics::StartRenderDocCapture();
              Core::Spaces[Core::SPACE_GAMEPLAY].GetGraphicsScene()
                ->BakeReflections();
              Graphics::EndRenderDocCapture();
            }

            ImGui::EndMenu();
          }

          ImGui::EndMainMenuBar();

          // Save As dialog
          if(saveAsDialogOpen)
            ImGui::OpenPopup("SavePopup");

          if(ImGui::BeginPopup("SavePopup"))
          {
            static char filename[1024];
            ImGui::InputText("Filename", filename, 1024);
            if(ImGui::Button("Save##SaveDialogButton"))
            {
              Core::Variable v(Core::TypeData::FindType<Core::Space>(),
                (void*)space);
              Json::Value val;
              v.Serialize(val);
              std::string path = Core::FileSystem::GetGameDir(
                Core::FileSystem::Dir::Levels) + "/" + std::string(filename);
              std::ofstream of(path);
              if(!of.is_open())
                Core::Log << Core::Priority::Warning << "Could not save to "
                  << path << std::endl;
              else
                of << val;
              saveAsDialogOpen = false;
              ImGui::CloseCurrentPopup();
            }
            ImGui::EndPopup();
          }
          else
            saveAsDialogOpen = false;

          // Open dialog
          if(openDialogOpen)
            ImGui::OpenPopup("OpenPopup");

          if(ImGui::BeginPopup("OpenPopup"))
          {
            static char filename[1024];
            ImGui::InputText("Filename", filename, 1024);
            if(ImGui::Button("Open##OpenDialogButton"))
            {
              Core::Spaces[Core::SPACE_GAMEPLAY].LoadLevel(filename);
              Core::Spaces[Core::SPACE_GAMEPLAY].Initialize();
              EditorCamera::Initialize();
              Widget::Initialize();
              openDialogOpen = false;
              ImGui::CloseCurrentPopup();
            }
            ImGui::EndPopup();
          }
          else
            openDialogOpen = false;

          // Editor preferences
          if(editorPreferencesOpen)
            ImGui::OpenPopup("Editor Preferences");
          if(ImGui::BeginPopup("Editor Preferences"))
          {
            ImGui::Checkbox("Snapping", &Widget::SnappingEnabled);
            ImGui::InputFloat("Snap Grid Size", &Widget::SnapGridSize);
            if(ImGui::Button("Done"))
            {
              ImGui::CloseCurrentPopup();
              editorPreferencesOpen = false;
            }
            ImGui::EndPopup();
          }
          else
            editorPreferencesOpen = false;
        }
      }
    }
  }
}