/*!
  \file Widget.hpp
  \author Chase A Rayment
  \date 2 June 2017
  \brief Contains the definition of the Editor Widget system.
*/
#pragma once

#include "Handle.hpp"
#include "Object.hpp"

namespace Echo
{
  namespace Editor
  {
    namespace Widget
    {
      /*!
        \brief Initializes the Widget system.
      */
      void Initialize();

      /*!
        \brief Updates the Widget system.
        \param currObj The currently selected object.
      */
      void Update(Core::Handle<Core::Object>& currObj);

      /*!
        \brief Terminates the Widget system.
      */
      void Terminate();

      extern bool SnappingEnabled; //!< Whether or not snapping is enabled.
      extern float SnapGridSize;   //!< The snapping grid size.
    }
  }
} 