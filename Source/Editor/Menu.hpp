/*!
  \file Menu.hpp
  \author Chase A Rayment
  \date 17 April 2017
  \brief Contains the definition of the Menu system.
*/

#pragma once

#include "Handle.hpp"
#include "Space.hpp"

namespace Echo
{
  namespace Editor
  {
    namespace Menu
    {
      /*!
        \brief Updates the menu.
        \param space The current space.
      */
      void Update(Core::Handle<Core::Space> space);
    }
  }
}