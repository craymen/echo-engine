/*!
  \file Editor.cpp
  \author Chase A Rayment
  \date 17 April 2017
  \brief Contains the implementation of the Editor system.
*/

#include "Editor.hpp"
#include <vector>
#include "Handle.hpp"
#include "Object.hpp"
#include "Space.hpp"
#include "GraphicsCore.hpp"
#include "ObjectList.hpp"
#include "FileSystem.hpp"
#include "QuitEvent.hpp"
#include "Messaging.hpp"
#include "ComponentList.hpp"
#include "Menu.hpp"
#include "Input.hpp"
#include "EditorCamera.hpp"
#include "DebugDrawer.hpp"
#include "Archetype.hpp"
#include "ResourceManager.hpp"
#include "Widget.hpp"
#include "PhysicsCore.hpp"
#include "AudioCore.hpp"
#include "Game.hpp"

namespace Echo
{
  namespace Editor
  {
    static bool shouldQuit = false;

    struct QuitListener
    {
      void Initialize()
      {
        Core::ConnectEvent(nullptr, "Quit", this, &QuitListener::HandleQuit);
      }

      void HandleQuit(Core::QuitEvent&)
      {
        shouldQuit = true;
      }
    };

    static QuitListener quit;

    static Core::Handle<Core::Object> selectedObject;

    void Initialize()
    {
      Core::FileSystem::Initialize();
      Core::RegisterTypes();
      Core::Game::SetEditorMode(true);
      Core::Log.Init("Log.txt");
      Graphics::Initialize();
      Core::LoadAllResources();
      Core::Spaces[Core::SPACE_GAMEPLAY].Initialize();
      Input::Initialize();
      quit.Initialize();
      EditorCamera::Initialize();
      Widget::Initialize();
      Audio::Initialize();

    }

    void Run()
    {
      while(!shouldQuit)
      {
        Input::Update();
        Graphics::Update();
        Physics::Update(1.0f / 60.0f);
        Core::UpdateAllResources();
        Audio::Update();
        // TODO: put an actual FPS controller here
        EditorCamera::Update(1.0f / 60.0f);

        Widget::Update(selectedObject);
        Core::Spaces[0].SetPaused(true);
        Menu::Update(Core::Handle<Core::Space>(&Core::Spaces[0]));
        ObjectList::Update(Core::Handle<Core::Space>(&Core::Spaces[0]),
          selectedObject);
        ComponentList::Update(selectedObject);

        Graphics::DebugDrawer& d = Core::Spaces[Core::SPACE_GAMEPLAY]
          .GetGraphicsScene()->GetDebugDrawer();
        d.DrawLine(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f))
          ->SetColor(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
        d.DrawLine(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f))
          ->SetColor(glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
        d.DrawLine(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f))
          ->SetColor(glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));

        if(selectedObject)
        {
          d.DrawBox(selectedObject->Transform.GetPosition(), 
            glm::vec3(0.25f, 0.25f, 0.25f));
          selectedObject->DebugDraw();
        }
      }
    }

    void Terminate()
    {
      Widget::Terminate();
	    Core::Spaces[Core::SPACE_GAMEPLAY].Terminate();
      Graphics::Terminate();
      Core::Log.Terminate();
      Audio::Terminate();
    }
  }
}