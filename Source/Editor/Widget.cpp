/*!
  \file Widget.cpp
  \author Chase A Rayment
  \date 2 June 2017
  \brief Contains the implementation of the Widget system.
*/

#include "Widget.hpp"
#include "Space.hpp"
#include "PhysicsScene.hpp"
#include "BoxColliderObject.hpp"
#include "Transform.hpp"
#include "Scene.hpp"
#include "CameraObject.hpp"
#include "Input.hpp"
#include "Log.hpp"
#include "RigidBodyObject.hpp"
#include "PhysicsUtilities.hpp"

namespace Echo
{
  namespace Editor
  {
    namespace Widget
    {

      static glm::vec3 initialClickDiff;
      static bool isActive = false;

      bool SnappingEnabled = false;
      float SnapGridSize = 1.0f;

      class TranslateWidget
      {
      private:
        Core::Handle<Physics::RigidBodyObject> body;
        glm::vec3 axis;
        bool active;
        glm::vec3 currPos;
        btBoxShape tbox;

        void Activate()
        {
          active = true;
          Input::Mouse::SetLocked(true);
        }

        void Deactivate()
        {
          active = false;
          Input::Mouse::SetLocked(false);          
        }

      public:
        TranslateWidget() : body(nullptr), axis(1, 0, 0),
          active(false), currPos(), tbox(btVector3(1.0f, 1.0f, 1.0f))
        {

        }

        void Initialize()
        {
          body = Core::Spaces[Core::SPACE_GAMEPLAY].GetPhysicsScene()->
            AddRigidBody(&tbox, 0.0f);
        }

        void Terminate()
        {
          body = nullptr;
        }

        ~TranslateWidget()
        {

        }

        void Update(Core::Handle<Core::Object>& currObj)
        {
          tbox.setLocalScaling(Physics::GLMVec3ToBulletVector3(
            axis * 0.9f + glm::vec3(0.1f, 0.1f, 0.1f)));
          body->SetTransform(Graphics::Transform::ObjectToWorld(
            currObj->Transform.GetPosition() + axis * 0.5f,
            glm::quat(),
            glm::vec3(1, 1, 1)));

          glm::vec3 mStart, mDir;
          Core::Spaces[Core::SPACE_GAMEPLAY].GetGraphicsScene()->
            GetCurrentCamera()->ScreenCoordsToWorldRay(
            Input::Mouse::GetAbsoluteMousePos(), mStart, mDir);

          std::vector<Physics::RayCastResult> all = 
            Core::Spaces[Core::SPACE_GAMEPLAY].GetPhysicsScene()->
            RayCastAll(mStart, mDir);

          bool hit = false;

          Physics::RayCastResult rcr;

          for(auto r : all)
          {
            if(r.Hit && r.HitCollider == body->GetRigidBody())
            {
              rcr = r;
              hit = true;
              break;
            }
          }

          if(hit && Input::Mouse::MouseButtonIsPressed(SDL_BUTTON_LEFT))
          {
            Activate();
            currPos = currObj->Transform.GetPosition();
            if(Input::Keyboard::KeyIsDown(SDL_SCANCODE_LSHIFT))
              currObj = currObj->Clone();
          }

          if(active && !Input::Mouse::MouseButtonIsDown(SDL_BUTTON_LEFT))
            Deactivate();

          if(active)
          {
            Core::Spaces[Core::SPACE_GAMEPLAY].GetGraphicsScene()->
              GetDebugDrawer().DrawLine(currObj->Transform.GetPosition() - 
              axis * 1000.0f, currObj->Transform.GetPosition() + axis * 1000.0f)
              ->SetColor(glm::vec4(axis.x, axis.y, axis.z, 1.0f));
          }
          else if(hit)
          {
            Core::Spaces[Core::SPACE_GAMEPLAY].GetGraphicsScene()->
              GetDebugDrawer().DrawLine(currObj->Transform.GetPosition(), 
              currObj->Transform.GetPosition() +
              axis)->SetColor(glm::vec4(axis.x, axis.y, axis.z, 1.0f));
          }
          else
          {
            Core::Spaces[Core::SPACE_GAMEPLAY].GetGraphicsScene()->
              GetDebugDrawer().DrawLine(currObj->Transform.GetPosition(), 
              currObj->Transform.GetPosition() +
              axis)->SetColor(
              glm::vec4(axis.x / 2.0f, axis.y / 2.0f, axis.z / 2.0f, 1.0f));
          }

          if(active)
          {
            float val;

            Graphics::CameraObject* cam = Core::Spaces[Core::SPACE_GAMEPLAY].
              GetGraphicsScene()->GetCurrentCamera();

            glm::mat4 mat = cam->GetProjectionMatrix() * cam->GetViewMatrix();
            glm::vec4 projp1 = mat * glm::vec4(currObj->Transform.GetPosition(), 
              1.0f);
            glm::vec4 projp2 = mat * glm::vec4(currObj->Transform.GetPosition()
              + axis, 1.0f);

            glm::vec2 projVec = glm::vec2((projp2 - projp1).x, 
              (projp2 - projp1).y);
            glm::ivec2 mouseVec = Input::Mouse::GetRelativeMousePos();
            glm::vec2 fmouseVec = glm::vec2(mouseVec.x, -mouseVec.y);

            val = glm::dot(glm::normalize(projVec), fmouseVec) * 0.1f;

            if(axis.x > 0.5f)
              currPos.x += val;
            if(axis.y > 0.5f)
              currPos.y += val;
            if(axis.z > 0.5f)
              currPos.z += val;

            if(SnappingEnabled)
            {
              currObj->Transform.GetPosition().x = round(currPos.x / SnapGridSize) 
                * SnapGridSize;
              currObj->Transform.GetPosition().y = round(currPos.y / SnapGridSize) 
                * SnapGridSize;
              currObj->Transform.GetPosition().z = round(currPos.z / SnapGridSize) 
                * SnapGridSize;
            }
            else
            {
              currObj->Transform.GetPosition() = currPos;
            }
          }
        }

        void SetAxis(const glm::vec3 a)
        {
          axis = a;
        }
      };

      static TranslateWidget tw[3];

      void Initialize()
      {
        tw[0].SetAxis(glm::vec3(1.0f, 0.0f, 0.0f));
        tw[0].Initialize();
        tw[1].SetAxis(glm::vec3(0.0f, 1.0f, 0.0f));
        tw[1].Initialize();
        tw[2].SetAxis(glm::vec3(0.0f, 0.0f, 1.0f));
        tw[2].Initialize();
      }

      void Update(Core::Handle<Core::Object>& currObj)
      {
        if(!currObj)
          return;

        for(unsigned i = 0; i < 3; ++i)
        {
          tw[i].Update(currObj);
        }
      }

      void Terminate()
      {
        tw[0].Terminate();
        tw[1].Terminate();
        tw[2].Terminate();
      }

    }
  }
}