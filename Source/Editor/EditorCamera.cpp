/*!
  \file EditorCamera.cpp
  \author Chase A Rayment
  \date 11 May 2017
  \brief Contains the implementation of the EditorCamera class.
*/

#include "EditorCamera.hpp"
#include "Space.hpp"
#include "Scene.hpp"
#include "CameraObject.hpp"
#include "Object.hpp"
#include "Camera.hpp"
#include "Input.hpp"
#include "Handle.hpp"
#include "Log.hpp"
#include "imgui/imgui.h"

namespace Echo
{
  namespace Editor
  {
    namespace EditorCamera
    {
      static Core::Handle<Core::Object> cam;
      static bool active = false;

      void Initialize()
      {
        cam = Core::Spaces[Core::SPACE_GAMEPLAY].AddObject();
        cam->AddComponent("Camera");
        cam->SetName("EditorCamera");
        cam->GetComponent<Graphics::Camera>()->Initialize();
      }

      void Update(float dt)
      {
        cam->GetComponent<Graphics::Camera>()->SetActive();
        if(active)
        {
          if(Input::Keyboard::KeyIsDown(SDL_SCANCODE_W))
            cam->Transform.GetPosition() += cam->Transform.GetForward() * dt;
          if(Input::Keyboard::KeyIsDown(SDL_SCANCODE_S))
            cam->Transform.GetPosition() -= cam->Transform.GetForward() * dt;

          if(Input::Keyboard::KeyIsDown(SDL_SCANCODE_D))
            cam->Transform.GetPosition() += cam->Transform.GetRight() * dt;
          if(Input::Keyboard::KeyIsDown(SDL_SCANCODE_A))
            cam->Transform.GetPosition() -= cam->Transform.GetRight() * dt;

          glm::ivec2 pos = Input::Mouse::GetRelativeMousePos();
          cam->Transform.RotateAround(glm::vec3(0, 1, 0), 
            glm::radians(-dt * pos.x * 50.0f));
          cam->Transform.RotateAround(cam->Transform.GetRight(), 
            glm::radians(-dt * pos.y * 50.0f));
        }
        if(Input::Keyboard::KeyIsPressed(SDL_SCANCODE_Z) && 
          !ImGui::IsAnyItemActive())
        {
          active = !active;
          Input::Mouse::SetLocked(active);
        }
      }

      void Terminate()
      {

      }

    }
  }
}