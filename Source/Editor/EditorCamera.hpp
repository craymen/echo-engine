/*!
  \file EditorCamera.hpp
  \author Chase A Rayment
  \date 11 May 2017
  \brief Contains the definition of the EditorCamera system.
*/

#pragma once

namespace Echo
{
  namespace Editor
  {
    namespace EditorCamera
    {
      /*!
        \brief Initializes the editor camera. Should be called after
          a level is loaded.
      */
      void Initialize();

      /*!
        \brief Updates the editor camera
        \param dt The time since the last update.
      */
      void Update(float dt);

      /*!
        \brief Terminates the editor camera. Should be called before a level
          is loaded.
      */
      void Terminate();
    }
  }
}