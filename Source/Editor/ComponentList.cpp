/*!
  \file ComponentList.cpp
  \author Chase A Rayment
  \date 17 April 2017
  \brief Contains the implementation of the ComponentList view.
*/
#include "ComponentList.hpp"
#include "TypeData.hpp"
#include <cstring>
#include "imgui/imgui.h"
#include "TypeData.hpp"
#include "Component.hpp"
#include "FileSystem.hpp"
#include <fstream>
#include "Archetype.hpp"
#include "ResourceManager.hpp"
#include "Window.hpp"

/*! \brief The maximum name length a component can have. */
#define MAX_NAME_INPUT 1024

namespace Echo
{
  namespace Editor
  {
    namespace ComponentList
    {
      static Core::Handle<Core::Object> currObj;

      void Update(Core::Handle<Core::Object> selectedObject)
      {
        // Begin imgui window
        ImGui::SetNextWindowPos(ImVec2(Graphics::Window::GetWidth() - 300, 20));
        ImGui::SetNextWindowSize(
          ImVec2(300, Graphics::Window::GetHeight() - 20));
        ImGui::Begin("Component List", NULL, ImGuiWindowFlags_NoResize | 
          ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse);

        bool newSelection = false;
        if(currObj != selectedObject)
        {
          currObj = selectedObject;
          newSelection = true;
        }
        
        if(currObj)
        {
          // Object name field
          char name[MAX_NAME_INPUT];
          std::strcpy(name, currObj->GetName().c_str());
          ImGui::InputText("Name", name, MAX_NAME_INPUT);
          currObj->SetName(std::string(name));

          // Archetype name
          char archName[MAX_NAME_INPUT];
          std::strcpy(archName, currObj->ArchetypeName.c_str());
          ImGui::InputText("Archetype Name", archName, MAX_NAME_INPUT);
          currObj->ArchetypeName = std::string(archName);

          if (ImGui::Button("Save"))
          {
            std::string path = Core::FileSystem::GetGameDir(
              Core::FileSystem::Dir::Archetypes) + "/" + currObj->ArchetypeName
              + ".json";
            {
              std::ofstream file(path);
              Json::Value val;
              Core::Variable v(Core::TypeData::FindType<Core::Object>(), 
                (void*)currObj);
              std::string orig = currObj->ArchetypeName;
              currObj->ArchetypeName = "";
              Core::SerializeType<Core::Object>(v, val);
              currObj->ArchetypeName = orig;
              file << val;
            }
            Core::UpdateAllResources();
            for(auto o : Core::Spaces[Core::SPACE_GAMEPLAY].GetAllObjects())
              if(o->ArchetypeName == archName)
                Core::ResourceManager<Core::Archetype>::Find(archName)
                  ->ApplyArchetype(o);
          }

          ImGui::SameLine();

          if (ImGui::Button("Revert"))
          {
            Core::Handle<Core::Archetype> arch = 
              Core::ResourceManager<Core::Archetype>::Find(
              currObj->ArchetypeName);
            if (arch)
            {
              currObj->RemoveAllComponents();
              arch->ApplyArchetype(currObj);
              currObj->Reinitialize();
            }
          }

          // Transform fields
          Core::EditType<Graphics::Transform>(&currObj->Transform, 
            "Transform");

          const auto& components = currObj->GetAllComponents();
          for(auto comp : components)
          {
            const Core::TypeData* td = Core::TypeData::FindType(comp.first);
            td->GetEditorWidgetFunc()(comp.second, td->GetName());
          }

          ImGui::Separator();

          // Component add
          std::vector<const char*> comps;
          static int selected = 0;
          for(auto type : Core::TypeData::GetAllTypesDerivedFrom(
            Core::TypeData::FindType<Core::Component>()))
          {
            if(selectedObject->GetComponent(type.first) == nullptr)
              comps.push_back(type.second->GetName().c_str());
          }
          ImGui::Combo("##NewComponentName", &selected, &comps[0], comps.size());

          ImGui::SameLine();

          if(ImGui::Button("Add##AddComponent"))
          {
            Core::Handle<Core::Component> c = 
              selectedObject->AddComponent(std::string(comps[selected]));
            c->EditorInitialize();
          }

          // Component remove
          std::vector<const char*> comps2;
          static int selected2 = 0;
          for(auto type : Core::TypeData::GetAllTypesDerivedFrom(
            Core::TypeData::FindType<Core::Component>()))
          {
            if(selectedObject->GetComponent(type.first) != nullptr)
              comps2.push_back(type.second->GetName().c_str());
          }
		  if(comps2.size() > 0)
			ImGui::Combo("##RemoveComponentName", &selected2, &comps2[0], comps2.size());

          ImGui::SameLine();

          if(ImGui::Button("Remove##RemoveComponent"))
            selectedObject->RemoveComponent(std::string(comps2[selected2]));
        }

        ImGui::End();
      }
    }
  }
} 