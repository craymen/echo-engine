/*!
  \file Scene.hpp
  \author Chase A Rayment
  \date 10 May 2017
  \brief Contains the definition of the Scene class.
*/

#pragma once

#include "ModelObject.hpp"
#include "CameraObject.hpp"
#include "TextObject.hpp"
#include "PointLightObject.hpp"
#include <vector>
#include "DebugDrawer.hpp"
#include "FrameBufferObject.hpp"
#include "RenderBufferObject.hpp"
#include "Handle.hpp"
#include "Texture.hpp"
#include <unordered_map>
#include <string>
#include "Cubemap.hpp"
#include "ReflectionProbeObject.hpp"

namespace Echo
{
  namespace Graphics
  {
    void Update();

    /*!
      \brief A graphically-independent scene containing various kinds of
        objects.
    */
    class Scene
    {
    public:
      friend void Update();
      /*!
        \brief Creates a scene.
      */
      Scene();

      /*!
        \brief Destroys a scene.
      */
      ~Scene();

      /*!
        \brief Adds a model object to the scene.
        \returns The new model object.
      */
      ModelObject* AddModel();

      /*!
        \brief Removes a model object from the scene.
        \param obj The model object to remove.
      */
      void RemoveModel(ModelObject* obj);

      /*!
        \brief Adds a text object to the scene.
        \returns The new test object.
      */
      TextObject* AddText();

      /*!
        \brief Removes a text object from the scene.
        \param obj The text object to remove.
      */
      void RemoveText(TextObject* obj);

      /*!
        \brief Adds a camera to the scene.
        \returns The new camera object.
      */
      CameraObject* AddCamera();

      /*!
        \brief Removes a camera from the scene.
        \param obj The camera object to remove.
      */
      void RemoveCamera(CameraObject* obj);

      /*!
        \brief Adds a point light to the scene.
        \returns The new point light.
      */
      PointLightObject* AddPointLight();

      /*!
        \brief Removes a point light from the scene.
        \param obj The point light to remove.
      */
      void RemovePointLight(PointLightObject* obj);

      /*!
        \brief Adds a reflection probe to the scene.
        \returns The new reflection probe.
      */
      ReflectionProbeObject* AddReflectionProbe();

      /*!
        \brief Removes a reflection probe from the scene.
        \param obj The reflection probe to remove.
      */
      void RemoveReflectionProbe(ReflectionProbeObject* obj);

      /*!
        \brief Sets the scene's current camera.
        \param obj The camera to use.
      */
      void SetCurrentCamera(CameraObject* obj);

      /*!
        \brief Gets the scene's current camera.
      */
      CameraObject* GetCurrentCamera();

      /*!
        \brief Draws a scene.
      */
      void Draw();

      /*!
        \brief Gets the scene's debug drawer.
      */
      DebugDrawer& GetDebugDrawer();

      /*!
        \brief Sets the render depth of this scene.
      */
      void SetDepth(float depth);

      /*!
        \brief Sets the current skybox of this scene.
      */
      void SetSkybox(Core::Handle<Cubemap> skybox);

      /*!
        \brief Bakes all reflection probes.
      */
      void BakeReflections();

			/*!
				\brief Gets the polycount of this scene.
			*/
			unsigned GetPolycount();

    private:
      enum gbuffers { gbuffer_position, gbuffer_normal, gbuffer_diffuse,
        gbuffer_specular, gbuffer_diffibl, gbuffer_specibl, gbuffer_count };

      std::vector<ModelObject*> models;   // All the model objects in the scene
      std::vector<TextObject*> texts;     // All the text objects in the scene
      std::vector<CameraObject*> cameras; // All the camera objects in the scene
        // All the point lights in the scene
      std::vector<PointLightObject*> pointlights; 
      CameraObject* current_camera;       // The current camera
      DebugDrawer debug_drawer;           // The debug drawer
      FrameBufferObject hdr_fbo;          // The HDR framebuffer
        // The HDR depth stencil
      Core::Handle<RenderBufferObject> hdr_rbo;         
      Core::Handle<Texture> hdr_texture;  // The HDR texture
      Core::Handle<Texture> gbuffer[gbuffer_count];   // The G-Buffer.
      float depth;                        // Depth to draw the scene at
      FrameBufferObject gbuffer_fbo;      // The gbuffer framebuffer
      Core::Handle<Cubemap> skybox;       // The current skybox
      std::vector<ReflectionProbeObject*> reflection_probes;

      void draw_opaque_objects(const std::unordered_map<std::string, 
        std::vector<ModelObject*> >& objs, FrameBufferObject& out_fbo,
        FrameBufferObject& gbuf_fbo, Core::Handle<Texture>* gbuf, 
        const glm::mat4& V, const glm::mat4& P);
        
      void draw_transparent_objects(const std::unordered_map<std::string, 
        std::vector<ModelObject*> >& objs, FrameBufferObject& out_fbo,
        const glm::mat4& V, const glm::mat4& P);

      std::unordered_map<std::string, std::unordered_map<std::string, 
        std::vector<ModelObject*> > > separate_by_render_stage();

      void draw_skybox(const glm::mat4& V, const glm::mat4& P);
    };
  }
}