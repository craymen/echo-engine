/*!
  \file Mesh.cpp
  \author Chase A Rayment
  \date 10 May 2017
  \brief Contains the implementation of the Mesh class.
*/

#include "Mesh.hpp"
#include "FileSystem.hpp"
#include "Log.hpp"
#include "Debug.hpp"
#include "GraphicsCore.hpp"
#include "ResourceManager.hpp"

namespace Echo
{
  namespace Graphics
  {
    Mesh::Mesh(const std::string& path) : Resource(path), meshes(), polycount(0)
    {
      // Create scene
      Assimp::Importer importer;
      const aiScene* scene = importer.ReadFile(path.c_str(), 
        aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

      // Check for error
      if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || 
        !scene->mRootNode)
      {
        Core::Log << Core::Priority::Warning << "Error loading Mesh " << 
          path << std::endl;
        return;
      }

      // Recursively process nodes
      process_node(scene->mRootNode, scene);
    }

    Mesh::~Mesh()
    {
      // Free all VAOs
      for(VertexArrayObject* vao : meshes)
        delete vao;
    }

    void Mesh::process_node(aiNode* node, const aiScene* scene)
    {
      // Process all meshes in the node
      for(unsigned i = 0; i < node->mNumMeshes; ++i)
      {
        // Get the mesh
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];

        // Add the mesh to the mesh list
        meshes.push_back(process_mesh(mesh, scene));
      }

      // Recursively process children
      for(unsigned i = 0; i < node->mNumChildren; ++i)
        process_node(node->mChildren[i], scene);
    }

    VertexArrayObject* Mesh::process_mesh(aiMesh* mesh, const aiScene*)
    {
      // Create a new VertexArrayObject
      VertexArrayObject* result = new VertexArrayObject;

      // Process vertices
      std::vector<Vertex> vertices;
      for(unsigned i = 0; i < mesh->mNumVertices; ++i)
      {
        Vertex v;
        v.Position.x = mesh->mVertices[i].x;
        v.Position.y = mesh->mVertices[i].y;
        v.Position.z = mesh->mVertices[i].z;
        v.Normal.x = mesh->mNormals[i].x;
        v.Normal.y = mesh->mNormals[i].y;
        v.Normal.z = mesh->mNormals[i].z;
        if(mesh->mTextureCoords[0])
        {
          v.TexCoord.x = mesh->mTextureCoords[0][i].x;
          v.TexCoord.y = mesh->mTextureCoords[0][i].y;
        }
        if(mesh->mTangents)
        {
          v.Tangent.x = mesh->mTangents[i].x;
          v.Tangent.y = -mesh->mTangents[i].y;
          v.Tangent.z = mesh->mTangents[i].z;
        }
        vertices.push_back(v);
      }

      for(auto& v : vertices)
      {
        //if(maxcoord > 0.0f)
        //  v.Position /= maxcoord * 2.0f;
        result->GetVBO().PushVertex(v);
      }

      // Process indices
      for(unsigned i = 0; i < mesh->mNumFaces; ++i)
      {
        aiFace face = mesh->mFaces[i];
				++polycount;
        for(unsigned j = 0; j < face.mNumIndices; ++j)
          result->GetEBO().PushIndex(face.mIndices[j]);
      }

      result->Build();

      return result;
    }

    void Mesh::Draw()
    {
      for(VertexArrayObject* vao : meshes)
        vao->Draw();
    }

    void Mesh::DrawMultiple(const std::vector<glm::mat4>& transforms)
    {
      for(VertexArrayObject* vao : meshes)
        vao->DrawInstanced(transforms);
    }

    Core::Handle<Mesh> Mesh::GetDefault()
    {
      return Core::ResourceManager<Mesh>::Find("DefaultModel");
    }

		unsigned Mesh::GetPolycount()
		{
			return polycount;
		}
  }
}