/*!
  \file FrameBufferObject.hpp
  \author Chase A Rayment
  \date 21 May 2017
  \brief Contains the definition of the FrameBufferObject class.
*/

#pragma once

#include "Platform.hpp"
#include "Handle.hpp"
#include "RenderBufferObject.hpp"
#include "Texture.hpp"
#include <vector>
#include "Cubemap.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Represents an OpenGL FrameBufferObject.
    */
    class FrameBufferObject
    {
    public:
      /*!
        \brief Creates a new FrameBufferObject.
      */
      FrameBufferObject();

      /*!
        \brief Destroys a FrameBufferObject.
      */
      ~FrameBufferObject();

      /*!
        \brief Binds a FrameBufferObject.
      */
      void Bind();

      /*!
        \brief Unbinds all FrameBufferObjects.
      */
      static void Unbind();

      /*!
        \brief Attaches a texture to a FrameBufferObject.
        \param texture The texture to attach.
      */
      void AttachTexture(Core::Handle<Texture> texture);

      /*!
        \overload
      */
      void AttachTextures(Core::Handle<Texture>* textures, unsigned count);

      /*!
        \brief Attaches a RenderBufferObject to a FrameBufferObject.
        \param rbo The RenderBufferObject to attach.
      */
      void AttachRenderBuffer(Core::Handle<RenderBufferObject> rbo);

      /*!
        \brief Attaches a cubemap face to a FrameBufferObject.
        \param map The cubemap to attach.
        \param face The face to attach.
      */
      void AttachCubemapFace(Core::Handle<Cubemap> map, GLenum face, 
        int mipLevel=0);

    private:
      GLuint id;                // The OpenGL ID of the FBO.

      void bind(); // Binds the FBO.
    };
  }
}