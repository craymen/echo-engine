/*!
  \file Camera.hpp
  \author Chase A Rayment
  \date 11 May 2017
  \brief Contains the definition of the Camera class.
*/

#pragma once

#include "Component.hpp"
#include "PreDrawEvent.hpp"
#include "CameraObject.hpp"
#include "GLM/glm.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief A camera component for an Object.
    */
    class Camera : public Core::Component
    {
    public:
      /*!
        \brief Constructs a new Camera component.
      */
      Camera();
      
      /*!
        \brief Initializes a Camera component.
      */
      void Initialize();

      /*!
        \brief Terminates a Camera component.
      */
      void Terminate();

      /*!
        \brief Initializes a Model component in the editor.
      */
      void EditorInitialize();

      /*!
        \brief Terminates a Model component in the editor.
      */
      void EditorTerminate();

      /*!
        \brief Handles the pre-draw update by updating the scene's camera
          object.
      */
      void PreDraw(PreDrawEvent&);

      /*!
        \brief Sets this camera as the currently active camera.
      */
      void SetActive();

      /*!
        \brief Debug draws this Camera.
      */
      void DebugDraw();

      bool IsOrtho; //!< True if this camera is orthographic, false otherwise
      float Fov;    //!< The FOV of the camera, in degrees. (for perspective)
      float Size;   //!< The size of the camera (for orthographic)
      float NearZ;  //!< The near Z plane of the camera.
      float FarZ;   //!< The far Z plane of the camera.

    private:
      CameraObject* obj; // The camera object in the scene.
    };
  }
}