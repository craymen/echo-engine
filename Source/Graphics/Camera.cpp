/*!
  \file Camera.cpp
  \author Chase A Rayment
  \date 11 May 2017
  \brief Contains the implementation of the Camera class.
*/

#include "Camera.hpp"
#include "Space.hpp"
#include "Object.hpp"
#include "Scene.hpp"
#include "Handle.hpp"

namespace Echo
{
  namespace Graphics
  {
    Camera::Camera() : IsOrtho(false), Fov(90.0f), Size(5.0f), NearZ(0.01f), 
      FarZ(100.0f), obj(nullptr)
    {

    }
    
    void Camera::Initialize()
    {
      // Create the camera object
      obj = GetSpace()->GetGraphicsScene()->AddCamera();

      // Subscribe to predraw event
      Core::ConnectEvent(nullptr, "PreDraw", this, &Camera::PreDraw);

      // TODO: remove this
      GetSpace()->GetGraphicsScene()->SetCurrentCamera(obj);
    }
    
    void Camera::Terminate()
    {
      // Delete the camera object
      GetSpace()->GetGraphicsScene()->RemoveCamera(obj);

      Component::Terminate();
      Core::Handle<Camera>::InvalidateHandles(this);
    }
    
    void Camera::PreDraw(PreDrawEvent&)
    {
      Core::Handle<Core::Object> o = GetOwner();
      // TODO: determine if these actually need to be set for caching
      // optimization

      obj->SetEye(o->Transform.GetPosition());
      obj->SetLookDir(o->Transform.GetForward());
      obj->SetUp(o->Transform.GetUp());
      obj->SetIsOrtho(IsOrtho);
      obj->SetFov(Fov);
      obj->SetSize(Size);
      obj->SetNearZ(NearZ);
      obj->SetFarZ(FarZ);
    }

    void Camera::SetActive()
    {
      GetSpace()->GetGraphicsScene()->SetCurrentCamera(obj);
    }

    void Camera::DebugDraw()
    {
      GetSpace()->GetGraphicsScene()->GetDebugDrawer().DrawLine(
        GetOwner()->Transform.GetPosition(), GetOwner()->Transform.GetPosition() 
        + GetOwner()->Transform.GetForward());
      GetSpace()->GetGraphicsScene()->GetDebugDrawer().DrawLine(
        GetOwner()->Transform.GetPosition(), GetOwner()->Transform.GetPosition() 
        + GetOwner()->Transform.GetRight());
      GetSpace()->GetGraphicsScene()->GetDebugDrawer().DrawLine(
        GetOwner()->Transform.GetPosition(), GetOwner()->Transform.GetPosition() 
        + GetOwner()->Transform.GetUp());
    }

    void Camera::EditorInitialize()
    {
      Initialize();
    }

    void Camera::EditorTerminate()
    {
      Terminate();
    }
  }
}