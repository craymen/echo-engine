/*!
  \file Font.hpp
  \author Chase A Rayment
  \date 13 June 2017
  \brief Contains the definition of the Font class.
*/
#pragma once

#include <vector>
#include "ft2build.h"
#include FT_FREETYPE_H
#include "Platform.hpp"
#include "GLM/glm.hpp"
#include "Texture.hpp"
#include "Handle.hpp"
#include "Resource.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Holds information about a loaded font.
    */
    class Font : public Core::Resource
    {
    public:
      /*!
        \brief Holds data about a glyph in a font.
      */
      struct Character
      {
        Texture* Tex;      //!< The texture of the glyph.
        glm::vec2 Size;    //!< The size of the glyph.
        glm::vec2 Bearing; //!< The bearing coordinates of the glyph.
        float Advance;     //!< How long to advance after drawing this glyph.
      };

      /*!
        \brief Loads a new Font.
        \param filename The filename to load.
      */
      Font(const std::string& filename);

      /*!
        \brief Destroys a font.
      */
      ~Font();

      /*!
        \brief Gets a character from the font.
        \param c The character to get.
      */
      Character GetCharacter(unsigned char c);

      /*!
        \brief Gets the default font.
      */
      static Core::Handle<Font> GetDefault();

      /*!
        \brief Shuts down FreeType.
      */
      static void Shutdown();

    private:
      FT_Face face; // The FreeType face of the character.
      
        // All characters in the font.
      std::unordered_map<unsigned char, Character> characters;
    };
  }
}