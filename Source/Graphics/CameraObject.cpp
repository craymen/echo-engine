/*!
  \file CameraObject.cpp
  \author Chase A Rayment
  \date 10 May 2017
  \brief Contains the implementation of the CameraObject class.
*/

#include "CameraObject.hpp"
#include "Transform.hpp"
#include "Window.hpp"
#include "glm/gtx/string_cast.hpp"

namespace Echo
{
  namespace Graphics
  {
    CameraObject::CameraObject() : is_ortho(false), eye(), look_dir(), 
      near_z(0.01f), far_z(100.0f), size(5.0f), dirty_v(true), dirty_p(true),
      v(), p()
    {

    }

    void CameraObject::SetIsOrtho(bool val)
    {
      is_ortho = val;
      dirty_p = true;
    }

    bool CameraObject::GetIsOrtho() const
    {
      return is_ortho;
    }

    void CameraObject::SetEye(const glm::vec3& val)
    {
      eye = val;
      dirty_v = true;
    }

    const glm::vec3& CameraObject::GetEye() const
    {
      return eye;
    }

    void CameraObject::SetLookDir(const glm::vec3& val)
    {
      look_dir = val;
      dirty_v = true;
    }

    const glm::vec3& CameraObject::GetLookDir() const
    {
      return look_dir;
    }

    void CameraObject::SetUp(const glm::vec3& val)
    {
      up = val;
      dirty_v = true;
    }

    const glm::vec3& CameraObject::GetUp() const
    {
      return up;
    }

    void CameraObject::SetNearZ(float val)
    {
      near_z = val;
      dirty_p = true;
    }

    float CameraObject::GetNearZ() const
    {
      return near_z;
    }

    void CameraObject::SetFarZ(float val)
    {
      far_z = val;
      dirty_p = true;
    }

    float CameraObject::GetFarZ() const
    {
      return far_z;
    }

    void CameraObject::SetSize(float val)
    {
      size = val;
      dirty_p = true;
    }

    float CameraObject::GetSize() const
    {
      return size;
    }

    void CameraObject::SetFov(float val)
    {
      fov = val;
      dirty_p = true;
    }

    float CameraObject::GetFov() const
    {
      return fov;
    }

    const glm::mat4& CameraObject::GetViewMatrix() const
    {
      if(dirty_v)
      {
        v = Transform::WorldToCamera(eye, look_dir, up);
        dirty_v = false;
      }
      return v;
    }

    // TODO: mark projection as dirty on resize
    const glm::mat4& CameraObject::GetProjectionMatrix() const
    {
      if(true)
      {
        if(is_ortho)
          p = Transform::CameraToScreenOrth(size, 
            Window::GetAspectRatio(), 
            near_z, far_z);
        else
          p = Transform::CameraToScreenPerspective(glm::radians(fov), 
            Window::GetAspectRatio(), near_z, far_z);
        dirty_p = false;
      }
      return p;
    }

    void CameraObject::ScreenCoordsToWorldRay(glm::ivec2 coords, 
      glm::vec3& start, glm::vec3& dir) const
    {
      glm::vec2 screenpos;
      screenpos.x = coords.x / (Window::GetWidth() * 0.5f) - 1.0f;
      screenpos.y = coords.y / (Window::GetHeight() * 0.5f) - 1.0f;

      glm::mat4 invVP = glm::inverse(GetProjectionMatrix() * GetViewMatrix());
      glm::vec4 vec = glm::vec4(screenpos.x, -screenpos.y, -1.0f, 1.0f);
      glm::vec4 worldPos = invVP * vec;

      worldPos /= worldPos.w;

      start = glm::vec3(worldPos);
      if(!is_ortho)
        dir = glm::normalize(glm::vec3(worldPos) - eye);
      else
        dir = GetLookDir();
    }
  }
}