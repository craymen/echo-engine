/*!
  \file VertexArrayObject.hpp
  \author Chase A Rayment
  \date 16 April 2017
  \brief Contains the definition of the VertexArrayObject class.
*/
#pragma once

#ifdef _WIN32
#include "GLEW/glew.h"
#endif

#include "VertexBufferObject.hpp"
#include "ElementBufferObject.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Wraps an OpenGL VertexArrayObject.
    */
    class VertexArrayObject
    {
    public:

      /*!
        \brief Constructs a new VertexArrayObject.
      */
      VertexArrayObject();

      /*!
        \brief Destroys this VertexArrayObject.
      */
      ~VertexArrayObject();

      /*!
        \brief Binds this VertexArrayObject.
      */
      void Bind();

      /*!
        \brief Builds this VertexArrayObject.
      */
      void Build();

      /*!
        \brief Unbinds all VertexArrayObjects.
      */
      static void Unbind();

      /*!
        \brief Gets the VBO of this object.
      */
      VertexBufferObject& GetVBO();

      /*! \overload */
      const VertexBufferObject& GetVBO() const;

      /*!
        \brief Gets the EBO of this object.
      */
      ElementBufferObject& GetEBO();

      /*! \overload */
      const ElementBufferObject& GetEBO() const;

      /*!
        \brief Draws a VertexArrayObject.
      */
      void Draw();

      /*!
        \brief Draws a VertexArrayObject via instanced rendering.
        \param transforms A list of transforms to draw with.
      */
      void DrawInstanced(const std::vector<glm::mat4>& transforms);

      /*!
        \brief Clears all data from the object.
      */
      void Clear();

    private:
      GLuint id;
      VertexBufferObject vbo;
      ElementBufferObject ebo;
	  GLuint instance_id;            // The OpenGL id of the instance buffer.
    };
  }
}