/*!
  \file StressTester.hpp
  \author Chase A Rayment
  \date 18 January 2018
  \brief Contains the definition of the StressTester component.
*/

#pragma once

#include "Component.hpp"

namespace Echo
{
  namespace Graphics
  {
    class StressTester : public Core::Component
    {
    public:
	  StressTester();
      void Initialize();
      void Update(Core::UpdateEvent& evt);
      void Terminate();

    private:
	  std::vector<Core::Handle<Core::Object> > objs;
	};
  }
}