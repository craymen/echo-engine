/*!
  \file FrameBufferObject.cpp
  \author Chase A Rayment
  \date 21 May 2017
  \brief Contains the implementation of the FrameBufferObject class.
*/

#include "FrameBufferObject.hpp"
#include "GraphicsCore.hpp"
#include "Log.hpp"

namespace Echo
{
  namespace Graphics
  {
    FrameBufferObject::FrameBufferObject() : id(0)
    {
      glGenFramebuffers(1, &id);
      CheckGL();
    }

    FrameBufferObject::~FrameBufferObject()
    {
      glDeleteFramebuffers(1, &id);
      CheckGL();
    }
    
    void FrameBufferObject::Bind()
    {
      bind();
      if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        Core::Log << Core::Priority::Warning << 
          "Trying to use an incomplete FrameBufferObject!" << std::endl;
    }

    void FrameBufferObject::Unbind()
    {
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
      CheckGL();
    }

    void FrameBufferObject::AttachTexture(Core::Handle<Texture> texture)
    {
      bind();
      glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
        GL_TEXTURE_2D, texture->GetID(), 0);
      GLenum attach = GL_COLOR_ATTACHMENT0;
      CheckGL();
      Unbind();
    }

    void FrameBufferObject::AttachTextures(Core::Handle<Texture>* textures, 
      unsigned count)
    {
      bind();
      for(unsigned i = 0; i < count; ++i)
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i,
        GL_TEXTURE_2D, textures[i]->GetID(), 0);
      GLenum* attachments = new GLenum[count];
      for(unsigned i = 0; i < count; ++i)
        attachments[i] = GL_COLOR_ATTACHMENT0 + i;
      glDrawBuffers(count, attachments);
      delete[] attachments;
      CheckGL();
      Unbind();
    }

    void FrameBufferObject::AttachRenderBuffer(
      Core::Handle<RenderBufferObject> rbo)
    {
      bind();
      glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, 
        GL_RENDERBUFFER, rbo->GetID());
      CheckGL();
      Unbind();
    }

    void FrameBufferObject::bind()
    {
      glBindFramebuffer(GL_FRAMEBUFFER, id);
      CheckGL();
    }

    void FrameBufferObject::AttachCubemapFace(Core::Handle<Cubemap> map, 
      GLenum face, int mipLevel)
    {
      bind();
      glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
        face, map->GetID(), mipLevel);
      CheckGL();
    }
  }
}