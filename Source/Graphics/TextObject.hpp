/*!
  \file TextObject.hpp
  \author Chase A Rayment
  \date 13 June 2017
  \brief Contains the definition of the TextObject class.
*/

#pragma once

#include "Font.hpp"
#include "Handle.hpp"
#include "GLM/glm.hpp"
#include "Material.hpp"
#include "VertexArrayObject.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Represents 3D text in a scene.
    */
    class TextObject
    {
    public:
      /*!
        \brief Creates a new TextObject.
      */
      TextObject();
      
      /*!
        \brief Gets the current font.
      */
      Core::Handle<Font> GetFont() const;

      /*!
        \brief Sets the font.
        \param font The new font.
      */
      void SetFont(Core::Handle<Font> font);

      /*!
        \brief Gets the transform matrix.
      */
      glm::mat4 GetTransform() const;

      /*!
        \brief Sets the transform matrix.
        \param transform The new transform matrix.
      */
      void SetTransform(const glm::mat4& transform);

      /*!
        \brief Gets the text.
      */
      std::string GetText() const;

      /*!
        \brief Sets the text.
        \param text The new text.
      */
      void SetText(const std::string& text);

      /*!
        \brief Gets the material.
      */
      Core::Handle<Material> GetMaterial() const;

      /*!
        \brief Sets the material.
        \param material The new material.
      */
      void SetMaterial(Core::Handle<Material> material);

      /*!
        \brief Draws the object.
      */
      void Draw();

      /*!
        \brief Sets the horizontal centering mode.
      */
      void SetHorizontalCenteringMode(int mode);

      /*!
        \brief Sets the vertical centering mode.
      */
      void SetVerticalCenteringMode(int mode);

      /*!
        \brief Sets this object's visibility.
      */
      void SetVisibility(bool v);

      /*!
        \brief Gets this object's visibility.
      */
      bool GetVisibility();

    private:
      Core::Handle<Font> font;          // The font to draw with.
      glm::mat4 transform;              // The object-to-world matrix.
      std::string text;                 // The text to draw.
      Core::Handle<Material> material;  // The material to draw with.
      VertexArrayObject vao;            // The VAO for the text.
      int hcenter, vcenter;             // The centering modes.
      std::vector<glm::vec2> positions; // The positions to draw characters at.
      bool visible;                     // Whether or not this object is visible
    };
  }
}