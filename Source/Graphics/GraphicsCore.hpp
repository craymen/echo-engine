/*!
  \file GraphicsCore.hpp
  \author Chase A Rayment
  \date 15 April 2017
  \brief Contains the definition of the Graphics system.
*/

#pragma once

namespace Echo
{
  namespace Graphics
  {
    class Scene;

    /*!
      \brief Initializes the Graphics system.
    */
    void Initialize();

    /*!
      \brief Updates the Graphics system.
    */
    void Update();

    /*!
      \brief Terminates the Graphics system.
    */
    void Terminate();

    /*!
      \brief Gets a new scene.
    */
    Scene* AddScene();

    /*!
      \brief Removes a scene.
      \param scene The scene to remove.
    */
    void RemoveScene(Scene* scene);

    /*!
      \brief Triggers a frame capture in RenderDoc.
    */
    void TriggerRenderDocCapture();

    /*!
      \brief Starts a capture in RenderDoc.
    */
    void StartRenderDocCapture();

    /*!
      \brief Ends a capture in RenderDoc.
    */
    void EndRenderDocCapture();
  }
}

#include "GraphicsCore.inl"