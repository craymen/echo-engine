/*!
  \file ModelObject.cpp
  \author Chase A Rayment
  \date 10 May 2017
  \brief Contains the implementation of the ModelObject class.
*/

#include "ModelObject.hpp"
#include "Debug.hpp"
#include "Transform.hpp"
#include "Log.hpp"

namespace Echo
{
  namespace Graphics
  {
    ModelObject::ModelObject() : mesh_to_draw(nullptr), 
      material_to_draw(nullptr), transform()
    {

    }

    void ModelObject::SetMesh(Core::Handle<Mesh> mesh)
    {
      mesh_to_draw = mesh;
    }

    Core::Handle<Mesh> ModelObject::GetMesh() const
    {
      return mesh_to_draw;
    }

    void ModelObject::SetMaterial(Core::Handle<Material> mat)
    {
      material_to_draw = mat;
    }

    Core::Handle<Material> ModelObject::GetMaterial() const
    {
      return material_to_draw;
    }

    void ModelObject::Draw()
    {
      ASSERT(mesh_to_draw != nullptr);

      mesh_to_draw->Draw();
    }

    const glm::mat4& ModelObject::GetTransform() const
    {
      return transform;
    }

    void ModelObject::SetTransform(const glm::mat4& val)
    {
      transform = val;
    }

    glm::vec3 ModelObject::GetPosition() const
    {
      glm::vec4 v = transform * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
      return glm::vec3(v.x, v.y, v.z);
    }
  }
}
