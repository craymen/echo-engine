/*!
  \file Vertex.cpp
  \author Chase A Rayment
  \date 9 May 2017
  \brief Contains the implementation of the Vertex class.
*/

#include "Vertex.hpp"

namespace Echo
{
  namespace Graphics
  {
    const unsigned Vertex::AttributeCounts[Vertex::NumAttributes] = 
      { 3, 3, 4, 2, 3, 16 };

    const unsigned Vertex::AttributeSizes[Vertex::NumAttributes] = 
      { sizeof(glm::vec3), sizeof(glm::vec3), sizeof(glm::vec4), 
        sizeof(glm::vec2), sizeof(glm::vec3), sizeof(glm::mat4) };

    const bool Vertex::AttributeNormalized[Vertex::NumAttributes] = 
      { false, true, false, false, true, false };

	const unsigned Vertex::AttributeDivisor[NumAttributes] =
	{ 0, 0, 0, 0, 0, 1 };
  }
}