/*!
  \file IBL.cpp
  \author Chase A Rayment
  \date 16 July 2017
  \brief Contains the implementation of IBL utility functions.
*/

#include "IBL.hpp"
#include "FrameBufferObject.hpp"
#include "RenderBufferObject.hpp"
#include "Shader.hpp"
#include "Transform.hpp"
#include "Mesh.hpp"
#include "ResourceManager.hpp"

namespace Echo
{
  namespace Graphics
  {
    namespace IBL
    {
      Core::Handle<Cubemap> DiffuseConvolution(Core::Handle<Cubemap> 
        environment)
      {
        FrameBufferObject fbo;
        RenderBufferObject rbo(32U, 32U, GL_DEPTH24_STENCIL8);
        Core::Handle<Cubemap> result = new Cubemap(32, 32);
        fbo.AttachRenderBuffer(&rbo);
        Core::Handle<Shader> sh = Core::ResourceManager<Shader>::
          Find("DiffuseConvolution");
        sh->Bind();
        sh->SetUniform("EnvironmentMap", environment->Attach());
        sh->SetUniform("P", Transform::CameraToScreenPerspective(
          glm::radians(90.0f), 1.0f, 0.1f, 10.0f));
        glm::mat4 views[] = 
        {
          Transform::WorldToCamera(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), 
            glm::vec3(0, -1, 0)),
          Transform::WorldToCamera(glm::vec3(0, 0, 0), glm::vec3(-1, 0, 0), 
            glm::vec3(0, -1, 0)),
          Transform::WorldToCamera(glm::vec3(0, 0, 0), glm::vec3(0, 1, 0), 
            glm::vec3(0, 0, 1)),
          Transform::WorldToCamera(glm::vec3(0, 0, 0), glm::vec3(0, -1, 0), 
            glm::vec3(0, 0, -1)),
          Transform::WorldToCamera(glm::vec3(0, 0, 0), glm::vec3(0, 0, 1), 
            glm::vec3(0, -1, 0)),
          Transform::WorldToCamera(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), 
            glm::vec3(0, -1, 0))
        };
        glViewport(0, 0, 32, 32);
        glDepthFunc(GL_ALWAYS);
        glDisable(GL_CULL_FACE);
        fbo.Bind();
        for(unsigned i = 0; i < 6; ++i)
        {
          sh->SetUniform("V", views[i]);
          fbo.AttachCubemapFace(result, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i);
          glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
          Core::ResourceManager<Mesh>::Find("DefaultMesh")->Draw();
        }

        Cubemap::Unbind();
        FrameBufferObject::Unbind();
        return result;
      }

      Core::Handle<Cubemap> SpecularConvolution(Core::Handle<Cubemap>
        environment)
      {
        FrameBufferObject fbo;
        Core::Handle<Cubemap> result = new Cubemap(128, 128, true);
        Core::Handle<Shader> sh = Core::ResourceManager<Shader>::
          Find("SpecularConvolution");
        sh->Bind();
        sh->SetUniform("EnvironmentMap", environment->Attach());
        sh->SetUniform("P", Transform::CameraToScreenPerspective(
          glm::radians(90.0f), 1.0f, 0.1f, 10.0f));
        glm::mat4 views[] = 
        {
          Transform::WorldToCamera(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), 
            glm::vec3(0, -1, 0)),
          Transform::WorldToCamera(glm::vec3(0, 0, 0), glm::vec3(-1, 0, 0), 
            glm::vec3(0, -1, 0)),
          Transform::WorldToCamera(glm::vec3(0, 0, 0), glm::vec3(0, 1, 0), 
            glm::vec3(0, 0, 1)),
          Transform::WorldToCamera(glm::vec3(0, 0, 0), glm::vec3(0, -1, 0), 
            glm::vec3(0, 0, -1)),
          Transform::WorldToCamera(glm::vec3(0, 0, 0), glm::vec3(0, 0, 1), 
            glm::vec3(0, -1, 0)),
          Transform::WorldToCamera(glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), 
            glm::vec3(0, -1, 0))
        };
        glDepthFunc(GL_ALWAYS);
        glDisable(GL_CULL_FACE);
        for(unsigned mipLevel = 0; mipLevel < 5; ++mipLevel)
        {
          unsigned mipSize = 128 * std::pow(0.5, mipLevel);
          glViewport(0, 0, mipSize, mipSize);
          RenderBufferObject rbo(mipSize, mipSize, GL_DEPTH24_STENCIL8);
          fbo.AttachRenderBuffer(&rbo);
          fbo.Bind();
          float roughness = (float)mipLevel/4.0f;
          sh->SetUniform("Roughness", roughness);
          for(unsigned i = 0; i < 6; ++i)
          {
            sh->SetUniform("V", views[i]);
            fbo.AttachCubemapFace(result, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 
              mipLevel);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            Core::ResourceManager<Mesh>::Find("DefaultMesh")->Draw();
          }
        }

        Cubemap::Unbind();
        FrameBufferObject::Unbind();
        return result;
      }

      Core::Handle<Texture> BRDFIntegration()
      {
        FrameBufferObject fbo;
        Core::Handle<Texture> result = new Texture(512, 512);
        RenderBufferObject rbo(512U, 512U, GL_DEPTH24_STENCIL8);
        glViewport(0, 0, 512, 512);
        fbo.AttachTexture(result);
        fbo.AttachRenderBuffer(&rbo);
        fbo.Bind();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDepthFunc(GL_ALWAYS);
        glDisable(GL_CULL_FACE);        
        Core::Handle<Shader> sh = 
          Core::ResourceManager<Shader>::Find("IntegrateBRDF");
        sh->Bind();

        Core::ResourceManager<Mesh>::Find("Quad")->Draw();

        FrameBufferObject::Unbind();
        Texture::Unbind();
        return result;
      }
    }
  }
}