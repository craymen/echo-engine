/*!
  \file Font.cpp
  \author Chase A Rayment
  \date 13 June 2017
  \brief Contains the implementation of the Font class.
*/

#include "Font.hpp"
#include "Log.hpp"
#include "ResourceManager.hpp"

namespace Echo
{
  namespace Graphics
  {
    static FT_Library lib = nullptr;

    Font::Font(const std::string& filename) : Resource(filename), face()
    {
      if(lib == nullptr)
        if (FT_Init_FreeType(&lib))
          Core::Log << Core::Priority::Warning << 
            "Could not init FreeType Library!" << std::endl;

      if (FT_New_Face(lib, filename.c_str(), 0, &face))
          Core::Log << Core::Priority::Warning << "Could not load font " 
            << filename << std::endl;

      FT_Set_Pixel_Sizes(face, 0, 150);

      glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  
      for (unsigned char c = 0; c < 128; c++)
      {
          // Load character glyph 
          if (FT_Load_Char(face, c, FT_LOAD_RENDER))
          {
              Core::Log << Core::Priority::Warning << "Failed to load Glyph " 
                << c << " from font " << filename << std::endl;
              continue;
          }

          // Generate texture
          Texture* tex = new Texture(face->glyph->bitmap.width, 
            face->glyph->bitmap.rows);
          tex->Bind();

          glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width,
            face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE,
            face->glyph->bitmap.buffer);

          // Set texture options
          /*glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);*/

          // Store character
          Character character = {
            tex, 
            glm::vec2(face->glyph->bitmap.width, face->glyph->bitmap.rows) / 150.0f,
            glm::vec2(face->glyph->bitmap_left, face->glyph->bitmap_top) / 150.0f,
            (face->glyph->advance.x >> 6) / 150.0f
          };
          characters[c] = character;
      }  
    }

    Font::Character Font::GetCharacter(unsigned char c)
    {
      if(c < 128)
        return characters[c];
      else
        return characters['?'];
    }

    Font::~Font()
    {
      for(auto c : characters)
        delete c.second.Tex;
      FT_Done_Face(face);
    }

    void Font::Shutdown()
    {
      FT_Done_FreeType(lib);
    }

    Core::Handle<Font> Font::GetDefault()
    {
      return Core::ResourceManager<Font>::Find("DefaultFont");
    }
  }
}