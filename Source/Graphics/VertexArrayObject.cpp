/*!
  \file VertexArrayObject.cpp
  \author Chase A Rayment
  \date 16 April 2017
  \brief Contains the implementation of the VertexArrayObject class.
*/

#include "VertexArrayObject.hpp"
#include "GraphicsCore.hpp"

// Disable warning about casting unsigned to void*
#ifdef __linux__
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wint-to-void-pointer-cast"
#endif

namespace Echo
{
  namespace Graphics
  {
    VertexArrayObject::VertexArrayObject() : id(0), vbo(), 
      ebo(), instance_id()
    {
      glGenVertexArrays(1, &id);
	  glGenBuffers(1, &instance_id);
      CheckGL();
    }

    VertexArrayObject::~VertexArrayObject()
    {
      glDeleteVertexArrays(1, &id);
	  glDeleteBuffers(1, &instance_id);
	  instance_id = 0;
      id = 0;
      CheckGL();
    }

    void VertexArrayObject::Bind()
    {
      glBindVertexArray(id);
      CheckGL();
    }

    void VertexArrayObject::Build()
    {
      Bind();

      vbo.Build();

      unsigned offset = 0;
	  unsigned instance_offset = 0;
      unsigned num_ptrs = 0;
      for(unsigned i = 0; i < Vertex::NumAttributes; ++i)
      {
        if(Vertex::AttributeCounts[i] <= 4)
        {
		  vbo.Bind();
          glVertexAttribPointer(num_ptrs, Vertex::AttributeCounts[i], 
            GL_FLOAT, Vertex::AttributeNormalized[i], sizeof(Vertex), 
            (GLvoid*)offset);
          glEnableVertexAttribArray(num_ptrs);
          glVertexAttribDivisor(num_ptrs, Vertex::AttributeDivisor[i]);
		  num_ptrs++;
          CheckGL();
          offset += Vertex::AttributeSizes[i];
        }
        else if(Vertex::AttributeCounts[i] % 4 == 0)
        {
          for(unsigned j = 0; j < Vertex::AttributeCounts[i] / 4; ++j)
          {
			glBindBuffer(GL_ARRAY_BUFFER, instance_id);
            glEnableVertexAttribArray(num_ptrs);
			glVertexAttribPointer(num_ptrs, Vertex::AttributeCounts[i] / 4,
			  GL_FLOAT, Vertex::AttributeNormalized[i], sizeof(float) * Vertex::AttributeCounts[i],
              (GLvoid*)instance_offset);
            glVertexAttribDivisor(num_ptrs, 1);
			num_ptrs++;
            CheckGL();
            instance_offset += Vertex::AttributeSizes[i] / 4;
          }
        }
      }

      ebo.Build();
      ebo.Bind();

      Unbind();

      CheckGL();
    }

    void VertexArrayObject::Unbind()
    {
      glBindVertexArray(0);
      CheckGL();
    }

    VertexBufferObject& VertexArrayObject::GetVBO()
    {
      return vbo;
    }

    const VertexBufferObject& VertexArrayObject::GetVBO() const
    {
      return vbo;
    }

    ElementBufferObject& VertexArrayObject::GetEBO()
    {
      return ebo;
    }

    const ElementBufferObject& VertexArrayObject::GetEBO() const
    {
      return ebo;
    }

    void VertexArrayObject::Draw()
    {
      Bind();
      ebo.Draw();
      Unbind();
    }

    void VertexArrayObject::DrawInstanced(const std::vector<glm::mat4>& transforms)
    {
	  glBindBuffer(GL_ARRAY_BUFFER, instance_id);
	  CheckGL();
	  glBufferData(GL_ARRAY_BUFFER,
		  (sizeof(glm::mat4) * transforms.size()), 
		  &transforms[0], GL_STREAM_DRAW);
	  CheckGL();
	  Bind();
      ebo.DrawInstanced(transforms);
      Unbind();
    }

    void VertexArrayObject::Clear()
    {
      vbo.Clear();
      ebo.Clear();
    }
  }
}

#ifdef __linux__
  #pragma clang diagnostic pop
#endif
