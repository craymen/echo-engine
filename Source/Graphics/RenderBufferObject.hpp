/*!
  \file RenderBufferObject.hpp
  \author Chase A Rayment
  \date 21 May 2017
  \brief Contains the definition of the RenderBufferObject class.
*/

#pragma once

#include "Platform.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Represents an OpenGL RenderBufferObject.
    */
    class RenderBufferObject
    {
    public:
      /*!
        \brief Constructs a new RenderBufferObject.
        \param width The width of the object in pixels.
        \param height The height of the object in pixels.
        \param format The format of the object.
      */
      RenderBufferObject(unsigned width, unsigned height, GLenum format);

      /*!
        \brief Constructs a new resolution-dependent RenderBufferObject.
          Note that this should ALWAYS be dynamically allocated and referenced
          with a Handle.
        \param rel_width The relative width as a fraction of the screen
          width.
        \param rel_height The relative height as a fraction of the screen
          height.
        \param format The format of the object. 
      */
      RenderBufferObject(float rel_width, float rel_height, GLenum format);

      /*!
        \brief Destroys a RenderBufferObject.
      */
      ~RenderBufferObject();

      /*!
        \brief Binds a RenderBufferObject.
      */
      void Bind();

      /*!
        \brief Unbinds a RenderBufferObject.
      */
      static void Unbind();

      /*!
        \brief Gets the OpenGL ID of the RBO.
      */
      GLuint GetID();

      /*!
        \brief Handles a window resize by resizing all resolution-dependent
          objects.
      */
      static void HandleWindowResize();

    private:
      GLuint id;            // The OpenGL ID.
      GLenum format;        // The format.

        // Whether or not this object is relative to window size.
      bool reso_attached;
      float reso_w, reso_h; // The relative width/height.
    };
  }
}