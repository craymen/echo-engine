/*!
  \file ReflectionProbeObject.hpp
  \author Chase A Rayment
  \date 18 July 2017
  \brief Contains the definition of the ReflectionProbeObject class.
*/
#pragma once
#include "GLM/glm.hpp"
#include "Handle.hpp"
#include "Cubemap.hpp"

namespace Echo
{
  namespace Graphics
  {
    class ReflectionProbeObject
    {
    public:
      glm::vec3 Position;
      std::string Name;
      Core::Handle<Cubemap> DiffuseMap;
      Core::Handle<Cubemap> SpecularMap;
    };
  }
}