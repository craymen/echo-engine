/*!
  \file Texture.hpp
  \author Chase A Rayment
  \date 21 May 2017
  \brief Contains the definition of the Texture class.
*/
#pragma once

#include "Platform.hpp"
#include <unordered_map>
#include "Handle.hpp"
#include "Resource.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Represents a 2D texture.
    */
    class Texture : public Core::Resource
    {
      friend class Cubemap;
    public:
      /*!
        \brief Creates a new texture.
        \param width The width of the texture in pixels.
        \param height The height of the texture in pixels.
      */
      Texture(int width, int height);

      /*!
        \brief Creates a resolution-dependent texture.
        \param rel_width The relative width as a fraction of the screen width.
        \param rel_height The relative height as a fraction of the screen 
          height.
      */
      Texture(float rel_width, float rel_height);

      /*!
        \brief Loads a new texture.
        \param filename The filename of the texture.
      */
      Texture(const std::string& filename);

      /*!
        \brief Destroys a Texture.
      */
      ~Texture();

      /*!
        \brief Binds a Texture.
      */
      void Bind();

      /*!
        \brief Unbinds ALL Textures.
      */
      static void Unbind();

      /*!
        \brief Attaches a Texture.
        \returns The attachment ID.
      */
      int Attach();

      /*!
        \brief Gets the OpenGL ID of a Texture.
      */
      GLuint GetID();

      /*!
        \brief Handles a resize event by resizing all resolution-dependent 
          textures.
      */
      static void HandleWindowResize();

      /*!
        \brief Gets the default Texture.
      */
      static Core::Handle<Texture> GetDefault();

      /*!
        \brief Saves this texture.
        \param name The name to save this texture under.
      */
      void Save(const std::string& name);

      /*!
        \brief Sets the debug label of this texture.
        \param label The new label.
      */
      void SetLabel(const std::string& label);

    private:
      GLuint id;            // The OpenGL ID of the texture.

        // Whether or not this Texture is resolution dependent.
      bool reso_attached;
      float reso_w, reso_h; // The relative width/height.
      static unsigned num_attached_textures;
      std::string label;
    };
  }
}