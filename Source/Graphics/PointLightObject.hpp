/*!
  \file PointLightObject.hpp
  \author Chase A Rayment
  \date 12 May 2017
  \brief Contains the definition of the PointLightObject class.
*/

#pragma once

#include "GLM/glm.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Represents a point light in 3D space.
    */
    class PointLightObject
    {
    public:
      glm::vec3 Position; //!< The position of the object.
      glm::vec3 Color;    //!< The color of the object.
    };
  }
}