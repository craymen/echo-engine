/*!
  \file Material.hpp
  \author Chase A Rayment
  \date 12 May 2017
  \brief Contains the definition of the Material class.
*/

#pragma once

#include <unordered_map>
#include <string>
#include "GLM/glm.hpp"
#include "Handle.hpp"
#include "Resource.hpp"
#include "Texture.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Holds information about a surface material.
    */
    class Material : public Core::Resource
    {
    public:
      /*!
        \brief Creates a new Material.
      */
      Material();

      /*!
        \brief Loads a new Material from file.
        \param filename The filename to load.
      */
      Material(const std::string& filename);

      /*!
        \brief Gets the default Material.
      */
      static Core::Handle<Material> GetDefault();

      std::string ShaderName;     //!< The name of the shader to draw with.
      glm::vec4 AlbedoColor;      //!< The albedo surface color of the material.
      float Roughness;            //!< The surface roughness.
      float Metallic;             //!< The surface metallic property.
      Core::Handle<Graphics::Texture> AlbedoTexture; //!< The albedo texture.
      Core::Handle<Graphics::Texture> NormalMap; //!< The normal map.
    };
  }
}