/*!
  \file Shader.hpp
  \author Chase A Rayment
  \date 20 April 2017
  \brief Contains the definition of the Shader class.
*/
#pragma once

#include "Platform.hpp"
#include <string>
#include "GLM/glm.hpp"
#include <unordered_map>
#include "Material.hpp"
#include "PointLightObject.hpp"
#include "CameraObject.hpp"
#include "Handle.hpp"
#include "Resource.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Data to read from file about a shader.
    */
    struct ShaderSaveData
    {
      std::string VertexShader;   //!< The name of the vertex shader.
      std::string FragmentShader; //!< The name of the fragment shader.
      std::string GeometryShader; //!< The name of the geometry shader.
      std::string TessContShader; //!< The name of the tesscont shader.
      std::string TessEvalShader; //!< The name of the tesseval shader.
      std::string ComputeShader;  //!< The name of the compute shader.
      std::string RenderStage;    //!< Which render stage this shader is in.

      ShaderSaveData() : VertexShader(""), FragmentShader(""),
        GeometryShader(""), TessContShader(""), TessEvalShader(""),
        ComputeShader(""), RenderStage("Opaque") { }
    };

    /*!
      \brief Represents an OpenGL shader object.
    */
    class Shader : public Core::Resource
    {
    public:
      /*!
        \brief Constructs a new shader.
      */
      Shader(const std::string& path);
      
      /*!
        \brief Binds this Shader.
      */
      void Bind();

      /*!
        \brief Unbinds this Shader.
      */
      static void Unbind();

      /*!
        \brief Gets the default Shader.
      */
      static Core::Handle<Shader> GetDefault();

      /*!
        \brief Sets a uniform in this Shader.
      */
      void SetUniform(const std::string& name, int val);

      /*! \overload */
      void SetUniform(const std::string& name, float val);

      /*! \overload */
      void SetUniform(const std::string& name, bool val);

      /*! \overload */
      void SetUniform(const std::string& name, const glm::vec2& val);

      /*! \overload */
      void SetUniform(const std::string& name, const glm::vec3& val);

      /*! \overload */
      void SetUniform(const std::string& name, const glm::vec4& val);

      /*! \overload */
      void SetUniform(const std::string& name, const glm::mat4& val);

      /*! \overload */
      void SetUniform(const std::string& name, Material& val);

      /*! \overload */
      void SetUniform(const std::string& name, const PointLightObject* val);

      /*! \overload */
      void SetUniform(const std::string& name, const CameraObject* val);

      /*!
        \brief Gets the render stage of the shader.
      */
      std::string GetRenderStage() const;

    private:
      void add_shader(GLenum type, const std::string& filename);

      GLuint id;                                         // The program id.
      GLuint vert, frag, geom, tessCont, tessEval, comp; // The individual ids.
      std::string render_stage;                          // The render stage.
    };
  }
}