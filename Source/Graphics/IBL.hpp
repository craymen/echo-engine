/*!
  \file IBL.hpp
  \author Chase A Rayment
  \date 16 July 2017
  \brief Contains the definition of IBL utility functions.
*/
#pragma once

#include "Handle.hpp"
#include "Cubemap.hpp"
#include "Texture.hpp"

namespace Echo
{
  namespace Graphics
  {
    namespace IBL
    {
      Core::Handle<Cubemap> DiffuseConvolution(Core::Handle<Cubemap> 
        environment);

      Core::Handle<Cubemap> SpecularConvolution(Core::Handle<Cubemap>
        environment);

      Core::Handle<Texture> BRDFIntegration();
    }
  }
}