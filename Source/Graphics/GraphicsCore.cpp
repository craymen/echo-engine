/*!
  \file GraphicsCore.cpp
  \author Chase A Rayment
  \date 15 April 2017
  \brief Contains the implementation of the Graphics system.
*/

#include "GraphicsCore.hpp"
#include "Window.hpp"
#include "Log.hpp"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_sdl_gl3.h"
#include "VertexArrayObject.hpp"
#include "Shader.hpp"
#include "Transform.hpp"
#include "GLM/glm.hpp"
#include "GLM/gtc/quaternion.hpp"
#include "Mesh.hpp"
#include "Scene.hpp"
#include "ModelObject.hpp"
#include "CameraObject.hpp"
#include "Messaging.hpp"
#include "PreDrawEvent.hpp"
#include "Space.hpp"
#include "Platform.hpp"
#include "Texture.hpp"
#include "ResourceManager.hpp"
#include "FileSystem.hpp"
#include "IBL.hpp"
#include <windows.h>
#ifdef DEBUG
#include "RenderDoc/renderdoc_app.h"
#endif

#ifdef _WIN32
  extern "C" 
  {
    /*!
      \brief Disables Nvidia Optimus.
    */
    _declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
  }
#endif

namespace Echo
{
  namespace Graphics
  {
    static std::vector<Scene*> scenes;

    #ifdef DEBUG
      bool renderdoc_attached = false;
      RENDERDOC_API_1_1_1* rdapi;
    #endif

    static void init_glew()
    {
      // Init GLEW
      glewExperimental = GL_TRUE;
      GLenum err = glewInit();
      // Silence GL_INVALID_ENUM error from glewInit
      glGetError();
      if(err != GLEW_OK)
      {
        Core::Log << Core::Priority::Error << "Could not initialize GLEW!"
          " Error: " << glewGetErrorString(err) << std::endl;
        return;
      }

      // Check OpenGL version
      if(!glewIsSupported("GL_VERSION_4_4"))
        Core::Log << "Your graphics processor, " << glGetString(GL_RENDERER)
          << ", does not support OpenGL 4.4!" << std::endl;
    }

    void Initialize()
    {
      Window::Initialize();
      init_glew();

      // Initialize ImGUI
      ImGui_ImplSdlGL3_Init(Window::GetWindow());
      ImGui_ImplSdlGL3_NewFrame(Window::GetWindow());

      // Enable depth testing
      glEnable(GL_DEPTH_TEST);
      glDepthFunc(GL_LESS);

      glEnable(GL_CULL_FACE);
      glCullFace(GL_BACK);

      glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

      #ifdef DEBUG
        HMODULE h = GetModuleHandle(L"renderdoc.dll");
        if(h == NULL)
          renderdoc_attached = false;
        else
        {
          pRENDERDOC_GetAPI rdfunc = 
            (pRENDERDOC_GetAPI)GetProcAddress(h, "RENDERDOC_GetAPI");
          if(rdfunc(eRENDERDOC_API_Version_1_1_1, (void**)&rdapi))
          {
            Core::Log << Core::Priority::Info << "Successfully attached "
              "RenderDoc." << std::endl;
            renderdoc_attached = true;
          }
          else
          {
            Core::Log << Core::Priority::Warning << "Could not attach "
              "RenderDoc!" << std::endl;
            renderdoc_attached = false;
          }
        }
        glEnable(GL_DEBUG_OUTPUT);
      #endif

      CheckGL();
    }

    void Update()
    {
      // Clear screen
      glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // Send pre-draw event (to update graphics object positions)
      PreDrawEvent ev;
      Core::DispatchEvent(nullptr, "PreDraw", ev);

      // Sort scenes by depth
      std::sort(scenes.begin(), scenes.end(), 
      [](Scene* a, Scene* b)
      {
        return a->depth > b->depth;
      });

      // Draw scenes
      for(Scene* scene : scenes)
        scene->Draw();

      ImGui::Render();

      // Present to window
      glFinish();
      Window::Update();

      ImGui_ImplSdlGL3_NewFrame(Window::GetWindow());
    }

    void Terminate()
    {
      Window::Terminate();
    }

    Scene* AddScene()
    {
      scenes.push_back(new Scene);
      return scenes.back();
    }

    void RemoveScene(Scene* scene)
    {
      scenes.erase(std::remove(scenes.begin(), scenes.end(), scene), 
        scenes.end());
    }

    void TriggerRenderDocCapture()
    {
      #ifdef DEBUG
        if(renderdoc_attached)
        {
          rdapi->TriggerCapture();
          Core::Log << Core::Priority::Info << "Triggered frame capture." 
            << std::endl;
        }
      #endif
    }

    void StartRenderDocCapture()
    {
      #ifdef DEBUG
        if(renderdoc_attached)
        {
          rdapi->StartFrameCapture(NULL, NULL);
        }
      #endif
    }

    void EndRenderDocCapture()
    {
      #ifdef DEBUG
        if(renderdoc_attached)
        {
          rdapi->EndFrameCapture(NULL, NULL);
        }
      #endif
    }
  }
}