/*!
  \file Scene.cpp
  \author Chase A Rayment
  \date 10 May 2017
  \brief Contains the implementation of the Scene class.
*/

#include "Scene.hpp"
#include <algorithm>
#include "Shader.hpp"
#include "DebugDrawer.hpp"
#include "Texture.hpp"
#include "ResourceManager.hpp"
#include <unordered_map>
#include "Window.hpp"
#include "glm/gtx/string_cast.hpp"
#include <cmath>
#include "Cubemap.hpp"
#include "IBL.hpp"

namespace Echo
{
  namespace Graphics
  {
    static int scene_id = 0;

    Scene::Scene() : models(), cameras(), pointlights(), 
      current_camera(nullptr), debug_drawer(), hdr_fbo(), 
      hdr_rbo(new RenderBufferObject(1.0f, 1.0f, GL_DEPTH24_STENCIL8)), 
      hdr_texture(nullptr), depth(0.0f), gbuffer(), gbuffer_fbo(), 
      skybox(nullptr)
    {
      hdr_texture = new Texture(1.0f, 1.0f);
      for(unsigned i = 0; i < gbuffer_count; ++i)
      {
        gbuffer[i] = new Texture(1.0f, 1.0f);
      }
      gbuffer[0]->SetLabel("Position");
      gbuffer[1]->SetLabel("Normal");
      gbuffer[2]->SetLabel("Albedo");
      gbuffer[3]->SetLabel("Specular");
      gbuffer[4]->SetLabel("Diffuse IBL");
      gbuffer[5]->SetLabel("Specular IBL");
    }

    Scene::~Scene()
    {
      // Clean up objects
      for(ModelObject* m : models)
        delete m;
      for(CameraObject* c : cameras)
        delete c;
      for(PointLightObject* pl : pointlights)
        delete pl;
      delete (Texture*)hdr_texture;
      for(unsigned i = 0; i < gbuffer_count; ++i)
        delete (Texture*)gbuffer[i];
    }

    ModelObject* Scene::AddModel()
    {
      models.push_back(new ModelObject());
      return models.back();
    }

    void Scene::RemoveModel(ModelObject* obj)
    {
      delete obj;
      models.erase(std::remove(models.begin(), models.end(), obj), 
        models.end());
    }

    TextObject* Scene::AddText()
    {
      texts.push_back(new TextObject);
      return texts.back();
    }

    void Scene::RemoveText(TextObject* obj)
    {
      delete obj;
      texts.erase(std::remove(texts.begin(), texts.end(), obj), texts.end());
    }

    CameraObject* Scene::AddCamera()
    {
      cameras.push_back(new CameraObject());
      return cameras.back();
    }

    void Scene::RemoveCamera(CameraObject* obj)
    {
      delete obj;
      cameras.erase(std::remove(cameras.begin(), cameras.end(), obj), 
        cameras.end());
    }

    PointLightObject* Scene::AddPointLight()
    {
      pointlights.push_back(new PointLightObject);
      return pointlights.back();
    }

    void Scene::RemovePointLight(PointLightObject* obj)
    {
      delete obj;
      pointlights.erase(std::remove(pointlights.begin(), pointlights.end(),
        obj), pointlights.end());
    }

    void Scene::SetCurrentCamera(CameraObject* obj)
    {
      current_camera = obj;
    }

    CameraObject* Scene::GetCurrentCamera()
    {
      return current_camera;
    }

    void Scene::Draw()
    {
      // If the current camera doesn't exist, just return
      if(!current_camera)
        return;

      gbuffer_fbo.AttachTextures(gbuffer, gbuffer_count);
      gbuffer_fbo.AttachRenderBuffer(hdr_rbo);

      hdr_fbo.AttachTexture(hdr_texture);
      hdr_fbo.AttachRenderBuffer(hdr_rbo);

      glViewport(0, 0, Window::GetWidth(), Window::GetHeight());

      Core::Handle<Shader> sh;

      // Draw skybox
      hdr_fbo.Bind();

      glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      draw_skybox(current_camera->GetViewMatrix(), 
        current_camera->GetProjectionMatrix());

      // Separate models by render stage
      auto objs = separate_by_render_stage();

      hdr_fbo.AttachTexture(hdr_texture);

      // Draw each render stage
      draw_opaque_objects(objs["Opaque"], hdr_fbo, gbuffer_fbo, gbuffer,
        current_camera->GetViewMatrix(), current_camera->GetProjectionMatrix());
      draw_transparent_objects(objs["Transparent"], hdr_fbo,
        current_camera->GetViewMatrix(), current_camera->GetProjectionMatrix());

      // Bind HDR FBO
      hdr_fbo.Bind();

      // Debug draw
      sh = Core::ResourceManager<Shader>::Find("Debug");
      sh->SetUniform("Cam", current_camera);
      sh->SetUniform("V", current_camera->GetViewMatrix());
      sh->SetUniform("P", current_camera->GetProjectionMatrix());
      sh->Bind();
      debug_drawer.Draw();
      Shader::Unbind();

      // Unbind HDR FBO so we can draw to screen
      FrameBufferObject::Unbind();

      // Configure shader
      sh = Core::ResourceManager<Shader>::Find("FinalDraw");
      sh->Bind();
      sh->SetUniform("ScreenTex", hdr_texture->Attach());
      sh->SetUniform("ScreenDepth", depth);
      Core::ResourceManager<Mesh>::Find("Quad")->Draw();
      Texture::Unbind();
    }

    DebugDrawer& Scene::GetDebugDrawer()
    {
      return debug_drawer;
    }

    void Scene::draw_opaque_objects(const std::unordered_map<std::string, 
        std::vector<ModelObject*> >& objs, FrameBufferObject& out_fbo,
        FrameBufferObject& gbuf_fbo, Core::Handle<Texture>* gbuff, 
        const glm::mat4& V, const glm::mat4& P)
    {
      // GBUFFER STEP

      // Attach gbuffer textures
      gbuf_fbo.Bind();

      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // Disable blending
      glDisable(GL_BLEND);

      // Enable back face culling
      glEnable(GL_CULL_FACE);

      // Enable depth testing
      glEnable(GL_DEPTH_TEST);
      glDepthFunc(GL_LESS);

      glm::mat4 invV = glm::inverse(V);

      // For every shader listed:
      for(auto shad : objs)
      {
        // Get the shader
        Core::Handle<Shader> sh = 
          Core::ResourceManager<Shader>::Find(shad.first);

        // Update camera
        sh->SetUniform("Cam.Eye", glm::vec3(invV * glm::vec4(0, 0, 0, 1)));
        sh->SetUniform("Cam.LookDir", glm::vec3(invV * glm::vec4(0, 0, -1, 0)));
        sh->SetUniform("V", V);
        sh->SetUniform("P", P);

        // Draw model objects
        std::unordered_map<ReflectionProbeObject*, std::vector<ModelObject*> >
          objsbyprobes;
        for(ModelObject* obj : shad.second)
        {
          glm::vec3 objpos = glm::vec3(obj->GetTransform() 
            * glm::vec4(0, 0, 0, 1));
          // Set up IBL maps
          ReflectionProbeObject* closest = nullptr;
          float closest_dist = 9999999.9f;
          for(auto probe : reflection_probes)
          {
            float dist = glm::length(probe->Position - objpos);
            if(dist < closest_dist)
            {
              closest = probe;
              closest_dist = dist;
            }
          }
          objsbyprobes[closest].push_back(obj);
        }

        for(auto probe : objsbyprobes)
        {
          ReflectionProbeObject* closest = probe.first;
          if(closest != nullptr && closest->DiffuseMap != nullptr)
          {
            sh->SetUniform("DiffuseConvolution", 
              closest->DiffuseMap->Attach());
          }
          else
          {
            sh->SetUniform("DiffuseConvolution", 
              Core::ResourceManager<Cubemap>::Find("DefaultCubemap")->
              Attach());
          }

          if(closest != nullptr && closest->SpecularMap != nullptr)
          {
            sh->SetUniform("SpecularConvolution", 
              closest->SpecularMap->Attach());
          }
          else
          {
            sh->SetUniform("SpecularConvolution", 
              Core::ResourceManager<Cubemap>::Find("DefaultCubemap")->
              Attach());
          }

          sh->SetUniform("BRDFIntegration", Core::ResourceManager<Texture>::
            Find("BRDFIntegration")->Attach());

          sh->SetUniform("Mat", *(probe.second[0])->GetMaterial());

					std::unordered_map<Core::Handle<Mesh>, std::vector<ModelObject*> > modelsbymesh;
					for (auto obj : models)
					{
						modelsbymesh[obj->GetMesh()].push_back(obj);
					}

					for (auto obj : modelsbymesh)
					{
						if (obj.second.size() == 1)
						{
							sh->SetUniform("M", obj.second[0]->GetTransform());
							sh->SetUniform("Instanced", false);
							obj.second[0]->Draw();
						}
						else
						{
							sh->SetUniform("Instanced", true);
							std::vector<glm::mat4> transforms;
							for (auto o : probe.second)
								transforms.push_back(o->GetTransform());
							obj.second[0]->GetMesh()->DrawMultiple(transforms);
						}
					}

          Texture::Unbind();
        }
      }

      FrameBufferObject::Unbind();

      // Bind output FBO
      out_fbo.Bind();

      // GLOBAL ILLUMINATION STEP

      // Bind and configure global illumination shader
      Core::Handle<Shader> sh = 
        Core::ResourceManager<Shader>::Find("DeferredGlobal");
      sh->Bind();
      sh->SetUniform("Cam.Eye", glm::vec3(invV * glm::vec4(0, 0, 0, 1)));
      sh->SetUniform("Cam.LookDir", glm::vec3(invV * glm::vec4(0, 0, -1, 0)));
      sh->SetUniform("Position", gbuff[gbuffer_position]->Attach());
      sh->SetUniform("Normal", gbuff[gbuffer_normal]->Attach());
      sh->SetUniform("Diffuse", gbuff[gbuffer_diffuse]->Attach());
      sh->SetUniform("Specular", gbuff[gbuffer_specular]->Attach());
      sh->SetUniform("DiffIBL", gbuff[gbuffer_diffibl]->Attach());
      sh->SetUniform("SpecIBL", gbuff[gbuffer_specibl]->Attach());

      // Draw full screen quad
      Core::ResourceManager<Mesh>::Find("Quad")->Draw();

      Texture::Unbind();
      Shader::Unbind();
      Cubemap::Unbind();

      // POINT LIGHT ILLUMINATION STEP

      // Enable additive blending
      glEnable(GL_BLEND);
      glBlendFunc(GL_ONE, GL_ONE);

      // Disable back face culling
      glDisable(GL_CULL_FACE);

      // Disable depth testing
      glDisable(GL_DEPTH_TEST);

      // Bind and configure point light illumination shader
      sh = Core::ResourceManager<Shader>::Find("DeferredPointLight");
      sh->Bind();
      sh->SetUniform("Position", gbuffer[gbuffer_position]->Attach());
      sh->SetUniform("Normal", gbuffer[gbuffer_normal]->Attach());
      sh->SetUniform("Diffuse", gbuffer[gbuffer_diffuse]->Attach());
      sh->SetUniform("Specular", gbuffer[gbuffer_specular]->Attach());

      sh->SetUniform("Cam.Eye", glm::vec3(invV * glm::vec4(0, 0, 0, 1)));
      sh->SetUniform("Cam.LookDir", glm::vec3(invV * glm::vec4(0, 0, -1, 0)));
      sh->SetUniform("V", V);
      sh->SetUniform("P", P);

      sh->SetUniform("ScreenResolution", glm::vec2(Window::GetWidth(), 
        Window::GetHeight()));

      for(unsigned i = 0; i < pointlights.size(); ++i)
      {
        // Find intensity of light
        glm::vec3 c = pointlights[i]->Color;
        float intensity = pow(max(max(c.x, c.y), c.z), 1.0f / 2.2f);
        float radius = sqrt(255.0f * intensity);

        // Setup light transform
        Transform t;
        t.SetPosition(pointlights[i]->Position);
        t.SetScale(glm::vec3(radius, radius, radius));

        // Configure shader
        sh->SetUniform("M", t.GetMatrix());
        sh->SetUniform("LightColor", pointlights[i]->Color);
        sh->SetUniform("LightPosition", pointlights[i]->Position);

        // Draw sphere
        Core::ResourceManager<Mesh>::Find("Sphere")->Draw();        
      }
      Texture::Unbind();
      Shader::Unbind();
    }

    void Scene::draw_transparent_objects(const std::unordered_map<std::string, 
        std::vector<ModelObject*> >& objs, FrameBufferObject& out_fbo,
        const glm::mat4& V, const glm::mat4& P)
    {
      // Forward rendering step: attach screen texture
      out_fbo.Bind();

      // Enable blending
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

      // Enable back face culling
      glEnable(GL_CULL_FACE);

      // Enable depth testing
      glEnable(GL_DEPTH_TEST);

      glm::mat4 invV = glm::inverse(V);

      // For every shader listed:
      for(auto shad : objs)
      {
        // Get the shader
        Core::Handle<Shader> sh = 
          Core::ResourceManager<Shader>::Find(shad.first);

        sh->Bind();

        // Update lights
        sh->SetUniform("PointLightCount", (int)pointlights.size());
        for(unsigned i = 0; i < pointlights.size(); ++i)
        {
          sh->SetUniform("PointLights[" + std::to_string(i) + "]", 
            pointlights[i]);
        }

        // Update camera
        sh->SetUniform("Cam.Eye", glm::vec3(invV * glm::vec4(0, 0, 0, 1)));
        sh->SetUniform("Cam.LookDir", glm::vec3(invV * glm::vec4(0, 0, -1, 0)));
        sh->SetUniform("V", V);
        sh->SetUniform("P", P);

        // SORT model objects
        std::vector<ModelObject*> mobjs = shad.second;
        auto cmpfunc = [this](ModelObject* a, ModelObject* b)
        {
          return glm::length(a->GetPosition() - current_camera->GetEye())
          < glm::length(b->GetPosition() - current_camera->GetEye());
        };
        std::sort(mobjs.begin(), mobjs.end(), cmpfunc);

        // Draw model objects
        for(ModelObject* obj : shad.second)
        {
          sh->SetUniform("M", obj->GetTransform());
          sh->SetUniform("Mat", *obj->GetMaterial());
          obj->Draw();
          Texture::Unbind();
        }
      }

      // TODO: figure out better way to handle this so text is drawn in order
      // with transparent model objects
      for(auto obj : texts)
      {
        if(!obj->GetVisibility())
          continue;
          
        Core::Handle<Shader> sh = Core::ResourceManager<Shader>::Find(
          obj->GetMaterial()->ShaderName);

        sh->Bind();

        // Update lights
        sh->SetUniform("PointLightCount", (int)pointlights.size());
        for(unsigned i = 0; i < pointlights.size(); ++i)
        {
          sh->SetUniform("PointLights[" + std::to_string(i) + "]", 
            pointlights[i]);
        }

        // Update camera
        sh->SetUniform("Cam.Eye", glm::vec3(invV * glm::vec4(0, 0, 0, 1)));
        sh->SetUniform("Cam.LookDir", glm::vec3(invV * glm::vec4(0, 0, -1, 0)));
        sh->SetUniform("V", V);
        sh->SetUniform("P", P);

        sh->SetUniform("M", obj->GetTransform());
        sh->SetUniform("Mat", *obj->GetMaterial());

        // Draw object
        obj->Draw();

        Texture::Unbind();
      }

      // Unbind FBO
      FrameBufferObject::Unbind();
    }

    void Scene::SetDepth(float d)
    {
      depth = d;
    }

    void Scene::SetSkybox(Core::Handle<Cubemap> s)
    {
      skybox = s;
    }

    ReflectionProbeObject* Scene::AddReflectionProbe()
    {
      reflection_probes.push_back(new ReflectionProbeObject);
      return reflection_probes.back();
    }

    void Scene::RemoveReflectionProbe(ReflectionProbeObject* obj)
    {
      delete obj;
      reflection_probes.erase(std::remove(reflection_probes.begin(),
        reflection_probes.end(), obj), reflection_probes.end());
    }

    void Scene::BakeReflections()
    {
      glViewport(0, 0, 1024, 1024);

      // Calculate view matrices
      glm::mat4 P = Transform::CameraToScreenPerspective(
        glm::radians(90.0f), 1.0f, 0.01f, 1000.0f);
      
      for(auto probe : reflection_probes)
      {
        glm::mat4 views[] = 
        {
          Transform::WorldToCamera(probe->Position, glm::vec3(1, 0, 0), 
            glm::vec3(0, -1, 0)),
          Transform::WorldToCamera(probe->Position, glm::vec3(-1, 0, 0), 
            glm::vec3(0, -1, 0)),
          Transform::WorldToCamera(probe->Position, glm::vec3(0, 1, 0), 
            glm::vec3(0, 0, 1)),
          Transform::WorldToCamera(probe->Position, glm::vec3(0, -1, 0), 
            glm::vec3(0, 0, -1)),
          Transform::WorldToCamera(probe->Position, glm::vec3(0, 0, 1), 
            glm::vec3(0, -1, 0)),
          Transform::WorldToCamera(probe->Position, glm::vec3(0, 0, -1), 
            glm::vec3(0, -1, 0))
        };

        Cubemap scene_render(1024, 1024);

        for(unsigned i = 0; i < 6; ++i)
        {
          // Set up the gbuffer FBO
          Core::Handle<Texture> my_gbuf[gbuffer_count];
          FrameBufferObject gbuf_fbo;
          RenderBufferObject rbo(1024U, 1024U, GL_DEPTH24_STENCIL8);
          for(unsigned i = 0; i < gbuffer_count; ++i)
            my_gbuf[i] = new Texture(1024, 1024);
          gbuf_fbo.AttachTextures(my_gbuf, gbuffer_count);
          gbuf_fbo.Bind();
          glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
          glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

          // Set up the output FBO
          FrameBufferObject fbo;
          fbo.AttachCubemapFace(&scene_render, 
            GL_TEXTURE_CUBE_MAP_POSITIVE_X + i);
          fbo.AttachRenderBuffer(&rbo);
          fbo.Bind();
          glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
          glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

          // Draw skybox
          draw_skybox(views[i], P);

          // Render scene
          auto objs = separate_by_render_stage();
          draw_opaque_objects(objs["Opaque"], fbo, gbuf_fbo, my_gbuf, views[i], 
            P);
          draw_transparent_objects(objs["Transparent"], fbo, views[i], P);

          // Clean up
          for(unsigned i = 0; i < gbuffer_count; ++i)
            delete (Texture*)my_gbuf[i];
        }

        // IBL
        Core::Handle<Cubemap> diffuse = IBL::DiffuseConvolution(&scene_render);
        Core::Handle<Cubemap> specular = 
          IBL::SpecularConvolution(&scene_render);

        diffuse->Save(probe->Name + "Diffuse", 1);
        specular->Save(probe->Name + "Specular", 5);
        probe->DiffuseMap = diffuse;
        probe->SpecularMap = specular;
      }
    }

    std::unordered_map<std::string, std::unordered_map<std::string, 
      std::vector<ModelObject*> > > Scene::separate_by_render_stage()
    {
      std::unordered_map<std::string, std::unordered_map<std::string, 
        std::vector<ModelObject*> > > objs;
      for(auto obj : models)
      {
        if(obj->GetMaterial() == nullptr)
          continue;
        std::string rs = Core::ResourceManager<Shader>::Find(obj->
          GetMaterial()->ShaderName)->GetRenderStage();
        objs[rs][obj->GetMaterial()->ShaderName].push_back(obj);
      }
      return objs;
    }

    void Scene::draw_skybox(const glm::mat4& V, const glm::mat4& P)
    {
      if(skybox != nullptr)
      {
        // Disable back face culling
        glDisable(GL_CULL_FACE);

        // Disable depth test
        glDisable(GL_DEPTH_TEST);

        Core::Handle<Shader> sh = Core::ResourceManager<Shader>::Find("Skybox");
        sh->Bind();
        sh->SetUniform("Skybox", skybox->Attach());
        sh->SetUniform("V", glm::mat4(glm::mat3(V)));
        sh->SetUniform("P", P);
        Core::ResourceManager<Mesh>::Find("DefaultModel")->Draw();

        glClear(GL_DEPTH_BUFFER_BIT);

        Cubemap::Unbind();
        FrameBufferObject::Unbind();
      }
    }

		unsigned Scene::GetPolycount()
		{
			unsigned result = 0;
			for (auto obj : models)
			{
				if (obj == nullptr || obj->GetMesh() == nullptr) continue;
				result += obj->GetMesh()->GetPolycount();
			}
			return result;
		}

  }
}