/*!
  \file Transform.hpp
  \author Chase A Rayment
  \date 9 April 2017
  \brief Contains the definition of the Transform functions.
*/
#pragma once

#include "GLM/glm.hpp"
#include "GLM/gtx/euler_angles.hpp"
#include "GLM/gtx/quaternion.hpp"
#include "Handle.hpp"

namespace Echo
{
  namespace Core { class Object; void RegisterTypes(); 
    template <typename T>
    void EditType(void* p, const std::string& name);
  }

  namespace Graphics
  {
    /*!
      \brief Represents a 3D position, rotation, and scale.
    */
    class Transform
    {
      friend void Core::RegisterTypes();
      template <class>
      friend void Core::EditType(void* p,
        const std::string& name);
    public:
      /*!
        \brief Computes an object-to-world matrix.
        \param translation The object translation.
        \param rotation The object rotation.
        \param scale The object scale.
      */
      static glm::mat4 ObjectToWorld(const glm::vec3& translation, 
        const glm::quat& rotation, const glm::vec3& scale);

      /*!
        \brief Computes a world-to-camera matrix.
        \param eye The eye of the camera.
        \param lookDir The look direction of the camera.
        \param up The up direction of the camera.
      */
      static glm::mat4 WorldToCamera(const glm::vec3& eye, 
        const glm::vec3& lookDir, const glm::vec3& up);

      /*!
        \brief Computes an orthographic camera-to-screen matrix.
        \param size The size of the camera.
        \param aspect The aspect ratio of the screen (height / width)
        \param nearZ The near Z distance.
        \param farZ The far Z distance.
      */
      static glm::mat4 CameraToScreenOrth(float size, float aspect, float nearZ, 
        float farZ);

      /*!
        \brief Computes a perspective camera-to-screen matrix.
        \param fov The field of view of the camera.
        \param aspect The aspect ratio of the screen (height / width)
        \param nearZ The near Z distance.
        \param farZ The far Z distance.
      */
      static glm::mat4 CameraToScreenPerspective(float fov, float aspect, 
        float nearZ, float farZ);

      /*!
        \brief Creates a new Transform.
      */
      Transform();

      /*!
        \brief Sets the position.
        \param val The new position.
      */
      void SetPosition(const glm::vec3& val);

      /*!
        \brief Gets the position.
      */
      const glm::vec3& GetPosition() const;

      /*!
        \overload
      */
      glm::vec3& GetPosition();

      /*!
        \brief Gets the rotation represented as euler angles.
      */
      glm::vec3 GetRotationEuler() const;

      /*!
        \brief Gets the rotation represented as a quaternion.
      */
      glm::quat GetRotationQuaternion() const;

      /*!
        \brief Sets the rotation as euler angles.
        \param val The new rotation.
      */
      void SetRotationEuler(const glm::vec3& val);

      /*!
        \brief Sets the rotation as a quaternion.
        \param val The new rotation.
      */
      void SetRotationQuaternion(const glm::quat& val);

      /*!
        \brief Rotates a Rotation around an axis.
        \param v The axis to rotate around.
        \param ang The angle to rotate by, in radians.
      */
      void RotateAround(const glm::vec3& v, float ang);

      /*!
        \brief Sets the scale.
        \param val The new scale.
      */
      void SetScale(const glm::vec3& val);
      
      /*!
        \brief Gets the scale.
      */
      const glm::vec3& GetScale() const;

      /*!
        \overload
      */
      glm::vec3& GetScale();

      /*!
        \brief Gets the object-to-world matrix.
      */
      glm::mat4 GetMatrix() const;

      /*!
        \brief Gets the forward vector of this object. (0 0 -1 if not rotated)
      */
      glm::vec3 GetForward() const;

      /*!
        \brief Gets the up vector of this object. (0 1 0 if not rotated)
      */
      glm::vec3 GetUp() const;

      /*!
        \brief Gets the right vector of this object. (1 0 0 if not rotated)
      */
      glm::vec3 GetRight() const;

      /*!
        \brief Sets the parent of this Transform.
      */
      void SetParent(Core::Handle<Transform> parent);

      /*!
        \brief Gets the parent of this Transform. 
      */
      Core::Handle<Transform> GetParent() const;

      /*!
        \brief Sets the associated object of this Transform.
      */
      void SetObject(Core::Handle<Core::Object> object);

      /*!
        \brief Gets the associated object of this Transform. 
      */
      Core::Handle<Core::Object> GetObject() const;

      /*!
        \brief Initializes this Transform.
      */
      void Initialize();

      /*!
        \brief Terminates this Transform.
      */
      void Terminate();
      
      /*!
        \brief Gets all of this Transform's children.
      */
      const std::vector<Core::Handle<Transform> >& GetChildren() const;

      /*!
        \brief Sets caching disabled (so the matrix is recomputed every time)
      */
      void DisableCaching();

      /*!
        \brief Sets caching enabled (so the matrix is recomputed only when 
          changed)
      */
      void EnableCaching();

      /*!
        \brief Sets the transform from a matrix.
        \param mat The given matrix.
        \param ignore_scale Whether or not to ignore the scale part of the 
          matrix.
      */
      void SetTransform(const glm::mat4& mat, bool ignore_scale = false);

      /*!
        \brief Finds a child object by the given name.
        \param name The name to look for.
      */
      Core::Handle<Core::Object> FindChildByName(std::string name);

      private:
        glm::vec3 position;
        glm::vec3 angles;
        glm::quat rot;
        glm::vec3 scale;
        bool caching_disabled;
        mutable glm::mat4 matrix;
        mutable bool dirty;
        Core::Handle<Transform> parent;
        Core::Handle<Core::Object> object;
        mutable std::vector<Core::Handle<Transform> > children;
    };
  }
}