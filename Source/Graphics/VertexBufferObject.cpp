/*!
  \file VertexBufferObject.cpp
  \author Chase A Rayment
  \date 15 April 2017
  \brief Contains the implementation of the VertexBufferObject class.
*/

#include "VertexBufferObject.hpp"
#include "GraphicsCore.hpp"
#include "Debug.hpp"

namespace Echo
{
  namespace Graphics
  {
    VertexBufferObject::VertexBufferObject() : id(0), vertices(), 
      mode(StoreMode::Static), built(false)
    {
      // Create the buffer
	  glGenBuffers(1, &id);
      CheckGL();
    }

    void VertexBufferObject::PushVertex(const Vertex& vertex)
    {
      vertices.push_back(vertex);
    }

    void VertexBufferObject::Build()
    {
      // Determine the store mode
      GLenum sm = GL_STATIC_DRAW;
      switch(mode)
      {
        case StoreMode::Static:
          sm = GL_STATIC_DRAW;
          break;
        case StoreMode::Dynamic:
          sm = GL_DYNAMIC_DRAW;
          break;
        case StoreMode::Stream:
          sm = GL_STREAM_DRAW;
          break;
      }

      // Bind the buffer
      glBindBuffer(GL_ARRAY_BUFFER, id);

      // Send the data
      if(vertices.size() > 0)
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), 
          &vertices[0], sm);

      // Unbind the buffer
      Unbind();

      built = true;
      CheckGL();
    }

    void VertexBufferObject::Bind()
    {
      // Need to build the VAO before binding it
      ASSERT(built);

      glBindBuffer(GL_ARRAY_BUFFER, id);
      CheckGL();
    }

    void VertexBufferObject::Unbind()
    {
      glBindBuffer(GL_ARRAY_BUFFER, 0);
      CheckGL();
    }

    VertexBufferObject::~VertexBufferObject()
    {
      glDeleteBuffers(1, &id);
      id = 0;
      CheckGL();
    }

    void VertexBufferObject::SetStoreMode(StoreMode m)
    {
      mode = m;
    }

    void VertexBufferObject::Clear()
    {
      vertices.clear();
    }

    unsigned VertexBufferObject::GetCount()
    {
      return vertices.size();
    }
  }
}