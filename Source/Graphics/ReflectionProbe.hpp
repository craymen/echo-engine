/*!
  \file ReflectionProbe.hpp
  \author Chase A Rayment
  \date date
  \brief Contains the definition of the ReflectionProbe component.
*/
#pragma once

#include "ReflectionProbeObject.hpp"
#include "Component.hpp"
#include "PreDrawEvent.hpp"
#include "Handle.hpp"

namespace Echo
{
  namespace Graphics
  {
    class ReflectionProbe : public Core::Component
    {
    public:
      ReflectionProbe();
      void Initialize();
      void EditorInitialize();
      void Terminate();
      void EditorTerminate();
      void PreDraw(PreDrawEvent&);
    
    private:
      ReflectionProbeObject* obj;
    };
  }
}