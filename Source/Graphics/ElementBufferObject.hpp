/*!
  \file ElementBufferObject.hpp
  \author Chase A Rayment
  \date 21 April 2017
  \brief Contains the definition of the ElementBufferObject class.
*/
#pragma once

#include "Platform.hpp"
#include <vector>
#include "GLM/glm.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Represents an OpenGL Element Buffer Object.
    */
    class ElementBufferObject
    {
    public:
      /*!
        \brief The topology types for objects.
      */
      enum class Topology { Lines, Triangles };

      /*!
        \brief Creates a new ElementBufferObject.
      */
      ElementBufferObject();

      /*!
        \brief Destroys an ElementBufferObject.
      */
      ~ElementBufferObject();

      /*!
        \brief Sets the topology this ElementBufferObject draws with.
      */
      void SetTopology(Topology t);

      /*!
        \brief Pushes an index into the element buffer.
      */
      void PushIndex(unsigned i);

      /*!
        \brief Builds the element buffer.
      */
      void Build();

      /*!
        \brief Binds the element buffer.
      */
      void Bind();

      /*!
        \brief Draws the buffer.
      */
      void Draw();

      /*!
        \brief Draws the buffer via instanced rendering.
        \param transforms The transforms to draw with.
      */
      void DrawInstanced(const std::vector<glm::mat4>& transforms);

      /*!
        \brief Unbinds ALL element buffers.
      */
      static void Unbind();

      /*!
        \brief Clears all indices.
      */
      void Clear();

      /*!
        \brief Gets the number of indices in the buffer.
      */
      unsigned GetCount();

    private:
      GLuint id;                     // The OpenGL id of the buffer.
      std::vector<unsigned> indices; // The data of the buffer.
      Topology topology;             // The topology to draw with.
    };
  }
}