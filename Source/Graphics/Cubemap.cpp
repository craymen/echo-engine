/*!
  \file Cubemap.cpp
  \author Chase A Rayment
  \date 16 July 2017
  \brief Contains the implementation of the Cubemap class.
*/

#include "Cubemap.hpp"
#include "TypeData.hpp"
#include "Variable.hpp"
#include "json/json.h"
#include <fstream>
#include "Log.hpp"
#include "Texture.hpp"
#include "GraphicsCore.hpp"
#include "soil/SOIL.h"
#include "Log.hpp"

namespace Echo
{
  namespace Graphics
  {
    Cubemap::Cubemap(int width, int height, bool genMipmaps) : Resource(), id(0)
    {
      glGenTextures(1, &id);
      glBindTexture(GL_TEXTURE_CUBE_MAP, id);
      for (unsigned int i = 0; i < 6; ++i)
      {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, 
          width, height, 0, GL_RGB, GL_FLOAT, nullptr);
      }
      glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
      if(genMipmaps)
      {
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, 
          GL_LINEAR_MIPMAP_LINEAR);
        glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
      }
      else
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      CheckGL();
      Unbind();
    }


    Cubemap::Cubemap(const std::string& filename) : Resource(filename), id(0)
    {
      // Load the cubemap data
      Json::Value val;
      std::ifstream f(filename);

      // Check for error opening the file
      if(!f.is_open())
      {
        Core::Log << Core::Priority::Warning << "Could not open skybox " << 
          filename << std::endl;
        return;
      }

      f >> val;

      std::string root = Core::FileSystem::GetGameDir(Core::FileSystem::Dir::
        Cubemaps);

      glGenTextures(1, &id);
      Bind();

      std::string names[6] = 
      {
        "Right",
        "Left",
        "Top",
        "Bottom",
        "Front",
        "Back"
      };

      int maxMips = 0;

      for(std::string sub : val.getMemberNames())
      {
        for(unsigned i = 0; i < 6; ++i)
        {
          int width, height, channels;
          std::string f = root + "/" + val[sub][names[i]].asString();
          int mip = atoi(sub.c_str());
          if(mip > maxMips)
            maxMips = mip;
          unsigned char* data = SOIL_load_image(
            f.c_str(), &width, 
            &height, &channels, SOIL_LOAD_RGB);
          if(data == nullptr)
            Core::Log << "Could not read from " << f << ": " << 
              SOIL_last_result << std::endl;
          glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, mip,
            GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
          CheckGL();
          SOIL_free_image_data(data);
        }
          if(sub == "0")
            glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
      }

      // Set properties
      if(maxMips == 0)
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      else
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, 
          GL_LINEAR_MIPMAP_LINEAR);
      glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
      Unbind();
      CheckGL();
    }

    int Cubemap::Attach()
    {
      ASSERT(Texture::num_attached_textures <= 15);
      glActiveTexture(GL_TEXTURE0 + Texture::num_attached_textures++);
      CheckGL();
      Bind();
      return Texture::num_attached_textures - 1;
    }

    void Cubemap::Bind()
    {
      glBindTexture(GL_TEXTURE_CUBE_MAP, id);
      CheckGL();
    }

    void Cubemap::Unbind()
    {
      glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
      CheckGL();
      Texture::Unbind();
    }

    Cubemap::~Cubemap()
    {
      glDeleteTextures(1, &id);
      CheckGL();
    }

    Core::Handle<Cubemap> Cubemap::GetDefault()
    {
      return Core::ResourceManager<Cubemap>::Find("DefaultCubemap");
    }

    GLuint Cubemap::GetID()
    {
      return id;
    }

    void Cubemap::Save(const std::string& name, int mipLevels)
    {
      Bind();
      std::string root = 
        Core::FileSystem::GetGameDir(Core::FileSystem::Dir::Cubemaps) + "/";
      std::string files[6] = 
      {
        name + "_rt",
        name + "_lf",
        name + "_up",
        name + "_dn",
        name + "_ft",
        name + "_bk"
      };
      std::string names[6] = 
      {
        "Right",
        "Left",
        "Top",
        "Bottom",
        "Front",
        "Back"
      };
      Json::Value cmap;
      for(unsigned i = 0; i < 6; ++i)
      {
        for(unsigned m = 0; m < mipLevels; ++m)
        {
          int width, height;
          glGetTexLevelParameteriv(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, m, 
            GL_TEXTURE_WIDTH, &width);
          glGetTexLevelParameteriv(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, m, 
            GL_TEXTURE_WIDTH, &height);
          int bufSize = width * height * 3 * sizeof(unsigned char);
          unsigned char* buffer = new unsigned char[bufSize];
          glGetTexImage(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, m, GL_RGB, 
            GL_UNSIGNED_BYTE, buffer);
          std::string f = files[i] + "_" + std::to_string(m) + ".tga";
          cmap[std::to_string(m)][names[i]] = f;
          SOIL_save_image((root + "/" + f).c_str(), SOIL_SAVE_TYPE_TGA, width, 
            height, 3, buffer);
          delete[] buffer;
        }
      }
      CheckGL();
      Unbind();

      std::ofstream o(root + name + ".cmap");
      Resource::filename = name;
      o << cmap;
    }
  }
}