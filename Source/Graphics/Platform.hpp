/*!
  \file Graphics/Platform.hpp
  \author Chase A Rayment
  \date 18 May 2017
  \brief Contains platform-specific OpenGL includes.
*/

#ifdef __linux__
#include <GLEW/glew.h>
#endif

#ifdef _WIN32
#include <windows.h>
#include <GLEW/glew.h>
#endif