/*!
  \file ElementBufferObject.cpp
  \author Chase A Rayment
  \date 21 April 2017
  \brief Contains the implementation of the ElementBufferObject class.
*/
#include "ElementBufferObject.hpp"
#include "GraphicsCore.hpp"

#include <vector>

namespace Echo
{
  namespace Graphics
  {
    ElementBufferObject::ElementBufferObject() : id(0), 
      topology(Topology::Triangles)
    {
      glGenBuffers(1, &id);
      CheckGL();
    }

    ElementBufferObject::~ElementBufferObject()
    {
      glDeleteBuffers(1, &id);
      id = 0;
      CheckGL();
    }

    void ElementBufferObject::SetTopology(Topology t)
    {
      topology = t;
    }

    void ElementBufferObject::PushIndex(unsigned i)
    {
      indices.push_back(i);
    }

    void ElementBufferObject::Build()
    {
      Bind();

      if(indices.size() > 0)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned),
          &indices[0], GL_STATIC_DRAW);
      CheckGL();

      Unbind();
    }

    void ElementBufferObject::Bind()
    {
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
      CheckGL();
    }

    void ElementBufferObject::Unbind()
    {
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
      CheckGL();
    }

    void ElementBufferObject::Draw()
    {
      glDrawElements(topology == Topology::Triangles ? GL_TRIANGLES : GL_LINES, 
        indices.size(), GL_UNSIGNED_INT, 0);
      CheckGL();
    }

    void ElementBufferObject::DrawInstanced(
      const std::vector<glm::mat4>& transforms)
    {
      glDrawElementsInstanced(
        topology == Topology::Triangles ? GL_TRIANGLES : GL_LINES, 
        indices.size(), GL_UNSIGNED_INT, 0, transforms.size());
      CheckGL();
    }


    void ElementBufferObject::Clear()
    {
      indices.clear();
    }

    unsigned ElementBufferObject::GetCount()
    {
      return indices.size();
    }
  }
}