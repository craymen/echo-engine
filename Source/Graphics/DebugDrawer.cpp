/*!
  \file DebugDrawer.cpp
  \author Chase A Rayment
  \date 19 May 2017
  \brief Contains the implementation of the DebugDrawer class.
*/

#include "DebugDrawer.hpp"

namespace Echo
{
  namespace Graphics
  {
    DebugDrawer::DebugLine* DebugDrawer::DebugLine::SetColor1(const glm::vec4& c)
    {
      Color1 = c;
      return this;
    }

    DebugDrawer::DebugLine* DebugDrawer::DebugLine::SetColor2(const glm::vec4& c)
    {
      Color2 = c;
      return this;
    }

    DebugDrawer::DebugLine* DebugDrawer::DebugLine::SetColor(const glm::vec4& c)
    {
      Color1 = c;
      Color2 = c;
      return this;
    }

    DebugDrawer::DebugBox* DebugDrawer::DebugBox::SetColor(const glm::vec4& c)
    {
      Color = c;
      return this;
    }

    DebugDrawer::DebugPoint* DebugDrawer::DebugPoint::SetColor(const glm::vec4& c)
    {
      Color = c;
      return this;
    }

    DebugDrawer::DebugDrawer() : vao(), lines()
    {

    }

    DebugDrawer::DebugLine* DebugDrawer::DrawLine(const glm::vec3& p1, 
      const glm::vec3& p2)
    {
      DebugDrawer::DebugLine* newLine = new DebugDrawer::DebugLine;
      newLine->Point1 = p1;
      newLine->Point2 = p2;
      newLine->SetColor(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
      lines.push_back(newLine);
      return newLine;
    }

    DebugDrawer::DebugBox* DebugDrawer::DrawBox(const glm::vec3& center,
      const glm::vec3& size)
    {
      DebugDrawer::DebugBox* newBox = new DebugDrawer::DebugBox;
      newBox->Center = center;
      newBox->Size = size;
      newBox->SetColor(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
      boxes.push_back(newBox);
      return newBox;
    }

    DebugDrawer::DebugPoint* DebugDrawer::DrawPoint(const glm::vec3& point,
      float size)
    {
      DebugDrawer::DebugPoint* newPoint = new DebugDrawer::DebugPoint;
      newPoint->Point = point;
      newPoint->Size = size;
      newPoint->SetColor(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
      points.push_back(newPoint);
      return newPoint;
    }

    void DebugDrawer::draw_line(const Vertex& v1, const Vertex& v2)
    {
      unsigned start = vao.GetVBO().GetCount();
      vao.GetVBO().PushVertex(v1);
      vao.GetVBO().PushVertex(v2);
      vao.GetEBO().PushIndex(start);
      vao.GetEBO().PushIndex(start + 1);
    }

    void DebugDrawer::Draw()
    {
      glDisable(GL_DEPTH_TEST);
      vao.Clear();
      vao.GetVBO().SetStoreMode(VertexBufferObject::StoreMode::Dynamic);
      vao.GetEBO().SetTopology(ElementBufferObject::Topology::Lines);

      // Draw lines
      for(size_t i = 0; i < lines.size(); ++i)
      {
        DebugDrawer::DebugLine* dl = lines[i];
        Vertex v1, v2;
        v1.Position = dl->Point1;
        v1.Color = dl->Color1;
        v2.Position = dl->Point2;
        v2.Color = dl->Color2;
        draw_line(v1, v2);
        delete dl;
      }

      // Draw points
      for(size_t i = 0; i < points.size(); ++i)
      {
        DebugDrawer::DebugPoint* dp = points[i];
        glm::vec3 offsets[3] = 
        {
          glm::vec3(dp->Size / 2.0f, 0.0f, 0.0f),
          glm::vec3(0.0f, dp->Size / 2.0f, 0.0f),
          glm::vec3(0.0f, 0.0f, dp->Size / 2.0f)
        };

        for(unsigned i = 0; i < 3; ++i)
        {
          Vertex v1, v2;
          v1.Position = dp->Point + offsets[i];
          v2.Position = dp->Point - offsets[i];
          v1.Color = dp->Color;
          v2.Color = dp->Color;
          draw_line(v1, v2);
        }
        
        delete dp;
      }

      // Draw boxes
      for(size_t i = 0; i < boxes.size(); ++i)
      {
        DebugDrawer::DebugBox* db = boxes[i];
        Vertex v1, v2, v3, v4, v5, v6, v7, v8;
        v1.Position = db->Center + 
          glm::vec3(db->Size.x / 2.0f, db->Size.y / 2.0f, db->Size.z / 2.0f);
        v1.Color = db->Color;
        v2.Position = db->Center + 
          glm::vec3(-db->Size.x / 2.0f, db->Size.y / 2.0f, db->Size.z / 2.0f);
        v2.Color = db->Color;
        v3.Position = db->Center + 
          glm::vec3(-db->Size.x / 2.0f, db->Size.y / 2.0f, -db->Size.z / 2.0f);
        v3.Color = db->Color;
        v4.Position = db->Center + 
          glm::vec3(db->Size.x / 2.0f, db->Size.y / 2.0f, -db->Size.z / 2.0f);
        v4.Color = db->Color;
        v5.Position = db->Center + 
          glm::vec3(db->Size.x / 2.0f, -db->Size.y / 2.0f, db->Size.z / 2.0f);
        v5.Color = db->Color;
        v6.Position = db->Center + 
          glm::vec3(-db->Size.x / 2.0f, -db->Size.y / 2.0f, db->Size.z / 2.0f);
        v6.Color = db->Color;
        v7.Position = db->Center + 
          glm::vec3(-db->Size.x / 2.0f, -db->Size.y / 2.0f, -db->Size.z / 2.0f);
        v7.Color = db->Color;
        v8.Position = db->Center + 
          glm::vec3(db->Size.x / 2.0f, -db->Size.y / 2.0f, -db->Size.z / 2.0f);
        v8.Color = db->Color;
        
        unsigned start = vao.GetVBO().GetCount();
        vao.GetVBO().PushVertex(v1);
        vao.GetVBO().PushVertex(v2);
        vao.GetVBO().PushVertex(v3);
        vao.GetVBO().PushVertex(v4);
        vao.GetVBO().PushVertex(v5);
        vao.GetVBO().PushVertex(v6);
        vao.GetVBO().PushVertex(v7);
        vao.GetVBO().PushVertex(v8);

        // top face
        vao.GetEBO().PushIndex(start);
        vao.GetEBO().PushIndex(start + 1);
        vao.GetEBO().PushIndex(start + 1);
        vao.GetEBO().PushIndex(start + 2);
        vao.GetEBO().PushIndex(start + 2);
        vao.GetEBO().PushIndex(start + 3);
        vao.GetEBO().PushIndex(start + 3);
        vao.GetEBO().PushIndex(start);

        // bottom face
        vao.GetEBO().PushIndex(start + 4);
        vao.GetEBO().PushIndex(start + 5);
        vao.GetEBO().PushIndex(start + 5);
        vao.GetEBO().PushIndex(start + 6);
        vao.GetEBO().PushIndex(start + 6);
        vao.GetEBO().PushIndex(start + 7);
        vao.GetEBO().PushIndex(start + 7);
        vao.GetEBO().PushIndex(start + 4);

        // vert lines
        vao.GetEBO().PushIndex(start);
        vao.GetEBO().PushIndex(start + 4);
        vao.GetEBO().PushIndex(start + 1);
        vao.GetEBO().PushIndex(start + 5);
        vao.GetEBO().PushIndex(start + 2);
        vao.GetEBO().PushIndex(start + 6);
        vao.GetEBO().PushIndex(start + 3);
        vao.GetEBO().PushIndex(start + 7);
        
        delete db;
      }

      vao.Build();
      vao.Bind();
      vao.Draw();
      VertexArrayObject::Unbind();
      lines.clear();
      boxes.clear();
      points.clear();
      glEnable(GL_DEPTH_TEST);
    }
  }
}