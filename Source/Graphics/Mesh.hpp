/*!
  \file Mesh.hpp
  \author Chase A Rayment
  \date 10 May 2017
  \brief Contains the definition of the Mesh class.
*/

#pragma once

#include "VertexArrayObject.hpp"
#include <string>
#include <vector>
#include <unordered_map>
#include "Assimp/Importer.hpp"
#include "Assimp/scene.h"
#include "Assimp/postprocess.h"
#include "Handle.hpp"
#include "Resource.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Represents a triangular mesh.
    */
    class Mesh : public Core::Resource
    {
    public:
      /*!
        \brief Creates a new Mesh.
      */
      Mesh(const std::string& path);

      /*!
        \brief Draws a Mesh.
      */
      void Draw();

      /*!
        \brief Draws multiple Meshes via instanced rendering.
        \param transforms A list of transforms for the meshes.
      */
      void DrawMultiple(const std::vector<glm::mat4>& transforms);

      /*!
        \brief Destroys a Mesh.
      */
      ~Mesh();

      /*!
        \brief Gets the default mesh.
      */
      static Core::Handle<Mesh> GetDefault();

			/*!
				\brief Gets the polycount of this mesh.
			*/
			unsigned GetPolycount();

    private:
        // All the mesh VAOs.
      std::vector<VertexArrayObject*> meshes;

        // Processes an Assimp mesh node.
      void process_node(aiNode* node, const aiScene* scene);

        // Processes an Assimp mesh.
      VertexArrayObject* process_mesh(aiMesh* mesh, const aiScene* scene);

			unsigned polycount;
    };
  }
}