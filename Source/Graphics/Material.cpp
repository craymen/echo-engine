/*!
  \file Material.cpp
  \author Chase A Rayment
  \date 12 May 2017
  \brief Contains the implementation of the Material class.
*/

#include "Material.hpp"
#include "Variable.hpp"
#include "json/json.h"
#include <fstream>
#include "FileSystem.hpp"
#include "Log.hpp"
#include "TypeData.hpp"
#include "ResourceManager.hpp"
#include "GLM/gtx/string_cast.hpp"

namespace Echo
{
  namespace Graphics
  {
    Material::Material(): Resource(),
      AlbedoColor(1.0f, 1.0f, 1.0f, 1.0f), 
      Roughness(1.0f), AlbedoTexture(nullptr), ShaderName(""), Metallic(0.0)
    {

    }

    Material::Material(const std::string& filename) : Resource(filename),
      AlbedoColor(1.0f, 1.0f, 1.0f, 1.0f), 
      Roughness(1.0f), AlbedoTexture(nullptr), ShaderName(""), Metallic(0.0)
    {
      // Load the json from file
      Json::Value val;
      std::ifstream file(filename);
      if(!file.is_open())
      {
        Core::Log << Core::Priority::Warning << "Could not open Material "
          << filename << std::endl;
        return;
      }
      file >> val;

      // Load the material from json
      Core::Variable v(Core::TypeData::FindType<Material>(), this);
      v.Deserialize(val);
    }

    Core::Handle<Material> Material::GetDefault()
    {
      return Core::ResourceManager<Material>::Find("DefaultMaterial");
    }
  }
}