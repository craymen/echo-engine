/*!
  \file DebugDrawer.hpp
  \author Chase A Rayment
  \date 19 May 2017
  \brief Contains the definition of the DebugDrawer class.
*/
#pragma once

#include <vector>
#include "GLM/glm.hpp"
#include "VertexArrayObject.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Handles debug drawing in a space.
    */
    class DebugDrawer
    {
    public:
      /*!
        \brief Creates a new DebugDrawer.
      */
      DebugDrawer();
    
      /*!
        \brief Holds data about a line to debug draw.
      */
      struct DebugLine
      {
        glm::vec3 Point1; //!< The first point to draw.
        glm::vec3 Point2; //!< The second point to draw.
        glm::vec4 Color1; //!< The color of the first point.
        glm::vec4 Color2; //!< The color of the second point.

        /*!
          \brief Sets the color of the first point.
          \param c The new color.
        */
        DebugLine* SetColor1(const glm::vec4& c);

        /*!
          \brief Sets the color of the second point.
          \param c The new color.
        */
        DebugLine* SetColor2(const glm::vec4& c);

        /*!
          \brief Sets the color of both points.
          \param c The new color.
        */
        DebugLine* SetColor(const glm::vec4& c);
      };

      /*!
        \brief Holds line about an axis-aligned debug box to draw.
      */
      struct DebugBox
      {
        glm::vec3 Center; //!< The center of the box.
        glm::vec3 Size;   //!< The size of the box.
        glm::vec4 Color;  //!< The color of the box.

        /*!
          \brief Sets the color of the box.
          \param c The new color.
        */
        DebugBox* SetColor(const glm::vec4& c);
      };

      /*!
        \brief Holds data about a debug point to draw.
      */
      struct DebugPoint
      {
        glm::vec3 Point; //!< The point to draw.
        float Size;      //!< The size of the point.
        glm::vec4 Color; //!< The color of the point.

        /*!
          \brief Sets the color of the point.
          \param c The new color.
        */
        DebugPoint* SetColor(const glm::vec4& c);
      };

      /*!
        \brief Draws a line between two points.
        \param p1 The first point.
        \param p2 The second point.
        \returns A pointer to a struct containing the line data.
      */
      DebugLine* DrawLine(const glm::vec3& p1, const glm::vec3& p2);

      /*!
        \brief Draws a box.
        \param center The center of the box.
        \param size The size of the box.
        \returns A pointer to a struct containing the box data.
      */
      DebugBox* DrawBox(const glm::vec3& center, const glm::vec3& size);

      /*!
        \brief Draws a point.
        \param point The point to draw.
        \param size The size of the point.
        \returns A pointer to a struct containing the point data.
      */
      DebugPoint* DrawPoint(const glm::vec3& point, const float size = 0.25f);

      /*!
        \brief Draws all debug object lists and clears them.
      */
      void Draw();

    private:
      VertexArrayObject vao;           // The VAO for all debug lines
      std::vector<DebugLine*> lines;   // All lines to draw
      std::vector<DebugBox*> boxes;    // All boxes to draw
      std::vector<DebugPoint*> points; // All points to draw

        // Draws a line between two vertices
      void draw_line(const Vertex& v1, const Vertex& v2);
    };
  }
}