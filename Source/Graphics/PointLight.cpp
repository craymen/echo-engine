/*!
  \file PointLight.cpp
  \author Chase A Rayment
  \date 11 May 2017
  \brief Contains the implementation of the PointLight class.
*/

#include "PointLight.hpp"
#include "Messaging.hpp"
#include "Object.hpp"
#include "Scene.hpp"

namespace Echo
{
  namespace Graphics
  {
    PointLight::PointLight() : Color(1.0f, 1.0f, 1.0f), obj(nullptr)
    {

    }

    void PointLight::Initialize()
    {
      // Create the point light
      obj = GetSpace()->GetGraphicsScene()->AddPointLight();

      // Connect to the predraw event
      Core::ConnectEvent(nullptr, "PreDraw", this, &PointLight::PreDraw);
    }

    void PointLight::Terminate()
    {
      // Destroy the object
      GetSpace()->GetGraphicsScene()->RemovePointLight(obj);

      Component::Terminate();
      Core::Handle<PointLight>::InvalidateHandles(this);
    }

    void PointLight::PreDraw(PreDrawEvent&)
    {
      obj->Position = GetOwner()->Transform.GetPosition();
      obj->Color = Color;
    }

    void PointLight::EditorInitialize()
    {
      Initialize();
    }

    void PointLight::EditorTerminate()
    {
      Terminate();
    }
  }
}