/*!
  \file PreDrawEvent.hpp
  \author Chase A Rayment
  \date 10 May 2017
  \brief Contains the definition of the PreDrawEvent event.
*/

#pragma once

#include "Messaging.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief An event that is sent immediately before drawing.
    */
    struct PreDrawEvent : public Core::Event
    {
      // Empty for now
    };
  }
}