/*!
  \file Model.hpp
  \author Chase A Rayment
  \date 10 May 2017
  \brief Contains the definition of the Model component.
*/

#pragma once

#include "Component.hpp"
#include "ModelObject.hpp"
#include "PreDrawEvent.hpp"
#include "Handle.hpp"
#include "Material.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief A Model component for an Object.
    */
    class Model : public Core::Component
    {
    public:
      /*!
        \brief Constructs a new Model component.
      */
      Model();

      /*!
        \brief Initializes a Model component.
      */
      void Initialize();

      /*!
        \brief Terminates a Model component.
      */
      void Terminate();

      /*!
        \brief Initializes a Model component in the editor.
      */
      void EditorInitialize();

      /*!
        \brief Terminates a Model component in the editor.
      */
      void EditorTerminate();

      /*!
        \brief Handles the pre-draw event by updating the scene's model object.
      */
      void PreDraw(PreDrawEvent& evt);

      // TODO: replace this with a Mesh pointer and write serialization methods
      std::string ModelName;    //!< The name of the model to draw.
      Core::Handle<Material> CurrentMaterial; //!< The material to draw with.

    private:
      ModelObject* obj;
    };
  }
}
