/*!
  \file Window.cpp
  \author Chase A Rayment
  \date 15 April 2017
  \brief Contains the implementation of the Window class.
*/

#include "Window.hpp"
#include "SDL/SDL.h"
#include "Log.hpp"
#include "Game.hpp"
#include "Messaging.hpp"
#include "QuitEvent.hpp"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_sdl_gl3.h"
#include "Input.hpp"
#include "GraphicsCore.hpp"
#include "Texture.hpp"
#include "RenderBufferObject.hpp"

namespace Echo
{
  namespace Graphics
  {
    namespace Window
    {
      static SDL_Window* window;
      static SDL_GLContext context;
      static int width = 1280;
      static int height = 720;
      static float aspect_ratio = (float)width / (float)height;

      static void destroy_window()
      {
        SDL_GL_DeleteContext(context);
        SDL_DestroyWindow(window);
        window = nullptr;
      }

      static void set_gl_attributes()
      {
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
        SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, 
          SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
        SDL_GL_SetSwapInterval(0);
      }

      static void create_window()
      {
        // Delete window if it's already been created
        if(window)
          destroy_window();

        // Set OpenGL window attributes
        set_gl_attributes();

        // Create window
        // TODO: Support different window resolutions/modes
        window = SDL_CreateWindow("Echo Engine", SDL_WINDOWPOS_CENTERED,
          SDL_WINDOWPOS_CENTERED, width, height,
          SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

        // Output an error if the window wasn't created
        if(!window)
          Core::Log << Core::Priority::Error
            << "Failed to create window! Error: " << SDL_GetError()
            << std::endl;

        // Create context
        context = SDL_GL_CreateContext(window);

        // Set OpenGL window attributes (again -_-)
        set_gl_attributes();
      }

      void Initialize()
      {
        if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
          Core::Log << Core::Priority::Error 
            << "Failed to initialize SDL! Error: " << SDL_GetError() 
            << std::endl;
        create_window();
        CheckGL();
      }

      void Update()
      {
        // TODO: Handle other events
        SDL_Event e;
        while(SDL_PollEvent(&e) != 0)
        {
          ImGui_ImplSdlGL3_ProcessEvent(&e);
          Core::QuitEvent qe;
          switch(e.type)
          {
          case SDL_QUIT:
            Core::DispatchEvent(nullptr, "Quit", qe);
            break;
          case SDL_KEYDOWN:
          case SDL_KEYUP:
            Input::Keyboard::KeyCallback(e);
            break;
          case SDL_MOUSEMOTION:
            Input::Mouse::MouseCallback(e);
            break;
          case SDL_MOUSEBUTTONDOWN:
          case SDL_MOUSEBUTTONUP:
            Input::Mouse::MouseButtonCallback(e);
            break;
          case SDL_MOUSEWHEEL:
            Input::Mouse::MousewheelCallback(e);
          case SDL_WINDOWEVENT:
            if (e.window.windowID == SDL_GetWindowID(window))
            {
              switch (e.window.event)
              {
              case SDL_WINDOWEVENT_SIZE_CHANGED:
                HandleWindowResize(e.window.data1, e.window.data2);
                break;
              case SDL_WINDOWEVENT_MAXIMIZED:
              case SDL_WINDOWEVENT_RESTORED:
                int x, y;
                SDL_GetWindowSize(window, &x, &y);
                HandleWindowResize(x, y);
                break;
              }
            }
            break;
          }
        }

        SDL_GL_SwapWindow(window);
      }

      void Terminate()
      {
        destroy_window();
      }

      SDL_Window* GetWindow()
      {
        return window;
      }

      float GetAspectRatio()
      {
        return aspect_ratio;
      }

      int GetWidth()
      {
        return width;
      }

      int GetHeight()
      {
        return height;
      }

      void HandleWindowResize(int w, int h)
      {
        width = w;
        height = h;
        aspect_ratio = (float)width / (float)height;
        Texture::HandleWindowResize();
        RenderBufferObject::HandleWindowResize();
      }
    }
  }
}