/*!
  \file StressTester.cpp
  \author Chase A Rayment
  \date 18 January 2018
  \brief Contains the implementation of the StressTester component.
*/

#include "StressTester.hpp"
#include "imgui/imgui.h"
#include "Messaging.hpp"
#include "Handle.hpp"
#include "Model.hpp"
#include <cmath>
#include "TypeData.hpp"

namespace Echo
{
  namespace Graphics
  {
    StressTester::StressTester() : objs()
    {
    
    }

    void StressTester::Initialize()
    {
      Core::ConnectEvent(GetSpace(), "Update", this, &StressTester::Update);
    }

    void StressTester::Update(Core::UpdateEvent& evt)
    {
      ImGui::Begin("Stress Test");
      static int num_objs;
			static Core::Handle<Mesh> mesh_to_draw = Core::ResourceManager<Mesh>::Find("DefaultMesh");
			static Core::Handle<Mesh> curr = mesh_to_draw;
      ImGui::DragInt("Number of objects", &num_objs, 1.0f, 0, Core::Space::MaxObjects - 5);
			Core::TypeData::FindType<Core::Handle<Mesh> >()->GetEditorWidgetFunc()(&mesh_to_draw, "Mesh");
		  if (num_objs < 0) num_objs = 0;

			if (num_objs > 0 && mesh_to_draw != curr)
			{
				for (auto obj : objs)
				{
					objs.back()->Terminate();
					objs.pop_back();
				}
				curr = mesh_to_draw;
			}

      while(num_objs > objs.size())
      {
        Core::Handle<Core::Object> o = GetSpace()->AddObject();
        o->AddComponent<Model>();
		    o->GetComponent<Model>()->CurrentMaterial = Core::ResourceManager<Material>::Find("DefaultMaterial");
        o->Transform.SetPosition(glm::vec3(-rand() / (float)RAND_MAX * 5, 
          -rand() / (float)RAND_MAX * 5, 
          -rand() / (float)RAND_MAX * 5 - 5));
	    	o->Initialize();
        objs.push_back(o);
		    o->GetComponent<Model>()->ModelName = mesh_to_draw->GetName();
      }

      while(num_objs < objs.size())
      {
        Core::Handle<Core::Object> h = objs.back();
        objs.pop_back();
        h->Terminate();
      }

      ImGui::End();
    }

    void StressTester::Terminate()
    {
	  using namespace Core;
      COMPONENT_TERMINATE(StressTester);
    }

  }
}