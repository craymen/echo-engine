/*!
  \file RenderBufferObject.cpp
  \author Chase A Rayment
  \date 21 May 2017
  \brief Contains the definition of the RenderBufferObject class.
*/

#include "RenderBufferObject.hpp"
#include "GraphicsCore.hpp"
#include "Window.hpp"
#include "Handle.hpp"
#include <vector>
#include <algorithm>

namespace Echo
{
  namespace Graphics
  {
    static std::vector<Core::Handle<RenderBufferObject> > reso_attached_objs;

    RenderBufferObject::RenderBufferObject(unsigned width, unsigned height, 
      GLenum f) : id(0), format(f), reso_attached(false), reso_w(0.0f), 
      reso_h(0.0f)
    {
      glGenRenderbuffers(1, &id);
      CheckGL();

      Bind();

      glRenderbufferStorage(GL_RENDERBUFFER, format, width, height);
      CheckGL();
      Unbind();  
    }

    RenderBufferObject::RenderBufferObject(float rel_width, 
      float rel_height, GLenum format)
      : RenderBufferObject((unsigned)(rel_width * Window::GetWidth()), 
        (unsigned)(rel_height * Window::GetHeight()), format)
    {
      reso_attached = true;
      reso_w = rel_width;
      reso_h = rel_height;
      reso_attached_objs.push_back(this);
    }

    RenderBufferObject::~RenderBufferObject()
    {
      glDeleteRenderbuffers(1, &id);
      CheckGL();
      if (reso_attached)
        reso_attached_objs.erase(
          std::remove(reso_attached_objs.begin(),
          reso_attached_objs.end(), this), 
          reso_attached_objs.end());
    }

    void RenderBufferObject::Bind()
    {
      glBindRenderbuffer(GL_RENDERBUFFER, id);
      CheckGL();
    }

    void RenderBufferObject::Unbind()
    {
      glBindRenderbuffer(GL_RENDERBUFFER, 0);
      CheckGL();
    }

    GLuint RenderBufferObject::GetID()
    {
      return id;
    }

    void RenderBufferObject::HandleWindowResize()
    {
      unsigned size = reso_attached_objs.size();
      for(unsigned i = 0; i < size; ++i)
      {
        Core::Handle<RenderBufferObject> obj = reso_attached_objs[i];
        if(obj == nullptr)
          continue;
        RenderBufferObject* new_obj = 
          new RenderBufferObject(obj->reso_w, obj->reso_h, obj->format);
        delete (RenderBufferObject*)obj;
        Core::Handle<RenderBufferObject>::UpdateHandles(
          (RenderBufferObject*)obj, new_obj);
      }
    }
  }
}