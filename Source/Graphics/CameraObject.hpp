/*!
  \file CameraObject.hpp
  \author Chase A Rayment
  \date 10 May 2017
  \brief Contains the definition of the CameraObject class.
*/

#pragma once

#include "GLM/glm.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief A camera in a Scene.
    */
    class CameraObject
    {
    public:

      /*!
        \brief Creates a new CameraObject.
      */
      CameraObject();

      /*!
        \brief Sets whether or not the camera is orthographic.
        \param val True if the camera is orthographic, false otherwise.
      */
      void SetIsOrtho(bool val);

      /*!
        \brief Gets whether or not the camera is orthographic.
      */
      bool GetIsOrtho() const;

      /*!
        \brief Sets the eye position of the camera.
        \param val The new eye position.
      */
      void SetEye(const glm::vec3& val);

      /*!
        \brief Gets the eye position of the camera.
      */
      const glm::vec3& GetEye() const;

      /*!
        \brief Sets the look direction of the camera.
        \param val The new look direction.
      */
      void SetLookDir(const glm::vec3& val);

      /*!
        \brief Gets the look direction of the camera.
      */
      const glm::vec3& GetLookDir() const;

      /*!
        \brief Sets the up direction of the camera.
        \param up The new up direction.
      */
      void SetUp(const glm::vec3& up);

      /*!
        \brief Gets the up direction of the camera.
      */
      const glm::vec3& GetUp() const;

      /*!
        \brief Sets the near Z distance of the camera.
        \param val The new near Z distance.
      */
      void SetNearZ(float val);

      /*!
        \brief Gets the near Z distance of the camera.
      */
      float GetNearZ() const;

      /*!
        \brief Sets the far Z distance of the camera.
        \param val The new far Z distance.
      */
      void SetFarZ(float val);

      /*!
        \brief Gets the far Z distance of the camera.
      */
      float GetFarZ() const;

      /*!
        \brief Sets the size of the camera. (Only for orthographic cameras)
        \param val The new size of the camera.
      */
      void SetSize(float val);

      /*!
        \brief Gets the size of the camera. (Only for orthographic cameras)
      */
      float GetSize() const;

      /*!
        \brief Sets the FOV of the camera. (Only for perspective cameras)
        \param val The FOV of the camera.
      */
      void SetFov(float val);

      /*!
        \brief Gets the FOV of the camera. (Only for perspective cameras)
      */
      float GetFov() const;

      /*!
        \brief Gets the world-to-camera matrix of this camera.
      */
      const glm::mat4& GetViewMatrix() const;

      /*!
        \brief Gets the camera-to-screen matrix of this camera.
      */
      const glm::mat4& GetProjectionMatrix() const;

      /*!
        \brief Converts screen coordinates to a world-space ray.
        \param coords The screen coordinates.
        \param start The start of the ray.
        \param dir The direction of the ray.
      */
      void ScreenCoordsToWorldRay(glm::ivec2 coords, glm::vec3& start, 
        glm::vec3& dir) const;

    private:
      bool is_ortho;        // Whether or not this camera is orthographic.
      glm::vec3 eye;        // The eye of the camera.
      glm::vec3 look_dir;   // The look direction of the camera.
      glm::vec3 up;         // The up direction of the camera.
      float near_z;         // The near Z plane of the camera.
      float far_z;          // The far Z plane of the camera.
      float size;           // The size of the camera (if orthographic)
      float fov;            // The FOV of the camera (if perspective)
      mutable bool dirty_v; // Whether or not the V matrix is dirty.
      mutable bool dirty_p; // Whether or not the P matrix is dirty.
      mutable glm::mat4 v;  // The view matrix.
      mutable glm::mat4 p;  // The projection matrix.
    };
  }
}