/*!
  \file Texture.cpp
  \author Chase A Rayment
  \date 21 May 2017
  \brief Contains the implementation of the Texture class.
*/

#include "Texture.hpp"
#include "GraphicsCore.hpp"
#include "SOIL/SOIL.h"
#include <string>
#include "FileSystem.hpp"
#include "Log.hpp"
#include "ResourceManager.hpp"
#include "Window.hpp"
#include <vector>
#include <algorithm>
#include "Handle.hpp"

namespace Echo
{
  namespace Graphics
  {
    unsigned Texture::num_attached_textures = 0;
    static std::vector<Core::Handle<Texture> > reso_textures;

    Texture::Texture(int width, int height) : Resource(), id(0), 
      reso_attached(false), reso_w(0.0f), reso_h(0.0f), label()
    {
      // Create the texture
      glGenTextures(1, &id);
      CheckGL();

      // Bind the texture
      Bind();

      // Allocate the texture space
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, 
        GL_UNSIGNED_BYTE, NULL);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

      CheckGL();

      // Unbind the texture
      Unbind();
    }

    Texture::Texture(float rel_width, float rel_height) : 
      Texture((int)(rel_width * Window::GetWidth()), 
        (int)(rel_height * Window::GetHeight()))
    {
      reso_attached = true;
      reso_textures.push_back(this);
      reso_w = rel_width;
      reso_h = rel_height;
    }

    Texture::Texture(const std::string& filename) : Resource(filename), id(0), 
      label()
    {
      // Load the image
      id = SOIL_load_OGL_texture(filename.c_str(), SOIL_LOAD_AUTO, 
        SOIL_CREATE_NEW_ID, SOIL_LOAD_RGBA | SOIL_FLAG_INVERT_Y);

      CheckGL();
      if(id == 0)
      {
        Core::Log << Core::Priority::Warning << "Could not load texture " <<
          filename << ". Error: " << SOIL_last_result() << std::endl;
        return;
      }
    }

    Texture::~Texture()
    {
      glDeleteTextures(1, &id);
      CheckGL();
      if(reso_attached)
        reso_textures.erase(std::remove(reso_textures.begin(), 
          reso_textures.end(), this), reso_textures.end());
    }

    void Texture::Bind()
    {
      glBindTexture(GL_TEXTURE_2D, id);
      CheckGL();
    }

    void Texture::Unbind()
    {
      glBindTexture(GL_TEXTURE_2D, 0);
      CheckGL();
      num_attached_textures = 0;
    }

    int Texture::Attach()
    {
      // Ensure there are not more than 15 textures bound
      ASSERT(num_attached_textures <= 15);
      glActiveTexture(GL_TEXTURE0 + num_attached_textures++);
      CheckGL();
      Bind();
      return num_attached_textures - 1;
    }

    GLuint Texture::GetID()
    {
      return id;
    }

    Core::Handle<Texture> Texture::GetDefault()
    {
      return Core::ResourceManager<Texture>::Find("DefaultTexture");
    }

    void Texture::HandleWindowResize()
    {
      unsigned size = reso_textures.size();
      auto v = reso_textures;
      for(unsigned i = 0; i < size; ++i)
      {
        Core::Handle<Texture> tex = v[i];
        if(tex == nullptr)
          continue;
        Texture* new_tex = new Texture(tex->reso_w, tex->reso_h);
        new_tex->SetLabel(tex->label);
        delete (Texture*)tex;
        Core::Handle<Texture>::UpdateHandles((Texture*)tex, new_tex);
      }
    }

    void Texture::Save(const std::string& name)
    {
      Bind();
      
      std::string root = Core::FileSystem::GetGameDir(
        Core::FileSystem::Dir::Textures);

      int width, height;
      glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
      glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &height);

      int bufSize = width * height * 3 * sizeof(unsigned char);

      unsigned char* buffer = new unsigned char[bufSize];

      glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, buffer);

      SOIL_save_image((root + "/" + name + ".tga").c_str(), SOIL_SAVE_TYPE_TGA, 
        width, height, 3, buffer);
      delete[] buffer;
      Unbind();
    }

    void Texture::SetLabel(const std::string& l)
    {
      #ifdef DEBUG
        label = l;
        glObjectLabel(GL_TEXTURE, id, l.length(), l.c_str());
        CheckGL();
      #endif
    }
  }
}