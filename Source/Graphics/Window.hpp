/*!
  \file Window.hpp
  \author Chase A Rayment
  \date 15 April 2017
  \brief Contains the definition of the Window system.
*/

#pragma once

struct SDL_Window;

namespace Echo
{
  namespace Graphics
  {
    namespace Window
    {
      /*!
        \brief Initializes the Window.
      */
      void Initialize();

      /*!
        \brief Updates the Window.
      */
      void Update();

      /*!
        \brief Terminates the Window.
      */
      void Terminate();

      /*!
        \brief Gets the SDL window.
      */
      SDL_Window* GetWindow();

      /*!
        \brief Gets the aspect ratio of the window.
      */
      float GetAspectRatio();

      /*!
        \brief Gets the width of the window.
      */
      int GetWidth();

      /*!
        \brief Gets the height of the window.
      */
      int GetHeight();

      /*!
        \brief Handles a window resize.
      */
      void HandleWindowResize(int width, int height);
    }
  }
}