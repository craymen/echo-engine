/*!
  \file ModelObject.hpp
  \author Chase A Rayment
  \date 10 May 2017
  \brief Contains the definition of the ModelObject class.
*/

#pragma once

#include "GLM/glm.hpp"
#include "GLM/gtc/quaternion.hpp"
#include "Mesh.hpp"
#include "Material.hpp"
#include "Handle.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Represents a model to draw within a scene.
    */
    class ModelObject
    {
    public:
      /*!
        \brief Creates a new ModelObject.
      */
      ModelObject();

      /*!
        \brief Sets the mesh for this ModelObject to draw.
      */
      void SetMesh(Core::Handle<Mesh> mesh);

      /*!
        \brief Gets the mesh this ModelObject draws.
      */
      Core::Handle<Mesh> GetMesh() const;

      /*!
        \brief Sets the material for this ModelObject to draw with.
      */
      void SetMaterial(Core::Handle<Material> mat);

      /*!
        \brief Gets the material this ModelObject draws with.
      */
      Core::Handle<Material> GetMaterial() const;

      /*!
        \brief Draws the mesh associated with this ModelObject.
      */
      void Draw();

      /*!
        \brief Gets the object-to-world matrix.
      */
      const glm::mat4& GetTransform() const;

      /*!
        \brief Sets the object-to-world matrix.
        \param val The new matrix.
      */
      void SetTransform(const glm::mat4& val);

      /*!
        \brief Gets the position.
      */
      glm::vec3 GetPosition() const;

    private:
      Core::Handle<Mesh> mesh_to_draw;
      Core::Handle<Material> material_to_draw;
      glm::mat4 transform;
    };
  }
}