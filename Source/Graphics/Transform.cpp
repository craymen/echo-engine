/*!
  \file Transform.cpp
  \author Chase A Rayment
  \date 9 April 2017
  \brief Contains the implmentation of the Transform functions.
*/

#include "Transform.hpp"
#include "GLM/gtc/matrix_transform.hpp"
#include "GLM/gtx/euler_angles.hpp"
#include <algorithm>
#include "Object.hpp"
#include "GLM/gtx/matrix_decompose.hpp"

namespace Echo
{
  namespace Graphics
  {
    glm::mat4 Transform::ObjectToWorld(const glm::vec3& translation, 
      const glm::quat& rotation, const glm::vec3& scale)
    {
      const float& qx = rotation.x;
      const float& qy = rotation.y;
      const float& qz = rotation.z;
      const float& qw = rotation.w;
      float qx2 = 2 * qx * qx;
      float qy2 = 2 * qy * qy;
      float qz2 = 2 * qz * qz;
      float qwqz = qw * qz;
      float qxqy = qx * qy;
      float qwqy = qw * qy;
      float qxqz = qx * qz;
      float qwqx = qw * qx;
      float qyqz = qy * qz;

      return glm::mat4(
        -(qy2 + qz2 - 1) * scale.x  , 2 * (qwqz + qxqy) * scale.x , 
        -2 * (qwqy - qxqz) * scale.x, 0,
        -2 * (qwqz - qxqy) * scale.y, -(qx2 + qz2 - 1) * scale.y  , 
        2 * (qwqx + qyqz) * scale.y , 0,
        2 * (qwqy + qxqz) * scale.z , -2 * (qwqx - qyqz) * scale.z, 
        -(qx2 + qy2 - 1) * scale.z  , 0,
        translation.x               , translation.y               , 
        translation.z               , 1
      );
    }

    glm::mat4 Transform::WorldToCamera(const glm::vec3& eye, 
      const glm::vec3& lookDir, const glm::vec3& up)
    {
      return glm::lookAt(eye, eye + lookDir, up);
    }

    glm::mat4 Transform::CameraToScreenOrth(float size, float aspect, 
      float nearZ, float farZ)
    {
      aspect = 2.0f / aspect;
      return glm::ortho(-size / aspect, size / aspect, -size / 2.0f, 
        size / 2.0f, nearZ, farZ);
    }

    glm::mat4 Transform::CameraToScreenPerspective(float fov, float aspect, 
      float nearZ, float farZ)
    {
      return glm::perspective(fov, aspect, nearZ, farZ);
    }

    glm::vec3 Transform::GetRotationEuler() const
    {
      return angles;
    }

    glm::quat Transform::GetRotationQuaternion() const
    {
      return rot;
    }

    void Transform::SetRotationEuler(const glm::vec3& val)
    {
      angles = val;
      rot = glm::toQuat(glm::eulerAngleYXZ(val.y, val.x, val.z));
      dirty = true;
    }

    void Transform::SetRotationQuaternion(const glm::quat& val)
    {
      rot = val;
      angles = glm::eulerAngles(val);
      dirty = true;
    }

    void Transform::RotateAround(const glm::vec3& v, float ang)
    {
      rot = glm::angleAxis(ang, v) * rot;
      angles = glm::eulerAngles(rot);
      dirty = true;
    }

    Transform::Transform() : position(), angles(), rot(), 
      scale(1.0f, 1.0f, 1.0f), matrix(), dirty(true), parent(), object(),
      children(), caching_disabled(false)
    {

    }

    void Transform::SetPosition(const glm::vec3& val)
    {
      position = val;
      dirty = true;
    }

    const glm::vec3& Transform::GetPosition() const
    {
      return position;
    }

    glm::vec3& Transform::GetPosition()
    {
      dirty = true;
      return position;
    }

    void Transform::SetScale(const glm::vec3& val)
    {
      scale = val;
      dirty = true;
    }

    const glm::vec3& Transform::GetScale() const
    {
      return scale;
    }

    glm::vec3& Transform::GetScale()
    {
      dirty = true;
      return scale;
    }

    glm::mat4 Transform::GetMatrix() const
    {
      if(dirty || caching_disabled)
        matrix = ObjectToWorld(position, rot, scale);
      dirty = false;
      if(parent != nullptr && parent != this)
        return parent->GetMatrix() * matrix;
      else
        return matrix;
    }

    glm::vec3 Transform::GetForward() const
    {
      return rot * glm::vec3(0, 0, -1);
    }

    glm::vec3 Transform::GetUp() const
    {
      return rot * glm::vec3(0, 1, 0);
    }

    glm::vec3 Transform::GetRight() const
    {
      return rot * glm::vec3(1, 0, 0);
    }

    void Transform::SetParent(Core::Handle<Transform> p)
    {
      if(p == parent)
        return;
      if(parent != nullptr)
        parent->children.erase(std::remove(parent->children.begin(), 
          parent->children.end(), this), parent->children.end());
      parent = p;
      if (parent != nullptr)
      {
        parent->children.push_back(this);
        std::sort(parent->children.begin(), parent->children.end(), 
          [](Core::Handle<Transform> a, Core::Handle<Transform> b)
            {
              return a->GetObject()->GetID() < b->GetObject()->GetID();
        });
      }
    }

    Core::Handle<Transform> Transform::GetParent() const
    {
      return parent;
    }

    void Transform::SetObject(Core::Handle<Core::Object> o)
    {
      object = o;
    }

    Core::Handle<Core::Object> Transform::GetObject() const
    {
      return object;
    }

    void Transform::Initialize()
    {
      if(parent != nullptr && 
        std::find(parent->children.begin(), parent->children.end(), this) 
        == parent->children.end())
      {
        parent->children.push_back(this);
      }

      SetRotationEuler(angles);
    }

    void Transform::Terminate()
    {
      SetParent(nullptr);
      children.clear();
    }

    const std::vector<Core::Handle<Transform> >& Transform::GetChildren() const
    {
      children.erase(std::remove(children.begin(), children.end(), nullptr), 
        children.end());
      return children;
    }

    void Transform::EnableCaching()
    {
      caching_disabled = false;
    }

    void Transform::DisableCaching()
    {
      caching_disabled = true;
    }

    void Transform::SetTransform(const glm::mat4& mat, bool ignore_scale)
    {
      position = glm::vec3(mat * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
      SetRotationQuaternion(glm::quat(mat));
      if(!ignore_scale)
      {
        scale = glm::vec3(mat * glm::vec4(1.0f, 1.0f, 1.0f, 0.0f));
        matrix = mat;
        dirty = false;
      }
      else
      {
        dirty = true;
      }
    }

    Core::Handle<Core::Object> Transform::FindChildByName(std::string name)
    {
      for(auto obj : children)
        if(obj->GetObject() && obj->GetObject()->GetName() == name)
          return obj->GetObject();
      return nullptr;
    }
  }
}