/*!
  \file Vertex.hpp
  \author Chase A Rayment
  \date 16 April 2017
  \brief Contains the definition of the Vertex structure.
*/
#pragma once

#include "GLM/glm.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief Contains information about a vertex.
    */
    struct Vertex
    {
      glm::vec3 Position;          //!< The position of the vertex.
      glm::vec3 Normal;            //!< The normal of the vertex.
      glm::vec4 Color;             //!< The color of the vertex.
      glm::vec2 TexCoord;          //!< The texture coordinate of the vertex.
      glm::vec3 Tangent;           //!< The tangent vector at the vertex.
      glm::mat4 InstanceTransform; //!< The transform for instance rendering.

        /*! \brief The number of vertex attributes. */
      static const unsigned NumAttributes = 6;

        /*! \brief The counts of each attribute. */
      static const unsigned AttributeCounts[NumAttributes];

        /*! \brief The size of each attribute. */
      static const unsigned AttributeSizes[NumAttributes];

        /*! \brief Whether or not each attribute should be normalized. */
      static const bool AttributeNormalized[NumAttributes];

        /*! \brief The attribute divisor - for instanced rendering. */
      static const unsigned AttributeDivisor[NumAttributes];
    };
  }
}