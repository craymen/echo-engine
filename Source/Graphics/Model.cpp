/*!
  \file Model.cpp
  \author Chase A Rayment
  \date 10 May 2017
  \brief Contains the implementation of the Model component.
*/

#include "Model.hpp"
#include "Handle.hpp"
#include "Messaging.hpp"
#include "Scene.hpp"
#include "Space.hpp"
#include "PreDrawEvent.hpp"
#include "Object.hpp"
#include "Mesh.hpp"
#include "ResourceManager.hpp"

#include "Log.hpp"
#include "Input.hpp"

namespace Echo
{
  namespace Graphics
  {
    Model::Model() : ModelName("DefaultMesh"), 
      CurrentMaterial(Material::GetDefault()), obj(nullptr)
    {

    }

    void Model::Initialize()
    {
      // Create the model object
      obj = GetSpace()->GetGraphicsScene()->AddModel();

      // Connect to the predraw event
      Core::ConnectEvent(nullptr, "PreDraw", this, &Model::PreDraw);
    }
    
    void Model::Terminate()
    {
      // Destroy the model object
      GetSpace()->GetGraphicsScene()->RemoveModel(obj);

      Component::Terminate();
      Core::Handle<Model>::InvalidateHandles(this);
    }

    void Model::EditorInitialize()
    {
      Initialize();
    }

    void Model::EditorTerminate()
    {
      Terminate();
    }

    void Model::PreDraw(PreDrawEvent&)
    {
      Core::Handle<Core::Object> o = GetOwner();

      obj->SetTransform(o->Transform.GetMatrix());

      obj->SetMesh(Core::ResourceManager<Mesh>::Find(ModelName));
      obj->SetMaterial(CurrentMaterial);
    }
  }
}