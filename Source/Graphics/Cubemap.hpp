/*!
  \file Cubemap.hpp
  \author Chase A Rayment
  \date 16 July 2017
  \brief Contains the definition of the Cubemap class.
*/
#pragma once

#include "Resource.hpp"
#include "Platform.hpp"
#include "Handle.hpp"

namespace Echo
{
  namespace Graphics
  {
    class Cubemap : public Core::Resource
    {
    public:
      Cubemap(int width, int height, bool genMipmaps = false);

      Cubemap(const std::string& filename);

      int Attach();

      void Bind();

      static void Unbind();

      ~Cubemap();

      static Core::Handle<Cubemap> GetDefault();

      GLuint GetID();

      void Save(const std::string& name, int mipLevels);

    private:
      GLuint id;
    };
  }
}