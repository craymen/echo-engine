/*!
  \file Shader.cpp
  \author Chase A Rayment
  \date 21 April 2017
  \brief Contains the implementation of the Shader class.
*/

#include "Shader.hpp"

#include <vector>
#include <string>
#include <cstring>
#include <sstream>
#include <fstream>
#include "Log.hpp"
#include "GraphicsCore.hpp"
#include "FileSystem.hpp"
#include "Debug.hpp"
#include "Texture.hpp"
#include "ResourceManager.hpp"
#include "Variable.hpp"
#include "TypeData.hpp"
#include "json/json.h"
#include "ResourceManager.hpp"

namespace Echo
{
  namespace Graphics
  {
    static inline std::vector<char*> get_program_lines(
      const std::string& filename)
    {
      std::vector<char*> result;
      // Get the shaders root directory
      std::string root = 
        Core::FileSystem::GetGameDir(Core::FileSystem::Dir::Shaders) + "/";

      std::ifstream file(root + filename);
      std::string line;

      // Check file validity
      if(!file.is_open())
        Core::Log << Core::Priority::Warning << "Could not open " << filename
          << std::endl;

      // Read file lines
      while (std::getline(file, line))
      {
        if(line.substr(0, 8) == "#include")
        {
          char f[1024];
          sscanf(line.c_str(), "#include %s", f);
          std::vector<char*> lines = get_program_lines(f);
          result.insert(result.end(), lines.begin(), lines.end());
        }
        else
        {
          ASSERT(line.length() < 1023);
          result.push_back(new char[1024]);
          strcpy(result.back(), line.c_str());
          result.back()[line.length()] = '\n';
          result.back()[line.length() + 1] = 0;
        }
      }
      return result;
    }

    Shader::Shader(const std::string& path) : Resource(path), id(0), vert(0), 
      frag(0), geom(0), tessCont(0), tessEval(0), comp(0), 
      render_stage("Opaque")
    {
      // Load the shader save data
      ShaderSaveData ssd;
      Core::Variable v(Core::TypeData::FindType<ShaderSaveData>(), &ssd);
      Json::Value val;
      std::ifstream file(path);
      if(!file.is_open())
      {
        Core::Log << Core::Priority::Warning << "Could not open shader file "
          << path;
        return;
      }
      file >> val;
      v.Deserialize(val);

      // Add each shader
      if(ssd.VertexShader != "")
        add_shader(GL_VERTEX_SHADER, ssd.VertexShader);
      if(ssd.FragmentShader != "")
        add_shader(GL_FRAGMENT_SHADER, ssd.FragmentShader);
      if(ssd.GeometryShader != "")
        add_shader(GL_GEOMETRY_SHADER, ssd.GeometryShader);
      if(ssd.TessContShader != "")
        add_shader(GL_TESS_CONTROL_SHADER, ssd.TessContShader);
      if(ssd.TessEvalShader != "")
        add_shader(GL_TESS_EVALUATION_SHADER, ssd.TessEvalShader);
      if(ssd.ComputeShader != "")
        add_shader(GL_COMPUTE_SHADER, ssd.ComputeShader);

      render_stage = ssd.RenderStage;

      // Create the program
      id = glCreateProgram();

      // Attach shaders
      if(vert) glAttachShader(id, vert);
      if(frag) glAttachShader(id, frag);
      if(geom) glAttachShader(id, geom);
      if(tessEval) glAttachShader(id, tessEval);
      if(tessCont) glAttachShader(id, tessCont);
      if(comp) glAttachShader(id, comp);
      
      // Link program
      glLinkProgram(id);

      // Check for errors
      GLint success;
      GLchar infoLog[512];
      glGetProgramiv(id, GL_LINK_STATUS, &success);
      if(!success)
      {
        Core::Log << Core::Priority::Warning << "Error linking " << path << 
          std::endl;
        glGetProgramInfoLog(id, 512, NULL, infoLog);
        Core::Log << Core::Priority::Warning << infoLog << std::endl;
      }

      // Delete shaders
      if(vert) glDeleteShader(vert);
      if(frag) glDeleteShader(frag);
      if(geom) glDeleteShader(geom);
      if(tessEval) glDeleteShader(tessEval);
      if(tessCont) glDeleteShader(tessCont);
      if(comp) glDeleteShader(comp);
    }

    Core::Handle<Shader> Shader::GetDefault()
    {
      return Core::ResourceManager<Shader>::Find("DefaultShader");
    }

    void Shader::Bind()
    {
      // Ensure shader is built
      ASSERT(id != 0);

      glUseProgram(id);
      CheckGL();
    }

    void Shader::Unbind()
    {
      glUseProgram(0);
      CheckGL();
    }

    void Shader::add_shader(GLenum type, const std::string& filename)
    {
      std::vector<char*> lines = get_program_lines(filename);

      // Determine correct program id
      GLuint* prgm = nullptr;
      switch(type)
      {
        case GL_VERTEX_SHADER:
          prgm = &vert;
          break;
        case GL_FRAGMENT_SHADER:
          prgm = &frag;
          break;
        case GL_GEOMETRY_SHADER:
          prgm = &geom;
          break;
        case GL_TESS_CONTROL_SHADER:
          prgm = &tessCont;
          break;
        case GL_TESS_EVALUATION_SHADER:
          prgm = &tessEval;
          break;
        case GL_COMPUTE_SHADER:
          prgm = &comp;
          break;
      }
      
      // Check for invalid program type
      ASSERT(prgm != nullptr);

      // Create shader object
      *prgm = glCreateShader(type);
      CheckGL();

      // Send the shader source code
      glShaderSource(*prgm, lines.size(), lines.data(), NULL);
      CheckGL();

      // Compile the shader
      glCompileShader(*prgm);
      CheckGL();

      // Check for success
      GLint success;
      GLchar infoLog[512];
      glGetShaderiv(*prgm, GL_COMPILE_STATUS, &success);
      if(!success)
      {
        Core::Log << Core::Priority::Warning << "Error compiling " << 
          filename << std::endl;
        glGetShaderInfoLog(vert, 512, NULL, infoLog);
        Core::Log << Core::Priority::Warning << infoLog << std::endl;
        for(unsigned i = 0; i < lines.size(); ++i)
        {
          Core::Log << Core::Priority::Warning << i << " " << lines[i] << 
            std::endl;
        }
      }
      CheckGL();
    }

    void Shader::SetUniform(const std::string& name, int val)
    {
      Bind();
      GLint location = glGetUniformLocation(id, name.c_str());
      if(location >= 0)
        glUniform1i(location, val);
      CheckGL();
    }

    void Shader::SetUniform(const std::string& name, float val)
    {
      Bind();
      GLint location = glGetUniformLocation(id, name.c_str());
      if(location >= 0)
        glUniform1f(location, val);
      CheckGL();
    }

    void Shader::SetUniform(const std::string& name, bool val)
    {
      Bind();
      GLint location = glGetUniformLocation(id, name.c_str());
      if(location >= 0)
        glUniform1i(location, val ? 1 : 0);
      CheckGL();
    }

    void Shader::SetUniform(const std::string& name, const glm::vec2& val)
    {
      Bind();
      GLint location = glGetUniformLocation(id, name.c_str());
      if(location >= 0)
        glUniform2f(location, val.x, val.y);
      CheckGL();
    }

    void Shader::SetUniform(const std::string& name, const glm::vec3& val)
    {
      Bind();
      GLint location = glGetUniformLocation(id, name.c_str());
      if(location >= 0)
        glUniform3f(location, val.x, val.y, val.z);
      CheckGL();
    }

    void Shader::SetUniform(const std::string& name, const glm::vec4& val)
    {
      Bind();
      GLint location = glGetUniformLocation(id, name.c_str());
      if(location >= 0)
        glUniform4f(location, val.x, val.y, val.z, val.w);
      CheckGL();
    }

    void Shader::SetUniform(const std::string& name, const glm::mat4& val)
    {
      Bind();
      GLint location = glGetUniformLocation(id, name.c_str());
      if(location >= 0)
        glUniformMatrix4fv(location, 1, GL_FALSE, &val[0][0]);
      CheckGL();
    }

    void Shader::SetUniform(const std::string& name, Material& val)
    {
      SetUniform(name + ".AlbedoColor", val.AlbedoColor);
      SetUniform(name + ".Roughness", val.Roughness);
      SetUniform(name + ".Metallic", val.Metallic);
      SetUniform(name + ".UseAlbedoTexture", val.AlbedoTexture != nullptr);
      if(val.AlbedoTexture != nullptr)
        SetUniform(name + ".AlbedoTexture", val.AlbedoTexture->Attach());
      SetUniform(name + ".UseNormalMap", val.NormalMap != nullptr);
      if(val.NormalMap != nullptr)
        SetUniform(name + ".NormalMap", val.NormalMap->Attach());
    }

    void Shader::SetUniform(const std::string& name, 
      const PointLightObject* val)
    {
      SetUniform(name + ".Position", val->Position);
      SetUniform(name + ".Color", val->Color);
    }

    void Shader::SetUniform(const std::string& name, 
      const CameraObject* val)
    {
      SetUniform(name + ".Eye", val->GetEye());
      SetUniform(name + ".LookDir", val->GetLookDir());
    }

    std::string Shader::GetRenderStage() const
    {
      return render_stage;
    }
  }
}