/*!
  \file PointLight.hpp
  \author Chase A Rayment
  \date 11 May 2017
  \brief Contains the definition of the PointLight component.
*/

#pragma once

#include "PointLightObject.hpp"
#include "Component.hpp"
#include "PreDrawEvent.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief A component that represents a point light.
    */
    class PointLight : public Core::Component
    {
    public:
      /*!
        \brief Creates a new PointLight.
      */
      PointLight();

      /*!
        \brief Initializes a PointLight.
      */
      void Initialize();

      /*!
        \brief Terminates a PointLight.
      */
      void Terminate();

      /*!
        \brief Initializes a PointLight component in the editor.
      */
      void EditorInitialize();

      /*!
        \brief Terminates a PointLight component in the editor.
      */
      void EditorTerminate();

      /*!
        \brief Handles the graphics pre-draw event.
      */
      void PreDraw(PreDrawEvent&);

      glm::vec3 Color; //!< The color of the light.

    private:
      PointLightObject* obj;
    };
  }
}