/*!
  \file TextObject.cpp
  \author Chase A Rayment
  \date 13 June 2017
  \brief Contains the implementation of the TextObject class.
*/

#include "TextObject.hpp"
#include "Shader.hpp"
#include "ResourceManager.hpp"
#include "VertexBufferObject.hpp"

namespace Echo
{
  namespace Graphics
  {
    TextObject::TextObject() : font(nullptr), transform(), text(""), 
      material(nullptr), vao(), hcenter(0), vcenter(0), positions()
    {
      vao.GetVBO().SetStoreMode(VertexBufferObject::StoreMode::Stream);
    }

    Core::Handle<Font> TextObject::GetFont() const
    {
      return font;
    }

    void TextObject::SetFont(Core::Handle<Font> f)
    {
      font = f;
    }

    glm::mat4 TextObject::GetTransform() const
    {
      return transform;
    }

    void TextObject::SetTransform(const glm::mat4& t)
    {
      transform = t;
    }

    std::string TextObject::GetText() const
    {
      return text;
    }

    void TextObject::SetText(const std::string& t)
    {
      text = t;

      positions.clear();

      if (t == "")
        return;

      float curr_x = 0.0f;
      float max_size = 0.0f;

      // Find positions to draw characters at
      for(unsigned i = 0; i < text.length(); ++i)
      {
        // Get the character
        Font::Character ch = font->GetCharacter(text[i]);

        // Get the X/Y positions
        float xpos = curr_x + ch.Bearing.x;
        float ypos = -ch.Size.y + ch.Bearing.y;
        if(ch.Size.y > max_size)
          max_size = ch.Size.y;

        // Push the position
        positions.push_back(glm::vec2(xpos, ypos));

        // Advance the x coordinate
        curr_x += ch.Advance;        
      }

      Font::Character lastch = font->GetCharacter(text.back());

      curr_x -= lastch.Advance - lastch.Size.x;

      for(unsigned i = 0; i < text.size(); ++i)
      {
        positions[i].x -= curr_x / 2.0f * hcenter;
        positions[i].y -= max_size / 2.0f * hcenter;
      }
    }

    Core::Handle<Material> TextObject::GetMaterial() const
    {
      return material;
    }

    void TextObject::SetMaterial(Core::Handle<Material> m)
    {
      material = m;
    }

    void TextObject::Draw()
    {
      // Get the shader
      Core::Handle<Shader> sh = Core::ResourceManager<Shader>::Find(
        material->ShaderName);

      // Loop through all characters in the string
      for(unsigned i = 0; i < text.length(); ++i)
      {
        // Clear the VAO
        vao.Clear();

        // Get the character
        Font::Character ch = font->GetCharacter(text[i]);

        // Compute the VAO vertices
        Vertex v1, v2, v3, v4;
        float xpos = positions[i].x;
        float ypos = positions[i].y;
        v1.Position = glm::vec3(xpos, ypos + ch.Size.y, 0.0f);
        v1.TexCoord = glm::vec2(0.0f, 0.0f);
        v1.Normal = glm::vec3(0.0f, 0.0f, 1.0f);
        v2.Position = glm::vec3(xpos, ypos, 0.0f);
        v2.TexCoord = glm::vec2(0.0f, 1.0f);
        v2.Normal = glm::vec3(0.0f, 0.0f, 1.0f);        
        v3.Position = glm::vec3(xpos + ch.Size.x, ypos, 0.0f);
        v3.TexCoord = glm::vec2(1.0f, 1.0f);
        v3.Normal = glm::vec3(0.0f, 0.0f, 1.0f);        
        v4.Position = glm::vec3(xpos + ch.Size.x, ypos + ch.Size.y, 0.0f);
        v4.TexCoord = glm::vec2(1.0f, 0.0f);
        v4.Normal = glm::vec3(0.0f, 0.0f, 1.0f);        

        // Bind the VAO
        vao.Bind();

        // Push the vertices into the VAO
        vao.GetVBO().PushVertex(v1);
        vao.GetVBO().PushVertex(v2);
        vao.GetVBO().PushVertex(v3);
        vao.GetVBO().PushVertex(v4);

        // Set up the EBO
        vao.GetEBO().PushIndex(0);
        vao.GetEBO().PushIndex(1);
        vao.GetEBO().PushIndex(2);
        vao.GetEBO().PushIndex(0);
        vao.GetEBO().PushIndex(2);
        vao.GetEBO().PushIndex(3);

        // Build the VAO
        vao.Build();

        // Set the shader texture
        sh->SetUniform("GlyphTexture", ch.Tex->Attach());

        // Draw the VAO
        vao.Draw();

        // Unattach the texture
        Texture::Unbind();
      }
      VertexArrayObject::Unbind();
    }

    void TextObject::SetHorizontalCenteringMode(int mode)
    {
      hcenter = mode;
    }

    void TextObject::SetVerticalCenteringMode(int mode)
    {
      vcenter = mode;
    }

    void TextObject::SetVisibility(bool v)
    {
      visible = v;
    }
    
    bool TextObject::GetVisibility()
    {
      return visible;
    }
  }
}
