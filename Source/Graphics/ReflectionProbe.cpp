/*!
  \file ReflectionProbe.cpp
  \author Chase A Rayment
  \date date
  \brief Contains the implementation of the ReflectionProbe component.
*/

#include "ReflectionProbe.hpp"
#include "Space.hpp"
#include "Scene.hpp"
#include "ResourceManager.hpp"

namespace Echo
{
  namespace Graphics
  {
    ReflectionProbe::ReflectionProbe() : obj(nullptr)
    {
      
    }
     
    void ReflectionProbe::Initialize()
    {
      obj = GetSpace()->GetGraphicsScene()->AddReflectionProbe();
      Core::ConnectEvent(nullptr, "PreDraw", this, &ReflectionProbe::PreDraw);
    }

    void ReflectionProbe::EditorInitialize()
    {
      Initialize();
    }
     
    void ReflectionProbe::Terminate()
    {
      GetSpace()->GetGraphicsScene()->RemoveReflectionProbe(obj);
      Component::Terminate();
      Core::Handle<ReflectionProbe>::InvalidateHandles(this);
    }

    void ReflectionProbe::EditorTerminate()
    {
      Terminate();
    }

    void ReflectionProbe::PreDraw(PreDrawEvent&)
    {
      obj->Position = GetOwner()->Transform.GetPosition();
      obj->Name = GetSpace()->GetLastLoadedLevelName() + "_" + 
        std::to_string(GetOwner()->GetID());
      if(obj->DiffuseMap == nullptr)
        obj->DiffuseMap = Core::ResourceManager<Cubemap>::Find(obj->Name + 
          "Diffuse");
      if(obj->SpecularMap == nullptr)
        obj->SpecularMap = Core::ResourceManager<Cubemap>::Find(obj->Name + 
          "Specular");
    }
  }
}