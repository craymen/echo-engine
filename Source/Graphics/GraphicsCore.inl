#include "Platform.hpp"
#include "Log.hpp"
#include "Debug.hpp"

#ifdef DEBUG
#define CheckGL() do {                                                         \
  GLenum err;                                                                  \
  while((err = glGetError()) != GL_NO_ERROR) {                                 \
    std::string text;                                                          \
    switch(err) {                                                              \
      case GL_INVALID_ENUM:                                                    \
        text = "GL_INVALID_ENUM";                                              \
        break;                                                                 \
      case GL_INVALID_VALUE:                                                   \
        text = "GL_INVALID_VALUE";                                             \
        break;                                                                 \
      case GL_INVALID_OPERATION:                                               \
        text = "GL_INVALID_OPERATION";                                         \
        break;                                                                 \
      case GL_STACK_OVERFLOW:                                                  \
        text = "GL_STACK_OVERFLOW";                                            \
        break;                                                                 \
      case GL_OUT_OF_MEMORY:                                                   \
        text = "GL_OUT_OF_MEMORY";                                             \
        break;                                                                 \
      case GL_STACK_UNDERFLOW:                                                 \
        text = "GL_STACK_UNDERFLOW";                                           \
        break;                                                                 \
      case GL_INVALID_FRAMEBUFFER_OPERATION:                                   \
        text = "GL_INVALID_FRAMEBUFFER_OPERATION";                             \
        break;                                                                 \
      case GL_CONTEXT_LOST:                                                    \
        text = "GL_CONTEXT_LOST";                                              \
        break;                                                                 \
      case GL_TABLE_TOO_LARGE:                                                 \
        text = "GL_TABLE_TOO_LARGE";                                           \
        break;                                                                 \
      default:                                                                 \
        text = "Unknown error";                                                \
    }                                                                          \
    Echo::Core::Log << Echo::Core::Priority::Error <<                          \
      "OpenGL error encountered! " << __func__ << ", line " << __LINE__        \
      << ", file " << __FILE__ << ": " << text << std::endl;                   \
    DEBUG_BREAK();                                                             \
  }                                                                            \
}                                                                              \
while(0)
#else
#define CheckGL()
#endif