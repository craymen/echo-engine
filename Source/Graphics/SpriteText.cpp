/*!
  \file SpriteText.cpp
  \author Chase A Rayment
  \date 13 June 2017
  \brief Contains the implementation of the Text component.
*/

#include "SpriteText.hpp"
#include "Scene.hpp"
#include "Messaging.hpp"
#include "Transform.hpp"
#include "ResourceManager.hpp"
#include "Font.hpp"
#include "Handle.hpp"
#include "Material.hpp"

namespace Echo
{
  namespace Graphics
  {
    SpriteText::SpriteText() : Text(""), FontName("DefaultFont"), 
      MaterialName("DefaultTextMaterial"), obj(nullptr), 
      HorizontalCenterMode(0), VerticalCenterMode(0)
    {
      
    }
     
    void SpriteText::Initialize()
    {
      // Create the text object
      obj = GetSpace()->GetGraphicsScene()->AddText();

      obj->SetTransform(GetOwner()->Transform.GetMatrix());
      obj->SetFont(Core::ResourceManager<Font>::Find(FontName));
      obj->SetMaterial(Core::ResourceManager<Material>::Find(MaterialName));
      obj->SetText(Text);

      // Connect to the predraw event
      Core::ConnectEvent(nullptr, "PreDraw", this, &SpriteText::PreDraw);
    }

    void SpriteText::PreDraw(PreDrawEvent&)
    {
      // Set the text object attributes
      obj->SetTransform(GetOwner()->Transform.GetMatrix());
      obj->SetFont(Core::ResourceManager<Font>::Find(FontName));
      obj->SetMaterial(Core::ResourceManager<Material>::Find(MaterialName));
      obj->SetText(Text);
      obj->SetHorizontalCenteringMode(HorizontalCenterMode);
      obj->SetVerticalCenteringMode(VerticalCenterMode);
    }
     
    void SpriteText::Terminate()
    {
      // Destroy the text object
      GetSpace()->GetGraphicsScene()->RemoveText(obj);

      Component::Terminate();
      Core::Handle<SpriteText>::InvalidateHandles(this);
    }

    void SpriteText::EditorInitialize()
    {
      Initialize();
    }

    void SpriteText::EditorTerminate()
    {
      Terminate();
    }
  }
}