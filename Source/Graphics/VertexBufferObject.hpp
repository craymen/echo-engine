/*!
  \file VertexArrayObject.hpp
  \author Chase A Rayment
  \date 15 April 2017
  \brief Contains the definition of the VertexArrayObject class.
*/
#pragma once

#include "Platform.hpp"
#include "Vertex.hpp"
#include <vector>

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief A representation of an OpenGL Vertex Buffer Object.
    */
    class VertexBufferObject
    {
    public:
      /*!
        \brief The GPU store mode of the mesh.
      */
      enum class StoreMode { Static, Dynamic, Stream };

      /*!
        \brief Creates a new VertexBufferObject.
      */
      VertexBufferObject();


      // TODO: Make this support generic vertices (so not just position)
      /*!
        \brief Pushes a new vertex into the VertexBufferObject. Must be built
          for the change to take effect.
      */
      void PushVertex(const Vertex& vertex);

      /*!
        \brief Sends vertex data to the graphics card.
      */
      void Build();

      /*!
        \brief Sets this VertexBufferObject as the currently bound VAO.
      */
      void Bind();

      /*!
        \brief Unbinds all VertexBufferObject.
      */
      static void Unbind();

      /*!
        \brief Destroys this VertexBufferObject.
      */
      ~VertexBufferObject();

      /*!
        \brief Sets the store mode.
      */
      void SetStoreMode(StoreMode mode);

      /*!
        \brief Clears all data from this object.
      */
      void Clear();

      /*!
        \brief Gets the number of vertices in the buffer.
      */
      unsigned GetCount();

    private:
      GLuint id;                       // The ID of the object.
      std::vector<Vertex> vertices;    // The vertices the object stores.
      StoreMode mode;                  // The store mode of the VBO
      bool built;                      // Whether or not this VBO has been built
    };
  }
}