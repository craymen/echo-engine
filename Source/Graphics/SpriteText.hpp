/*!
  \file SpriteText.hpp
  \author Chase A Rayment
  \date 13 June 2017
  \brief Contains the definition of the SpriteText component.
*/
#pragma once

#include "Component.hpp"
#include "TextObject.hpp"
#include "PreDrawEvent.hpp"

namespace Echo
{
  namespace Graphics
  {
    /*!
      \brief A component that represents a text object.
    */
    class SpriteText : public Core::Component
    {
    public:
      /*!
        \brief Creates a new SpriteText.
      */
      SpriteText();

      /*!
        \brief Initializes a SpriteText.
      */
      void Initialize();

      /*!
        \brief Terminates a SpriteText.
      */
      void Terminate();

      /*!
        \brief Initializes a SpriteText component in the editor.
      */
      void EditorInitialize();

      /*!
        \brief Terminates a SpriteText component in the editor.
      */
      void EditorTerminate();

      /*!
        \brief Handles the graphics pre-draw event.
      */
      void PreDraw(PreDrawEvent&);
      
      std::string FontName;          //!< The name of the font to use.
      std::string MaterialName;      //!< The name of the material to use.
      std::string Text;              //!< The text to draw.
      int HorizontalCenterMode;      //!< The horizontal centering mode.
      int VerticalCenterMode;        //!< The vertical centering mode.
    
    private:
      TextObject* obj;          // The text object in the scene. 
    };
  }
}