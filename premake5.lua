workspace "Block and Load"
  filename "BlockAndLoad"
  configurations { "Debug", "Release" }
  location "Build"
  flags { "C++11" }
  language "C++"
  warnings "Extra"
  defines { "GLEW_STATIC" }
  filter "system:windows"
    defines { "_WIN32", "WIN32", "_CRT_SECURE_NO_WARNINGS" }
    linkoptions { "/NODEFAULTLIB:libcmt.lib", "/ignore:4099", "/ignore:4006", "/ignore:4221", "/ignore:4503" }
  filter {}
  
buildoptions {"/MP8"}

  filter "configurations:Debug"
    defines { "DEBUG" }
    runtime ("Debug")
    buildoptions { "/EHsc /Oy- /Ob0" }
  filter "configurations:Release"
    defines { "RELEASE", "AK_OPTIMIZED" }
    runtime ("Release")
  filter {}

debugdir "Bin"

libdirs { "Libs/lib/**", "Bin"}
removelibdirs "Libs/lib/Wwise/**"
filter "configurations:Debug"
-- libdirs { "C:/Program Files (x86)/Audiokinetic/Wwise 2016.2.3.6077/SDK/Win32_vc140/Debug/lib/" }
libdirs { "Libs/lib/Wwise/Debug/lib/" }
filter "configurations:Release"
libdirs { "Libs/lib/Wwise/Release/lib/" }
filter {}

filter "system:windows"
  disablewarnings { "4201" }
filter {}

filter "system:linux"
  toolset "clang"
filter {}

includedirs { "Libs/include", "Libs/include/Bullet", "Libs/include/freetype2" }

project "Core"
  kind "StaticLib"
  location "Build/Core"
  targetdir "Bin"
  files { "Source/Core/**.cpp", "Source/Core/**.c" }
  includedirs { "Source/Graphics", "Source/Input", "Source/Core", "Source/Audio", "Source/Physics", "Source/Gameplay" }

project "Graphics"
  kind "StaticLib"
  location "Build/Graphics"
  targetdir "Bin"
  includedirs { "Source/Core", "Source/Input", "Source/Graphics", "Source/Physics", "Source/Audio", "Source/Audio" }
  files { "Source/Graphics/**.cpp", "Source/Graphics/**.c" }
  filter {"system:linux"}
    excludes { "Source/Graphics/SOIL/*.*" }

project "Input"
  kind "StaticLib"
  location "Build/Input"
  targetdir "Bin"
  includedirs { "Source/Core", "Source/Graphics", "Source/Input" }
  files { "Source/Input/**.cpp", "Source/Input/**.c" }

project "Physics"
  kind "StaticLib"
  location "Build/Physics"
  targetdir "Bin"
  includedirs { "Source/Core", "Source/Graphics", "Source/Physics", "Source/Audio"}
  files { "Source/Physics/**.cpp", "Source/Physics/**.c" }

project "Gameplay"
  kind "StaticLib"
  location "Build/Gameplay"
  targetdir "Bin"
  includedirs { "Source/Core", "Source/Graphics", "Source/Physics", "Source/Audio", "Source/Input" }
  files { "Source/Gameplay/**.cpp", "Source/Gameplay/**.c" }

project "Audio"
  kind "StaticLib"
  location "Build/Audio"
  targetdir "Bin"
  files { "Source/Audio/**.cpp", "Source/Input/**.c" }
  filter "system:windows"
    includedirs { "Libs/include/Wwise/", "Source/Audio", "Source/Core", "Source/Graphics" }
    excludes "Source/Audio/AK/SoundEngine/POSIX/*.*"
  filter "system:linux"
    includedirs { "Libs/include/Wwise/", "Source/Audio", "Source/Audio/AK/POSIX", "Source/Core", "/usr/lib/xorg/modules/extensions" }
    excludes "Source/Audio/AK/SoundEngine/Win32/*.*"
  filter {}    

project "*"
  links { "opengl32", "glew32s", "zlibstatic", "assimp-vc140-mt" }
  links
  {
    "AkSoundEngine",
    "AkMusicEngine",
    "AkMemoryMgr",
    "AkStreamMgr",
    "AkVorbisDecoder",
    "dxguid",
    "freetype"
  }
  filter "configurations:Debug"
  links
  {
    "BulletCollisiond",
    "BulletDynamicsd",
    "vhacdd",
    "LinearMathd",
    "CommunicationCentral"
  }
  filter "configurations:Release"
  links
  {
    "BulletCollision",
    "BulletDynamics",
    "vhacd",
    "LinearMath"
  }
filter {}

project "Main"
  kind "ConsoleApp"
  location "Build/Main"
  targetdir "Bin"
  includedirs { "Source/Core", "Source/Graphics", "Source/Physics", "Source/Audio" }
  files { "Source/Main/**.cpp", "Source/Main/**.c" }
  links { "Core", "Graphics", "Input", "Audio", "Physics", "Gameplay", "SDL2" }

project "Editor"
  kind "ConsoleApp"
  location "Build/Editor"
  targetdir "Bin"
  includedirs { "Source/Core", "Source/Graphics", "Source/Input", "Source/Physics", "Source/Audio" }
  files { "Source/Editor/**.cpp", "Source/Editor/**.c" }
  links { "Core", "Graphics", "Input", "Audio", "Physics", "Gameplay", "SDL2" }

project "MaterialEditor"
  kind "ConsoleApp"
  location "Build/MaterialEditor"
  targetdir "Bin"
  includedirs { "Source/Core", "Source/Graphics", "Source/Input", "Source/Physics", "Source/Audio" }
  files { "Source/MaterialEditor/**.cpp", "Source/MaterialEditor/**.c" }
  links { "Core", "Graphics", "Input", "Audio", "Physics", "Gameplay", "SDL2" }